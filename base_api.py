#!/usr/bin/env python3
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: api基类
@file:
@time: 2019/12/30 11:49
@desc:
'''
import html
import re
import sys
import urllib.parse
from concurrent.futures import ThreadPoolExecutor
from functools import partial, wraps

import tornado
from tornado import web
from tornado.concurrent import Future
from tornado.gen import coroutine

import cache.user_cache as uc
from control_url_list import WHITE_URL_LIST, WHITE_URL_LIST_PREFIX
from enums.enum_db import EnumDb
from enums.enum_error_msg_type import EnumErrorType
from framework.db_session import DbSession
from framework.log_controller import trycatch
from framework.msg import failure, ok
from framework.utilities import to_dict, to_string, get_len, echo, to_int, gen_access_token
from models.users.user import User

__EXECUTOR__ = ThreadPoolExecutor(max_workers=50)

GLOBALAPISCACHE = {}


class BaseApiHandler(tornado.web.RequestHandler):
    """
    api基类
    所有的api类需要从该类进行继承
    """

    '''  存放对post数据解析为的字典数据,调用get_post_body()时就不必重复解析了    '''
    _post_body = None

    _name = None
    __usr = {}
    __login_type = None

    __server_login__ = False  # 是否为服务器登录

    def initialize(self):
        # self.request.method = 'POST'
        # print(self.request.method, type(self.request.method))
        self.set_default_headers()

    # 全局设置跨域问题
    def set_default_headers(self):
        """
            设置默认的headers，推荐放入BaseHandler
            :return:
        """
        # print("setting headers!!!")
        self.set_header('Access-Control-Allow-Origin', '*')
        # self.set_header('Access-Control-Allow-Origin', 'http://localhost:8080')
        # self.set_header('Access-Control-Allow-Headers', 'X-Requested-With')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
        self.set_header('Content-Type', 'application/json; charset=UTF-8')
        self.set_header('Access-Control-Allow-Headers', 'Content-Type')

    def get_usr_apis(self):
        url = self.request.path.lower().strip(' ')
        if not url:
            return (1, 404, 'url不存在！')

        usr = self.get_usr_dic()
        if not usr:
            return (1, 401, '请登录！')

        with DbSession.create(EnumDb.main) as db:
            user_query = db.query(User).filter(User.user_id == usr['user_id']).first()
            if user_query.is_super_admin or user_query.is_admin:
                return (1, 200, '通过!')

        return (1, 200, '通过!')

    def get_post_data(self, key=None):
        '''
        获取post body中的data
        '''
        try:
            if not self._post_body:
                try:
                    self._post_body = to_dict(self.request.query_arguments)
                    if not self._post_body:
                        self._post_body = to_dict(self.request.arguments)
                except:
                    self._post_body = to_dict(self.request.arguments)

            # 如果没有key的话,直接返回 post body
            if not key:
                for v in self._post_body:
                    value = self._post_body[v]
                    if isinstance(value, list):
                        if get_len(value) == 1:
                            if isinstance(value[0], bytes):
                                self._post_body[v] = to_string(self._post_body[v][0])
                return self._post_body

            return self._post_body.get(key)
        except:
            return None

    def get_thirdparty_post_data(self, key=None):
        '''
        获取post body中的data
        '''
        try:
            if not self._post_body:
                self._post_body = to_dict(self.request.body)
            # 如果没有key的话,直接返回 post body
            if not key:
                for v in self._post_body:
                    value = self._post_body[v]
                    if isinstance(value, list):
                        if get_len(value) == 1:
                            if isinstance(value[0], bytes):
                                self._post_body[v] = to_string(self._post_body[v][0])
                return self._post_body

            return self._post_body.get(key)
        except:
            return None

    def get_name(self):
        '''
        获取电话号码(只用用户传入电话号码时才有电话号码)
        app适用
        :return:
        :rtype:int 或 None
        '''
        if self._name:
            return self._name

        try:
            name = to_string(self.get_secure_cookie('n', None))
            if not name:
                name = to_string(self.get_cookie('n', None))

            if not name:
                try:
                    data_name = to_string(to_dict(self.request.query_arguments).get('name')[0])
                except:
                    data_name = to_string(to_dict(self.request.arguments).get('name')[0])

                if data_name:
                    self._name = data_name
                    return self._name
                return None

            try:
                new_name = self.unescape(name)
            except:
                new_name = name
            self._name = new_name
            return self._name
        except:
            return None

    # 应对前端 escape 方式对name加密的解码方式
    def unescape(self, string):
        new_string = urllib.parse.unquote(string)
        quoted = html.unescape(new_string).encode(sys.getfilesystemencoding()).decode('utf-8')
        # 转成中文
        res = re.sub(r'%u([a-fA-F0-9]{4}|[a-fA-F0-9]{2})', lambda m: chr(int(m.group(1), 16)), quoted)
        return res

    # 获取和设置用户信息.在验证权限的时候会从缓存获取数据,然后对数据进行检查.如果此时直接把数据保存在本地,就避免了获取缓存
    def get_usr_dic(self):
        try:
            if self.__usr:
                return self.__usr

            self.__usr = uc.get(name=self.get_name())
            return self.__usr
        except:
            return None

    def get_current_user(self):
        return self.get_usr_dic()

    @trycatch()
    def get_cur_uid(self):
        if not self.get_usr_dic():
            self.set_status(401, '请登录')
            return None
        return self.get_usr_dic()['user_id']

    def on_finish(self):
        try:
            self.finish()
        except:
            pass

    def run_as_async(self, f):
        '''
        将阻塞函数添加到线程池中,实现异步
        董新强
        '''
        return __EXECUTOR__.submit(f)

    @coroutine
    def prepare(self):
        '''
        进行权限检查
        '''
        # self.initialize()

        url = self.request.path.lower().strip(' ')
        echo(url, '请求地址')

        # print('请求头')
        # print(self.request.method)

        if not self.get_name():
            return (0, 401, '请登录!')

        if self.is_api(path=url) and not is_in_white_url(url):
            result = yield self.run_as_async(self.check_token_before_access)
            if result[0] != 1:
                self.set_status(result[1])
                self.finish(failure(msg=result[2]).responseMsg())

            # res = self.get_usr_apis()
            # if res[0] != 1:
            #     self.set_status(res[1])
            #     self.finish(failure(msg=res[2]).responseMsg())

        self.hooks()

    def hooks(self):
        # del self.request.headers['If-None-Match']
        pass

    def check_token_before_access(self):
        '''
        返回数据格式(是否车工,错误代码,错误提示)
        :return:
        :rtype:
        :come_from 0-web端 1-app端
        '''
        try:
            body = to_dict(self.request.query_arguments)
            if body == 0:
                return (0, 400, '请求参数错误!')

            name = to_string(self.get_secure_cookie('n', None))
            if not name:
                name = to_string(self.get_cookie('n', None))

            if not name:
                try:
                    name = to_string(to_dict(self.request.query_arguments).get('name')[0])
                except:
                    name = to_string(to_dict(self.request.arguments).get('name')[0])

            try:
                name = self.unescape(name)
            except:
                name = name

            login_come_from = 0
            if 'login_come_from' in body:
                try:
                    login_come_from = to_int(to_string(to_dict(self.request.query_arguments).get('login_come_from')[0]), 0)
                except:
                    login_come_from = to_int(to_string(to_dict(self.request.arguments).get('login_come_from')[0]), 0)

            self.__usr = uc.get(name=name)

            if not self.__usr or get_len(self.__usr) == 0:
                return (0, 401, '用户不存在!')

            if login_come_from == 0:
                access_token = to_string(self.get_secure_cookie('token', None))
                if not access_token:
                    access_token = to_string(self.get_cookie('token', None))

                real_access_token = gen_access_token(expire=self.__usr['expire'], token=self.__usr['token'], name=name)
            else:
                access_token = to_string(self.get_secure_cookie('app_token', None))
                if not access_token:
                    access_token = to_string(self.get_cookie('app_token', None))

                real_access_token = gen_access_token(expire=self.__usr['app_expire'], token=self.__usr['app_token'], name=name)

            if not real_access_token or access_token != real_access_token:
                return (-2, 401, '登录过期,请重新登陆!')

            return (1, 200, '通过!')
        except:
            return (0, 401, '请登录!')

    def is_api(self, path):
        '''
        判断是否是api
        '''
        pattern = r'/api/.*'
        r = re.match(pattern, path)
        if not r:
            return False
        return True

    # 异步实现的post函数-------------------------------------
    @coroutine
    def post(self):
        try:
            result = yield self.run_as_async(self.async_post)

            # if not result:
            #     self.finish()

            if isinstance(result, Future):
                res = yield result
            else:
                res = result

            if isinstance(res, dict):
                if EnumErrorType.param_error.value in res.get('msg', ''):
                    self.set_status(400)  # 参数错误
                return self.finish(res)

            if isinstance(res, ok):
                return self.finish(res.responseMsg())

            if isinstance(res, failure):
                if res.error_type == EnumErrorType.param_error.value:
                    self.set_status(400)  # 参数错误
                return self.finish(res.responseMsg())

            return self.finish(res)
        except:
            return self.finish(failure().responseMsg())

    @coroutine
    def async_post(self):
        pass


def unblock(f):
    @tornado.web.asynchronous
    @wraps(f)
    def wrapper(*args, **kwargs):
        self = args[0]

        def callback(future):
            try:
                res = future.result()
                if isinstance(res, dict):
                    if EnumErrorType.param_error.value in res.get('msg', ''):
                        self.set_status(400)  # 参数错误
                    return self.finish(res)

                if isinstance(res, ok):
                    return self.finish(res.responseMsg())

                if isinstance(res, failure):
                    if res.error_type == EnumErrorType.param_error.value:
                        self.set_status(400)  # 参数错误
                    return self.finish(res.responseMsg())

                self.write(res)
            except:
                pass
            finally:
                if not self._finished:
                    self.finish()

        __EXECUTOR__.submit(partial(f, *args, **kwargs)) \
            .add_done_callback(
            lambda future: tornado.ioloop.IOLoop.instance().add_callback(partial(callback, future)))

    return wrapper


def is_in_white_url(url):
    '''
    url是否在白名单中
    '''
    if not url:
        return False

    if url in WHITE_URL_LIST:
        return True

    for v in WHITE_URL_LIST_PREFIX:
        if v in url:
            return True

    return False
