#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: user_api.py
@time: 2020/10/28 17:17
@desc:
'''
from base_api import BaseApiHandler, unblock
from cache import user_cache
from enums.enum_error_msg_type import EnumErrorType
from framework.authority.authorities import check_args
from framework.msg import failure
from framework.utilities import to_string, check_pwd_format, to_int
from services.user_manage import UserManage
from thirdpart.redis_helper import set as redis_set


# 创建用户
class UserCreatHandler(BaseApiHandler):
    @check_args(['name', 'is_admin', 'mobile'])
    @unblock
    def post(self):
        data = self.get_post_data()
        name = data.get('name').replace(' ', '')
        mobile = data.get('mobile')

        remark = data.get('remark')
        is_admin = data.get('is_admin')
        crm_user = data.get('crm_user')
        logo = data.get('logo')
        role = data.get('role')

        usr = user_cache.get(name)
        if usr:
            return failure(msg='用户已存在，请勿重复创建！')

        user = self.get_usr_dic()

        um = UserManage(name=name)
        res = um.user_creat(remark=remark, usr=user, mobile=mobile, crm_user=crm_user, is_admin=is_admin, logo=logo, role=role)

        return res


class UserLoginHandler(BaseApiHandler):
    @unblock
    def post(self):
        data = self.get_post_data()
        name = to_string(data.get('name'), '').replace(' ', '') or self.get_name()
        pwd = data.get('pwd')
        token = self.get_cookie(name='token') or self.get_secure_cookie(name='token')
        # token = self.get_cookie(name='token')

        if (not token and not name) or (not name and not pwd):
            return failure(msg='登录失败!', error_type=EnumErrorType.param_error)

        usr = user_cache.get(name)
        if not usr or not usr['user_id']:
            return failure(msg='号码未注册!')

        um = UserManage(name=name)
        res = um.user_login(usr=usr, pwd=pwd, token=token)
        if not res.is_ok():
            return failure(msg=res.msg)

        self.set_cookie(name='token', value=res.data['token'], expires_days=1)
        self.set_cookie(name='n', value=to_string(name), expires_days=1)

        self.set_secure_cookie(name='token', value=res.data['token'], expires_days=1)
        self.set_secure_cookie(name='n', value=to_string(name), expires_days=1)

        redis_set(key=to_string(name), value=res, time=60 * 60 * 24 * 7)
        return res


class UserRemindUnReadListHandler(BaseApiHandler):
    @unblock
    def post(self):
        user = self.get_usr_dic()

        data = self.get_post_data()
        page = to_int(data.get('page'), 0)
        size = to_int(data.get('size'), 20)

        um = UserManage(name=self.get_name())
        res = um.remind_un_read_list(user, page, size)
        if not res:
            return failure(msg='登出失败')

        return res


class UserRemindListHandler(BaseApiHandler):
    @unblock
    def post(self):
        user = self.get_usr_dic()

        data = self.get_post_data()
        page = to_int(data.get('page'), 0)
        size = to_int(data.get('size'), 20)

        um = UserManage(name=self.get_name())
        res = um.remind_list(user, page, size)

        return res


class UserRemindReadHandler(BaseApiHandler):
    @check_args(['remind_id'])
    @unblock
    def post(self):
        user = self.get_usr_dic()

        data = self.get_post_data()
        remind_id = data.get('remind_id')

        um = UserManage(name=self.get_name())
        res = um.remind_read(user, remind_id)
        if not res:
            return failure(msg='登出失败')

        return res


class UserLogoutHandler(BaseApiHandler):
    @unblock
    def post(self):
        user = self.get_usr_dic()
        um = UserManage(name=self.get_name())
        res = um.logout(usr=user)
        if not res:
            return failure(msg='登出失败')

        self.clear_all_cookies()

        return res


class UserEditBasicsHandler(BaseApiHandler):
    @check_args(['user_id'])
    @unblock
    def post(self):
        data = self.get_post_data()
        usr = self.get_usr_dic()
        user_id = to_int(data.get('user_id'))
        phone = data.get('mobile').replace(' ', '')

        name = data.get('user_name')
        remark = data.get('remark')
        crm_user = data.get('crm_user')
        logo = data.get('logo')
        role = data.get('role')

        um = UserManage(name=name)
        res = um.user_edit_basics(user_id=user_id,
                                  phone=phone, name=name,
                                  remark=remark,
                                  usr=usr,
                                  crm_user=crm_user,
                                  logo=logo,
                                  role=role)

        return res


class UserEditPwdHandler(BaseApiHandler):
    @check_args(['user_id', 'pwd'])
    @unblock
    def post(self):
        data = self.get_post_data()
        user_id = to_int(data.get('user_id'))
        pwd = data.get('pwd')
        is_true, failure_msg = check_pwd_format(pwd)
        if not is_true:
            return failure(msg=failure_msg)
        usr = self.get_usr_dic()

        um = UserManage()
        res = um.user_edit_pwd(user_id, pwd, usr)

        return res


class UserEditAdminHandler(BaseApiHandler):
    @check_args(['user_id', 'is_admin'])
    @unblock
    def post(self):
        data = self.get_post_data()
        user_id = to_int(data.get('user_id'))
        is_admin = to_int(data.get('is_admin'))
        usr = self.get_usr_dic()

        um = UserManage()
        res = um.user_edit_admin(user_id, is_admin, usr)

        return res


class UserEditMobileHandler(BaseApiHandler):
    @check_args(['user_id', 'mobile'])
    @unblock
    def post(self):
        data = self.get_post_data()
        user_id = to_int(data.get('user_id'))
        mobile = to_int(data.get('mobile'))
        usr = self.get_usr_dic()

        um = UserManage()
        res = um.user_edit_mobile(user_id, mobile, usr)

        return res


class UserEditCrmuserHandler(BaseApiHandler):
    @check_args(['user_id', 'crm_user'])
    @unblock
    def post(self):
        data = self.get_post_data()
        user_id = to_int(data.get('user_id'))
        crm_user = data.get('crm_user')
        usr = self.get_usr_dic()

        um = UserManage()
        res = um.user_edit_crm_user(user_id, crm_user, usr)

        return res


class UserEditChandaouserHandler(BaseApiHandler):
    @check_args(['user_id', 'chandao_user'])
    @unblock
    def post(self):
        data = self.get_post_data()
        user_id = to_int(data.get('user_id'))
        chandao_user = data.get('chandao_user')
        usr = self.get_usr_dic()

        um = UserManage()
        res = um.user_edit_chandao_user(user_id, chandao_user, usr)

        return res


class UserDeleteHandler(BaseApiHandler):
    @check_args(['user_id'])
    @unblock
    def post(self):
        data = self.get_post_data()
        user_id = to_int(data.get('user_id'))
        usr = self.get_usr_dic()

        um = UserManage()
        res = um.user_delete(user_id=user_id, usr=usr)

        return res


class UserGetListHandler(BaseApiHandler):
    @unblock
    def post(self):
        data = self.get_post_data()
        select = data.get('select') or data.get('select_name')
        page = to_int(data.get('page'), 0)
        size = to_int(data.get('size'), 999)

        um = UserManage()
        res = um.user_get_list(select, page, size)

        return res


class UserSysOperationLogListHandler(BaseApiHandler):
    @unblock
    def post(self):
        data = self.get_post_data()
        user_id = to_int(data.get('user_id'))
        start_time = data.get('start_time')
        end_time = data.get('end_time')
        size = to_int(data.get('size'), 20)
        page = to_int(data.get('page'), 0)

        um = UserManage()
        res = um.user_sys_operation_log_list(user_id=user_id,
                                             start_time=start_time,
                                             end_time=end_time,
                                             size=size,
                                             page=page)

        return res
