# coding: utf-8
# author: lyd
# mobile: 15288343493
# email : 339789089@qq.com
# file  : project_payback_api.py
# time  : 2024-07-29 10:56
# desc  :
from base_api import BaseApiHandler, unblock
from framework.authority.authorities import check_args
from framework.msg import failure
from framework.utilities import to_int
from services.project_payback_manage import ProjectPaybackManage


class ProjectPaybackAdd(BaseApiHandler):
    @check_args(['project_id', 'chandao_project_id', 'payback_amount', 'payback_point', 'payback_time'])
    @unblock
    def post(self):
        data = self.get_post_data()

        chandao_project_id = to_int(data.get('chandao_project_id'))
        project_id = to_int(data.get('project_id'))
        payback_amount = data.get('payback_amount')
        payback_point = data.get('payback_point')
        payback_time = data.get('payback_time')
        desc = data.get('desc')

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        tp = ProjectPaybackManage(user)
        res = tp.project_payback_add(project_id, chandao_project_id, payback_amount, payback_point, payback_time, desc)
        return res


class ProjectPaybackGetList(BaseApiHandler):
    @check_args(['chandao_project_id'])
    @unblock
    def post(self):
        data = self.get_post_data()

        chandao_project_id = to_int(data.get('chandao_project_id'))

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        tp = ProjectPaybackManage(user)
        res = tp.project_payback_get_list(chandao_project_id)
        return res

class ProjectPaybackGetFirst(BaseApiHandler):
    @check_args(['payback_id'])
    @unblock
    def post(self):
        data = self.get_post_data()

        payback_id = to_int(data.get('payback_id'))

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        tp = ProjectPaybackManage(user)
        res = tp.project_payback_get_first(payback_id)
        return res


class ProjectPaybackUpdate(BaseApiHandler):
    @check_args(['payback_id', 'payback_amount', 'payback_point', 'payback_time'])
    @unblock
    def post(self):
        data = self.get_post_data()

        payback_id = to_int(data.get('payback_id'))
        payback_amount = data.get('payback_amount')
        payback_point = data.get('payback_point')
        payback_time = data.get('payback_time')
        desc = data.get('desc')

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        tp = ProjectPaybackManage(user)
        res = tp.project_payback_update(payback_id, payback_amount, payback_point, payback_time, desc)
        return res


class ProjectPaybackUpdateConfirmed(BaseApiHandler):
    @check_args(['payback_id', 'is_confirmed'])
    @unblock
    def post(self):
        data = self.get_post_data()

        payback_id = to_int(data.get('payback_id'))
        is_confirmed = to_int(data.get('is_confirmed'))

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        tp = ProjectPaybackManage(user)
        res = tp.project_payback_confirmed(payback_id, is_confirmed)
        return res
