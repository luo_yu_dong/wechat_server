#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: thirdparty_probation_api.py
@time: 2020/10/22 17:36
@desc:
'''
from base_api import BaseApiHandler, unblock
from framework.authority.authorities import thirdparty_check_args
from services.thirdparty_probation_manage import ThirdpartyProbationManage


class ThirdpartyVerifyHandler(BaseApiHandler):
    @thirdparty_check_args(['appkey', 'sing', 'timestamp'])
    @unblock
    def post(self):
        data = self.get_thirdparty_post_data()
        appkey = data.get('appkey')
        sing = data.get('sing')
        timestamp = data.get('timestamp')

        tp = ThirdpartyProbationManage(appkey=appkey, sing=sing, timestamp=timestamp)
        res = tp.thirdparty_verify()
        return res.responseMsg()
