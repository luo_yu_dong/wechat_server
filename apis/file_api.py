#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: file_api.py
@time: 2020/10/20 11:55
@desc:
'''
from base_api import BaseApiHandler, unblock
from framework.msg import failure
from services.file_manage import FileManage
from setting import FILE_ACCESS_HEADER


class FileAddHandler(BaseApiHandler):
    @unblock
    def post(self):
        try:
            header = self.get_post_data()
            ext = header['ext']  # 扩展名称
            file_name = header['file_name']
            # print('调取上传图片接口')
            # print(file_name)
            # file_name = '测试图片1111'
            if not ext or header.get('key', '') != FILE_ACCESS_HEADER:
                self.set_status(400)
                return failure()
            body = self.request.body_arguments.get('body')[0]

            fm = FileManage()
            result = fm.upload_one_file_to_dir(ext, body, file_name)  # 保存图片

            print('上传图片成功')
            return result.responseMsg()
        except Exception as e:
            # print(e)
            return failure().responseMsg()
