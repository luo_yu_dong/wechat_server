#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: statistics_api.py
@time: 2020/11/9 8:13
@desc: 统计分析
'''
from base_api import unblock, BaseApiHandler
from framework.authority.authorities import check_args
from framework.msg import failure
from framework.utilities import to_int
from services.statistics_manage import StatisticsManage


class StatisticsHomeHandler(BaseApiHandler):
    @unblock
    def post(self):
        um = StatisticsManage(user=self.get_usr_dic())
        res = um.statistics_home()

        return res


class ProjectProgressBoard(BaseApiHandler):
    @check_args(['chandao_project_id'])
    @unblock
    def post(self):
        data = self.get_post_data()

        chandao_project_id = data.get('chandao_project_id')

        um = StatisticsManage(user=self.get_usr_dic())
        res = um.project_progress_board_statistics(chandao_project_id)

        return res


class ProjectDataStatisticians(BaseApiHandler):
    @unblock
    def post(self):
        data = self.get_post_data()

        project_name = data.get('project_name')

        um = StatisticsManage(user=self.get_usr_dic())
        res = um.project_data_statistics(project_name)

        return res


class ProjectStatisticians(BaseApiHandler):
    @check_args(['come_from'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        project_name = data.get('project_name')

        um = StatisticsManage(user=user)
        res = um.project_list_statistics(project_name)

        return res
