#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: chandao_api.py
@time: 2024/3/17 17:23
@desc:
'''
import datetime

from base_api import BaseApiHandler, unblock
from cache import cache
from framework.authority.authorities import check_args
from framework.msg import failure
from framework.utilities import to_int, to_list, to_dict, to_datetime, date_minus
from services.chandao_manage import ChandaoManage
from services.wechat_wiki_manage import WeChatWikiManage


class ChandaoGetUserProjectList(BaseApiHandler):
    @check_args(['come_from', 'page', 'size'])
    @unblock
    def post(self):
        '''
        获取跟我相关的项目，管理员获取所有项目视野，

        售后成员根据登陆用户查询，
        小程序用户，根据小程序账号与用户的绑定关系查询
        客户端？？
        :return:
        '''

        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源
        wechat_user_id = to_int(data.get('wechat_user_id'))

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = to_int(data.get('bind_user_id'))

            if bind_user_id:
                user = {
                    'user_id': bind_user_id,
                    'user_name': bind_user_name
                }
            else:
                user = {
                    'user_id': 0,
                    'user_name': ''
                }

            if not bind_user_id and not wechat_user_id:
                return failure(msg='请求参数错误')

        is_head_party_a = to_int(data.get("is_head_party_a"))
        '''
        当 customer_server_user_id:有值是 is_head_party_a = 0， bind_user_id传customer_server_user_id，bind_user_name 传 bind_user_nam
        当 customer_server_user_id:无值是 is_head_party_a = 1 bind_user_id传wechat_user_id，bind_user_name 传 nike_name
        如果既有 customer_server_user_id 又有 wechat_user_id 传 is_head_party_a=0，bind_user_id传customer_server_user_id，bind_user_name 传 bind_user_name
        '''
        select_name = data.get('select_name')
        select_type = to_int(data.get('select_type'), 0)  # select_type  0:查询项目名称。1:查询成员名称
        page = to_int(data.get('page'), 0)
        size = to_int(data.get('size'), 20)
        status = data.get('status')
        is_refresh = to_int(data.get('is_refresh'), 0)

        tp = ChandaoManage(user)
        res = tp.get_user_project_list(select_name, status, page, size, is_refresh, wechat_user_id, come_from, select_type)
        return res


class ChandaoGetSysProjectPhaseConfigurationTree(BaseApiHandler):
    # @check_args(['page', 'size'])
    @unblock
    def post(self):
        '''
        获取系统配置的项目阶段配置

        :return:
        '''
        data = self.get_post_data()
        user = self.get_usr_dic()

        tp = ChandaoManage(user)
        res = tp.get_sys_project_phase_configuration_tree()
        return res


class ChandaoAddSysProjectPhaseConfiguration(BaseApiHandler):
    @check_args(['data_list'])
    @unblock
    def post(self):
        '''
        新增系统配置的项目阶段配置
        data_list = [{
        "point_name": "",
        "pid":"",
        "sort":"",
        }]

        :return:
        '''
        data = self.get_post_data()
        user = self.get_usr_dic()

        add_list = to_list(data.get('data_list'))
        if not add_list:
            return failure(msg='请检查传入参数！')

        tp = ChandaoManage(user)
        res = tp.add_sys_project_phase_configuration(add_list)
        return res


class ChandaoUpdateSysProjectPhaseConfiguration(BaseApiHandler):
    @check_args(['update_dict'])
    @unblock
    def post(self):
        '''
        修改系统配置的项目阶段配置
        update_dict = {
            "id":{"project_name": "","pid":"","sort":"","is_delete":""},
            "id":{"project_name": "","pid":"","sort":"","is_delete":""}
        }

        :return:
        '''
        data = self.get_post_data()
        user = self.get_usr_dic()

        update_dict = to_dict(data.get('update_dict'))
        if not update_dict:
            return failure(msg='请检查传入参数！')

        tp = ChandaoManage(user)
        res = tp.update_sys_project_phase_configuration(update_dict)
        return res


class ChandaoSetProjectPhase(BaseApiHandler):
    @check_args(['data_list', 'project_id'])
    @unblock
    def post(self):
        '''
        新增系统配置的项目阶段配置
        data_list = [{
            "point_name": "",
            "configuration_id":"",
            "configuration_pid":"",
            "configuration_sort":"",
            "plan_start_time":"",
            "plan_end_time"::"",
        }]
        project_id: 项目id
        :return:
        '''
        data = self.get_post_data()
        user = self.get_usr_dic()

        project_id = to_int(data.get('project_id'))
        data_list = to_list(data.get('data_list'))

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        if not data_list:
            return failure(msg='请检查传入参数！')

        tp = ChandaoManage(user)
        res = tp.chandao_set_project_phase(project_id, data_list, come_from)
        return res


class ChandaoGetProjectPhase(BaseApiHandler):
    @check_args(['project_id'])
    @unblock
    def post(self):
        data = self.get_post_data()

        user = {'user_id': 0, 'user_name': ''}

        project_id = to_int(data.get('project_id'))

        tp = ChandaoManage(user)
        res = tp.chandao_get_project_phase(project_id)
        return res


class ChandaoProjectPhaseUpdate(BaseApiHandler):
    @check_args(['project_id', 'come_from', 'phase_id'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        project_id = to_int(data.get('project_id'))
        phase_id = to_int(data.get('phase_id'))
        start_time = data.get('start_time')
        end_time = data.get('end_time')

        if not start_time and not end_time:
            return failure(msg='请检查传入参数！')

        tp = ChandaoManage(user)
        res = tp.chandao_project_phase_update(project_id, phase_id, start_time, end_time)
        return res


class ChandaoProjectShareBinding(BaseApiHandler):
    @check_args(['project_id', 'share_user_id'])
    @unblock
    def post(self):
        data = self.get_post_data()
        project_id = to_int(data.get('project_id'))
        share_user_id = to_int(data.get('share_user_id'))
        session_key = data.get('session_key')
        wechat_user_id = to_int(data.get('wechat_user_id'))

        if session_key:
            if cache.get(session_key + '_sh_wechat_user_id'):
                wechat_user_id = cache.get(session_key + '_sh_wechat_user_id')
        else:
            if self.get_weweb_cookie(name='sh_wechat_user_id'):
                wechat_user_id = to_int(self.get_weweb_cookie(name='sh_wechat_user_id'))
        if not wechat_user_id:
            return failure("提交失败！")

        tp = ChandaoManage(user={})
        res = tp.chandao_project_share_binding(project_id, share_user_id, wechat_user_id)
        return res


class ChandaoProjectShareBindingGetList(BaseApiHandler):
    @check_args(['project_id'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        project_id = to_int(data.get('project_id'))

        tp = ChandaoManage(user)
        res = tp.chandao_project_share_binding_get_list(project_id)
        return res


class ChandaoProjectTeamGetList(BaseApiHandler):
    @check_args(['chandao_project_id'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        chandao_project_id = to_int(data.get('chandao_project_id'))

        tp = ChandaoManage(user)
        res = tp.chandao_project_team_get_list(chandao_project_id)
        return res


class ChandaoGetProjectTaskFirst(BaseApiHandler):
    @check_args(['project_id'])
    @unblock
    def post(self):
        data = self.get_post_data()

        user = {'user_id': 0}

        project_id = to_int(data.get('project_id'))

        tp = ChandaoManage(user)
        res = tp.chandao_get_project_task_first(project_id)
        return res


class ChandaoGetProjectTaskSubset(BaseApiHandler):
    @check_args(['project_id', 'parent'])
    @unblock
    def post(self):
        data = self.get_post_data()

        user = {'user_id': 0}

        project_id = to_int(data.get('project_id'))
        parent = data.get('parent')

        tp = ChandaoManage(user)
        res = tp.chandao_get_project_task_subset(project_id, parent)
        return res


class ChandaoAddProjectCustomerPlan(BaseApiHandler):
    @check_args(['project_id', 'come_from', 'task', 'plan_time', 'customer_facilitator'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        project_id = to_int(data.get('project_id'))
        task = data.get('task')
        information_list = data.get('information_list')
        plan_time = data.get('plan_time')
        customer_facilitator = data.get('customer_facilitator')

        tp = ChandaoManage(user)
        res = tp.chandao_add_project_customer_plan(come_from, project_id, task, information_list, plan_time, customer_facilitator)
        return res


class ChandaoGetProjectCustomerPlan(BaseApiHandler):
    @check_args(['project_id', 'page', 'size'])
    @unblock
    def post(self):
        data = self.get_post_data()

        user = {
            'user_id': 0,
            'user_name': ''
        }

        project_id = to_int(data.get('project_id'))
        page = to_int(data.get('page'), 0)
        size = to_int(data.get('size'), 20)

        tp = ChandaoManage(user)
        res = tp.chandao_get_project_customer_plan(project_id, page, size)
        return res


class ChandaoUpdateProjectCustomerPlan(BaseApiHandler):
    @check_args(['project_id', 'end_time', 'customer_plan_id'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = to_int(data.get('bind_user_id'))
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        project_id = to_int(data.get('project_id'))
        end_time = data.get('end_time')
        customer_plan_id = to_int(data.get('customer_plan_id'))

        tp = ChandaoManage(user)
        res = tp.chandao_update_project_customer_plan(project_id, end_time, customer_plan_id)
        return res


class ChandaoAddProjectCustomerPlanTrackRecord(BaseApiHandler):
    @check_args(['project_id', 'customer_plan_id', 'come_from'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = to_int(data.get('bind_user_id'))
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        project_id = to_int(data.get('project_id'))
        customer_plan_id = to_int(data.get('customer_plan_id'))
        track_record = data.get('track_record')

        tp = ChandaoManage(user)
        res = tp.chandao_add_project_customer_plan_track_record(come_from, project_id, customer_plan_id, track_record)
        return res


class ChandaoGetProjectCustomerPlanTrackRecord(BaseApiHandler):
    @check_args(['project_id', 'customer_plan_id'])
    @unblock
    def post(self):
        user = {
            'user_id': 0,
            'user_name': ''
        }

        data = self.get_post_data()
        project_id = to_int(data.get('project_id'))
        customer_plan_id = to_int(data.get('customer_plan_id'))

        tp = ChandaoManage(user)
        res = tp.chandao_get_project_customer_plan_track_record(project_id, customer_plan_id)
        return res


class ChandaoAddProjectRisk(BaseApiHandler):
    @check_args(['project_id', 'come_from', 'risk_comment', 'plan_end_time', 'risk_level'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        project_id = to_int(data.get('project_id'))
        risk_comment = data.get('risk_comment')
        plan_end_time = data.get('plan_end_time')
        risk_level = data.get('risk_level')

        tp = ChandaoManage(user)
        res = tp.chandao_add_project_risk(project_id, come_from, risk_comment, plan_end_time, risk_level)
        return res


class ChandaoGetProjectRisk(BaseApiHandler):
    @check_args(['project_id', 'page', 'size'])
    @unblock
    def post(self):
        user = {
            'user_id': 0,
            'user_name': ''
        }

        data = self.get_post_data()
        project_id = to_int(data.get('project_id'))
        page = to_int(data.get('page'), 0)
        size = to_int(data.get('size'), 20)

        tp = ChandaoManage(user)
        res = tp.chandao_get_project_risk(project_id, page, size)
        return res


class ChandaoUpdateProjectRisk(BaseApiHandler):
    @check_args(['project_id', 'come_from', 'end_time', 'risk_id'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = to_int(data.get('bind_user_id'))
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        project_id = to_int(data.get('project_id'))
        risk_id = data.get('risk_id')
        end_time = data.get('end_time')

        tp = ChandaoManage(user)
        res = tp.chandao_update_project_risk(project_id, risk_id, end_time)
        return res


class ChandaoAddProjectRiskTrackRecord(BaseApiHandler):
    @check_args(['project_id', 'come_from', 'risk_id', 'track_content'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        project_id = to_int(data.get('project_id'))
        risk_id = to_int(data.get('risk_id'))
        track_content = data.get('track_content')

        tp = ChandaoManage(user)
        res = tp.chandao_add_project_risk_track_record(project_id, risk_id, track_content, come_from)
        return res


class ChandaoGetProjectRiskTrackRecord(BaseApiHandler):
    @check_args(['project_id', 'risk_id', 'page', 'size'])
    @unblock
    def post(self):
        data = self.get_post_data()

        user = {
            'user_id': 0,
            'user_name': ''
        }

        project_id = to_int(data.get('project_id'))
        risk_id = to_int(data.get('risk_id'))
        page = to_int(data.get('page'), 0)
        size = to_int(data.get('size'), 20)

        tp = ChandaoManage(user)
        res = tp.chandao_get_project_risk_track_record(project_id, risk_id, page, size)
        return res


class ChandaoGetProjectRiskFirst(BaseApiHandler):
    @check_args(['project_id', 'itme_id', 'page', 'size'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        project_id = to_int(data.get('project_id'))
        itme_id = to_int(data.get('itme_id'))
        page = to_int(data.get('page'))
        size = to_int(data.get('size'))

        tp = ChandaoManage(user)
        res = tp.chandao_get_project_risk_first(project_id, itme_id, page, size)
        return res


class ChandaoProjectTaskGetList(BaseApiHandler):
    @check_args(['project_id', 'time_type'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        project_id = to_int(data.get('project_id'))
        time_type = to_int(data.get('time_type'), 0)  # 0-本周，1-所有，-1-自定义时间，
        page = to_int(data.get('page'), 0)
        size = to_int(data.get('size'), 20)
        task_name = data.get('task_name')
        start_time = data.get('start_time')
        end_time = data.get('end_time')
        if time_type == -1 and (not start_time or not end_time):
            return failure(msg='请求参数错误！')
        is_my = to_int(data.get('is_my'), 0)  # 是否只查我的

        tp = ChandaoManage(user)
        res = tp.chandao_project_task_get_list(project_id, task_name, start_time, end_time, is_my, time_type, page, size)
        return res


class ChandaoProjectTaskBatchAdd(BaseApiHandler):
    @check_args(['project_id', 'data_list'])
    @unblock
    def post(self):
        try:
            data = self.get_post_data()

            come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

            if not come_from:
                user = self.get_usr_dic()
            else:
                bind_user_name = data.get('bind_user_name')
                bind_user_id = data.get('bind_user_id')
                if not bind_user_id:
                    return failure(msg='请求参数错误')
                user = {
                    'user_id': bind_user_id,
                    'user_name': bind_user_name
                }

            project_id = to_int(data.get('project_id'))
            item_list = to_list(data.get('data_list'))

            tp = ChandaoManage(user)
            res = tp.chandao_project_task_add(project_id, item_list)
            return res
        except Exception as e:
            print(e)
            return failure().responseMsg()


class ChandaoProjectTaskMandaysOverrun(BaseApiHandler):
    @check_args(['user_id'])
    @unblock
    def post(self):
        try:
            data = self.get_post_data()

            come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

            if not come_from:
                user = self.get_usr_dic()
            else:
                bind_user_name = data.get('bind_user_name')
                bind_user_id = data.get('bind_user_id')
                if not bind_user_id:
                    return failure(msg='请求参数错误')
                user = {
                    'user_id': bind_user_id,
                    'user_name': bind_user_name
                }

            user_id = to_int(data.get('user_id'))
            new_taks_plan_staff_time = to_int(data.get('new_taks_plan_staff_time'))  # 新任务的计划工时
            task_start_time = data.get('start_time')
            end_start_time = data.get('end_start_time')

            tp = ChandaoManage(user)
            res = tp.chandao_project_task_mandays_overrun(user_id, new_taks_plan_staff_time, task_start_time, end_start_time)
            return res
        except Exception as e:
            print(e)
            return failure().responseMsg()


class ChandaoProjectTaskDelete(BaseApiHandler):
    @check_args(['project_id', 'task_id_list'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        project_id = to_int(data.get('project_id'))
        task_id_list = to_list(data.get('task_id_list'))

        tp = ChandaoManage(user)
        res = tp.chandao_project_task_delete(project_id, task_id_list)
        return res


class ChandaoProjectTaskUpdateTime(BaseApiHandler):
    @check_args(['project_id', 'task_id', 'start_time'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        project_id = to_int(data.get('project_id'))
        task_id = to_int(data.get('task_id'))
        start_time = data.get('start_time')
        end_time = data.get('end_time')
        if not start_time and end_time:
            return failure(msg='请填写开始时间！')
        responsible_person_is_confirmed = to_int(data.get('responsible_person_is_confirmed'), 0)
        responsible_person_is_confirmed_time = data.get('responsible_person_is_confirmed_time')
        if responsible_person_is_confirmed and not responsible_person_is_confirmed_time:
            return failure(msg='请求参数错误')

        tp = ChandaoManage(user)
        res = tp.chandao_project_task_update_time(project_id, task_id, start_time, end_time, responsible_person_is_confirmed)
        return res


class ChandaoProjectResponsibleConfirmed(BaseApiHandler):
    @check_args(['project_id', 'task_id_list'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        project_id = to_int(data.get('project_id'))
        task_id = to_int(data.get('task_id'))

        tp = ChandaoManage(user)
        res = tp.chandao_project_task_responsible_confirmed(project_id, task_id)
        return res


class ChandaoProjectPmConfirmed(BaseApiHandler):
    @check_args(['project_id', 'task_id_list'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        project_id = to_int(data.get('project_id'))
        task_id = to_int(data.get('task_id'))

        tp = ChandaoManage(user)
        res = tp.chandao_project_task_pm_confirmed(project_id, task_id, come_from)
        return res


class ChandaoProjectTaskConfirmed(BaseApiHandler):
    @check_args(['project_id', 'task_id_list'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_id = to_int(data.get('wechat_user_id'))
            wechat_user_name = data.get('wechat_user_name')
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }

        project_id = to_int(data.get('project_id'))
        task_id_list = to_list(data.get('task_id_list'))

        tp = ChandaoManage(user)
        res = tp.chandao_project_task_confirmed(project_id, task_id_list)
        return res


class ChandaoProjectTaskProgressAdd(BaseApiHandler):
    @check_args(['project_id', 'task_id', 'progress'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_name = data.get('wechat_user_name')
            wechat_user_id = to_int(data.get('wechat_user_id'))
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }

        project_id = to_int(data.get('project_id'))
        task_id = to_int(data.get('task_id'))
        progress = data.get('progress')

        tp = ChandaoManage(user)
        res = tp.chandao_project_task_progress_add(project_id, task_id, progress)
        return res


class ChandaoProjectTaskProgressGet(BaseApiHandler):
    @check_args(['project_id', 'task_id'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_id = to_int(data.get('wechat_user_id'))
            wechat_user_name = data.get('wechat_user_name')
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }

        project_id = to_int(data.get('project_id'))
        task_id = to_int(data.get('task_id'))
        page = to_int(data.get('page'), 0)
        size = to_int(data.get('size'), 20)

        tp = ChandaoManage(user)
        res = tp.chandao_project_task_progress_get(project_id, task_id, page, size)
        return res


class ChandaoProjectTaskProgressDelete(BaseApiHandler):
    @check_args(['project_id', 'task_id', 'progress_id_list'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_id = to_int(data.get('wechat_user_id'))
            wechat_user_name = data.get('wechat_user_name')
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }

        project_id = to_int(data.get('project_id'))
        task_id = to_int(data.get('task_id'))
        progress_id_list = to_list(data.get('progress_id_list'))

        tp = ChandaoManage(user)
        res = tp.chandao_project_task_progress_delete(project_id, task_id, progress_id_list)
        return res


class ChandaoProjectInformationUpdate(BaseApiHandler):
    @check_args(['project_id', 'project_objectives'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_id = to_int(data.get('wechat_user_id'))
            wechat_user_name = data.get('wechat_user_name')
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }

        project_id = to_int(data.get('project_id'))
        project_objectives = data.get('project_objectives')
        order_staff_data = to_int(data.get('order_staff_data'))
        budget_staff_data = to_int(data.get('budget_staff_data'))

        tp = ChandaoManage(user)
        res = tp.chandao_project_update_information_update(project_id, project_objectives, order_staff_data, budget_staff_data)
        return res


class ChandaoProjectInformationGet(BaseApiHandler):
    @check_args(['project_id'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_id = to_int(data.get('wechat_user_id'))
            wechat_user_name = data.get('wechat_user_name')
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }

        project_id = to_int(data.get('project_id'))

        tp = ChandaoManage(user)
        res = tp.chandao_project_update_information_get(project_id)
        return res


class ChandaoProjectCalendar(BaseApiHandler):
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_id = to_int(data.get('wechat_user_id'))
            wechat_user_name = data.get('wechat_user_name')
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }
        now = datetime.datetime.now()
        year = to_int(data.get('year'), now.year)  # 年
        month = to_int(data.get('month'), now.month)  # 月份

        tp = ChandaoManage(user)
        res = tp.chandao_project_calendar(year, month)
        return res


class ChandaoProjectCalendarUpdate(BaseApiHandler):
    @check_args(['update_dict'])
    @unblock
    def post(self):
        '''
        update_dict = {calendar_id:is_workdays}
        :return:
        '''
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_id = to_int(data.get('wechat_user_id'))
            wechat_user_name = data.get('wechat_user_name')
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }
        update_dict = to_dict(data.get('update_dict'))

        tp = ChandaoManage(user)
        res = tp.chandao_project_calendar_update(update_dict)
        return res


class ChandaoProjectTaskWorkingDailyGet(BaseApiHandler):
    @check_args(['page', 'size'])
    @unblock
    def post(self):
        '''
        update_dict = {calendar_id:is_workdays}
        :return:
        '''
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_id = to_int(data.get('bind_user_id'))
            wechat_user_name = data.get('bind_user_name')
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }

        select_user_name = data.get('select_user_name')
        page = to_int(data.get('page'), 0)
        size = to_int(data.get('size'), 20)

        tp = ChandaoManage(user)
        res = tp.chandao_project_task_working_daily_get_list(select_user_name, page, size)
        return res


class ChandaoProjectTaskWorkingDailyAdd(BaseApiHandler):
    @check_args(['reporting_msg', 'reporting_date'])
    @unblock
    def post(self):
        '''
        update_dict = {calendar_id:is_workdays}
        :return:
        '''
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_id = to_int(data.get('bind_user_id'))
            wechat_user_name = data.get('bind_user_name')
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }

        reporting_msg = data.get('reporting_msg')
        reporting_date = to_datetime(data.get('reporting_date'))
        now_data = datetime.date.today()
        if date_minus(now_data, reporting_date) > 1:
            return failure(msg='汇报日期不能早于昨天！')

        tp = ChandaoManage(user)
        res = tp.chandao_project_task_working_daily_add(reporting_msg, reporting_date)
        return res


class WeChatProjectWxacodeHandler(BaseApiHandler):
    @check_args(['project_id'])
    @unblock
    def post(self):
        '''
        is_customer_service, type, item_id, user_id, project_id, model
        model=1 项目
        :return:
        '''
        data = self.get_post_data()

        is_customer_service = 0  # 是否是客服
        type = 1  # 小程序
        item_id = 0
        user_id = 0
        project_id = data.get('project_id')  # 项目id
        model = 1

        wpb = WeChatWikiManage()
        res = wpb.wechat_make_WXACode(project_id)

        return res


class ChandaoProjectActualityAdd(BaseApiHandler):
    @check_args(['actuality_list', 'year', 'month'])
    @unblock
    def post(self):
        '''
        update_dict = {calendar_id:is_workdays}
        :return:
        '''
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_id = to_int(data.get('bind_user_id'))
            wechat_user_name = data.get('bind_user_name')
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }

        actuality_list = data.get('actuality_list')
        year = to_int(data.get('year'))
        month = to_int(data.get('month'))

        tp = ChandaoManage(user)
        res = tp.chandao_project_actuality_add(actuality_list, year, month)
        return res


class ChandaoProjectActualityEdit(BaseApiHandler):
    @check_args(['actuality_list', 'year', 'month'])
    @unblock
    def post(self):
        '''
        update_dict = {calendar_id:is_workdays}
        :return:
        '''
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_id = to_int(data.get('bind_user_id'))
            wechat_user_name = data.get('bind_user_name')
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }

        actuality_list = data.get('actuality_list')
        year = to_int(data.get('year'))
        month = to_int(data.get('month'))

        tp = ChandaoManage(user)
        res = tp.chandao_project_actuality_add(actuality_list, year, month, 1)
        return res

class ChandaoProjectActualityGetList(BaseApiHandler):
    @unblock
    def post(self):
        '''
        update_dict = {calendar_id:is_workdays}
        :return:
        '''
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_id = to_int(data.get('bind_user_id'))
            wechat_user_name = data.get('bind_user_name')
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }

        now = datetime.datetime.now()
        year = to_int(data.get('year'), now.year)
        month = to_int(data.get('month'), now.month)
        user_id = to_int(data.get('user_id'), user['user_id'])

        tp = ChandaoManage(user)
        res = tp.chandao_project_actuality_get_list(year, month, user_id)
        return res

class ChandaoProjectActualityGetFirst(BaseApiHandler):
    @check_args(['user_id'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_id = to_int(data.get('bind_user_id'))
            wechat_user_name = data.get('bind_user_name')
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }

        now = datetime.datetime.now()
        year = to_int(data.get('year'), now.year)
        month = to_int(data.get('month'), now.month)
        user_id = data.get('user_id')

        tp = ChandaoManage(user)
        res = tp.chandao_project_actuality_get_first(year, month, user_id)
        return res

class ChandaoProjectActualityGetMonth(BaseApiHandler):
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_id = to_int(data.get('bind_user_id'))
            wechat_user_name = data.get('bind_user_name')
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }

        now = datetime.datetime.now()
        year = to_int(data.get('year'), now.year)
        start_month = to_int(data.get('start_month'), now.month)
        end_month = to_int(data.get('start_month'), now.month)

        tp = ChandaoManage(user)
        res = tp.chandao_project_actuality_get_month(year, start_month, end_month)
        return res


class ChandaoProjectActualityGetMonthExport(BaseApiHandler):
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_id = to_int(data.get('bind_user_id'))
            wechat_user_name = data.get('bind_user_name')
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }

        now = datetime.datetime.now()
        year = to_int(data.get('year'), now.year)
        start_month = to_int(data.get('start_month'), now.month)
        end_month = to_int(data.get('end_month'), now.month)

        tp = ChandaoManage(user)
        res = tp.chandao_project_actuality_get_month_export(year, start_month, end_month)
        return res