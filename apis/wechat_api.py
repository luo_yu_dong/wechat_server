#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: wechat_api.py
@time: 2020/10/19 9:02
@desc:
'''
import traceback

from base_api import BaseApiHandler, unblock
from cache import cache
from enums.enum_cache_type import EnumCacheType
from framework.authority.authorities import check_args
from framework.log_controller import log
from framework.msg import failure
from framework.utilities import to_string, to_dict, to_int, xml_to_dict, current_timestamp
from services.wechat_customer_manage import WechatCustomerManage
from services.wechat_program_base import WechatProgramBase
from services.wechat_wiki_manage import WeChatWikiManage
from setting import WECHAT_WIKI_APPID, WECHAT_WIKI_SECRET, WECHAT_PRO_APPID, WECHAT_PRO_SECRET, WECHAT_VERIFY
from thirdpart import redis_helper as re


class WechatProgramImgSecCheckHandler(BaseApiHandler):
    @unblock
    def post(self):
        try:
            wpb = WechatProgramBase()
            access_token_time = to_int(re.get(key='program_access_token_time', cache_type=EnumCacheType.sns))
            if access_token_time:
                now = current_timestamp()  # 当前时间戳
                if access_token_time - now > 200:
                    access_token = re.get(key='program_access_token', cache_type=EnumCacheType.sns)
                else:
                    timestamp, access_token = wpb.get_access_token(WECHAT_PRO_APPID, WECHAT_PRO_SECRET)
            else:
                timestamp, access_token = wpb.get_access_token(WECHAT_PRO_APPID, WECHAT_PRO_SECRET)

            if not access_token:
                return failure(msg='请求失败！')

            req_data = self.request.files.get('upload_file')[0]
            # ext = req_data.get('content_type').split('/')[1]  # 扩展名称
            old_ext = req_data.get('content_type')
            file_name = req_data.get('filename')
            ext = file_name.split('.')[-1]
            body = req_data.get('body')
            file_url = self.get_post_data().get('file_url')
            new_file_name = to_string(self.get_post_data().get('new_file_name'), '')
            if new_file_name:
                file_name = new_file_name

            result = wpb.add_imgs(req_data, ext, old_ext, body, file_name, access_token, file_url)  # 保存图片

            return result
        except Exception as e:
            # print(e)
            return failure().responseMsg()


class WechatVerifyFileHandler(BaseApiHandler):
    @unblock
    def get(self):
        try:
            path = self.request.path
            resques_path = path.split('/')[-1]

            server_path = WECHAT_VERIFY + resques_path

            self.set_header('Content-Type', 'application/octet-stream')
            self.set_header('Content-Disposition', 'attachment; ')
            with open(server_path, "rb") as imageFile:
                f = imageFile.read()
                self.write(f)

        except:
            log(traceback.format_exc(), 'WechatVerifyFileHandler')
            self.set_status(400)


class WechatVerifyDomainFileHandler(BaseApiHandler):
    @unblock
    def get(self):
        try:
            path = self.request.path
            resques_path = path.split('/')[-1]

            server_path = WECHAT_VERIFY + resques_path

            self.set_header('Content-Type', 'application/octet-stream')
            self.set_header('Content-Disposition', 'attachment; ')
            with open(server_path, "rb") as imageFile:
                f = imageFile.read()
                self.write(f)

        except Exception as e:
            print(e)
            self.set_status(400)


class WeChatMakeWikiWXACodeHandler(BaseApiHandler):
    @unblock
    def post(self):
        wechat = WechatProgramBase()
        res = wechat.make_qrcode()

        return res


class WeChatVerifyHandler(BaseApiHandler):
    @unblock
    def get(self):
        data = self.request.query_arguments

        wechat = WeChatWikiManage()
        res = wechat.verify(data)

        return res

    def post(self):
        body = self.request.body
        dict = xml_to_dict(body)
        ToUserName = dict.get('ToUserName')  # 开发者微信号
        FromUserName = dict.get('FromUserName')  # 发送方帐号（一个OpenID）
        MsgType = dict.get('MsgType')  # 消息类型

        wpb = WechatProgramBase()
        wiki = WeChatWikiManage()

        access_token_time = to_int(re.get(key='wiki_access_token_time', cache_type=EnumCacheType.sns))
        # access_token_time = None
        if access_token_time:
            now = current_timestamp()  # 当前时间戳
            if access_token_time - now > 200:
                access_token = re.get(key='wiki_access_token', cache_type=EnumCacheType.sns)
            else:
                timestamp, access_token = wpb.get_access_token(WECHAT_WIKI_APPID, WECHAT_WIKI_SECRET)

                re.set(key='wiki_access_token_time', value=to_string(current_timestamp() + 7200), time=60 * 60 * 1.8, cache_type=EnumCacheType.sns)
                re.set(key='wiki_access_token', value=to_string(access_token), time=60 * 60 * 1.8, cache_type=EnumCacheType.sns)
        else:
            timestamp, access_token = wpb.get_access_token(WECHAT_WIKI_APPID, WECHAT_WIKI_SECRET)
            re.set(key='wiki_access_token_time', value=to_string(current_timestamp() + 7200), time=60 * 60 * 1.8, cache_type=EnumCacheType.sns)
            re.set(key='wiki_access_token', value=to_string(access_token), time=60 * 60 * 1.8, cache_type=EnumCacheType.sns)

        if MsgType == 'event':
            Event = dict.get('Event')  # 事件类型
            if Event == 'subscribe' or Event == 'SCAN':
                # 获取用户信息
                user_info = wiki.user_info(access_token, FromUserName)
                # 写入缓存
                self.set_secure_cookie(name='unionid', value=user_info['unionid'], expires_days=30)
                self.set_secure_cookie(name='gzh_opend_id', value=FromUserName, expires_days=30)

                EventKey = dict.get('EventKey')
                if Event == 'subscribe' or Event == 'SCAN':
                    if EventKey:
                        if Event == 'subscribe':
                            new_EventKey = EventKey.split('qrscene_')[1]
                        if Event == 'SCAN':
                            new_EventKey = EventKey

                        is_customer_service, type, item_id, user_id, project_id, model = new_EventKey.split('-_-')
                    else:
                        is_customer_service = 0
                        project_id = 0

                    # 生成回调url
                    url = wiki.get_redirect_uri(WECHAT_WIKI_APPID, is_customer_service)
                    # 保存用户信息
                    if user_info:
                        wechat_user_info = wpb.save_user_msg(user_info=user_info,
                                                             is_customer_service=is_customer_service,
                                                             type=2,
                                                             is_attention_wiki=1,
                                                             sessionKey=to_string(user_info.get('unionid'), ''))
                    # 保存关注信息
                    if user_info.get('unionid'):
                        wiki.attention_accounts(user_info['unionid'])

                else:  # 再次关注的时候
                    if EventKey:
                        is_customer_service, type, item_id, user_id, project_id, model = EventKey.split('-_-')
                    else:
                        is_customer_service = 0
                        project_id = 0

                    # 生成回调url
                    url = wiki.get_redirect_uri(WECHAT_WIKI_APPID, is_customer_service)
                    # 保存用户信息
                    wechat_user_info = wpb.save_user_msg(user_info=user_info,
                                                         is_customer_service=is_customer_service,
                                                         type=2,
                                                         is_attention_wiki=1,
                                                         sessionKey=to_string(user_info.get('unionid'), ''))
                    # 保存关注信息
                    if user_info.get('unionid'):
                        wiki.attention_accounts(user_info.get('unionid'))

                # 保存绑定信息
                if to_int(project_id) and user_info.get('unionid'):
                    wiki.save_project_dinding(user_info.get('unionid'), project_id)

                out_msg = wiki.push_attention_msg(url, FromUserName, ToUserName)
                self.write(out_msg)
                self.finish()

            elif Event == 'unsubscribe':
                # 获取用户信息
                user_info = wiki.user_info(access_token, FromUserName)
                # 保存关注信息
                wiki.un_attention_accounts(user_info['openid'])
                self.write('')
                # 删除绑定信息
                wiki.un_chandao_project_share_binding_get_list(user_info.get('unionid'))

        if MsgType == 'text':
            replayText = ''
            self.write('')


class WeChatAuthorizationHandler(BaseApiHandler):
    @unblock
    def get(self):
        try:
            data = self.request.query_arguments
            code = to_string(data.get('code')[0])
            state = to_string(data.get('state')[0])

            wechat = WeChatWikiManage()
            res = wechat.authorization(code, state)

            if not res.is_ok():
                redirect_uri = '/pages/logs/wxml?is_authorization=1&state=%s' % (to_string(state))
            else:
                wechat_user_id = res.data['wechat_user_id']
                access_token = res.data['access_token']
                wiki_access_token_time = res.data['wiki_access_token_time']

                re.set(key='wiki_access_token_time', value=to_string(wiki_access_token_time + 7200), time=60 * 60 * 3, cache_type=EnumCacheType.sns)
                re.set(key='wiki_access_token', value=to_string(access_token), time=60 * 60 * 3, cache_type=EnumCacheType.sns)

                redirect_uri = res.data['redirect_uri']

                cache.set(key=to_string(res.data['session_key']) + '_unionid', value=to_string(res.data['unionid']), time=24 * 3600)
                cache.set(key=to_string(res.data['session_key']) + '_gzh_wechat_user_id', value=to_string(wechat_user_id), time=24 * 3600)
                self.set_cookie(name='session_key', value=to_string(res.data['session_key']))

                self.set_secure_cookie('unionid', to_string(res.data['unionid']), expires_days=30)
                self.set_secure_cookie('gzh_wechat_user_id', to_string(wechat_user_id), expires_days=30)

            self.redirect(redirect_uri)
        except Exception as e:
            error_msg = to_string(e)
            self.render('WeChatAuthorizationHandler', error_msg=error_msg)
        finally:
            if not self._finished:
                self.finish()


class WeChatProgramLoginHandler(BaseApiHandler):
    @unblock
    def post(self):
        try:
            req_data = to_string(self.request.body)
            js_code = to_dict(req_data).get('code')
            is_customer_service = 0  # 这个参数废了，后面去查的

            # 这里是换取用户的信息
            wechat = WechatProgramBase()
            user_info = wechat.wechat_login(js_code, WECHAT_PRO_APPID, WECHAT_PRO_SECRET)

            unionid = user_info.get('unionid')
            openid = user_info.get('openid')
            session_key = user_info.get('session_key')
            if not session_key:
                return failure(msg='登陆失败')

            # 保存用户信息
            res = wechat.save_user_msg(user_info=user_info, is_customer_service=is_customer_service, type=1, sessionKey=session_key)
            if not res.is_ok():
                return failure(msg='登陆失败')

            wechat_user_id = res.data['wechat_user_id']

            # 写入缓存
            if unionid:
                cache.set(key=session_key + '_unionid', value=unionid, time=7 * 3600 * 24)

            cache.set(key=session_key + '_sh_open_id', value=openid, time=7 * 3600 * 24)
            cache.set(key=session_key + '_sh_wechat_user_id', value=to_string(wechat_user_id), time=7 * 3600 * 24)
            cache.set(key=to_string(wechat_user_id) + '_sh_session_key', value=to_string(session_key), time=3600)

            # 写入缓存
            if unionid:
                self.set_secure_cookie(name='unionid', value=unionid, expires_days=30)

            self.set_secure_cookie(name='sh_open_id', value=openid, expires_days=30)
            self.set_secure_cookie(name='sh_wechat_user_id', value=to_string(wechat_user_id), expires_days=30)
            self.set_secure_cookie(name=to_string(wechat_user_id) + '_sh_session_key', value=to_string(session_key), expires_days=30)

            res.data['session_key'] = session_key

            return res
        except Exception as e:
            log(traceback.format_exc(), 'WeChatProgramLoginHandler', '登陆失败!')
            return failure(msg='登陆失败，请重新登陆')


class WechatProgramUserInfoHandler(BaseApiHandler):
    # @check_args(['wechat_user_id', 'detail'])
    @unblock
    def post(self):
        data = self.get_post_data()
        result = to_dict(data.get('detail'))
        user_info = result.get('userInfo')
        wechat_user_id = user_info.get('wechat_user_id')

        print('---------------------------------- 打印result ---------------------------------- ')
        print(result)

        if wechat_user_id:
            session_key = cache.get(wechat_user_id + '_sh_session_key')
            if not session_key:
                session_key = to_string(self.get_secure_cookie(wechat_user_id + '_sh_session_key'))
            if not session_key:
                session_key = ''
        else:
            return failure('提交失败')

        encryptedData = result.get('encryptedData').replace(' ', '+')

        wechat = WechatProgramBase()
        res = wechat.save_user_msg(user_info=user_info, is_customer_service=0, type=1, encryptedData=encryptedData, sessionKey=session_key)
        unionid = res.data['unionid']
        # 写入缓存
        if unionid:
            cache.set(key=to_string(wechat_user_id) + '_unionid', value=unionid, time=3600 * 24)
            self.set_secure_cookie(name=to_string(wechat_user_id) + 'unionid', value=unionid, expires_days=1)

        return res


# ----------------------------------- 业务处理 -----------------------------------
class WeChatCustomerSubmmintFormCreatHandler(BaseApiHandler):
    @check_args(['session_key', 'wechat_user_id', 'business_name', 'contacts', 'contacts_phone', 'comment', 'form_type', 'title', 'is_server_time'])
    @unblock
    def post(self):
        data = self.get_post_data()
        session_key = data.get('session_key')
        wechat_user_id = to_int(data.get('wechat_user_id'))
        form_type = data.get('form_type')
        is_server_time = data.get('is_server_time')
        title = data.get('title')
        business_name = data.get('business_name')
        contacts = data.get('contacts')
        contacts_phone = data.get('contacts_phone')
        comment = data.get('comment')
        imgs = data.get('imgs')

        if session_key:
            if cache.get(session_key + '_sh_wechat_user_id'):
                wechat_user_id = cache.get(session_key + '_sh_wechat_user_id')
        else:
            if self.get_weweb_cookie(name='sh_wechat_user_id'):
                wechat_user_id = to_int(self.get_weweb_cookie(name='sh_wechat_user_id'))
        if not wechat_user_id:
            return failure("提交失败！")

        wpb = WechatProgramBase()
        access_token_time = to_int(re.get(key='program_access_token_time', cache_type=EnumCacheType.sns))
        if access_token_time:
            now = current_timestamp()  # 当前时间戳
            if access_token_time - now > 200:
                access_token = re.get(key='program_access_token', cache_type=EnumCacheType.sns)
            else:
                timestamp, access_token = wpb.get_access_token(WECHAT_PRO_APPID, WECHAT_PRO_SECRET)
        else:
            timestamp, access_token = wpb.get_access_token(WECHAT_PRO_APPID, WECHAT_PRO_SECRET)

        if not access_token:
            return failure(msg='请求失败！')

        wpm = WechatCustomerManage(wechat_user_id)
        res = wpm.submmint_form(form_type, business_name, contacts, contacts_phone, comment, imgs, title, access_token, is_server_time)

        return res


class WeChatCustomerSubmmintFormListHandler(BaseApiHandler):
    @check_args(['session_key', 'wechat_user_id'])
    @unblock
    def post(self):
        data = self.get_post_data()
        session_key = data.get('session_key')
        wechat_user_id = to_int(data.get('wechat_user_id'))
        page = to_int(data.get('page'), 0)
        size = to_int(data.get('size'), 20)
        select_name = data.get('select_name')
        form_type = to_int(data.get('form_type'))
        is_reply = data.get('is_reply')
        judge_is_customer_service_admin = to_int(data.get('judge_is_customer_service_admin'))
        judge_is_customer_service = to_int(data.get('judge_is_customer_service'))
        customer_server_user_id = to_int(data.get('customer_server_user_id'))
        select_comment = data.get('select_comment')

        if session_key:
            if cache.get(session_key + '_sh_wechat_user_id'):
                wechat_user_id = cache.get(session_key + '_sh_wechat_user_id')
        else:
            if self.get_weweb_cookie(name='sh_wechat_user_id'):
                wechat_user_id = to_int(self.get_weweb_cookie(name='sh_wechat_user_id'))
        if not wechat_user_id:
            return failure("提交失败！")

        wpm = WechatCustomerManage(wechat_user_id)
        if judge_is_customer_service:
            res = wpm.customer_server_submmint_form_list(select_name, form_type, is_reply, page, size, judge_is_customer_service_admin, customer_server_user_id,
                                                         select_comment)
        else:
            res = wpm.submmint_form_list(select_name, form_type, is_reply, page, size, select_comment)

        return res


class WeChatCustomerSubmmintFormFirstHandler(BaseApiHandler):
    @check_args(['id'])
    @unblock
    def post(self):
        data = self.get_post_data()
        id = data.get('id')
        user_id = to_int(data.get('user_id'), 0)

        wpm = WechatCustomerManage(wechat_user_id=0)
        res = wpm.submmint_form_first(id, user_id)

        return res


class WeChatCustomerSubmmintFormScoreHandler(BaseApiHandler):
    @check_args(['session_key', 'wechat_user_id', 'id'])
    @unblock
    def post(self):
        data = self.get_post_data()
        session_key = data.get('session_key')
        wechat_user_id = to_int(data.get('wechat_user_id'))
        id = data.get('id')
        score = data.get('score')
        desc = data.get('desc')

        if session_key:
            if cache.get(session_key + '_sh_wechat_user_id'):
                wechat_user_id = cache.get(session_key + '_sh_wechat_user_id')
        else:
            if self.get_weweb_cookie(name='sh_wechat_user_id'):
                wechat_user_id = to_int(self.get_weweb_cookie(name='sh_wechat_user_id'))
        if not wechat_user_id:
            return failure("提交失败！")

        wpm = WechatCustomerManage(wechat_user_id)
        res = wpm.submmint_form_score(id, score, desc)

        return res


class WeChatUserGetListHandler(BaseApiHandler):
    @unblock
    def post(self):
        um = WechatCustomerManage(wechat_user_id=0)
        res = um.user_get_list()

        return res
