# coding: utf-8
# author: lyd
# mobile: 15288343493
# email : 339789089@qq.com
# file  : working_daily_api.py
# time  : 2024-07-08 09:55
# desc  :

from base_api import BaseApiHandler, unblock
from framework.authority.authorities import check_args
from framework.msg import failure
from framework.utilities import to_list, to_string, to_datetime, to_int
from services.working_daily_manage import WorkingDailyManage


class GetSysProjectPhaseConfiguration(BaseApiHandler):
    # @check_args(['page', 'size'])
    @unblock
    def post(self):
        '''
        获取系统配置的项目阶段配置

        :return:
        '''

        tp = WorkingDailyManage(user={'user_id': 0, 'user_name': ''})
        res = tp.get_sys_project_phase_configuration()
        return res


class ProjectTaskWorkingDailySelect(BaseApiHandler):
    @check_args(['report_time'])
    @unblock
    def post(self):
        '''
        获取系统配置的项目阶段配置

        :return:
        '''
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }
        report_time = to_string(data.get('report_time'))[:10]

        tp = WorkingDailyManage(user)
        res = tp.project_task_working_daily_select(report_time)
        return res


class ProjectTaskWorkingDailyAdd(BaseApiHandler):
    @check_args(['task_working_detail_list', 'report_time'])
    @unblock
    def post(self):
        '''
        获取系统配置的项目阶段配置

        :return:
        '''
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }
        task_working_detail_list = to_list(data.get('task_working_detail_list'))
        report_time = to_string(data.get('report_time'))[:10]

        tp = WorkingDailyManage(user)
        res = tp.project_task_working_daily_add(report_time, task_working_detail_list)
        return res


class ProjectTaskWorkingDailyEdit(BaseApiHandler):
    @check_args(['task_working_detail_list', 'report_time', 'task_working_id'])
    @unblock
    def post(self):
        '''
        获取系统配置的项目阶段配置

        :return:
        '''
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }
        task_working_detail_list = to_list(data.get('task_working_detail_list'))
        report_time = to_string(data.get('report_time'))[:10]
        task_working_id = data.get('task_working_id')

        tp = WorkingDailyManage(user)
        res = tp.project_task_working_daily_add(report_time, task_working_detail_list, is_edit=1, task_working_id=task_working_id)
        return res


class ProjectTaskWorkingDailyGetList(BaseApiHandler):
    @check_args(['page', 'size'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        page = to_int(data.get('page'))
        size = to_int(data.get('size'))
        report_time = to_string(data.get('report_time'))
        if report_time:
            report_time = report_time[:10]

        is_my = to_int(data.get('is_my'), -1)
        '''
        is_my=1  我的
        is_my=2  销售、行政、财务
        is_my=-1 全部
        '''
        select_user_name = data.get('select_user_name')

        tp = WorkingDailyManage(user)
        res = tp.project_task_working_daily_get_list(select_user_name, page, size, report_time, is_my)
        return res


class ProjectTaskWorkingDailyGetFirst(BaseApiHandler):
    @check_args(['report_id'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        report_id = data.get('report_id')

        tp = WorkingDailyManage(user)
        res = tp.project_task_working_daily_get_first(report_id)
        return res


class ProjectTaskWorkingDailyExport(BaseApiHandler):
    @check_args(['start_time', 'end_time'])
    @unblock
    def post(self):
        data = self.get_post_data()

        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            wechat_user_id = to_int(data.get('bind_user_id'))
            wechat_user_name = data.get('bind_user_name')
            user = {
                'user_id': wechat_user_id,
                'user_name': wechat_user_name
            }

        start_time = to_string(to_datetime(data.get('start_time')))[:10]
        end_time = to_string(to_datetime(data.get('end_time')))[:10]

        tp = WorkingDailyManage(user)
        res = tp.project_task_working_daily_export(start_time, end_time)
        return res


class ProjectTaskWorkingDailyGetDay(BaseApiHandler):
    @check_args(['report_time', 'work_type'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        report_time = to_string(data.get('report_time'))[:10]
        work_type = to_int(data.get('work_type'))
        '''
        work_type=1 项目
        work_type=2 销售、行政、财务
        '''

        tp = WorkingDailyManage(user)
        res = tp.project_task_working_daily_get_day(report_time, work_type)
        return res


class ProjectTaskWorkingDailyGetUser(BaseApiHandler):
    @check_args(['start_report_time', 'end_report_time', 'query_user_name'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        start_report_time = to_string(data.get('start_report_time'))[:10]
        end_report_time = to_string(data.get('end_report_time'))[:10]
        work_type = to_int(data.get('work_type'))
        '''
        work_type=1 项目
        work_type=2 销售、行政、财务
        '''
        query_user_name = data.get('query_user_name')

        tp = WorkingDailyManage(user)
        res = tp.project_task_working_daily_get_user(start_report_time, end_report_time, work_type, query_user_name)
        return res


class ProjectTaskWorkingPlanAdd(BaseApiHandler):
    @check_args(['chandao_project_id', 'plan_list', 'chandao_project_name'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        chandao_project_id = to_int(data.get('chandao_project_id'))
        chandao_project_name = data.get('chandao_project_name')
        plan_time = data.get('plan_time')
        plan_list = to_list(data.get('plan_list'))
        file = data.get('file')

        tp = WorkingDailyManage(user)
        res = tp.project_task_working_plan_add(chandao_project_id, chandao_project_name, plan_time, plan_list, file)
        return res


class ProjectTaskWorkingPlanGetList(BaseApiHandler):
    @check_args(['page', 'size', 'chandao_project_id'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        page = to_int(data.get('page'))
        size = to_int(data.get('size'))
        chandao_project_id = data.get('chandao_project_id')

        tp = WorkingDailyManage(user)
        res = tp.project_task_working_plan_get_list(page, size, chandao_project_id)
        return res


class ProjectTaskWorkingPlanGetFirst(BaseApiHandler):
    @check_args(['plan_id'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        plan_id = to_int(data.get('plan_id'))

        tp = WorkingDailyManage(user)
        res = tp.project_task_working_plan_get_first(plan_id)
        return res


class ProjectTaskWorkingPlanUpdate(BaseApiHandler):
    @check_args(['plan_list', 'plan_time', 'plan_id', 'chandao_project_id', 'chandao_project_name'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        plan_time = data.get('plan_time')
        plan_list = to_list(data.get('plan_list'))
        plan_id = data.get('plan_id')
        chandao_project_id = data.get('chandao_project_id')
        chandao_project_name = data.get('chandao_project_name')
        file = data.get('file')

        tp = WorkingDailyManage(user)
        res = tp.project_task_working_plan_update(chandao_project_id, chandao_project_name, plan_time, plan_list, plan_id, file)
        return res


class ProjectTaskWorkingPlanUpdateOther(BaseApiHandler):
    @check_args(['plan_id'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        plan_id = data.get('plan_id')

        weekly_wrap_up = data.get('weekly_wrap_up')
        party_a_comments = data.get('party_a_comments')
        if party_a_comments:
            party_a_comments_wechat_user_id = data.get('party_a_comments_wechat_user_id')
            party_a_comments_wechat_user_name = data.get('party_a_comments_wechat_user_name')
        else:
            party_a_comments_wechat_user_id = 0
            party_a_comments_wechat_user_name = ''
        evaluation_response = data.get('evaluation_response')

        tp = WorkingDailyManage(user)
        res = tp.project_task_working_plan_update_other(plan_id, weekly_wrap_up, party_a_comments, evaluation_response, party_a_comments_wechat_user_id,
                                                        party_a_comments_wechat_user_name)
        return res


class ProjectTaskWorkingDailyGetListProject(BaseApiHandler):
    @check_args(['project_id', 'size', 'page'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        project_id = data.get('project_id')
        page = to_int(data.get('page'))
        size = to_int(data.get('size'))
        report_time = to_string(data.get('report_time'))

        tp = WorkingDailyManage(user)
        res = tp.project_task_working_daily_get_list_project(project_id, page, size, report_time)
        return res


class ProjectTaskWorkingAudits(BaseApiHandler):
    @check_args(['detail_ids', 'chandao_project_id'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }
        chandao_project_id = data.get('chandao_project_id')
        detail_ids = to_list(data.get('detail_ids'))

        tp = WorkingDailyManage(user)
        res = tp.project_task_working_audits(chandao_project_id, detail_ids)
        return res


class ProjectTaskWorkingUnsubmitUserList(BaseApiHandler):
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        report_time = data.get('report_time')

        tp = WorkingDailyManage(user)
        res = tp.project_task_working_unsubmit_user_list(report_time)
        return res


class ProjectPhasesAdd(BaseApiHandler):
    @check_args(['chandao_project_id', 'phase_name', 'construction_content', 'planned_completion_time', 'detail_list'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        chandao_project_id = data.get('chandao_project_id')
        phase_name = data.get('phase_name')
        construction_content = data.get('construction_content')
        planned_completion_time = data.get('planned_completion_time')
        detail_list = to_list(data.get('detail_list'))
        contractor_days = data.get('contractor_days')
        budget_days = data.get('budget_days')
        payment_this_period = data.get('payment_this_period')  # 本期回款

        tp = WorkingDailyManage(user)
        res = tp.project_phases_add(chandao_project_id, phase_name, construction_content, planned_completion_time, detail_list, contractor_days, budget_days, payment_this_period)
        return res


class ProjectPhasesUpdate(BaseApiHandler):
    @check_args(['chandao_project_id', 'phase_name', 'construction_content', 'planned_completion_time', 'detail_list'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        chandao_project_id = data.get('chandao_project_id')
        phase_name = data.get('phase_name')
        construction_content = data.get('construction_content')
        planned_completion_time = data.get('planned_completion_time')
        detail_list = to_list(data.get('detail_list'))
        contractor_days = data.get('contractor_days')
        budget_days = data.get('budget_days')
        payment_this_period = data.get('payment_this_period')

        tp = WorkingDailyManage(user)
        res = tp.project_phases_update(chandao_project_id, phase_name, construction_content, planned_completion_time, detail_list, contractor_days, budget_days,
                                       payment_this_period)
        return res


class ProjectPhasesGetList(BaseApiHandler):
    @check_args(['page', 'size', 'chandao_project_id'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        chandao_project_id = to_int(data.get('chandao_project_id'))
        page = to_int(data.get('page'))
        size = to_int(data.get('size'))

        tp = WorkingDailyManage(user)
        res = tp.project_phases_get_list(chandao_project_id, page, size)
        return res


class ProjectPhasesGetFirst(BaseApiHandler):
    @check_args(['phases_id'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        phases_id = to_int(data.get('phases_id'))

        tp = WorkingDailyManage(user)
        res = tp.project_phases_get_first(phases_id)
        return res


class ProjectPhasesConfirmed(BaseApiHandler):
    @check_args(['phases_id', 'is_confirmed'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        phases_id = to_int(data.get('phases_id'))
        is_confirmed = data.get('is_confirmed')

        tp = WorkingDailyManage(user)
        res = tp.project_phases_confirmed(phases_id, is_confirmed)
        return res


class ProjectWeekPlanGetUserList(BaseApiHandler):
    @check_args(['is_head_party_a', 'page', 'size'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        is_head_party_a = to_int(data.get('is_head_party_a'))
        '''
        当 customer_server_user_id:有值是 is_head_party_a = 0， bind_user_id传customer_server_user_id，bind_user_name 传 bind_user_nam
        当 customer_server_user_id:无值是 is_head_party_a = 1 bind_user_id传wechat_user_id，bind_user_name 传 nike_name
        如果既有 customer_server_user_id 又有 wechat_user_id 传 is_head_party_a=0，bind_user_id传customer_server_user_id，bind_user_name 传 bind_user_name
        '''

        page = to_int(data.get('page'))
        size = to_int(data.get('size'))

        tp = WorkingDailyManage(user)
        res = tp.project_week_get_user_list(is_head_party_a, page, size)
        return res


class ProjectTaskWeekDailyAdd(BaseApiHandler):
    @check_args(['task_working_detail_list', 'report_week_start_time', 'report_week_end_time'])
    @unblock
    def post(self):
        '''
        获取系统配置的项目阶段配置

        :return:
        '''
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }
        report_week_start_time = data.get('report_week_start_time')
        report_week_end_time = data.get('report_week_end_time')
        next_plan = data.get('next_plan')
        week_review = data.get('week_review')

        task_working_detail_list = to_list(data.get('task_working_detail_list'))
        task_working_next_detail_list = to_list(data.get('task_working_next_detail_list'))

        tp = WorkingDailyManage(user)
        res = tp.project_task_week_daily_add(report_week_start_time=report_week_start_time,
                                             report_week_end_time=report_week_end_time,
                                             task_working_detail_list=task_working_detail_list,
                                             next_plan=next_plan,
                                             week_review=week_review,
                                             task_working_next_detail_list=task_working_next_detail_list)
        return res


class ProjectTaskWeekDailySelect(BaseApiHandler):
    @check_args(['report_week_start_time', 'report_week_end_time'])
    @unblock
    def post(self):
        '''
        获取系统配置的项目阶段配置

        :return:
        '''
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }
        report_week_start_time = data.get('report_week_start_time')
        report_week_end_time = data.get('report_week_end_time')

        tp = WorkingDailyManage(user)
        res = tp.project_task_week_daily_select(report_week_start_time, report_week_end_time)
        return res


class ProjectTaskWeekDailyGetList(BaseApiHandler):
    @check_args(['page', 'size'])
    @unblock
    def post(self):
        '''
        获取系统配置的项目阶段配置

        :return:
        '''
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }
        page = to_int(data.get('page'))
        size = to_int(data.get('size'))
        user_name = data.get('user_name')

        is_my = to_int(data.get('is_my'), -1)
        '''
        is_my=1  我的
        is_my=2  销售
        is_my=-1 全部
        '''

        tp = WorkingDailyManage(user)
        res = tp.project_task_week_daily_get_list(page, size, user_name, is_my)
        return res


class ProjectTaskWeekDailyGetFirst(BaseApiHandler):
    @check_args(['task_week_id'])
    @unblock
    def post(self):
        '''
        获取系统配置的项目阶段配置

        :return:
        '''
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }
        task_week_id = to_int(data.get('task_week_id'))

        tp = WorkingDailyManage(user)
        res = tp.project_task_week_daily_get_first(task_week_id)
        return res


class ProjectTaskWeekDailyUpdate(BaseApiHandler):
    @check_args(['task_week_id'])
    @unblock
    def post(self):
        '''
        获取系统配置的项目阶段配置

        :return:
        '''
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }
        task_week_id = to_int(data.get('task_week_id'))
        report_week_start_time = data.get('report_week_start_time')
        report_week_end_time = data.get('report_week_end_time')
        next_plan = data.get('next_plan')
        week_review = data.get('week_review')

        task_working_detail_list = to_list(data.get('task_working_detail_list'))
        task_working_next_detail_list = to_list(data.get('task_working_next_detail_list'))

        tp = WorkingDailyManage(user)
        res = tp.project_task_week_daily_add(report_week_start_time=report_week_start_time,
                                             report_week_end_time=report_week_end_time,
                                             task_working_detail_list=task_working_detail_list,
                                             next_plan=next_plan,
                                             week_review=week_review,
                                             is_edit=1,
                                             task_week_id=task_week_id,
                                             task_working_next_detail_list=task_working_next_detail_list)
        return res


class ProjectTaskWeekDailySelectWorkingAll(BaseApiHandler):
    @check_args(['report_week_start_time', 'report_week_end_time'])
    @unblock
    def post(self):
        '''
        获取系统配置的项目阶段配置

        :return:
        '''
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }
        report_week_start_time = data.get('report_week_start_time')
        report_week_end_time = data.get('report_week_end_time')
        report_user_id = to_int(data.get('report_user_id'))
        # if report_user_id != to_int(bind_user_id):
        #     return failure(msg='不能同步其他人的本周工时数据！')

        tp = WorkingDailyManage(user)
        res = tp.project_task_week_daily_select_wroking_all(report_week_start_time=report_week_start_time,
                                                            report_week_end_time=report_week_end_time)
        return res


class ProjectTaskWeekDailyEvaluations(BaseApiHandler):
    @check_args(['task_week_id', 'evaluations'])
    @unblock
    def post(self):
        '''
        获取系统配置的项目阶段配置

        :return:
        '''
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        evaluations = data.get('evaluations')
        task_week_id = data.get('task_week_id')

        tp = WorkingDailyManage(user)
        res = tp.project_task_week_daily_evaluations(evaluations=evaluations, task_week_id=task_week_id)
        return res


class ProjectTaskWeekDailyGetWeek(BaseApiHandler):
    @check_args(['report_week_start_time', 'report_week_end_time', 'work_type'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)  # 请求来源 0-PC端，1-小程序端

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='请求参数错误')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        report_week_start_time = to_string(data.get('report_week_start_time'))[:10]
        report_week_end_time = to_string(data.get('report_week_end_time'))[:10]
        work_type = to_int(data.get('work_type'))
        '''
        work_type=1 项目
        work_type=2 销售、行政、财务
        '''

        tp = WorkingDailyManage(user)
        res = tp.project_task_week_daily_get_week(report_week_start_time, report_week_end_time, work_type)
        return res
