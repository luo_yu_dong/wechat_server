#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: work_server_api.py
@time: 2020/10/22 17:23
@desc:
'''

from base_api import BaseApiHandler, unblock
from framework.authority.authorities import check_args
from framework.msg import failure
from framework.utilities import to_int, to_list, to_decimal
from services.work_server_manage import WorkServerManage


class WorkServerUserListHandler(BaseApiHandler):
    @unblock
    def post(self):
        data = self.get_post_data()
        select_name = data.get('select_name')
        page = to_int(data.get('page'), 0)
        size = to_int(data.get('size'), 20)

        user = self.get_usr_dic()

        tp = WorkServerManage(user)
        res = tp.work_server_user_list(select_name, page, size)
        return res


class WorkServerUserBindHandler(BaseApiHandler):
    @check_args(['wechat_user_id', 'user_id'])
    @unblock
    def post(self):
        data = self.get_post_data()

        user_id = data.get('user_id')
        wechat_user_id = data.get('wechat_user_id')

        user = self.get_usr_dic()

        tp = WorkServerManage(user)
        res = tp.work_server_user_bind(user_id, wechat_user_id)
        return res


class WorkServerFormGetListHandler(BaseApiHandler):
    @unblock
    def post(self):
        data = self.get_post_data()

        page = to_int(data.get('page'), 0)
        size = to_int(data.get('size'), 20)
        select_type = to_int(data.get('select_type'), -1)
        select_business_name = data.get('select_business_name')
        select_is_reply = to_int(data.get('select_is_reply'), -1)
        if not data.get('select_score') or to_int(data.get('select_score')) == -1:
            select_score = -1
        else:
            select_score = to_decimal(data.get('select_score'))
        select_user_id = to_int(data.get('select_user_id'), -1)
        usr = self.get_usr_dic()
        select_comment = data.get('select_comment')  # 内容模糊搜索

        time_type = to_int(data.get('time_type'), -1)  # -1,全部，1-创建时间，2-处理时间
        start_time = data.get('start_time')
        end_time = data.get('endt_time')
        is_server_time = to_int(data.get('is_server_time'), -1)

        user_id = usr['user_id']
        is_admin = usr['is_admin']

        tp = WorkServerManage(usr)
        res = tp.form_get_list(user_id, is_admin, page, size, select_type, select_business_name, select_is_reply, select_score, select_user_id, select_comment, time_type,
                               start_time, end_time, is_server_time)
        return res


class WorkServerFormGetListExportHandler(BaseApiHandler):
    # @check_args(['start_time', 'end_time'])
    @unblock
    def post(self):
        # ------------------------------------------- 下载文件 -------------------------------------------
        data = self.get_post_data()

        page = 0
        size = 9999999
        select_type = to_int(data.get('select_type'), -1)
        select_business_name = data.get('select_business_name')
        select_is_reply = to_int(data.get('select_is_reply'), -1)
        if not data.get('select_score') or to_int(data.get('select_score')) == -1:
            select_score = -1
        else:
            select_score = to_decimal(data.get('select_score'))
        select_user_id = to_int(data.get('select_user_id'), -1)
        usr = self.get_usr_dic()
        select_comment = data.get('select_comment')  # 内容模糊搜索

        time_type = to_int(data.get('time_type'), -1)  # -1,全部，1-创建时间，2-处理时间
        start_time = data.get('start_time')
        end_time = data.get('endt_time')
        is_server_time = to_int(data.get('is_server_time'), -1)

        user_id = usr['user_id']
        is_admin = usr['is_admin']

        tp = WorkServerManage(usr)
        res = tp.form_export(user_id, is_admin, page, size, select_type, select_business_name, select_is_reply, select_score, select_user_id, select_comment, time_type,
                             start_time, end_time, is_server_time)

        return res


class WorkServerFormAllocationBachHandler(BaseApiHandler):
    @check_args(['item_id_list', 'reply_user_id', 'reply_user_name'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)
        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='操作失败')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        item_id_list = to_list(data.get('item_id_list'))
        reply_user_id = data.get('reply_user_id')
        reply_user_name = data.get('reply_user_name')

        tp = WorkServerManage(user)
        res = tp.form_allocation_bach(item_id_list, reply_user_id, reply_user_name)
        return res


class WorkServerFormGetFirstHandler(BaseApiHandler):
    @check_args(['item_id'])
    @unblock
    def post(self):
        data = self.get_post_data()

        item_id = data.get('item_id')

        user = self.get_usr_dic()

        tp = WorkServerManage(user)
        res = tp.form_get_first(item_id)
        return res


class WorkServerFormReplyHandler(BaseApiHandler):
    @check_args(['item_id', 'reply_result'])
    @unblock
    def post(self):
        data = self.get_post_data()
        come_from = to_int(data.get('come_from'), 0)

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='操作失败')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        item_id = data.get('item_id')
        reply_result = data.get('reply_result')
        reply_imgs = data.get('reply_imgs')

        tp = WorkServerManage(user)
        res = tp.form_reply(item_id, reply_result, reply_imgs)

        return res


class WorkServerFormReplyDescHandler(BaseApiHandler):
    @check_args(['item_id', 'reply_desc'])
    @unblock
    def post(self):
        data = self.get_post_data()
        item_id = data.get('item_id')
        reply_desc = data.get('reply_desc')
        come_from = to_int(data.get('come_from'), 0)

        if not come_from:
            user = self.get_usr_dic()
        else:
            bind_user_name = data.get('bind_user_name')
            bind_user_id = data.get('bind_user_id')
            if not bind_user_id:
                return failure(msg='操作失败')
            user = {
                'user_id': bind_user_id,
                'user_name': bind_user_name
            }

        tp = WorkServerManage(user)
        res = tp.form_reply_desc(item_id, reply_desc, come_from)

        return res


class WorkServerFormDeleteBachHandler(BaseApiHandler):
    @check_args(['item_id_list'])
    @unblock
    def post(self):
        data = self.get_post_data()
        user = self.get_usr_dic()
        item_id_list = to_list(data.get('item_id_list'))

        is_admin = user['is_admin']
        if not is_admin:
            return failure(msg='您不是管理员无权进行删除操作！')

        tp = WorkServerManage(user)
        res = tp.form_delete_bach(item_id_list)

        return res
