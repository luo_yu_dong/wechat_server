#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: thirdparty_msg.py
@time: 2020/7/29 12:14
@desc:
'''
from enums.enum_thirdparty_out_type import thirdparty_out_get_stage_name_by_value


class ThirdpartyResponseMessage():
    def __init__(self, code, msg, data):
        self.code = code
        self.msg = msg
        self.data = data

    def is_ok(self):
        return self.code is not None and self.code == 10000

    def responseMsg(self):
        pass


class thirdparty_ok(ThirdpartyResponseMessage):
    def __init__(self, code=10000, msg='', data={}):
        super().__init__(code, msg, data)

    def responseMsg(self):
        return {
            'msg': thirdparty_out_get_stage_name_by_value(10000),
            'data': self.data,
            'code': 10000
        }

    def __str__(self):
        return str(self.responseMsg())


class thirdparty_failure(ThirdpartyResponseMessage):
    def __init__(self, code, msg='', data={}):
        super().__init__(code, msg, data)

    def responseMsg(self):
        return {
            'msg': thirdparty_out_get_stage_name_by_value(self.code),
            'data': {},
            'code': self.code
        }

    def __str__(self):
        return str(self.responseMsg())
