# coding=utf-8
'''
author:董新强
createdate:2016年11月22日
description:正则表达式pattern
'''
import re
from decimal import Decimal

from framework.utilities import get_len, to_int


class RegexPatterns():
    __email_pattern = re.compile(r'^[A-Za-z0-9]+([-_.][A-Za-z0-9]+)*@([A-Za-z0-9]+[-.])+[A-Za-z0-9]{2,5}$')
    __mobile_pattern = re.compile(r'1\d{10}')
    __sns_image_name_pattern = re.compile(
        r'(\d{1,5}-){0,1}[a-zA-z0-9]{8}-[a-zA-z0-9]{4}-[a-zA-z0-9]{4}-[a-zA-z0-9]{4}-[a-zA-z0-9]{12}\.\w{3,4}')
    __speak_params_pattern = re.compile(r'#.*?#')
    __chs = re.compile(r'^[\u4E00-\u9FA5]+$')

    __time_pattern = re.compile('\d{2}:\d{2}:\d{2}')  # 获取时间
    __positive_integer_pattern = re.compile('\d+')  # 整整数
    __float_pattern__ = re.compile('-?\d+(\.?\d+)?\d*')

    @classmethod
    def is_email(cls, email):
        '''
        是否为email
        :param str:带简称
        :type str:
        :return:
        :rtype:
        '''
        if not email:
            return None

        res = cls.__email_pattern.match(email)
        if not res:
            return None
        return res.group()

    @classmethod
    def is_mobile(cls, mobile, int_mobile=False):
        '''
        是否为手机号码
        :param str:带检查字符串
        :type str:
        :return:电话号码
        :rtype:str
        '''
        if not mobile:
            return None

        res = cls.__mobile_pattern.match(str(mobile))
        if not res:
            return None
        return res.group() if not int_mobile else to_int(res.group(), None)

    @classmethod
    def is_sns_image(cls, image_name):
        '''
        是否是sns或者群头像的图片
        :param image_name:
        :type image_name:
        :return:
        :rtype:
        '''
        if not image_name:
            return None

        res = cls.__sns_image_name_pattern.match(image_name)
        if not res:
            return None
        return res.group()

    @classmethod
    def get_speak_params(cls, content):
        if not content:
            return []

        res = cls.__speak_params_pattern.findall(content)
        if res:
            return set(res)

        return None

    @classmethod
    def is_chs(cls, content):
        '''
        是否是中文
        '''
        if not content:
            return content

        res = cls.__chs.match(content)
        if not res:
            return None
        return res.group()

    @classmethod
    def get_time(cls, time_str):
        res = cls.__time_pattern.search(time_str)
        if not res:
            return '00:00:00'
        return res.group()

    @classmethod
    def get_date(cls, s):
        if not s:
            return None

        n = cls.__positive_integer_pattern.findall(s)

        if get_len(n) == 3:  # 年月日
            return '-'.join(n) + ' 00:00:00'
        if get_len(n) == 4:
            return '-'.join(n[:3]) + ' ' + n[3] + ':00:00'
        if get_len(n) == 5:
            return '-'.join(n[:3]) + ' %s:%s:00' % (str(n[3]), str(n[4]))
        if get_len(n) == 6:
            return '-'.join(n[:3]) + ' %s:%s:%s' % (str(n[3]), str(n[4]), str(n[5]))

        return None

    @classmethod
    def img_code(self, code, length=4):
        '''
        是否是验证码
        :param code: 图形验证码
        :type code:str
        :param length:图形验证码长度(默认4位)
        :type length: int
        :return:
        :rtype:
        '''
        if not length:
            return None
        code = code.strip(' ')
        if len(code) != length:
            return None
        return code.lower()

    @classmethod
    def shot_msg_code(self, code, length=4):
        '''
        是否是短信
        :param code: 图形验证码
        :type code:str
        :param length:短信验证码长度(默认4位)
        :type length: int
        :return:
        :rtype:
        '''
        if not length:
            return None

        code = code.strip(' ')
        if len(code) != length:
            return None

        code = to_int(code, 0)
        if code < (10 ** (length - 1)) or code > (10 ** length - 1):
            return None
        return code

    @classmethod
    def is_num(cls, f):
        '''
        是否是数字
        :param f:
        :type f:
        :return:
        :rtype:
        '''
        try:
            if isinstance(f, (int, float, Decimal)):
                return True
            if not isinstance(f, str):
                return False
            res = cls.__float_pattern__.match(f)
            if not res:
                return False
            return True
        except:
            return False


if __name__ == '__main__':
    print(RegexPatterns.is_num(22))
