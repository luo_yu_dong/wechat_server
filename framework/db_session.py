# coding=utf-8
'''
author:lyd
createdate:
description:
'''
import os
import time
import traceback
from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session

import setting
from enums.enum_db import EnumDb, EnumBind
from framework.log_controller import log as sys_log
from setting import MONITOR_DB_ACCESS_TIME

ECHO_SQL_DETAILS = setting.is_debug
has_slave = hasattr(setting, 'dbsetting_slave')


class DbSession():

    @staticmethod
    def get_engines_dic():
        binds = [EnumBind.master.value]

        # 如果没有只读实例配置，就不创建只读实例引擎

        if has_slave:
            binds.append(EnumBind.slave.value)

        def get_bind_setting(bind, db_type):
            if has_slave:
                st = setting.dbsetting if bind == EnumBind.master else setting.dbsetting_slave

            else:
                st = setting.dbsetting

            return st[db_type]

        res = {
            bind:
                {
                    EnumDb.main.value: create_engine(get_bind_setting(bind, EnumDb.main.value), echo=ECHO_SQL_DETAILS, pool_recycle=3600, pool_size=100),
                    EnumDb.chandao.value: create_engine(get_bind_setting(bind, EnumDb.chandao.value), echo=ECHO_SQL_DETAILS, pool_recycle=3600, pool_size=20)
                } for bind in binds}
        return res

    _DB_ENGINES_DICT = get_engines_dic.__func__()

    @classmethod
    def get_session(cls, db_type=EnumDb.main, bind=EnumBind.master):
        assert isinstance(db_type, EnumDb)
        assert isinstance(bind, EnumBind)
        cur_bind = bind
        if not has_slave:
            # 如果没有只读实例，直接走读写实例
            cur_bind = EnumBind.master
        return sessionmaker(cls._DB_ENGINES_DICT[cur_bind.value][db_type.value])

    @classmethod
    @contextmanager
    def create(cls, db_type: EnumDb = EnumDb.main, bind: EnumBind = EnumBind.master) -> Session:
        if not isinstance(db_type, EnumDb):
            raise Exception('connection_key非法!')

        if not isinstance(bind, EnumBind):
            raise Exception('connection_bind非法!')

        session = cls.get_session(db_type, bind)

        db = session()
        exception = None

        if MONITOR_DB_ACCESS_TIME:  # 如果需要监控数据库范围时间
            st = time.time()

        try:
            yield db
            db.commit()
        except Exception as e:
            exception = e
            # ----回滚----------------------1
            try:
                db.rollback()
            except Exception as e:
                print(e)
        finally:
            if db is not None:
                db.close()

        # -----------------------监控数据库时间--------------------------------------------------------------
        if MONITOR_DB_ACCESS_TIME:  # 如果需要监控数据库范围时间
            try:
                span = time.time() - st
                if span > 1:  # 如果执行时间超过1秒,就进行记录
                    stack = traceback.extract_stack()[-3]
                    info = '文件=>' + str(os.path.basename(stack[0])) + '   方法=>' + str(stack[2])
                    sys_log('DB访问耗时监控', info, ' 耗时(秒)=>' + str(round(span, 2)), 9)
            except:
                pass

        # ----------重新抛出异常----------
        if exception:
            raise exception

    @classmethod
    @contextmanager
    def twophase_session(cls, table_dbtype_dict):
        '''
        两段式提交
        :param table_dbtype_dict: {类名:数据库类型#枚举}
        :type table_dbtype_dict: dict
        :return:
        :rtype:
        '''
        # assert isinstance(table_dbtype_dict, dict) and len(table_dbtype_dict) > 1

        binds = {}
        for tb, dbtype in table_dbtype_dict.items():
            binds[tb] = cls._DB_ENGINES_DICT[EnumBind.master.value][dbtype.value]

        Session = sessionmaker(twophase=True)
        Session.configure(binds=binds)
        db = Session()

        exception = None

        if MONITOR_DB_ACCESS_TIME:  # 如果需要监控数据库范围时间
            st = time.time()

        try:
            yield db
            db.commit()
        except Exception as e:
            if setting.is_debug:
                print('-----------commit----------to rollback-----------')
                print(e)
            exception = e
            # ----回滚----------------------1
            try:
                db.rollback()
            except Exception as e:
                print(e)
        finally:
            if db is not None:
                db.close()

        # -----------------------监控数据库时间--------------------------------------------------------------
        if MONITOR_DB_ACCESS_TIME:  # 如果需要监控数据库范围时间
            try:
                span = time.time() - st
                if span > 1:  # 如果执行时间超过1秒,就进行记录
                    stack = traceback.extract_stack()[-3]
                    info = '文件=>' + str(os.path.basename(stack[0])) + '   方法=>' + str(stack[2])
                    sys_log('DB访问耗时监控', info, ' 耗时(秒)=>' + str(round(span, 2)), 9)
            except:
                pass

        # ----------重新抛出异常----------
        if exception:
            raise exception
