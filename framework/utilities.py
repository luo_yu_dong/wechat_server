# coding=utf-8
"""
该文件提供系统相关的加解密方法
"""
import calendar
import enum
import functools
import hashlib
import json
import os
import re
import time as timepy
from datetime import datetime, timedelta, date
from decimal import Decimal
from random import sample as randomsample

import xmltodict
from pypinyin import lazy_pinyin

from enums.enum_request_type import EnumRequestType
from framework.msg import ResponseMessage
from setting import is_debug

PRECISION_MODEL = re.compile('^0\.0+$')
DATE_PATTERN = re.compile(r'\d{4}[-/年]\d{1,2}[-/月]\d{1,2}日?')
INT_PATTERN = re.compile(r'^-?\d+')


def get_quanpin(hanzi):
    result = ''
    res = lazy_pinyin(hanzi)
    for v in res:
        result += v
    return result


def __util_try_except(fail_value=None):
    def innertrycatch(function):
        @functools.wraps(function)
        def warp(*args, **kwargs):
            try:
                return function(*args, **kwargs)
            except:
                return fail_value

        return warp

    return innertrycatch


def obj_to_dict(obj):
    """
    将对象的属性转换为字典
    :param obj:
    :type obj:
    :return:
    :rtype:
    """
    if not obj or not isinstance(obj, object):
        return {}
    return obj.__dict__


def dic_to_obj(dic):
    """
    将字典转换为对象
    :param dic:
    :type dic:
    :return:
    :rtype:
    """
    top = type('new', (object,), dic)
    seqs = tuple, list, set, frozenset
    for i, j in dic.items():
        if isinstance(j, dict):
            setattr(top, i, dic_to_obj(j))
        elif isinstance(j, seqs):
            setattr(top, i,
                    type(j)(dic_to_obj(sj) if isinstance(sj, dict) else sj for sj in j))
        else:
            setattr(top, i, j)
    return top


private_keys = "123oAGaY98kL6gEmjksdjkfj8G*&**(*TP1o/Vo=33"
"""
	控制来自app的权限
	来自app的权限和si_admin模块中的不一样,因为si_api使用的是回调
	在此独立于si_admin模块
	如果不用回调,authorize()已经完全可以搞定了
	日期:2015-08-05
	id+expire=>token
	(token, gen_identifier_token(id))=>accesstoken
"""


def gen_token(name, expires_in):
    """
    根据用户ID和exprie生成token
    id+expire=>token
    (token, gen_identifier_token(id))=>access_token
    :param user_id:
    :type user_id:
    :param expires_in:
    :type expires_in:
    :return:
    :rtype:
    """
    m = hashlib.md5(to_string(name).encode("utf8"))
    m.update(private_keys.encode("utf8"))
    m.update(str(expires_in).encode("utf8"))
    return m.hexdigest()


def to_md5(string):
    m = hashlib.md5(to_string(string).encode("utf8"))
    m.update(b'88&IKKadj12')
    return m.hexdigest()


def gen_encrypt_password(phone, password):
    """
    获取加密后的密码
    """
    m = hashlib.md5(password.encode("utf8"))
    m.update(private_keys.encode("utf8"))
    m.update(to_string(phone).encode("utf8"))
    return m.hexdigest()


def gen_identifier_token(phone, pwd):
    """
    生成identifier_token
    id+expire=>token
    (token, gen_identifier_token(id))=>access_token
    """
    if ((not phone) or (not pwd)):
        raise Exception('非法操作')
    # 检验 identifier
    encyptStr = to_string(phone) + to_string(pwd)
    m = hashlib.sha1(encyptStr.encode("utf8"))
    m.update(private_keys.encode("utf8"))
    return m.hexdigest()


"""
返回accesstoken,由原accesstoken和后identifiertoken组合
加密方式有待优化
"""


def gen_access_token(expire, token, name):
    if not expire or not token or not name:
        raise Exception('非法操作,参数不全!')
    ident = gen_identifier_token(expire, name)
    return ident[0:5] + token + "|" + ident[5:]


def gen_share_token(dynamic_id):
    """
    生成分享token
    """
    if not dynamic_id:
        raise Exception('非法操作')
    # 检验 identifier
    encyptStr = to_string(dynamic_id) + to_string(current_timestamp())
    m = hashlib.sha1(encyptStr.encode("utf8"))
    m.update(private_keys.encode("utf8"))
    ident = m.hexdigest()
    return ident[3:]


def gen_expire_in():
    return current_timestamp() + 604800  # 14天 即两周


def docode_access_token(access_token):
    """
    将有access_token进行还原
    """
    if not access_token:
        raise Exception('非法操作')

    ls = access_token.split('|')
    if not ls or get_len(ls) != 2:
        raise Exception('非法操作')

    tokens = {}
    tokens["acc"] = ls[0][5:]
    tokens["ident"] = ls[0][0:5] + ls[1]
    return tokens


# ------------------------------------------------------------------------------------------------
def gen_reques_from_type(phone, is_from_web=False, request_type=EnumRequestType.service):
    '''
    加密一个字段,用来标识是请求来自 web还是来自app

    当is_from_web=False时:
      app_type=0时,说明来自服务app  (默认)
      app_type=1时,来自业主app
    '''
    if isinstance(request_type, EnumRequestType):
        request_type = request_type.value

    if not phone:
        raise Exception('非法操作')
    if is_from_web:  # 来自web
        encyptStr = to_string("fsd@(!#&(*(!@;a" + to_string(phone))
    else:
        if request_type == EnumRequestType.service.value:  # 来自服务app
            encyptStr = to_string("app@(#!&_=(@;app" + to_string(phone))
        elif request_type == EnumRequestType.public.value:  # 来自业主app
            encyptStr = to_string("app@(#)^!(*^(@;public" + to_string(phone))
        else:  # 微信
            encyptStr = to_string("wec@(#!!-*^::~;public" + to_string(phone))

    return __gen_request_from(encyptStr)


def __gen_request_from(encyptStr):
    m = hashlib.sha1(encyptStr.encode("utf8"))
    m.update(private_keys.encode("utf8"))
    ident = m.hexdigest()
    return ident[2:7] + ident[9:15]


@__util_try_except(None)
def get_request_from(phone, encyptStr):
    '''
    获取请求来源
    '''
    if not phone or not encyptStr:
        return None

    if encyptStr == __gen_request_from(to_string("app@(#!&_=(@;app" + to_string(phone))):
        return EnumRequestType.service.value

    if encyptStr == __gen_request_from(to_string("fsd@(!#&(*(!@;a" + to_string(phone))):
        return EnumRequestType.web.value

    if encyptStr == __gen_request_from(to_string("app@(#)^!(*^(@;public" + to_string(phone))):
        return EnumRequestType.public.value

    if encyptStr == __gen_request_from(to_string("wec@(#!!-*^::~;public" + to_string(phone))):
        return EnumRequestType.wechat.value

    return None


# -------------------------------------------------------------------------------------------------------------

def to_string(obj, failure_replace_value=None, strip_all_blank=False):
    '''
    将对象转换为字符串
    :param obj:带转换的类型
    :type obj:
    :param failure_replace_value:转换失败时代替的类型
    :type failure_replace_value: str
    :param strip_all_blank:是否移除所有空格 ,默认不移除
    :type strip_all_blank: bool
    :return:返回值
    :rtype: str
    '''
    try:
        if obj is None:
            return failure_replace_value

        if isinstance(obj, str):
            new_str = obj
        elif isinstance(obj, bytes):
            new_str = obj.decode(encoding='utf-8')
        elif isinstance(obj, (list, dict)):
            new_str = json.dumps(obj, ensure_ascii=False)
        else:
            new_str = str(obj)

        if strip_all_blank and new_str:
            return new_str.replace(' ', '')

        return new_str
    except:
        return failure_replace_value


def to_byte(str_or_byte):
    '''
    将str转换为bytes
    :param str_or_byte:
    :type str_or_byte:
    :return:
    :rtype:
    '''
    if not str_or_byte:
        return None
    if isinstance(str_or_byte, bytes):
        return str_or_byte
    if isinstance(str_or_byte, str):
        return str_or_byte.encode(encoding='utf-8')
    return None


def to_dict(str_or_byte, replace_obj={}):
    """
    将字符串或bytes转换为字典
    :param s:
    :type s:
    :return:
    :rtype:
    """
    if isinstance(str_or_byte, dict):
        return str_or_byte
    try:
        return json.loads(to_string(str_or_byte), encoding='utf-8')
    except:
        return replace_obj


def phone_check(phone):
    s = to_string(phone)
    if not s or get_len(s) != 11:
        return False
    if s.isdigit() and to_int(s[:1]) == 1:
        return True
    return False


def to_int(str_num, failure_result=None):
    '''
    字符串转换为数字
    :param str_num:
    :type str_num:
    :return:
    :rtype:
    '''
    if isinstance(str_num, int):
        return str_num

    if str_num is None:
        return failure_result
    try:
        if isinstance(str_num, enum.Enum):
            return int(str_num.value)

        if isinstance(str_num, (float, Decimal)):
            return int(str_num)

        str_num = to_string(str_num)
        if not str_num:
            return failure_result

        new_str_num = INT_PATTERN.match(str_num)
        if new_str_num:
            return int(new_str_num.group())

        return failure_result
    except Exception as e:
        echo(e, 'to_int')
        return failure_result


def dic_to_list(dic, need_clear=True):
    '''
    字典转数组
    :param dic:
    :type dic:
    :param need_clear:
    :type need_clear:
    :return:
    :rtype:
    '''
    if not dic:
        return []
    res = []
    for v in dic:
        res.append(dic[v])
    if need_clear:
        dic.clear()
    return res


def to_timestamp(str_or_datetime):
    '''
    将时间或者时间字符串转换为时间戳
    :param str_or_datetime:
    :type str_or_datetime:
    :return: int or None
    :rtype: int or None
    '''
    try:
        if isinstance(str_or_datetime, int):
            return str_or_datetime

        if not str_or_datetime:
            return 0

        '''
        datetime->timestamp
        '''
        if isinstance(str_or_datetime, datetime):
            return int(str_or_datetime.timestamp())

        '''
        timeArray->timestamp
        '''
        if isinstance(str_or_datetime, timepy.struct_time):
            return int(timepy.mktime(str_or_datetime))

        '''
        string->timestamp
        '''
        if isinstance(str_or_datetime, str):
            if (get_len(str_or_datetime) > 10):
                timeArray = timepy.strptime(str_or_datetime, "%Y-%m-%d %H:%M:%S")
            else:
                timeArray = timepy.strptime(str_or_datetime, "%Y-%m-%d")
            return int(timepy.mktime(timeArray))

        timeArray = timepy.strptime(str(str_or_datetime), "%Y-%m-%d")
        return int(timepy.mktime(timeArray))
    except:
        return 0


@__util_try_except(0)
def int_boolean(value):
    '''
    根据值返回0或1,其实主要是怕外部传如数据有误和而已传入不符合要求的数字
    :param value:传入值
    '''
    if to_int(value):
        return 1
    return 0


def to_list(obj, failure_result=[]):
    """
    转换为list
    :param s:
    :type s:
    :return:
    :rtype:
    """
    if isinstance(obj, list):
        return obj
    if isinstance(obj, dict):
        return dic_to_list(obj)
    if isinstance(obj, set):
        return list(obj)
    try:
        return json.loads(to_string(obj), encoding='utf-8')
    except:
        return failure_result


def to_set(anything):
    '''
    转换为集合(主要针对字符串的list)
    :param anything:
    :type anything:
    :return:
    :rtype:
    '''
    if not anything:
        return set()
    if isinstance(anything, list):
        return set(anything)
    try:
        return set(json.loads(to_string(anything), encoding='utf-8'))
    except:
        return set()


def delete_file(path):
    '''
    删除文件
    :param path:
    :type path:
    :return:
    :rtype:
    '''
    try:
        basename = os.path.basename(path)
        if os.path.exists(path):
            os.remove(path)
        return True
    except:
        return False


def encode_id(id, uid):
    ''' 加密id,针对修改信息的时候    董新强'''
    if not id or not uid:
        return None
    # 检验 identifier
    encyptStr = to_string(id) + to_string(uid)
    m = hashlib.sha1(encyptStr.encode("utf8"))
    m.update(private_keys.encode("utf8"))
    ident = m.hexdigest()
    return ident[4:] + ident[7:11]


def deep_copy(obj):
    '''
    深拷贝
    '''
    if isinstance(obj, dict):
        new_dic = {}
        for v in obj:
            new_dic[v] = obj[v]
        return new_dic
    if isinstance(obj, list):
        new_list = []
        new_list.extend(obj)
        return new_list
    return obj


def to_decimal(obj, precision='0.0000', failure_num=0):
    '''
    转换为 Decimal类型
    :param obj:待转数字
    :type obj:object
    :param precision:精度(^0\.0+$) 例如:精度为两位 在传入参数为 0.00
    :type precision: str
    :param failure_num: 是否时候需要返回的值
    :type failure_num: Decimal
    :return: 转换好的数字
    :rtype: Decimal
    '''
    if obj is None:
        return 0

    try:
        if isinstance(obj, Decimal):
            decimal_num = obj

        elif isinstance(obj, int):
            decimal_num = Decimal(obj)

        # elif isinstance(obj, float):
        #     decimal_num = Decimal.from_float(obj)

        elif isinstance(obj, str):
            if r'%' in obj:
                decimal_num = Decimal(obj.replace('%', '')) / 100
            else:
                decimal_num = Decimal(obj)

        else:
            decimal_num = Decimal(to_string(obj, '0'))

        if not precision:
            return decimal_num

        m = PRECISION_MODEL.match(precision)
        if not m:
            return decimal_num

        return decimal_num.quantize(Decimal(m.group()))
    except:
        return failure_num


@__util_try_except()
def extract_phone(phone_str):
    '''
    从给定的字符串中抽取电话号码
    :rtype:  int
    '''
    if not phone_str:
        return 0
    phone_pattern = '1\d{10}'
    m = re.search(phone_pattern, to_string(phone_str))
    if not m:
        return None
    return to_int(m.group())


# --------------------枚举类型相关方法--------------------------------------
def get_enum_value(enum_class, attr_name):
    '''
    根据名字获取枚举值
    :rtype:
    '''
    if not enum_class or not isinstance(enum_class, enum.EnumMeta) or not attr_name or not isinstance(attr_name, str):
        return None
    try:
        return getattr(enum_class, attr_name).value
    except:
        return None


def to_future(dt, days):
    '''
    将时间向后推迟 days后得到的的新日期
    '''
    delta = timedelta(days=days)
    return dt + delta


@__util_try_except()
def get_ext(file_name):
    if not file_name:
        return None
    arr = file_name.split('.')
    if not arr or get_len(arr) < 2:
        return None
    arr.reverse()
    for v in arr:
        if v:
            return v
    return None


def to_datetime(time_string):
    if not time_string:
        return None

    if isinstance(time_string, datetime):
        return time_string

    try:
        if isinstance(time_string, str):
            if time_string.find(r'/') > -1:
                time_string = time_string.replace(r'/', '-')

            if (get_len(time_string) > 10):
                timeArray = datetime.strptime(time_string, "%Y-%m-%d %H:%M:%S")
            else:
                timeArray = datetime.strptime(time_string, "%Y-%m-%d")
            return timeArray

        # 时间戳转为时间
        if isinstance(time_string, int):
            # LYD 2019-08-12 这个时间戳转换成时间好像是有点问题的，改了一下
            # date_array = datetime.utcfromtimestamp(time_string)
            # return date_array.strftime("%Y-%m-%d %H:%M:%S")

            return timepy.strftime("%Y-%m-%d %H:%M:%S", timepy.localtime(time_string))

        return datetime.now()
    except:
        return None


@__util_try_except([])
def to_int_list(ls, force_convert=False):
    '''
    将list转为元素为int的list
    如果列表的第一个元素为int类型,那么就认为整个列表的元素都是int类型
    :param force_convert:   强制转换
    '''
    if not ls:
        return []
    if not force_convert and isinstance(ls[0], int):
        return ls
    return [to_int(v, 0) for v in ls]


def weekends_between(start_date, end_date):
    '''
    计算指定开始到结束时间段内有多少个非工作日
    :param d1:
    :type d1:
    :param d2:
    :type d2:
    :return:
    :rtype:
    '''
    days_between = (end_date - start_date).days
    weekends, leftover = divmod(days_between, 7)
    if leftover:
        start_day = (end_date - timedelta(leftover)).isoweekday()
        end_day = start_day + leftover
        if start_day <= 6 and end_day > 6:
            weekends += 1
        if start_day <= 7 and end_day > 7:
            weekends += 1

    return weekends


def strip_zero(num, failure_num='0'):
    '''
    除去小数点后面多余的零
    '''
    try:
        if num is None:
            return num

        s = str(num)
        if s == '0':
            return '0'

        if '.' not in s:
            return s

        return s.rstrip('0').rstrip('.')
    except:
        return failure_num


def calc_proceed_rate(x, y):
    '''
    计算金额百分比(仅限于计算金额) rate= x/y
    :param x:
    :type x:
    :param y:
    :type y:
    :return:
    :rtype:
    '''
    try:
        x_decimal = to_decimal(x)
        y_decimal = to_decimal(y)

        if x_decimal == 0 or y_decimal == 0:
            return '0'

        if x_decimal == y_decimal and x_decimal != 0:
            return str(100)

        rate = to_decimal(x_decimal / y_decimal, '0.0000')

        if rate == 0 and x_decimal != y_decimal:
            return str(1)

        if rate == to_decimal('1.0000'):
            return str(99)

        return to_string(int(to_decimal(rate, '0.00') * 100))

    except:
        return str(0)


def show_date_format(date_obj):
    '''
    本年的（月+日）；跨年的（年+月+日）
    2016-10-13
    '''
    if not date_obj:
        return None
    try:
        this_year = datetime.now().year
        if isinstance(date_obj, str):
            if to_int(date_obj[0:4]) == this_year:
                return to_datetime(date_obj).strftime('%m-%d')
            return to_datetime(date_obj).strftime('%Y-%m-%d')
        if date_obj.year == this_year:
            return date_obj.strftime('%m-%d')
        return date_obj.strftime('%Y-%m-%d')
    except:
        return str(date_obj)


def to_int_date(time_string, convert_to_time=False):
    '''
    将时间转换为 20161012 的形式
    :param date_obj:
    :type date_obj:
    :return:
    :rtype: int
    '''
    if isinstance(time_string, int):
        return time_string

    if not time_string:
        return 0

    try:
        if isinstance(time_string, str):
            if time_string.find(r'/') > -1:
                time_string = time_string.replace(r'/', '-')

            # ----------------------------------------
            pattern = r'\d{4}-\d{1,2}-\d{1,2}'
            n = re.match(pattern, time_string)
            if not n:
                return 0

            dt = datetime.strptime(n.group(), "%Y-%m-%d")
        else:
            dt = time_string

        month = str(dt.month) if dt.month > 9 else '0' + str(dt.month)
        day = str(dt.day) if dt.day > 9 else '0' + str(dt.day)

        if not convert_to_time:
            return to_int(str(dt.year) + month + day)

        hour = str(dt.hour) if dt.hour > 9 else '0' + str(dt.hour)
        minute = str(dt.minute) if dt.minute > 9 else '0' + str(dt.minute)
        second = str(dt.second) if dt.second > 9 else '0' + str(dt.second)
        return to_int(str(dt.year) + month + day + hour + minute + second)

    except:
        return 0


def to_float(obj, failure_replace_value=None):
    '''
    转换为浮点数类型
    '''
    try:
        if isinstance(obj, float):
            return obj
        if isinstance(obj, int):
            return obj

        return float(to_string(obj, '0'))
    except:
        return failure_replace_value


@__util_try_except((False, '密码校验失败!'))
def check_pwd_format(pwd):
    failure_result = (False, '密码长度至少为6位,且必须包含字母和数字!')
    if not pwd or get_len(pwd) < 6:
        return failure_result

    # ------必须有数字--------------------
    # re.match(r'(\d.*[A-Za-z])|([A-Za-z].*\d){6,20}')
    if not re.match(r'.{6,20}', pwd) or not re.search(r'\d+', pwd) or not re.search(r'[A-Za-z]+', pwd):
        return failure_result

    return (True, '')


@__util_try_except((0, 0, False))
def convert_phone_to_range(phone):
    '''
    将电话号码转换为范围 (要求号码至少3位)
    '''
    if not phone:
        return (0, 0, False)

    bool_number = is_number(phone)
    if not bool_number:
        return (0, 0, False)

    n = re.match(r'\d{3,11}', str(phone).replace(' ', ''))
    if not n:
        return (0, 0, False)

    new_phome = n.group()
    l = get_len(new_phome)
    if l == 11:
        return (to_int(new_phome), to_int(new_phome), True)

    min_num = list(new_phome)
    max_num = list(new_phome)
    for v in range(11 - l):
        min_num.append('0')
        max_num.append('9')
    return (to_int(''.join(min_num)) - 1, to_int(''.join(max_num)) + 1, True)


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass

    return False


def debug_tags(show_msg, title=None):
    '''
    调试标签
    '''
    if not is_debug:
        return
    print('\n--------------------调试标签------------------------开始----------------')
    print('<< %s >>' % ('' if title is None else str(title)))
    print('-----' + str(datetime.now()))
    if show_msg is not None:
        if isinstance(show_msg, ResponseMessage):
            print('-----' + str(show_msg.responseMsg()))
            return
        print('-----' + str(show_msg))
    print('--------------------调试标签------------------------结束----------------\n')


def echo(show_msg, title=None):
    '''
    debug_tags的简写
    '''
    debug_tags(show_msg, title)


def gen_crm_token(customer_uid, alert_obj_id, alert_type):
    '''
    生成反馈互动的token
    '''
    try:
        if not customer_uid or not alert_obj_id or not alert_type:
            return None

        m = hashlib.sha1((to_string(customer_uid) + str(alert_obj_id)).encode("utf8"))
        m.update((str(alert_obj_id) + to_string(customer_uid) + to_string(alert_type) + str('+=&$adb1')).encode("utf8"))
        est = m.hexdigest()
        return est[2:get_len(est) - 5]
    except:
        return None


def current_timestamp(to_str=False):
    '''
    获取当前时间戳(精确到秒)
    :return: 时间戳
    :rtype: int
    '''

    if to_str:
        return to_string(int(timepy.time()))
    return int(timepy.time())


def pop_dict(dic, key, replace_value=None):
    '''
    由于从dic中弹出不存在的key会报错,所以添加该方法以简化相关操作
    :param dic:字典
    :type dic: dict
    :param key:key
    :type key:
    :return:
    :rtype:
    '''
    if not dic or not key or not isinstance(dic, dict):
        return None
    if key not in dic:
        return None
    try:
        return dic.pop(key) or replace_value
    except:
        return replace_value


def del_list_ele(ls, ele):
    '''
    删除列表中指定的元素(所有)
    :param ls:列表
    :type ls:list
    :param ele:元素
    :type ele:obj
    :return:
    :rtype:
    '''
    if not ls:
        return True

    # assert isinstance(ls, list)
    try:
        while ls and ele in ls:
            ls.remove(ele)
        return True
    except:
        return False


def is_none_or_empty(obj, zero_is_none=True):
    '''
    判断是否为空 (None,(None,None),{}, '' )为空
    :param obj:
    :type obj:
    :param zero_is_none: 0 是否算作空  (默认是)
    :type zero_is_none: bool
    :return:
    :rtype:
    '''
    if obj is None:
        return True

    if isinstance(obj, (list, tuple)):
        for v in obj:
            if v is not None:
                return False
        return True

    if isinstance(obj, (dict, str, set)):
        return not obj

    if isinstance(obj, (int, Decimal)):
        if zero_is_none and obj == 0:
            return True
        return False

    if isinstance(obj, ResponseMessage):
        return not obj.is_ok()

    return False


def date_minus(time1, time2):
    '''
    求两时间差(天数)
    '''
    try:
        if not time1 or not time2:
            return 0

        val = to_datetime(time1) - to_datetime(time2)
        if not val:
            return 0

        return val.days
    except:
        return 0


def to_date_str(obj, failure_value=''):
    '''
    转换为日期
    :param obj:
    :type obj:
    :param failure_value:
    :type failure_value:
    :return:
    :rtype:
    '''
    try:
        if not obj:
            return ''

        if not isinstance(obj, str):
            obj = to_string(obj)

        gp = re.search(DATE_PATTERN, obj)
        if gp:
            return gp.group()
        return ''
    except:
        return failure_value


def injection_filter(condition):
    '''
    mysql条件检查 (主要针对like)
    :param condition: 查询条件
    :type condition: str
    :return:查询条件
    :rtype:str
    '''
    if not condition:
        return None
    if '=' in condition:
        return None
    if "'" in condition:
        return None
    if ';' in condition:
        return None
    if r'/*' in condition:
        return None
    if r'--' in condition:
        return None
    if r'#' in condition:
        return None
    if r' ' in condition:
        return None
    return condition.strip(' ')


def get_len(obj):
    '''
    获取对象长度
    '''
    if not obj:
        return 0

    try:
        if isinstance(obj, (int, float, Decimal)):
            return len(str(obj).replace('.', '').replace('-', ''))
        return len(obj)
    except:
        return 0


def gen_server_access_token(token, expire, client_secret, business_id):
    '''
    用来生成供第三方服务器单点登录的token  (2017-6-2)
    :param token:用来加密的token
    :type token: str
    :param expire: 有效截止时间戳(秒)
    :type expire: int
    :param client_secret:客户秘钥
    :type client_secret:str
    :return: access_token
    :rtype: str
    '''
    # assert token and expire and client_secret
    m = hashlib.sha256(to_string(client_secret + str(expire - business_id)).encode("utf8"))
    ls = list(token + client_secret + str(business_id))
    ls.sort()
    m.update(':'.join(ls).encode("utf8"))
    return 's:' + m.hexdigest()[:48]


def to_sha256(content):
    return hashlib.sha256(to_string(content).encode("utf8")).hexdigest()


def get_random_chars(length=12, letter_only=False):
    '''
    获取字符串
    :param length:
    :type length:
    :return:
    :rtype:
    '''
    # assert isinstance(length, int)
    if length > 60:
        length = 60
    if not letter_only:
        ls = randomsample(r'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0987654321~!@#$%^&*()_+<>?:{},./;\|',
                          length)
    else:
        ls = randomsample(r'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0987654321', length)
    return ''.join(ls)


@__util_try_except(([], {}))
def get_list_from_sql(sql_query, convert_to_str=True, col_index_list=[], show_name=None, skip_col_none=True, convert_to_time=True):
    '''
    获取查询结果(列表形式)
    :param sql_query:sql语句
    :type sql_query:
    :param convert_to_str:将decimal和datetime转换为string来类型
    :type convert_to_str:bool
    :param col_as_list: 取第第几列的值作为列表
    :type col_as_list: int
    :return:结果
    :rtype: ( dict, list)
    '''
    if not show_name:
        fields = [v['name'] for v in sql_query.column_descriptions]
    else:
        fields = show_name

    ls = []
    col_ls_dic = {}
    for v in col_index_list:
        col_ls_dic[v] = set()

    for data_tuple in sql_query:
        item = {}
        cur_index = -1
        for m in col_ls_dic:
            value = data_tuple[m]
            if skip_col_none and not value:  # 移除空值
                continue
            col_ls_dic[m].add(value)

        for v in fields:
            cur_index += 1
            if not convert_to_str:
                item[v] = data_tuple[cur_index]
                continue

            d = data_tuple[cur_index]
            if isinstance(d, Decimal):
                item[v] = str(d)
                continue

            if isinstance(d, datetime):
                if convert_to_time:
                    item[v] = to_string(d)
                else:
                    item[v] = to_date_str(d)
                continue

            item[v] = d

        ls.append(item)
    return (ls, col_ls_dic)


@__util_try_except({})
def get_dict_from_sql(sql_query, convert_to_str=True, key_col_index=0, contain_key=False, value_as_list=False):
    '''
    获取查询结果(字典形式)
    :param sql_query:sql语句
    :type sql_query:
    :param convert_to_str:将decimal和datetime转换为string来类型
    :type convert_to_str:bool
    :param key_col_index:作为key所在的位置
    :type key_col_index: int
    :param value_as_list: key:value的value的值是否要取列表 ,ture的话返回的value为list,否则它就是它
    :type value_as_list: bool
    :return:  结果
    :rtype: dict
    '''
    dic = {}
    col_ls_dic = {}
    fields = [v['name'] for v in sql_query.column_descriptions]

    for data_tuple in sql_query:
        item = {}
        cur_index = 0
        for m in col_ls_dic:
            col_ls_dic[m].append(data_tuple[m])

        for v in fields:
            if cur_index == key_col_index and not contain_key:  # key不再包含在字典value中(节省空间)
                cur_index += 1
                continue

            if convert_to_str:
                d = data_tuple[cur_index]
                if isinstance(d, (datetime, Decimal)):
                    item[v] = str(d)
                else:
                    item[v] = d
            else:
                item[v] = data_tuple[cur_index]
            cur_index += 1

        key = data_tuple[key_col_index]
        if value_as_list:  # key:value的value的值去列表 2017-9-6
            if key not in dic:
                dic[key] = [item]
            else:
                dic[key].append(item)
        else:
            dic[key] = item
    return dic


def now_timestamp():
    return int(timepy.time())


def get_one_from_dict(dic):
    if not isinstance(dic, dict) or not dic:
        return (None, None)
    for v in dic.items():
        return v


def add_to_dict(dic, key, value):
    '''
    将value添加到字典的key下,可以对应的值是一个字典
    :param dic:字典
    :type dic:dict
    :param key:
    :type key:
    :param value:
    :type value:
    :return:
    :rtype:
    '''
    if dic is None or not key:
        return

    if key in dic:
        dic[key].append(value)
    else:
        dic[key] = [value]


def ravel_list(ls):
    '''
    将多维list展平
    :param ls: list
    :type ls: list
    :return: 展平后的list
    :rtype: list
    '''
    # assert ls is None or isinstance(ls, list)

    new_ls = []

    def revl(l):
        for v in l:
            if isinstance(v, list):
                revl(v)
            else:
                new_ls.append(v)

    revl(ls)
    return new_ls


def convert_single_num(num):
    if num == '0':
        return '零'
    if num == '1':
        return '一'
    if num == '2':
        return '二'
    if num == '3':
        return '三'
    if num == '4':
        return '四'
    if num == '5':
        return '五'
    if num == '6':
        return '六'
    if num == '7':
        return '七'
    if num == '8':
        return '八'
    if num == '9':
        return '九'
    return '零'


def get_chs_num(nums):
    if nums is None:
        return '零'
    ls = [convert_single_num(num) for num in str(nums)]
    return ''.join(ls)


def dic_num(dic, field_name):
    if not dic:
        return 0
    return dic.get(field_name) or 0


def format_date_or_decimal(decimal_or_date, convert_to_str=True):
    '''
    将2017年11月20日或Decimal转换为字符串
    注意:当将Decimal转为字符串的时候会保留 2 位小数.
    :param decimal_num:
    :type decimal_num: Decimal
    :param convert_to_str: 是否需要转换为str,乍一看觉得多余,但在好多get_dict中是需要这样操作的.
    :type convert_to_str: bool
    :return:
    :rtype:str
    '''
    if decimal_or_date is None:
        return None

    if not convert_to_str or (convert_to_str and isinstance(decimal_or_date, str)):  # 如果不转为字符串的话就直接返回参数本身.
        return decimal_or_date

    if isinstance(decimal_or_date, datetime):
        return to_string(decimal_or_date)
    if isinstance(decimal_or_date, (float, Decimal)):  # process decimal num
        return to_string(to_decimal(decimal_or_date, '0.00'), '0.00')
    return decimal_or_date  # 只转换decmial和时间,其他的不进行转换.


def convert_intTime_to_str(inttime):
    if not inttime:
        return ''

    if isinstance(inttime, str) and '-' in inttime:
        return inttime

    s = str(inttime)
    if len(s) < 9:
        return '{0}-{1}-{2}'.format(s[:4], s[4:6], s[6:8])
    return '{0}-{1}-{2} {3}:{4}:{5}'.format(s[:4], s[4:6], s[6:8], s[8:10], s[10:12], s[12:14])


def get_int_now():
    dt = datetime.now()
    month = str(dt.month) if dt.month > 9 else '0' + str(dt.month)
    day = str(dt.day) if dt.day > 9 else '0' + str(dt.day)
    hour = str(dt.hour) if dt.hour > 9 else '0' + str(dt.hour)
    minute = str(dt.minute) if dt.minute > 9 else '0' + str(dt.minute)
    second = str(dt.second) if dt.second > 9 else '0' + str(dt.second)
    return to_int(str(dt.year) + month + day + hour + minute + second)


def get_cmd_params(argv_ls):
    dic = {}
    try:
        if not argv_ls:
            return dic

        for v in argv_ls:
            if r'=' not in v:
                continue
            res = v.split('=')
            dic[res[0].strip(' ')] = res[1].strip(' ')
        return dic
    except:
        return dic


# sql 语句转换list
def sql_to_list(sql_query, col_index_list=[], decimal_to_string=True, time_to_string=True, none_to_string=True, decimal_precision='0.0000'):
    '''
    LYD 2018-12-11
    获取查询结果(列表形式)
    :param self:
    :param sql_query: 查询的sql语句
    :param col_index_list: 需要查询的列的index
    :param decimal_to_string: 是否小数转字符串
    :param time_to_string: 是否时间传字符串
    :param none_to_string: 是否None值转空字符串
    :param decimal_precision: 小数精度
    :return:
    '''
    fields = [v['name'] for v in sql_query.column_descriptions]

    ls = []
    col_ls_dic = {}
    for v in col_index_list:
        col_ls_dic[v] = set()

    for data_tuple in sql_query:
        item = {}
        cur_index = 0
        for m in col_ls_dic:
            value = data_tuple[m]
            col_ls_dic[m].add(value)

        for v in fields:
            d = data_tuple[cur_index]
            cur_index += 1
            if none_to_string and d is None:
                item[v] = ''
                continue

            if isinstance(d, Decimal):
                if decimal_to_string:
                    if decimal_precision:
                        item[v] = str(to_decimal(d, decimal_precision))
                    else:
                        item[v] = str(d)
                else:
                    if decimal_precision:
                        item[v] = to_decimal(d, decimal_precision)
                    else:
                        item[v] = d
                continue

            if isinstance(d, datetime) or isinstance(d, date):
                if time_to_string:
                    item[v] = to_string(d, '')
                else:
                    item[v] = to_date_str(d, '')
                continue

            item[v] = d

        ls.append(item)

    return (ls, col_ls_dic)


def format_dict(dic):
    '''
    格式化字典,将时间和decimal转换为字符串
    :param dic:
    :type dic:
    :return:
    :rtype:
    '''
    if not dic or not isinstance(dic, dict):
        return dic
    for key in dic:
        val = dic[key]
        if isinstance(val, Decimal):
            dic[key] = to_string(val, '0')
            continue
        if isinstance(val, float):
            dic[key] = to_string(val, '0')
            continue
        if isinstance(val, datetime):
            dic[key] = to_string(val)[:19]
            continue
        if isinstance(val, date):
            dic[key] = to_string(val)[:19]
            continue
        if 'is_delete' == key:
            dic[key] = val

    return dic


def deta_int_time(obj):
    '''
    int类型的时间转换为时间格式
    :param obj:
    :return:
    '''
    if isinstance(obj, int):
        obj = to_string(obj)
        year = obj[:4]
        month = obj[4:6]
        day = obj[6:8]

        tiem = year + '-' + month + '-' + day

        return to_datetime(tiem)

    if isinstance(obj, str):
        year = obj[:4]
        month = obj[4:6]
        day = obj[6:8]

        tiem = year + '-' + month + '-' + day

        return to_datetime(tiem)


def format_amount(amount):
    '''金额格式化'''
    if not amount:
        return "0.00"
    if not isinstance(amount, str):
        amount = str(amount)
    if re.compile('-?\d+.\d{2}00').match(amount):  # 最后两位00
        return amount[:-2]
    elif re.compile('-?\d+.\d{3}0').match(amount):
        return amount[:-1]
    else:
        return amount


# 全角 — 转 半角 -
def SBC_case_to_half_angle(obj):
    if not obj:
        return obj

    obj = to_string(obj).replace(' ', '')
    obj = to_string(obj).replace('—', '-')
    return obj


# 去除 emoji 表情
def filter_emoji(obj, restr=''):
    obj = to_string(obj, '')
    try:
        co = re.compile(u'[\U00010000-\U0010ffff]')
    except re.error:
        co = re.compile(u'[\uD800-\uDBFF][\uDC00-\uDFFF]')
    return co.sub(restr, obj)


def xml_to_dict(xml_str):
    try:
        return xmltodict.parse(xml_str)['xml']
    except:
        return {}


def get_FileSize(path, filename):
    full_path = os.path.join(path, filename)

    fsize = os.path.getsize(full_path)
    fsize = fsize / (1024 * 1024)
    return round(fsize, 4)


# 获取指定年指定月的工作日
def get_workdays(year, month):
    result = []
    cal = calendar.Calendar()
    for day in cal.itermonthdates(year, month):
        if day.month == month:
            if day.weekday() < 5:
                result.append(day)
    return result


if __name__ == '__main__':
    filter_emoji("西夏一品堂")
    # echo(to_int('4', 0.0))
    # echo(to_decimal('5.1%'))
    # echo(to_decimal(1.21))
    # echo(to_decimal('32'))
