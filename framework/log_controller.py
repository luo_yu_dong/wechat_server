#!/usr/bin/env python3
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 日志处理
@file: Base_api.py
@time: 2019/12/30 11:49
@desc:
'''
import functools
import traceback
from datetime import datetime

from tornado.gen import coroutine

from enums.enum_cache_type import EnumCacheType
from framework.msg import failure
from framework.utilities import to_string, to_int
from setting import is_debug
from thirdpart.redis_helper import RedisList


def log(e, except_position='代码中未指定', option_value='', error_type=0):
    if not e:
        return

    if is_debug:  # 调试模式的时候不写入日志
        print('----------------异常信息--------------------------------------------start-----------------')
        print('异常位置 =>', to_string(except_position))
        print('说明=>' + to_string(option_value, ''))
        print('异常消息=>', e)
        print('----------------异常信息---------------------------------------------end------------------')
        return

    try:
        # -------------失败时候的提示信息---------------------------------
        if not option_value:
            option_value_str = ''
        elif isinstance(option_value, failure):
            option_value_str = option_value.msg or ''
        else:
            option_value_str = to_string(option_value, '')

        body = {
            'body': to_string(e, '未获到异常结果'),
            'type': to_string(to_int(error_type)),
            'pos': except_position,
            'val': option_value_str,
            'time': to_string(datetime.now())
        }
        RedisList.append_to_bottom('__sys_exceptions', to_string(body), cache_type=EnumCacheType.log)
    except:
        pass


def trycatch(optional_exception_return_value=None, func_full_name=None):
    '''
    异常捕获函数
    使用方式:
        @trycatch()
    :param optional_exception_return_value:如果发生异常时的可选返回值
    '''

    def innertrycatch(function):
        '''
        该方法用来捕获异常,并将其写入日志
        :param function:
        :type function:
        :return:如果成功正常返回,如果发生异常返回用户制定值(optional_exception_return_value)或None
        :rtype:
        '''

        @functools.wraps(function)
        def warp(*args, **kwargs):
            try:
                return function(*args, **kwargs)
            except Exception as e:
                # ----------------记录日志--------------------------------
                if func_full_name:
                    except_position = to_string(func_full_name, '获取不到函数名')
                else:
                    try:
                        except_position = to_string(function.__code__, '获取不到函数名')
                    except:
                        except_position = to_string(function, '获取不到函数名')

                # 添加传入参数记录
                except_position += '; 传入参数: arg=>%s,kwargs=>%s' % (to_string(args or '无'), to_string(kwargs or '无'))

                log(traceback.format_exc(), except_position, optional_exception_return_value)

                if is_debug:
                    print('----------------异常信息--------------------------------------------start-----------------')
                    print('异常位置 =>', except_position)
                    print('异常消息=>', e)
                    print('----------------异常信息---------------------------------------------end------------------')

                return optional_exception_return_value

        return warp

    return innertrycatch


def trycatch_async(optional_exception_return_value=None, func_full_name=None):
    '''
    异常捕获函数  (不推荐继续使用)
    使用方式:
        @trycatch()
    :param optional_exception_return_value:如果发生异常时的可选返回值
    '''

    def innertrycatch(function):
        '''
        该方法用来捕获异常,并将其写入日志
        :param function:
        :type function:
        :return:如果成功正常返回,如果发生异常返回用户制定值(optional_exception_return_value)或None
        :rtype:
        '''

        @coroutine
        @functools.wraps(function)
        def warp(*args, **kwargs):
            try:
                res = yield function(*args, **kwargs)
                return res
            except Exception as e:
                # 如果直接在异常捕获的时候指明了函数位置,就不在根据函数获取函数的名称
                if func_full_name:
                    except_position = to_string(func_full_name, '获取不到函数名')
                else:
                    try:
                        except_position = to_string(function.__code__, '获取不到函数名')
                    except:
                        except_position = to_string(function, '获取不到函数名')

                if is_debug:
                    print('----------------异常信息--------------------------------------------start-----------------')
                    print('异常位置 =>', except_position)
                    print('异常消息=>', e)
                    print('----------------异常信息---------------------------------------------end------------------')
                else:
                    log(traceback.format_exc(), except_position=except_position, option_value=optional_exception_return_value)

                return optional_exception_return_value

        return warp

    return innertrycatch


class TryCatch():
    '''
    异步异常捕获
    '''

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_val:
            try:
                exc_info = []
                for filename, linenum, funcname, source in traceback.extract_tb(exc_tb):
                    exc_info.append("%s.%s(),行:%s,源:%s" % (filename, funcname, linenum, source))
                exc_info.append('传入参数,args=>%s,kwargs=>%s' % (to_string(self.args), to_string(self.kwargs)))

                if is_debug:
                    print('---------------发生异常------------------------------------start---')
                    print('异常时间=>%s', to_string(datetime.now()))
                    print('异常类型=>%s', to_string(exc_type))
                    print('异常详情=>%s', to_string(exc_val))
                    print('---------------发生异常------------------------------------end-----')
                else:
                    log(traceback.format_exc(), except_position='\n'.join(exc_info), option_value='', error_type=0)
            except:
                pass
        return True
