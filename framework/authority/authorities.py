# coding=utf-8
# url及必填参数验证
# lyd
# 2020年10月20日
import functools

from enums.enum_error_msg_type import EnumErrorType
from framework.msg import failure
from framework.utilities import to_dict, get_len


def check_args(args=None):
    '''
    检查请求url必须包含某些参数
    '''

    def checkpara(func):
        @functools.wraps(func)
        def deco(self):
            if args:
                try:
                    body = to_dict(self.request.query_arguments)
                    if not body:
                        body = to_dict(self.request.arguments)
                except:
                    body = to_dict(self.request.arguments)

                if not body or get_len(body) == 0:
                    self.set_status(400)
                    return self.finish(failure(msg='请求参数错误!', error_type=EnumErrorType.param_error).responseMsg())

                for v in args:
                    if v not in body:
                        self.set_status(400)
                        return self.finish(failure(msg='请求参数错误', error_type=EnumErrorType.param_error).responseMsg())
            return func(self)

        return deco

    return checkpara


def thirdparty_check_args(args=None):
    '''
    检查请求url必须包含某些参数
    '''

    def checkpara(func):
        @functools.wraps(func)
        def deco(self):
            if args:
                body = to_dict(self.request.body)

                if not body or get_len(body) == 0:
                    self.set_status(400)
                    return self.finish(failure(msg='请求参数错误!', error_type=EnumErrorType.param_error).responseMsg())

                for v in args:
                    if v not in body:
                        self.set_status(400)
                        return self.finish(failure(msg='请求参数错误', error_type=EnumErrorType.param_error).responseMsg())

            return func(self)

        return deco

    return checkpara
