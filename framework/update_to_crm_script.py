#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: update_to_crm_script.py
@time: 2020/10/22 17:31
@desc: 用于处理未上传到crm的脚本
'''
from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.msg import ok
from framework.utilities import get_quanpin, to_string
from models.customer_form.customer_submmint_form import CustomerSubmmintForm
from models.users.user import User
from services.lingdang_crm_manage import LingdangCRMManage


class WorkServerManage():
    def __init__(self):
        pass

    def update_to_crm(self):
        i = 1
        b = 1
        to_crm_list = []
        with DbSession.create(EnumDb.main) as db:
            query = db.query(CustomerSubmmintForm). \
                filter(CustomerSubmmintForm.is_to_crm == 0). \
                filter(CustomerSubmmintForm.is_delete == 0). \
                filter(CustomerSubmmintForm.int_date != 0)

            for v in query:
                print("query", i)
                i += 1
                query_dict = v.get_dict()

                if query_dict.get('form_type') == 1:
                    form_type_name = '需求'
                elif query_dict.get('form_type') == 2:
                    form_type_name = '投诉'
                else:
                    form_type_name = '问题'

                crm_user_query = db.query(User.crm_user).filter(User.user_id == query_dict.get('reply_user_id')).first()
                if not crm_user_query:
                    crm_user_name = ''
                else:
                    crm_user_name = crm_user_query[0]

                query_dict['form_type_name'] = form_type_name
                query_dict['crm_user_name'] = crm_user_name

                to_crm_list.append(query_dict)

        for dict in to_crm_list:
            print("add", b)
            b += 1
            if get_quanpin(dict.get('reply_user_name')) == 'lvhengshan':
                dict['reply_user_name'] = 'luhengshan'
            ldCRM = LingdangCRMManage()
            ldCRM.add_crm_hepldesk(
                business_name=dict.get('business_name'),
                form_contacts=dict.get('contacts'),
                user_name=dict.get('reply_user_name'),
                crm_username=get_quanpin(dict.get('reply_user_name')),
                ticket_no=dict.get('no'),
                ticket_title=dict.get('title'),
                solution=dict.get('reply_result'),
                realtime=to_string(dict.get('replyl_time'))[:19],
                createdtime=to_string(dict.get('creat_time'))[:19],
                description=dict.get('comment'),
                form_id=dict['id'],
                ticketcategories=dict.get('form_type_name'),
                form_contacts_phone=dict.get('contacts_phone')
            )
        return ok()


if __name__ == '__main__':
    a = WorkServerManage()
    res = a.update_to_crm()
    print(res)
