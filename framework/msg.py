#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 智慧桥梁-返回消息模型
@file: msg.py
@time: 2019/12/30 11:58
@desc:
    status 状态码 int类型  0 成功  1或其他数字表示失败
	msg		状态描述
	data	实际消息体
'''

# coding=utf-8
from enums.enum_error_msg_type import EnumErrorType

"""
	该类为http返回的消息模型
	status 状态码 int类型  0 成功  1或其他数字表示失败
	msg		状态描述
	data	实际消息体
"""


class ResponseMessage():
    def __init__(self, msg, data, status):
        self.msg = msg
        self.data = data
        self.status = status

    def is_ok(self):
        return self.status is not None and self.status == 1

    def responseMsg(self):
        pass

    def get_code(self):
        return None


class ok(ResponseMessage):
    """
    该类为http返回的消息模型
    status 状态码 int类型  1 成功  0或其他数字表示失败
    msg		状态描述
    data	实际消息体
    """

    def __init__(self, msg='', data={}):
        super().__init__(msg, data, 1)

    def responseMsg(self):
        return {
            'status': 1,
            'msg': self.msg,
            'data': self.data,
            'server_code': '',
            'table_status': 0  # 表格返回状态，成功=0 失败=1
        }

    def __str__(self):
        return str(self.responseMsg())


class failure(ResponseMessage):
    def __init__(self, msg='', data={}, error_type=EnumErrorType.common):
        super().__init__(msg, data, 0)

        if isinstance(error_type, EnumErrorType):
            self.error_type = error_type.value
        else:
            self.error_type = str(error_type)

    def responseMsg(self):
        if self.status == 1:  # failure不允许返回 1
            return {'status': 0, 'msg': self.msg, 'data': self.data, 'table_status': 1}

        if self.error_type == EnumErrorType.common.value:  # 一般错误就不提示错误编码了
            return {'status': self.status, 'msg': self.msg, 'data': self.data, 'table_status': 1}

        if self.error_type == EnumErrorType.common_error.value:  # 正常返回错误信息
            return {'status': self.status, 'msg': self.msg, 'data': self.data, 'table_status': 1}

        return {
            'status': self.status,
            'msg': self.msg + '(错误代码:%s)' % (self.error_type),
            'data': self.data,
            'server_code': self.error_type,
            'table_status': 1
        }

    def set_status(self, status):
        if status == 1:
            return
        self.status = status

    def __str__(self):
        return str(self.responseMsg())

    def get_code(self):
        return self.error_type
