class Register(object):
    def login(self, key, ws_instance):
        self.waiters[key] = ws_instance
        # print('用户{}加入了连接'.format(key))

    def logout(self, key):
        if key in self.waiters:
            del self.waiters[key]
            # print('用户{}断开了连接'.format(key))

    def __init__(self):
        self.waiters = dict()