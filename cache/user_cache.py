#!/usr/bin/env python3
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 该文件用来存放用户缓存数据,数据用于权限验证时候不去读取数据库
@file: user_cache.py
@time: 2019/12/30 11:49
@desc:
'''

from cache import cache
from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.utilities import to_string
from models.users.user import User


def set(name, dic):
    """
    添加用户信息作为缓存
    """
    if not dic:
        cache.set(name, {})
        return
    if isinstance(dic, dict):
        cache.set(name, dic)
    elif isinstance(dic, User):
        cache.set(name, dic.get_dict())
    else:
        cache.delete(key=name)


def get(name):
    '''
    获取用户缓存
    :param phone:电话号码
    :type phone:int
    :param user_type:用户类型
    :type user_type:EnumUsrType
    :param reload:是否重载(从数据库直接重载覆盖缓存,即更新缓存)
    :type reload:bool
    :return:user字典
    :rtype:dict
    '''
    if not name:
        return None

    """
    如果此处没有用户数据,从数据库提取数据
    """
    with DbSession.create(EnumDb.main) as db:
        user = db.query(User).filter(User.user_name == name).filter(User.is_delete == 0).first()
        if not user:
            return None
        userinfo = user.get_dict()

    set(name, userinfo)
    return userinfo


def delete(name):
    cache.delete(to_string(name))


def clear_user_role_cache(uid):
    '''
    清空权限缓存
    '''
    cache.delete('r_' + to_string(uid))
