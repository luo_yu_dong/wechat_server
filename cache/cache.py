# coding=utf-8

from framework.utilities import to_dict, to_string
from thirdpart import redis_helper as re


def set(key, value, time=1800):
    try:
        re.set(to_string(key), value, time)
    except Exception as e:
        print('error\t{0}'.format(e))


def get(key):
    try:
        res = re.get(to_string(key))
        if res:
            return to_dict(res)
    except Exception as e:
        print('error\t{0}'.format(e))
    return None


def delete(key):
    '''
    该方式用来删除缓存中的键
    :param key:
    :type key:
    :return:
    :rtype:
    '''
    try:
        re.delete(to_string(key))
    except Exception as e:
        print('error\t{0}'.format(e))
