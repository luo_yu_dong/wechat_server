#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: urls.py
@time: 2020/10/20 10:04
@desc:
'''

from apis import frontend_api, wechat_api, file_api, work_server_api, thirdparty_probation_api, user_api, statistics_api, chandao_api, working_daily_api, project_payback_api

GLOBAL_URLS = [
    (r'/api/wechat/program/img/sec/check', wechat_api.WechatProgramImgSecCheckHandler),  # 图片鉴黄
    (r'/api/file/add', file_api.FileAddHandler),  # 上传文件

    (r'/iuKAM6Zjsl.txt', wechat_api.WechatVerifyFileHandler),  # 文件校验
    (r'/MP_verify_vRPceIgHlziabkx9.txt', wechat_api.WechatVerifyDomainFileHandler),  # 微信验证接口 正式
    (r'/api/wechat/make/wiki/WXACode', wechat_api.WeChatMakeWikiWXACodeHandler),  # 生成公众号客服专用二维码
    (r'/api/wechat/verify', wechat_api.WeChatVerifyHandler),  # 验证授权公众号
    (r'/api/wechat/authorization', wechat_api.WeChatAuthorizationHandler),  # 授权
    (r'/api/wechat/program/login', wechat_api.WeChatProgramLoginHandler),  # 小程序登陆
    (r'/api/wechat/program/user/info', wechat_api.WechatProgramUserInfoHandler),  # 解密用户信息

    # 客户业务处理
    (r'/api/wechat/customer/submmint/form/creat', wechat_api.WeChatCustomerSubmmintFormCreatHandler),  # 客户提交表单数据
    (r'/api/wechat/customer/submmint/form/list', wechat_api.WeChatCustomerSubmmintFormListHandler),  # 查询我发起的表单
    (r'/api/wechat/customer/submmint/form/first', wechat_api.WeChatCustomerSubmmintFormFirstHandler),  # 详情
    (r'/api/wechat/customer/submmint/form/score', wechat_api.WeChatCustomerSubmmintFormScoreHandler),  # 评价打分
    (r'/api/wechat/user/get/list', wechat_api.WeChatUserGetListHandler),  # 查询用户列表

    # 用户体系
    (r'/api/user/creat', user_api.UserCreatHandler),  # 创建用户
    (r'/api/user/login', user_api.UserLoginHandler),  # 登录
    (r'/api/user/remind/un/read/list', user_api.UserRemindUnReadListHandler),  # 获取未读消息和列表
    (r'/api/user/remind/list', user_api.UserRemindListHandler),  # 消息列表
    (r'/api/user/remind/read', user_api.UserRemindReadHandler),  # 读消息
    (r'/api/user/logout', user_api.UserLogoutHandler),  # 登出
    (r'/api/user/edit/basics', user_api.UserEditBasicsHandler),  # 编辑用户基础信息
    (r'/api/user/edit/pwd', user_api.UserEditPwdHandler),  # 修改用户密码
    (r'/api/user/edit/admin', user_api.UserEditAdminHandler),  # 设置、取消管理员
    (r'/api/user/edit/mobile', user_api.UserEditMobileHandler),  # 修改电话
    (r'/api/user/edit/crmuser', user_api.UserEditCrmuserHandler),  # 绑定CRM账号
    (r'/api/user/edit/chandaouser', user_api.UserEditChandaouserHandler),  # 绑定禅道账号
    (r'/api/user/delete', user_api.UserDeleteHandler),  # 删除用户
    (r'/api/user/get/list', user_api.UserGetListHandler),  # 查询用户列表
    (r'/api/user/sys/operation/log/list', user_api.UserSysOperationLogListHandler),  # 获取用户操作日志
    (r'/api/user/remind/logs', user_api.UserSysOperationLogListHandler),  # 获取用户消息

    # 统计分析
    (r'/api/statistics/home', statistics_api.StatisticsHomeHandler),

    # 配置客服
    (r'/api/work/server/user/list', work_server_api.WorkServerUserListHandler),  # 获取已关注公众号的客服列表
    (r'/api/work/server/user/bind', work_server_api.WorkServerUserBindHandler),  # 绑定客服账号

    # 业务处理
    (r'/api/work/server/form/get/list', work_server_api.WorkServerFormGetListHandler),  # 客服端查询表单列表
    (r'/api/work/server/form/get/list/export', work_server_api.WorkServerFormGetListExportHandler),  # 客服端查询表单列表导出
    (r'/api/work/server/form/allocation/bach', work_server_api.WorkServerFormAllocationBachHandler),  # 批量分配表单
    (r'/api/work/server/form/get/first', work_server_api.WorkServerFormGetFirstHandler),  # 客服端查询详情
    (r'/api/work/server/form/reply', work_server_api.WorkServerFormReplyHandler),  # 表单回复
    (r'/api/work/server/form/reply/desc', work_server_api.WorkServerFormReplyDescHandler),  # 表单备忘记录
    (r'/api/work/server/form/delete/bach', work_server_api.WorkServerFormDeleteBachHandler),  # 批量删除单据

    # 禅道
    (r'/api/chandao/get/project/task/first', chandao_api.ChandaoGetProjectTaskFirst),  # 获取一级禅道任务
    (r'/api/chandao/get/project/task/subset', chandao_api.ChandaoGetProjectTaskSubset),  # 获取子级禅道任务

    # 项目
    (r'/api/chandao/get/sys/project/phase/configuration/tree', chandao_api.ChandaoGetSysProjectPhaseConfigurationTree),  # 获取系统项目阶段配置
    (r'/api/chandao/add/sys/project/phase/configuration', chandao_api.ChandaoAddSysProjectPhaseConfiguration),  # 新增系统项目阶段配置
    (r'/api/chandao/update/sys/project/phase/configuration', chandao_api.ChandaoUpdateSysProjectPhaseConfiguration),  # 更新系统项目阶段配置
    (r'/api/chandao/get/user/get/list', chandao_api.ChandaoGetUserProjectList),  # 获取视野下的项目列表
    (r'/api/chandao/set/project/phase', chandao_api.ChandaoSetProjectPhase),  # 设置项目阶段里程碑
    (r'/api/chandao/get/project/phase', chandao_api.ChandaoGetProjectPhase),  # 获取项目里程碑
    (r'/api/chandao/project/phase/update', chandao_api.ChandaoProjectPhaseUpdate),  # 更新项目里程碑
    (r'/api/chandao/project/share/binding', chandao_api.ChandaoProjectShareBinding),  # 项目分享绑定
    (r'/api/chandao/project/share/binding/get/list', chandao_api.ChandaoProjectShareBindingGetList),  # 查询分享的人-甲方的人
    (r'/api/chandao/project/team/get/list', chandao_api.ChandaoProjectTeamGetList),  # 查询项目团队

    # 客户任务
    (r'/api/chandao/add/project/customer/plan', chandao_api.ChandaoAddProjectCustomerPlan),  # 设置客户需要客户协助的计划
    (r'/api/chandao/get/project/customer/plan', chandao_api.ChandaoGetProjectCustomerPlan),  # 获取客户需要客户协助的计划
    (r'/api/chandao/update/project/customer/plan', chandao_api.ChandaoUpdateProjectCustomerPlan),  # 更新完成时间
    (r'/api/chandao/add/project/customer/plan/track/record', chandao_api.ChandaoAddProjectCustomerPlanTrackRecord),  # 添加跟踪记录
    (r'/api/chandao/get/project/customer/plan/track/record', chandao_api.ChandaoGetProjectCustomerPlanTrackRecord),  # 查询跟踪记录

    # 项目风险
    (r'/api/chandao/add/project/risk', chandao_api.ChandaoAddProjectRisk),  # 新增项目风险
    (r'/api/chandao/get/project/risk', chandao_api.ChandaoGetProjectRisk),  # 查询项目风险
    (r'/api/chandao/update/project/risk', chandao_api.ChandaoUpdateProjectRisk),  # 更新项目风险完成时间
    (r'/api/chandao/add/project/risk/track/record', chandao_api.ChandaoAddProjectRiskTrackRecord),  # 新增风险跟踪记录
    (r'/api/chandao/get/project/risk/track/record', chandao_api.ChandaoGetProjectRiskTrackRecord),  # 查询风险跟踪记录
    (r'/api/chandao/get/project/risk/first', chandao_api.ChandaoGetProjectRiskFirst),  # 查询项目风险

    # 项目任务
    (r'/api/chandao/project/task/get/list', chandao_api.ChandaoProjectTaskGetList),  # 获取项目任务列表
    (r'/api/chandao/project/task/batch/add', chandao_api.ChandaoProjectTaskBatchAdd),  # 批量新增任务
    (r'/api/chandao/project/task/mandays/overrun', chandao_api.ChandaoProjectTaskMandaysOverrun),  # 人员工时超期提示 逻辑有点难，没实现
    (r'/api/chandao/project/task/delete', chandao_api.ChandaoProjectTaskDelete),  # 批量删除任务
    (r'/api/chandao/project/task/update/time', chandao_api.ChandaoProjectTaskUpdateTime),  # 更新实际时间
    (r'/api/chandao/project/task/responsible/confirmed', chandao_api.ChandaoProjectResponsibleConfirmed),  # 责任人确实任务完成
    (r'/api/chandao/project/task/pm/confirmed', chandao_api.ChandaoProjectPmConfirmed),  # 项目经理确认任务完成情况
    (r'/api/chandao/project/task/progress/add', chandao_api.ChandaoProjectTaskProgressAdd),  # 新增任务进度跟踪
    (r'/api/chandao/project/task/progress/get', chandao_api.ChandaoProjectTaskProgressGet),  # 查询任务进度跟踪
    (r'/api/chandao/project/task/progress/delete', chandao_api.ChandaoProjectTaskProgressDelete),  # 删除任务进度跟踪
    (r'/api/chandao/project/task/working/daily/get', chandao_api.ChandaoProjectTaskWorkingDailyGet),  # 工作日报查询
    (r'/api/chandao/project/task/working/daily/add', chandao_api.ChandaoProjectTaskWorkingDailyAdd),  # 工作日报
    (r'/api/chandao/make/wxacode', chandao_api.WeChatProjectWxacodeHandler),  # 获取项目专属二维码

    # 客户确认任务
    (r'/api/chandao/project/task/confirmed', chandao_api.ChandaoProjectTaskConfirmed),  # 批量确认任务-客户端

    # 项目信息
    (r'/api/chandao/project/information/update', chandao_api.ChandaoProjectInformationUpdate),  # 更新项目信息
    (r'/api/chandao/project/information/get', chandao_api.ChandaoProjectInformationGet),  # 查看项目信息

    # 实际人天统计
    (r'/api/chandao/project/calendar', chandao_api.ChandaoProjectCalendar),  # 月度日历
    (r'/api/chandao/project/calendar/update', chandao_api.ChandaoProjectCalendarUpdate),  # 修改日历工作人天
    (r'/api/chandao/project/actuality/add', chandao_api.ChandaoProjectActualityAdd),  # 新增人天统计
    (r'/api/chandao/project/actuality/edit', chandao_api.ChandaoProjectActualityEdit),  # 新增人天统计
    (r'/api/chandao/project/actuality/get/list', chandao_api.ChandaoProjectActualityGetList),  # 查询人天统计
    (r'/api/chandao/project/actuality/get/fisrt', chandao_api.ChandaoProjectActualityGetFirst),  # 查询人天统计
    (r'/api/chandao/project/actuality/get/month', chandao_api.ChandaoProjectActualityGetMonth),  # 月度报表
    (r'/api/chandao/project/actuality/get/month/export', chandao_api.ChandaoProjectActualityGetMonthExport),  # 月度报表导出

    (r'/api/get/sys/project/phase/configuration', working_daily_api.GetSysProjectPhaseConfiguration),  # 获取阶段列表
    (r'/api/project/task/working/daily/select', working_daily_api.ProjectTaskWorkingDailySelect),  # 查询是否已有工时汇报
    (r'/api/project/task/working/daily/add', working_daily_api.ProjectTaskWorkingDailyAdd),  # 新增工时汇报
    (r'/api/project/task/working/daily/edit', working_daily_api.ProjectTaskWorkingDailyEdit),  # 编辑工时汇报
    (r'/api/project/task/working/daily/get/list', working_daily_api.ProjectTaskWorkingDailyGetList),  # 获取工时汇报列表
    (r'/api/project/task/working/daily/get/first', working_daily_api.ProjectTaskWorkingDailyGetFirst),  # 获取工时汇报详情
    (r'/api/project/task/working/daily/export', working_daily_api.ProjectTaskWorkingDailyExport),  # 工时汇报导出
    (r'/api/project/task/working/daily/get/day', working_daily_api.ProjectTaskWorkingDailyGetDay),  # 按天汇总工时汇报
    (r'/api/project/task/working/daily/get/user', working_daily_api.ProjectTaskWorkingDailyGetUser),  # 用户维度，按项目分组统计工时汇报

    # 周工作汇报
    (r'/api/project/task/week/daily/add', working_daily_api.ProjectTaskWeekDailyAdd),  # 新增周工作汇报
    (r'/api/project/task/week/daily/select', working_daily_api.ProjectTaskWeekDailySelect),  # 查询是否已有工时汇报
    (r'/api/project/task/week/daily/get/list', working_daily_api.ProjectTaskWeekDailyGetList),  # 查询周工作汇报
    (r'/api/project/task/week/daily/get/first', working_daily_api.ProjectTaskWeekDailyGetFirst),  # 查询周工作汇报详情
    (r'/api/project/task/week/daily/update', working_daily_api.ProjectTaskWeekDailyUpdate),  # 更新周工作汇报
    (r'/api/project/task/week/daily/select/working/all', working_daily_api.ProjectTaskWeekDailySelectWorkingAll),  # 查询本人的指定时间段的所有工时汇报
    (r'/api/project/task/week/daily/evaluations', working_daily_api.ProjectTaskWeekDailyEvaluations),  # 周工作评价
    (r'/api/project/task/week/daily/get/week', working_daily_api.ProjectTaskWeekDailyGetWeek),  # 按周汇总工时汇报

    (r'/api/project/task/week/plan/add', working_daily_api.ProjectTaskWorkingPlanAdd),  # 新增项目周计划
    (r'/api/project/task/week/plan/get/list', working_daily_api.ProjectTaskWorkingPlanGetList),  # 查询项目周计划
    (r'/api/project/task/week/plan/get/first', working_daily_api.ProjectTaskWorkingPlanGetFirst),  # 查询项目周计划
    (r'/api/project/task/week/plan/update', working_daily_api.ProjectTaskWorkingPlanUpdate),  # 更新项目周计划
    (r'/api/project/task/week/plan/update/other', working_daily_api.ProjectTaskWorkingPlanUpdateOther),  # 查询项目总结，评价，等信息
    (r'/api/project/task/working/daily/get/list/project', working_daily_api.ProjectTaskWorkingDailyGetListProject),  # 按项目维度查询工时汇报
    (r'/api/project/task/working/audits', working_daily_api.ProjectTaskWorkingAudits),  # 工时审核
    (r'/api/project/task/working/unsubmit/user/list', working_daily_api.ProjectTaskWorkingUnsubmitUserList),  # 查看未提交工时的用户

    (r'/api/project/phases/add', working_daily_api.ProjectPhasesAdd),  # 新增项目阶段概要
    (r'/api/project/phases/update', working_daily_api.ProjectPhasesUpdate),  # 更新项目阶段概要
    (r'/api/project/phases/get/list', working_daily_api.ProjectPhasesGetList),  # 查询项目阶段
    (r'/api/project/phases/get/first', working_daily_api.ProjectPhasesGetFirst),  # 查询项目阶段
    (r'/api/project/phases/confirmed', working_daily_api.ProjectPhasesConfirmed),  # 概要确认
    (r'/api/project/week/plan/get/user/list', working_daily_api.ProjectWeekPlanGetUserList),  # 查询与我相关的周计划列表

    # 项目回款
    (r'/api/project/payback/add', project_payback_api.ProjectPaybackAdd),  # 新增项目回款
    (r'/api/project/payback/get/list', project_payback_api.ProjectPaybackGetList),  # 查询项目回款
    (r'/api/project/payback/get/first', project_payback_api.ProjectPaybackGetFirst),  # 查询项目回款详情
    (r'/api/project/payback/update', project_payback_api.ProjectPaybackUpdate),  # 更新项目回款
    (r'/api/project/payback/confirmed', project_payback_api.ProjectPaybackUpdateConfirmed),  # 回款确认

    # 报表
    (r'/api/project/progress/board', statistics_api.ProjectProgressBoard),  # 项目进度看板
    (r'/api/project/data/statisticians', statistics_api.ProjectDataStatisticians),  # 项目填报数据统计
    (r'/api/project/list/statisticians', statistics_api.ProjectStatisticians),  # 列表维度项目概况

    # 预留接口
    (r'/api/thirdparty/verify', thirdparty_probation_api.ThirdpartyVerifyHandler),  # 接口鉴权

    # ---------------- 前端路由 ----------------
    (r'/', frontend_api.IndexHandler),  # 首页
]
