# coding=utf-8
import os.path
import sys
import traceback
from datetime import datetime

import tornado.escape
import tornado.ioloop
import tornado.web
from tornado.options import define, options

from framework.log_controller import log
from framework.utilities import get_cmd_params, to_int
from framework.websockets.ws_register import Register
from setting import is_debug
from urls import GLOBAL_URLS


def main(urls, port):
    define("port", default=port, help="运行在%d端口" % (port), type=int)
    define("debug", default=is_debug, help="运行在调试模式")

    options.parse_command_line()

    app = tornado.web.Application(urls,
                                  cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
                                  # template_path=os.path.join(os.path.dirname(__file__), "static/src/views"),
                                  static_path=os.path.join(os.path.dirname(__file__), "static"),
                                  xsrf_cookie=True,
                                  debug=options.debug,
                                  gzip=True
                                  )
    app.register = Register()
    options.parse_command_line()
    app.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == '__main__':
    try:
        params = get_cmd_params(sys.argv)
        port = to_int(params.get('-port'))
        # port = 8500

        print(datetime.now())
        if not port:
            print('[失败]请指定端口')
        else:
            print('[成功]启动端口:%d' % (port))
            main(GLOBAL_URLS, port)
            print('[成功]停止端口:%d' % (port))
    except Exception as e:
        log(traceback.format_exc())
        print(e)
        input("Press any key to exit...")
        # 关闭程序
        sys.exit()

