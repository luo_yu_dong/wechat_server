#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: time_send_message_run.py
@time: 2020/10/23 14:30
@desc: 定时发消息
'''
import sched
import time
import traceback

import requests

from enums.enum_cache_type import EnumCacheType
from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.utilities import to_string, to_dict, to_int, current_timestamp
from models.exception_log.send_msg_log import SendMsgLog
from services.wechat_program_base import send_tem_post
from setting import WECHAT_WIKI_APPID, WECHAT_WIKI_SECRET
from thirdpart import redis_helper as re

schedule = sched.scheduler(time.time, time.sleep)


class TimeSendMessage():

    # @staticmethod
    def run(self):
        send_list = []
        with DbSession.create(EnumDb.main) as db:
            query = db.query(SendMsgLog).filter(SendMsgLog.send_ok == 0).filter(SendMsgLog.creat_time <= int(time.time()) + 60)
            for v in query:
                send_list.append(v.get_dict())

        if not send_list:
            return

        # 获取
        access_token_time = to_int(re.get(key='wiki_access_token_time', cache_type=EnumCacheType.sns))
        if access_token_time:
            now = current_timestamp()  # 当前时间戳
            if access_token_time - now > 200:
                access_token = re.get(key='wiki_access_token', cache_type=EnumCacheType.sns)
            else:
                access_token = self.get_access_token(WECHAT_WIKI_APPID, WECHAT_WIKI_SECRET)

                re.set(key='wiki_access_token_time', value=to_string(current_timestamp()), time=60 * 60 * 3, cache_type=EnumCacheType.sns)
                re.set(key='wiki_access_token', value=access_token, time=60 * 60 * 3, cache_type=EnumCacheType.sns)
        else:
            access_token = self.get_access_token(WECHAT_WIKI_APPID, WECHAT_WIKI_SECRET)
            re.set(key='wiki_access_token_time', value=to_string(current_timestamp()), time=60 * 60 * 3, cache_type=EnumCacheType.sns)
            re.set(key='wiki_access_token', value=access_token, time=60 * 60 * 3, cache_type=EnumCacheType.sns)

        update_list = []
        for v in send_list:
            data = to_dict(v['json'])

            url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=%s" % (to_string(access_token))

            rsp = send_tem_post(url, data)
            res = rsp.json()

            if to_string(res.get('errcode'), '') == '0':
                logs = {
                    'id': v['id'],
                    'send_ok': 1,
                    'error_msg': {
                        'errcode': '',
                        'errmsg': '',
                        'msgid': res.get('msgid')
                    }
                }
            else:
                if res.get('errcode') == 43004:
                    send_ok = 1
                else:
                    send_ok = 0
                logs = {
                    'id': v['id'],
                    'send_ok': send_ok,
                    'error_msg': {
                        'errcode': res.get('errcode'),
                        'errmsg': to_string(res.get('errmsg'), ''),
                        'msgid': res.get('msgid')
                    }
                }
            logs['error_msg'] = to_string(logs['error_msg'])
            print('========================================================================================================')
            print(logs)
            update_list.append(logs)

        if update_list:
            with DbSession.create(EnumDb.main) as db:
                db.bulk_update_mappings(SendMsgLog, update_list)

        return

    @classmethod
    def get_access_token(self, appid, secret):
        req_result = requests.get('https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s' %
                                  (to_string(appid), to_string(secret)))

        access_token = req_result.json()['access_token']

        return access_token


def perform_command(inc):
    # 安排inc秒后再次运行自己，即周期运行
    schedule.enter(inc, 0, perform_command, argument=(inc,))

    # ------------执行脚本---------------------------------------
    try:
        print('-------- 开始 ---------')
        tsm = TimeSendMessage()
        tsm.run()
        print('-------- 结束 ---------')
    except:
        print(traceback.format_exc())
        # pass


if __name__ == '__main__':
    inc = 60  # 1分钟
    schedule.enter(inc, 0, perform_command, (inc,))
    schedule.run()