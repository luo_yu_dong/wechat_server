#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: file_cleaner_run.py
@time: 2020/1/14 10:55
@desc: 清除无用文件
'''
import datetime
import os
import sched
import time

from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.log_controller import log
from models.file.file import File

schedule = sched.scheduler(time.time, time.sleep)


class FileCleaner():
    def __init__(self):
        pass

    @classmethod
    def empty_file(cls):
        '''
        清空回收站
        '''
        try:
            with DbSession.create(EnumDb.main) as db:
                delete_ids = []
                files = db.query(File).filter(File.is_temporary == 1)
                for v in files:
                    difference_time = datetime.datetime.now() - v.time
                    difference_sec = difference_time.days * 24 * 3600 + difference_time.seconds
                    if difference_sec > 60 * 60 * 24:  # 1天才清除
                        delete_ids.append(v.id)
                        try:
                            os.remove(os.path.join(v.path, v.file_name))
                        except Exception as e:
                            log(e)
                            continue

            with DbSession.create(EnumDb.main) as db:
                db.query(File).filter(File.id.in_(delete_ids)).delete(synchronize_session='fetch')
        except Exception as e:
            log(e)


def perform_command(inc):
    # 安排inc秒后再次运行自己，即周期运行
    schedule.enter(inc, 0, perform_command, argument=(inc,))

    # ------------执行脚本---------------------------------------
    try:
        # print('清楚文件开始')
        FileCleaner.empty_file()
        # print('清除文件结束')
    except:
        pass


if __name__ == '__main__':
    inc = 60  # 1分钟
    schedule.enter(inc, 0, perform_command, (inc,))
    schedule.run()
