# coding=utf-8
'''
author:LYD
createdate:2020-10-19
description:权限检查是忽略的url(小写)
'''
WHITE_URL_LIST = (
    r'/api/file/add',
    r'/api/user/login',
    r'/*.txt',
    r'/api/work/server/form/reply',
    r'/api/work/server/form/allocation/bach',
    r'/api/work/server/form/reply/desc',
    r'/api/chandao/get/sys/project/phase/configuration'
)

WHITE_URL_LIST_PREFIX = {
    r'/api/wechat/',
    r'/api/lingdang/'
    r'/api/chandao/'
}
