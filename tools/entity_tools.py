#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 智慧桥梁实体表生成工具
@time: 2020/01/02 11:53
@desc:
'''
import os
import platform
from datetime import datetime

import pymysql

from enums.enum_db import get_session_enumdb
from framework.utilities import get_len, to_string

conn = pymysql.connect(host='124.71.184.87', port=3306, user='root', passwd='qianxing#123#@!', db='information_schema', charset='utf8')

# 禅道链接
# conn = pymysql.connect(host=chandao_SERVER_HOST, port=3366, user='root', passwd=chandao_SERVER_HOST_PASS, db='information_schema', charset='utf8')
_tables = {}
_dir = ''

_datatable = 'main'
# _datatable = 'zentao'
creat_name = 'lyd'


def get_tables():
    '''
    获取数据库中所有的表
    '''
    try:
        cur = conn.cursor()
        cur.execute("select TABLE_NAME, TABLE_COMMENT from TABLES WHERE TABLE_SCHEMA='" + _datatable + "' order by TABLE_NAME")
        print('表名:')
        for v in cur:
            _tables[v[0]] = v[1]
            print(v[0])
        cur.close()

        print('请输入需要生成的表,输入-q退出')
        tb = input('')
        while tb != '-q':
            cur = conn.cursor()

            if not tb or not tb in _tables:
                print('表名错误,请重新输入')
            else:

                sql = "select COLUMN_NAME,ORDINAL_POSITION,IS_NULLABLE,DATA_TYPE,COLUMN_TYPE,COLUMN_KEY,COLUMN_COMMENT from columns  where table_schema='" + _datatable + "' and TABLE_NAME ='" + tb + "'"
                cur.execute(sql)

                write_to_file(cur, tb, _tables[tb])
                cur.close()
                print('完成表到实体的转换')
            tb = input('')
        print('退出')
    except:
        print('异常=>')
    finally:
        conn.close()


def write_to_file(row, tb, tbdesc):
    '''   -----------------------写入文件--------------------------------------   '''
    if (platform.system() == 'Linux'):
        path = _dir + r'/' + tb + '.py'
    else:
        path = _dir + '\\' + tb + ".py"
    f = open(path, 'w', encoding='utf-8')
    f.write('#!/usr/bin/env python\n')
    f.write('# -*- coding:utf-8 -*-\n')
    f.write("'''\n")
    f.write('@author:%s\n' % (to_string(creat_name)))
    f.write('@software: 实体表\n')
    f.write('@time:%s\n' % (to_string(datetime.now())[:19]))
    f.write('@desc:\n')
    f.write("'''\n")
    f.write('\n')
    f.write('from datetime import datetime')
    f.write('\n')
    f.write('import time')
    f.write('\n')
    f.write('from sqlalchemy.orm import relationship, backref, sessionmaker')
    f.write('\n')
    f.write('from sqlalchemy.ext.declarative import declarative_base')
    f.write('\n')
    f.write('from sqlalchemy import Column, Integer, String, DateTime, Numeric, Boolean, BigInteger, Float, TEXT, Date')
    f.write('\n')
    f.write('from enums.enum_db import EnumDb')
    f.write('\n')
    f.write('BaseOrmClass = declarative_base()')
    f.write('\n')
    f.write('class ')
    f.write(tb.title().replace('_', ""))
    f.write('(BaseOrmClass):')
    f.write('\n')
    f.write("    '''\n")
    f.write('    模型(表)备注:\n')
    f.write(tbdesc)
    f.write('\n')
    f.write("    '''")
    f.write('\n')
    f.write("    __tablename__ = '")
    f.write(tb + "'")
    f.write('\n\n')
    f.write("    __table_args__ = {'schema': '%s'} " % (to_string(_datatable)))
    f.write('\n\n')

    reprs = []
    dicstr = []
    setstr = []
    for v in row:
        item = '    %s = Column(%s' % (v[0], getType(v[4]),)
        setstr.append('self.%s = obj.get(\'%s\')' % (v[0], v[0]))
        if v[5] == 'PRI':
            item += ', primary_key=True'
            comment = "    '''   " + v[6] + "   '''"
            dicstr.append("\n                '%s': self.%s" % (v[0], v[0]))
        elif v[2] == 'NO':
            item += ', nullable=False'
            comment = "    '''   " + v[6] + "   '''"
            reprs.append("%s='{self.%s}'" % (v[0], v[0]))

            dicstr.append("\n                '%s': self.%s" % (v[0], v[0]))
        else:
            comment = "    '''   " + v[6] + "   '''"
            dicstr.append("\n                '%s': self.%s" % (v[0], v[0]))
        item += ')'
        f.write(comment)
        f.write('\n')
        f.write(item)
        f.write('\n\n')

    '''   构造__repr__函数   '''
    if (get_len(reprs) > 0):
        f.write('\n')
        f.write('    def __repr__(self):')
        f.write('\n')
        f.write("        return \"")
        f.write(tb)
        f.write("(")
        f.write(','.join(reprs))
        f.write(")\"")
        f.write('.format(self=self)')

    '''   构造字典函数   '''
    f.write('\n\n')
    f.write('    def get_dict(self):')
    f.write('\n')
    f.write("        return ")
    f.write('{')
    f.write(','.join(dicstr))
    f.write('\n                }\n')
    """------------添加set_obj函数 """
    f.write('\n')
    f.write('    def set_object(self, obj={}):')
    f.write('\n')
    for v in setstr:
        f.write('        ' + v)
        f.write('\n')

    f.write('\n\n')
    f.write('    @staticmethod')
    f.write('    \n')
    f.write('    def db_map():')
    f.write("    \n")
    f.write('        return {%s: EnumDb.%s}' % (tb.title().replace('_', ""), get_session_enumdb(_datatable)))

    '''   -----------------------写入文件--------------------------------------   #endregion '''
    f.close()


def getType(tp):
    t = tp.split('(')
    if (get_len(t) > 1):
        t[1:2] = t[1].split(')')
    if t[0] == 'varchar':
        return 'String(%s)' % t[1]
    if t[0] == 'datetime':
        return 'DateTime(), default=datetime.now'
    if t[0] == 'int':
        return "Integer()"
    if t[0] == 'decimal':
        return 'Numeric(' + (t[1]) + ')'
    if t[0] == 'char':
        return 'String(%s)' % t[1]
    if t[0] == 'bit':
        return 'Boolean()'
    if t[0] == 'bigint':
        return 'BigInteger()'
    if t[0] == 'varbinary':
        return 'Binary(%s)' % t[1]
    if t[0] == 'text':
        return 'TEXT()'
    if t[0] == 'mediumtext':
        return 'TEXT()'
    if t[0] == 'double':
        return 'Numeric(' + (t[1]) + ')'
    if t[0] == 'float':
        return 'Numeric(' + (t[1]) + ')'
    if t[0] == 'date':
        return 'Date()'
    return tp


if __name__ == "__main__":
    system = platform.system()
    if system == 'Linux':
        _dir = r'/home/shinvi/models'
        if not os.path.exists(_dir):
            os.mkdir(_dir)
    else:
        _dir = r'E:/createdmodel'
        if not os.path.exists(_dir):
            os.mkdir(_dir)
    print('存储路径为=>', _dir)
    get_tables()
