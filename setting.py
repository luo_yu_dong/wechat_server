#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: setting.py
@time: 2019/12/30 11:56
@desc:
'''
'''
小程序账号：3500522004@qq.com；密码：dajian@0313
公众号账号: wangyang@qianxingit.com 密码：qianxing
开放平台账号：2959118205@qq.com 密码：dajian@0313
'''

SERVER_TYPE = 'beta'  # 服务器类型 std-正式服务器  beta -测试
# SERVER_TYPE = 'std'

MONITOR_DB_ACCESS_TIME = True

# 人天工时基准
MAN_DAY_BASIS = 8

# 禅道path
CHANDAO_SERVER_HOST = 'http://119.167.207.13:81/zentao/api.php/v1'
CHANDAO_USERNAME = 'admin'
CHANDAO_PASSWORD = 'QXstrong123#@!'

# beta
# FILE_CACHE = r'/home/data/imgs'
# EXCEL_CACHE = r'/home/data/excel'  # excel导入

# localhost
FILE_CACHE = r'E:\QXfiles\imgs'  # 文件
EXCEL_CACHE = r'E:\QXfiles\excel'  # excel导入

DOMAIN_CURRENT = r'https://wechatserver.qianxingit.com'  # 当前域名

SERVER_HOST = '119.167.207.14'
SERVER_HOST_PASS = 'qianxing123#@!'
SERVER_IMGS_POST = 'https://wechatserver.qianxingit.com/images/'

chandao_SERVER_HOST = '117.78.51.143'
chandao_SERVER_HOST_PASS = 'QXstrong123'

# 公众号appid
WECHAT_WIKI_APPID = 'wxeb8790122e9314fa'
WECHAT_WIKI_SECRET = 'c03dcd275d3009f1d842df919524a69f'
# 小程序appid
WECHAT_PRO_APPID = 'wx2017fd04a28e0853'
WECHAT_PRO_SECRET = '838a49ac92b96892cda0c29b0aedd9f4'
# 消息模板
WECHAT_NEW_FORM_TO_WORK_TEMPLATE_ID = 'mH2HlylVRafinVRc2vRVuy2_5rGLYBYVpzVo91Vwxmo'  # 新创建的服务单，给客服发消息的模板
WECHAT_SCORE_TO_WORK_TEMPLATE_ID = 'P_W8v0yEaR7ECoGzAGAg7I8kO41COzfAQXJArYnKi_8'  # 给单据打分，发给客服的消息模板
WECHAT_COMPLETE_FORM_TO_CUSTOMER_TEMPLATE_ID = 'OInC3fbNLFNEaF3eNq4ELC6arOFIMcoYqIX0BOpJt0Q'  # 服务单受理后的结果，给客户发‘消息的模板
WECHAT_ASSIGNING_JOB_TEMPLATE_ID = '4_5s-yKT7hG_2l46oRg--Q0xOyYsZjmgVizn-fND5Tg'  # 分配服务单
WECHAT_EXPOSURES_TEMPLATE_ID = 'dChPRoD7eERdix7MEBUWsFnY8vxo6GS5yCbt40aQ4yg'  # 风险问题通知

WECHAT_VERIFY = '/home/server/wx_verify/'  # 测试服务器
# WECHAT_VERIFY = r'E:\QXfiles\wx_verify\'  # 本地服务器'

SERVER_ID = 1
is_debug = True

FILE_ACCESS_HEADER = 'asr(!)*K)!P&'

dbsetting = {
    'main': 'mysql+pymysql://root:qianxing#123#@!@119.167.207.14:3306/main?charset=utf8',
    'chandao': 'mysql+pymysql://QXadmin:QXstrong123@119.167.207.13:3366/zentao?charset=utf8'
}

redisConfig = {
    'connection_settings': {'host': '119.167.207.14', 'port': 6379, 'password': 'luoyudong_redis_123#@!', 'db': 0},
    'log_connection_settings': {'host': '119.167.207.14', 'port': 6379, 'password': 'luoyudong_redis_123#@!', 'db': 1},
    'sns_setting': {'host': '119.167.207.14', 'port': 6379, 'password': 'luoyudong_redis_123#@!', 'db': 2},
}
