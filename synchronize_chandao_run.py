#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@software: 实体表
@file: synchronize_chandao_run.py
@software: AT_server
@time: 2024/6/20 下午4:55
@author:lyd
@email: 339789089@qq.com
定时同步数据至erp
'''
import sched
import time
import traceback

from framework.log_controller import log
from services.chandao_manage import ChandaoManage

schedule = sched.scheduler(time.time, time.sleep)


def perform_command(inc):
    # 安排inc秒后再次运行自己，即周期运行
    schedule.enter(inc, 0, perform_command, argument=(inc,))

    # ------------执行脚本---------------------------------------
    try:
        print('-------- 开      始 synchronize_erp_manager  ---------')
        cdm = ChandaoManage({'user_id': 1, 'user_name': 'super_admin'})
        res = cdm.save_project()
        print('-------- 结      束 synchronize_erp_manager  ---------')
    except:
        log(traceback.format_exc(), '定时任务.Kingdee.synchronize_erp_manager')
        print(traceback.format_exc())
        pass


if __name__ == '__main__':
    inc = 600  # 10分钟
    schedule.enter(inc, 0, perform_command, (inc,))
    schedule.run()
