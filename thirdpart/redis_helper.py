#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: redis
@file: redis_helper.py
@time: 2019/12/30 12:04
@desc:
'''
import json

import redis

import setting
from enums.enum_cache_type import EnumCacheType


class RedisClient():
    __pool__ = None
    __log_pool__ = None
    __sns_pool__ = None

    @classmethod
    def client(cls, cache_type=EnumCacheType.normal):
        if cache_type == EnumCacheType.normal:
            return cls.__get_pool_conn()

        if cache_type == EnumCacheType.log:
            return cls.__get_log_pool_conn()

        if cache_type == EnumCacheType.sns:
            return cls.__get_sns_pool_conn()

        return None

    @classmethod
    def __get_log_pool_conn(cls):
        '''
        获取日志缓存的连接
        :return:
        :rtype:
        '''
        if not cls.__log_pool__:
            cls.__log_pool__ = redis.ConnectionPool(**setting.redisConfig['log_connection_settings'])
        return redis.Redis(connection_pool=cls.__log_pool__)

    @classmethod
    def __get_pool_conn(cls):
        '''
        获取缓存连接
        :return:
        :rtype:
        '''
        if not cls.__pool__:
            cls.__pool__ = redis.ConnectionPool(**setting.redisConfig['connection_settings'])
        return redis.Redis(connection_pool=cls.__pool__)

    @classmethod
    def __get_sns_pool_conn(cls):
        '''
        获取即时通讯的缓存连接池
        '''
        if not cls.__sns_pool__:
            cls.__sns_pool__ = redis.ConnectionPool(**setting.redisConfig['sns_setting'])
        return redis.Redis(connection_pool=cls.__sns_pool__)


def set(key, value, time, cache_type=EnumCacheType.normal):
    try:
        if isinstance(value, (int, float)):
            RedisClient.client(cache_type).setex(key, time, str(value))  # 更新redis到3.xx版本，3.xx里的setex参数顺序变了 by jgp 20191108
            return
        if isinstance(value, str):
            RedisClient.client(cache_type).setex(key, time, value)
            return
        if isinstance(value, (dict, list)):
            RedisClient.client(cache_type).setex(key, time, json.dumps(value))
            return
        else:
            res = json.dumps(value.__dict__)
            RedisClient.client(cache_type).setex(key, time, res)
    except Exception as e:
        print(e)
        pass


def get(key, cache_type=EnumCacheType.normal):
    try:
        bts = RedisClient.client(cache_type).get(key)
        if not bts:
            return None

        value = bts.decode(encoding='utf-8')
        try:
            return json.loads(value, encoding='utf-8')
        except:
            return value
    except:
        return None


def incr(key, cache_type=EnumCacheType.normal):
    '''
    递增key的值,+1
    :param key:
    :type key:
    :param cache_type:
    :type cache_type:
    :return:
    :rtype:
    '''
    try:
        return RedisClient.client(cache_type).incr(key)
    except:
        return 0


def delete(key, cache_type=EnumCacheType.normal):
    try:
        RedisClient.client(cache_type).delete(key)
    except:
        pass


def decr(key, cache_type=EnumCacheType.normal):
    '''
    递key的值,-1
    :param key:
    :type key:
    :param cache_type:
    :type cache_type:
    :return:
    :rtype:
    '''
    try:
        return RedisClient.client(cache_type).decr(key)
    except:
        return 0


class RedisSet():
    '''
    reddis集合管理
    '''

    @classmethod
    def add(cls, set_name, values, cache_type=EnumCacheType.normal):
        '''
        向集合添加元素
        :param name:
        :type name:
        :param values:
        :type values:
        :return:
        :rtype:
        '''

        try:
            return RedisClient.client(cache_type).sadd(set_name, values)
        except:
            return 0

    @classmethod
    def remove(cls, set_name, values, cache_type=EnumCacheType.normal):
        '''
        从集合set_name中移除values
        :param set_name:
        :type set_name:
        :param values:
        :type values:
        :return:
        :rtype:
        '''
        try:
            return RedisClient.client(cache_type).srem(set_name, values)
        except:
            return 0

    @classmethod
    def has_key(cls, set_name, values, cache_type=EnumCacheType.normal):
        '''
        结合set_name是否包含values
        :param set_name:
        :type set_name:
        :param values:
        :type values:
        :return:
        :rtype:
        '''
        try:
            return RedisClient.client(cache_type).sismember(set_name, values)
        except:
            return False

    @classmethod
    def delete_all_set(cls, set_name, cache_type=EnumCacheType.normal):
        '''
        删除整个集合
        :param set_name:
        :type set_name:
        :return:
        :rtype:
        '''
        try:
            return RedisClient.client(cache_type).delete(set_name)
        except:
            return 0


class RedisList():
    @classmethod
    def length(cls, list_name, cache_type):
        '''
        添加到list头(左边)
        :param list_name: 列表的名称
        :type list_name:str
        :param values:
        :type values:
        :return:
        :rtype:
        '''
        try:
            return RedisClient.client(cache_type).llen(list_name)
        except:
            return 0

    @classmethod
    def add_to_head(cls, list_name, values, cache_type=EnumCacheType.normal):
        '''
        添加到list头(左边)
        :param list_name: 列表的名称
        :type list_name:str
        :param values:
        :type values:
        :return:
        :rtype:
        '''
        try:
            return RedisClient.client(cache_type).lpush(list_name, values)
        except:
            return 0

    @classmethod
    def append_to_bottom(cls, list_name, values, cache_type=EnumCacheType.normal):
        '''
        添加到list尾(右边)
        :param list_name: 列表的名称
        :type list_name:str
        :param values:
        :type values:
        :return:
        :rtype:
        '''
        try:
            return RedisClient.client(cache_type).rpush(list_name, values)
        except:
            return 0

    @classmethod
    def pop_from_head(cls, list_name, cache_type=EnumCacheType.normal):
        '''
        从list头弹出第一个元素(从左端弹出第一个元素)
        :param list_name: 列表的名称
        :type list_name:str
        :return:
        :rtype:
        '''
        try:
            return RedisClient.client(cache_type).lpop(list_name)
        except:
            return 0

    @classmethod
    def pop_from_bottom(cls, list_name, cache_type=EnumCacheType.normal):
        '''
        从list尾弹出第一个元素(从右端弹出第一个元素)
        :param list_name: 列表的名称
        :type list_name:str
        :return:
        :rtype:
        '''
        try:
            return RedisClient.client(cache_type).rpop(list_name)
        except:
            return 0

    @classmethod
    def get(cls, list_name, position_index=0, cache_type=EnumCacheType.normal):
        '''
        从指定位置获取一个元素
        :param list_name: 列表的名称
        :type list_name:str
        :param values:
        :type values:
        :return:
        :rtype:
        '''
        try:
            return RedisClient.client(cache_type).lindex(list_name, position_index)
        except:
            return None

    @classmethod
    def get_many(cls, list_name, position_start=0, position_end=0, cache_type=EnumCacheType.normal):
        '''
        获取指定开始位置和结束位置之间的列表(片段)
        :param list_name: 列表的名称
        :type list_name:str
        :param values:
        :type values:
        :return:
        :rtype:
        '''
        try:
            return RedisClient.client(cache_type).lrange(list_name, position_start, position_end)
        except:
            return []

    @classmethod
    def get_all(cls, list_name, cache_type=EnumCacheType.normal):
        '''
        获取整个里列表
        :param list_name: 列表的名称
        :type list_name:str
        :return:
        :rtype:
        '''
        try:
            return RedisClient.client(cache_type).lrange(list_name, 0, -1)
        except:
            return []

    @classmethod
    def delete_all_list(cls, list_name, cache_type=EnumCacheType.normal):
        '''
        删除整个集合
        :param list_name: 列表的名称
        :type list_name:str
        :return:
        :rtype:
        '''
        try:
            return RedisClient.client(cache_type).delete(list_name)
        except:
            return 0


class RedisHash():
    @classmethod
    def __to_str(cls, obj, failure_replace_value=None):
        '''
        将对象转换为字符串
        :param obj:带转换的类型
        :type obj:
        :param failure_replace_value:转换失败时代替的类型
        :type failure_replace_value: str
        :param strip_all_blank:是否移除所有空格 ,默认不移除
        :type strip_all_blank: bool
        :return:返回值
        :rtype: str
        '''
        try:
            if obj is None:
                return failure_replace_value

            if isinstance(obj, str):
                new_str = obj
            elif isinstance(obj, bytes):
                new_str = obj.decode(encoding='utf-8')
            elif isinstance(obj, dict):
                new_str = json.dumps(obj)
            elif isinstance(obj, list):
                new_str = json.dumps(obj)
            else:
                new_str = str(obj)

            return new_str
        except:
            return failure_replace_value

    @classmethod
    def add(cls, main_key, sub_key, values, cache_type=EnumCacheType.normal):
        '''
        在mani_key中添加 sub_key:values
        :param main_key: 主hash 键
        :type main_key: str
        :param sub_key: 明细 hash 键
        :type sub_key: str
        :param values: 明细 hash 对应的值
        :type values: str
        :return:
        :rtype:
        '''
        try:
            return RedisClient.client(cache_type).hset(main_key, sub_key, values)
        except:
            return 0

    @classmethod
    def get(cls, main_key, sub_key, cache_type=EnumCacheType.normal):
        '''
        在mani_key中添加 sub_key:values
        :param main_key: 主hash 键
        :type main_key: str
        :param sub_key: 明细 hash 键
        :type sub_key: str
        :return:
        :rtype:
        '''
        try:
            return cls.__to_str(RedisClient.client(cache_type).hget(main_key, sub_key))
        except:
            return None

    @classmethod
    def get_all(cls, main_key, cache_type=EnumCacheType.normal):
        '''
        在mani_key中添加 sub_key:values
        :param main_key: 主hash 键
        :type main_key: str
        :return:
        :rtype:
        '''
        try:
            result = RedisClient.client(cache_type).hgetall(main_key)

            dic = {}
            for v in result:
                dic[cls.__to_str(v)] = cls.__to_str(result[v])
            return dic
        except:
            return {}

    @classmethod
    def remove(cls, main_key, sub_keys, cache_type=EnumCacheType.normal):
        '''
        在mani_key中添加 sub_key:values
        :param main_key: 主hash 键
        :type main_key: str
        :param sub_key: 明细 hash 键
        :type sub_key: str
        :return:
        :rtype:
        '''
        try:
            return RedisClient.client(cache_type).hdel(main_key, sub_keys)
        except:
            return 0

    @classmethod
    def delete_all_hash(cls, main_key, cache_type=EnumCacheType.normal):
        '''
        删除整个集合
        :param list_name: 列表的名称
        :type list_name:str
        :return:
        :rtype:
        '''
        try:
            return RedisClient.client(cache_type).delete(main_key)
        except:
            return 0


if __name__ == '__main__':
    # RedisHash.add(1,2,3)
    res = incr('cmd')
    print(res)
