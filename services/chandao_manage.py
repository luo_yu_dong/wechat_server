#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: chandao_manage.py
@time: 2024/03/14 10:15
@desc:
'''
import calendar
import datetime
import json
import os
import traceback
from datetime import timedelta
from urllib.parse import quote

import requests
import xlwt
from sqlalchemy import desc, func, and_
from xlwt import easyxf

from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.log_controller import log
from framework.msg import failure, ok
from framework.utilities import to_dict, to_string, format_dict, to_list, to_int, to_decimal, to_timestamp, to_datetime, get_workdays, get_quanpin, get_FileSize, get_len, \
    current_timestamp
from models.chandao.calendar import Calendar
from models.chandao.chandao_project import ChandaoProject
from models.chandao.chandao_project_customer_plan import ChandaoProjectCustomerPlan
from models.chandao.chandao_project_customer_plan_track_record import ChandaoProjectCustomerPlanTrackRecord
from models.chandao.chandao_project_payback import ChandaoProjectPayback
from models.chandao.chandao_project_phase import ChandaoProjectPhase
from models.chandao.chandao_project_risk import ChandaoProjectRisk
from models.chandao.chandao_project_risk_track_record import ChandaoProjectRiskTrackRecord
from models.chandao.chandao_project_share_binding import ChandaoProjectShareBinding
from models.chandao.chandao_project_summary_phases import ChandaoProjectSummaryPhases
from models.chandao.chandao_project_team_user import ChandaoProjectTeamUser
from models.chandao.chandao_project_week_plan import ChandaoProjectWeekPlan
from models.chandao.original.zt_task import ZtTask
from models.chandao.original.zt_team import ZtTeam
from models.chandao.project_actuality import ProjectActuality
from models.chandao.project_actuality_detail import ProjectActualityDetail
from models.chandao.project_information import ProjectInformation
from models.chandao.project_task import ProjectTask
from models.chandao.project_task_progress import ProjectTaskProgress
from models.chandao.sys_project_phase_configuration import SysProjectPhaseConfiguration
from models.chandao.work_reporting import WorkReporting
from models.exception_log.send_msg_log import SendMsgLog
from models.file.file import File
from models.task_working.project_task_working_daily_detail import ProjectTaskWorkingDailyDetail
from models.task_working.project_task_working_week_daily_detail import ProjectTaskWorkingWeekDailyDetail
from models.task_working.project_task_working_week_daily_next_detail import ProjectTaskWorkingWeekDailyNextDetail
from models.users.user import User
from models.users.wechat_user import WechatUser
from setting import CHANDAO_USERNAME, CHANDAO_PASSWORD, CHANDAO_SERVER_HOST, DOMAIN_CURRENT, WECHAT_PRO_APPID, WECHAT_EXPOSURES_TEMPLATE_ID, MAN_DAY_BASIS, \
    EXCEL_CACHE, SERVER_HOST


class ChandaoManage():

    def __init__(self, user, token=None):
        self.token = token
        self.user = user
        self.user_id = user['user_id']
        self.user_name = user['user_name']

    # 公共post方法
    def chandao_post(self, params, url, method_name):
        try:
            res = requests.post(url=url, data=json.dumps(params))

            status_code = res.status_code
            if status_code not in [200, 201]:
                error_msg = to_dict(res.content).get('error')
                return failure(msg=error_msg)

            result = to_dict(res.content)

            return ok(data=result)
        except:
            log(e=traceback.format_exc(), except_position=method_name, option_value='调取禅道接口失败(post)!')
            return failure(msg='调取禅道接口失败(post)!')

    # 公共get方法
    def chandao_get(self, url, method_name, headers={}, params=None):
        try:
            token_result = self.chandao_token()
            if not token_result.is_ok():
                return failure(msg='获取token失败')

            headers['Token'] = self.token

            if params:
                res = requests.get(url=url, params=json.dumps(params), headers=headers)
            else:
                res = requests.get(url=url, headers=headers)

            status_code = res.status_code
            if status_code not in [200, 201]:
                error_msg = to_dict(res.content).get('error')
                return failure(msg=error_msg)

            result = to_dict(res.content)

            return ok(data=result)
        except:
            log(e=traceback.format_exc(), except_position=method_name, option_value='调取禅道接口失败(get)!')
            return failure(msg='调取禅道接口失败(get)!')

    def chandao_token(self):
        try:
            params = {
                "account": CHANDAO_USERNAME,  # 管理员账号
                "password": CHANDAO_PASSWORD  # 管理员密码
            }
            token_url = CHANDAO_SERVER_HOST + '/tokens'

            result = self.chandao_post(params=params, url=token_url, method_name='ChandaoManage.chandao_token')
            if not result.is_ok():
                return failure(msg=result.msg)

            result_data = result.data
            token = result_data.get('token')

            self.token = token

            return ok(data={'token': token})
        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_token', '获取禅道token失败!')
            return failure(msg='获取禅道token失败!')

    # 获取项目集
    def chandao_get_porject_list(self):
        try:
            page = '1'
            limit = '9999'

            token_url = CHANDAO_SERVER_HOST + '/projects?page=%s&limit=%s&' % \
                        (to_string(page), to_string(limit))

            result = self.chandao_get(url=token_url, method_name='ChandaoManage.chandao_get_porject_list')

            if not result.is_ok():
                return failure(msg=result.msg)

            return ok(data=result.data.get('projects'))
        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_token', '获取项目失败!')
            return failure(msg='获取项目失败!')

    # 保存项目及团队信息到数据库
    def save_project(self):
        '''
        项目状态(wait 未开始 | doing 进行中 | suspend 已挂起 | closed 已关闭)

        :return:
        '''
        try:
            # 查询进行中的项目
            project_result = self.chandao_get_porject_list()
            if not project_result.is_ok():
                return failure(msg=project_result.msg)

            if not project_result.data:
                return ok()

            project_list = project_result.data

            # 查询现有的项目信息
            with DbSession.create(EnumDb.main) as db:
                now_project_list = [v[0] for v in db.query(ChandaoProject.chandao_project_id)]

            add_team_user_list = []
            ls = []
            chandao_project_id = []
            chandaoOldProjectId_account = {}
            account_list = []
            update_project = {}

            for obj in project_list:
                if obj.get('id') not in now_project_list:
                    item = ChandaoProject()
                    item.chandao_project_id = obj.get('id')
                    item.project_name = obj.get('name')
                    item.begin = obj.get('begin')
                    item.end = obj.get('end')
                    item.status = obj.get('status')
                    item.budget = to_decimal(obj.get('budget'))
                    item.code = obj.get('code')
                    ls.append(item)

                else:
                    update_project[obj.get('id')] = {
                        'project_name': obj.get('name'),
                        'begin': obj.get('begin'),
                        'end': obj.get('end'),
                        'status': obj.get('status'),
                        'budget': to_decimal(obj.get('budget')),
                        'code': obj.get('code')
                    }

                chandao_project_id.append(obj.get('id'))

                PM = obj.get('PM')
                if PM:
                    PM['position'] = 'PM'
                    PM['chandao_old_project_id'] = obj.get('id')
                    add_team_user_list.append(PM)
                    chandaoOldProjectId_account[obj.get('id')] = PM['account']
                    account_list.append(PM['account'])

            with DbSession.create(EnumDb.main) as db:
                # 批量插入
                db.bulk_save_objects(ls)
                db.commit()

                db.query(ChandaoProjectTeamUser).filter(ChandaoProjectTeamUser.chandao_old_project_id.in_(chandao_project_id)).delete(synchronize_session='fetch')

                newid_oldid_dist = {}
                query = db.query(ChandaoProject.id, ChandaoProject.chandao_project_id).filter(ChandaoProject.chandao_project_id.in_(chandao_project_id)).all()
                for v in query:
                    newid_oldid_dist[v[1]] = v[0]

                add_team_list = []
                for w in add_team_user_list:
                    team_user = ChandaoProjectTeamUser()
                    team_user.account = w.get('account')
                    team_user.chandao_user_id = w.get('id')
                    team_user.chandao_project_id = newid_oldid_dist.get(w.get('chandao_old_project_id'))
                    team_user.chandao_old_project_id = w.get('chandao_old_project_id')
                    team_user.position = w.get('position')
                    team_user.realname = w.get('realname')
                    team_user.come_from = 0
                    team_user.user_id = 0
                    add_team_list.append(team_user)

                db.bulk_save_objects(add_team_list)
                db.commit()

                if update_project:
                    for key, dic in update_project.items():
                        query = db.query(ChandaoProject).filter(ChandaoProject.chandao_project_id == key).first()
                        if not query:
                            continue

                        query.project_name = dic.get('project_name')
                        query.code = dic.get('code')
                        query.begin = dic.get('begin')
                        if dic.get('end') == '长期':
                            query.end = '2099-12-31'
                        else:
                            query.end = dic.get('end')
                        query.status = dic.get('status')
                        query.budget = to_decimal(dic.get('budget'))

            chaodao_team_list = []
            with DbSession.create(EnumDb.chandao) as db:
                team_query = db.query(ZtTeam).filter(ZtTeam.root.in_(chandao_project_id)).filter(ZtTeam.account != 'admin')
                for v in team_query:
                    chaodao_team_list.append(format_dict(v.get_dict()))

            new_add_team_list = []
            for w in chaodao_team_list:
                account = w.get('account')
                chandao_old_project_id = w.get('root')
                if chandao_old_project_id in chandaoOldProjectId_account:
                    if account in chandaoOldProjectId_account[chandao_old_project_id]:
                        continue
                team_user = ChandaoProjectTeamUser()
                team_user.account = w.get('account')
                team_user.chandao_user_id = 0
                team_user.chandao_project_id = newid_oldid_dist.get(chandao_old_project_id)
                team_user.chandao_old_project_id = chandao_old_project_id
                team_user.position = w.get('position')
                team_user.realname = w.get('realname')
                team_user.come_from = 0
                team_user.user_id = 0
                new_add_team_list.append(team_user)
                account_list.append(w.get('account'))

            with DbSession.create(EnumDb.main) as db:
                db.bulk_save_objects(new_add_team_list)

            account_userid = {}
            account_username = {}
            with DbSession.create(EnumDb.main) as db:
                query = db.query(User.chandao_user, User.user_id, User.user_name).filter(User.chandao_user.in_(account_list))
                for v in query:
                    account_userid[v[0]] = v[1]
                    account_username[v[0]] = v[2]

                for v in db.query(ChandaoProjectTeamUser).filter(ChandaoProjectTeamUser.account.in_(account_list)):
                    account = v.account
                    v.user_id = account_userid.get(account)
                    v.realname = account_username.get(account)

            return ok()
        except:
            log(traceback.format_exc(), 'ChandaoManage.save_project', '保存项目失败!')
            return failure(msg='保存项目失败!')

    # 查看跟我相关的项目
    def get_user_project_list(self, select_name, status, page, size, is_refresh, wechat_user_id, come_from, select_type):
        try:
            if is_refresh:
                self.save_project()

            if status:
                status_query = ChandaoProject.status == status
            else:
                status_query = ChandaoProject.status.in_(['doing', 'wait', 'suspended'])

            if select_name and select_type == 0:
                select_sql = ChandaoProject.project_name.like('%' + select_name + '%')
                select_user_name = True
            elif select_name and select_type == 1:
                select_sql = True
                select_user_name = ChandaoProjectTeamUser.account.like('%' + get_quanpin(select_name) + '%')
            else:
                select_sql = True
                select_user_name = True

            with DbSession.create(EnumDb.main) as db:
                # 查询是管理人员还是绑定的人员
                if not self.user_id and wechat_user_id:
                    wechat_user_query = db.query(WechatUser.union_id).filter(WechatUser.id == wechat_user_id).first()
                    if not wechat_user_query:
                        return ok(data={'count': 0, 'list': []})

                    wechat_user_union_id = wechat_user_query[0]

                    wechat_user_type_2_query = db.query(WechatUser.id). \
                        filter(WechatUser.union_id == wechat_user_union_id). \
                        filter(WechatUser.type == 2). \
                        first()
                    if not wechat_user_type_2_query:
                        return ok(data={'count': 0, 'list': []})

                    wechat_user_type_2_id = wechat_user_type_2_query[0]

                    cpsb_query = db.query(ChandaoProjectShareBinding.project_id). \
                        filter(ChandaoProjectShareBinding.wechat_user_id == wechat_user_type_2_id). \
                        filter(ChandaoProjectShareBinding.is_delete == 0). \
                        all()
                    if not cpsb_query:
                        return ok(data={'count': 0, 'list': []})

                chandao_project_list = []
                result_list = []
                project_ids = []
                count = 0

                if self.user_id:
                    query = db.query(User).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
                    is_admin = query.is_admin
                    if not is_admin:
                        user_team_query = db.query(ChandaoProjectTeamUser.chandao_old_project_id). \
                            filter(ChandaoProjectTeamUser.account == query.chandao_user). \
                            filter(ChandaoProjectTeamUser.is_delete == 0). \
                            all()
                        for v in user_team_query:
                            chandao_project_list.append(v[0])

                        admin_query = ChandaoProject.chandao_project_id.in_(chandao_project_list)

                    else:
                        admin_query = True
                else:
                    # 不是员工，只是客户
                    tpye_2_query = db.query(WechatUser.union_id).filter(WechatUser.id == wechat_user_id).first()
                    if not tpye_2_query:
                        return ok(data={'count': count, 'list': result_list})

                    union_id = tpye_2_query[0]

                    tpye_1_query = db.query(WechatUser.id).filter(WechatUser.union_id == union_id).filter(WechatUser.type == 2).first()
                    if not tpye_1_query:
                        return ok(data={'count': count, 'list': result_list})

                    wechat_user_id_type_1 = tpye_1_query[0]

                    cpsb_query = db.query(ChandaoProjectShareBinding.project_id). \
                        filter(ChandaoProjectShareBinding.wechat_user_id == wechat_user_id_type_1). \
                        filter(ChandaoProjectShareBinding.is_delete == 0). \
                        all()
                    for v in cpsb_query:
                        chandao_project_list.append(v[0])

                    admin_query = ChandaoProject.chandao_project_id.in_(chandao_project_list)

                if select_name and select_type == 1:
                    team_user_query = db.query(ChandaoProjectTeamUser.chandao_old_project_id). \
                        filter(select_user_name). \
                        filter(ChandaoProjectTeamUser.is_delete == 0). \
                        all()
                    select_team_user_project_list = [v[0] for v in team_user_query]

                    if chandao_project_list:
                        inter_projet_ids = list(set(chandao_project_list).intersection(set(select_team_user_project_list)))
                        select_team_user = ChandaoProject.chandao_project_id.in_(inter_projet_ids)
                    else:
                        select_team_user = ChandaoProject.chandao_project_id.in_(select_team_user_project_list)

                    admin_query = True

                else:
                    select_team_user = True

                count = db.query(func.count(ChandaoProject.id)). \
                    filter(select_team_user). \
                    filter(admin_query). \
                    filter(select_sql). \
                    filter(status_query). \
                    order_by(ChandaoProject.status). \
                    order_by(ChandaoProject.end.desc()). \
                    first()[0]
                if not count:
                    return ok(data={'count': count, 'list': result_list})

                project_query = db.query(ChandaoProject). \
                    filter(select_team_user). \
                    filter(admin_query). \
                    filter(select_sql). \
                    filter(status_query). \
                    order_by(ChandaoProject.status). \
                    order_by(ChandaoProject.end.desc()). \
                    offset(page * size).limit(size)
                for v in project_query:
                    project_ids.append(v.chandao_project_id)
                    result_list.append(format_dict(v.get_dict()))

                # 获取项目最新的阶段
                phases_id_list = []
                phases_query = db.query(ChandaoProjectSummaryPhases.chandao_project_id, func.max(ChandaoProjectSummaryPhases.id)). \
                    filter(ChandaoProjectSummaryPhases.chandao_project_id.in_(project_ids)). \
                    group_by(ChandaoProjectSummaryPhases.chandao_project_id). \
                    all()
                for v in phases_query:
                    phases_id_list.append(v[1])

                # 查询最大的阶段信息
                phases_dict = {}
                proejct_phases_query = db.query(ChandaoProjectSummaryPhases). \
                    filter(ChandaoProjectSummaryPhases.id.in_(phases_id_list))
                for v in proejct_phases_query:
                    phases_dict[v.chandao_project_id] = format_dict(v.get_dict())

                # 项目经理
                user_dict = {}
                team_user = db.query(ChandaoProjectTeamUser.chandao_old_project_id, ChandaoProjectTeamUser.realname). \
                    filter(ChandaoProjectTeamUser.chandao_old_project_id.in_(project_ids)). \
                    filter(ChandaoProjectTeamUser.position == 'PM'). \
                    all()
                for v in team_user:
                    user_dict[v[0]] = v[1]

            for v in result_list:
                chandao_project_id = v['chandao_project_id']
                if chandao_project_id in phases_dict:
                    v['phases_dict'] = phases_dict[chandao_project_id]
                else:
                    v['phases_dict'] = {}

                if chandao_project_id in user_dict:
                    v['pm_user_name'] = user_dict[chandao_project_id]
                else:
                    v['pm_user_name'] = ''

            return ok(data={'count': count, 'list': result_list})
        except:
            log(traceback.format_exc(), 'ChandaoManage.get_user_project_list', '获取项目失败！')
            return failure(msg='获取项目失败!')

    # 获取系统项目阶段配置（树）
    def get_sys_project_phase_configuration_tree(self):
        try:
            list = []

            with DbSession.create(EnumDb.main) as db:
                query = db.query(SysProjectPhaseConfiguration).filter(SysProjectPhaseConfiguration.is_delete == 0).order_by(SysProjectPhaseConfiguration.sort)

                list1 = []
                # 生成根节点
                for f in query:
                    if (f.pid == 0):
                        list1.append(f)
                for f in list1:
                    data = self.__get_sys_child(f, query)
                    list.append(data)

            return ok(data=list)
        except:
            log(traceback.format_exc(), 'ChandaoManage.get_sys_project_phase_configuration_tree', '获取系统项目阶段配置（树）失败！')
            return failure(msg='获取系统项目阶段配置（树）失败!')

    # 查询子集
    def __get_sys_child(self, obj=None, query=[]):
        if not obj:
            return None

        # 获取子节点
        ff = {
            "id": obj.id,
            "point_name": obj.point_name,
            "pid": obj.pid,
            "sort": obj.sort,
            "creat_time": to_string(obj.creat_time),
            "childs": []
        }
        childs = []
        parentID = obj.id
        for c in query:
            if parentID == c.pid:
                childs.append(c)
        for c in childs:
            ff.get("childs").append(self.__get_sys_child(c, query))
        return ff

    # 新增系统项目阶段配置
    def add_sys_project_phase_configuration(self, data_list):
        try:
            with DbSession.create(EnumDb.main) as db:
                query = db.query(User).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
                is_admin = query.is_admin
                if not is_admin:
                    return failure(msg='系统管理员才能进行阶段配置！')

            add_list = []
            for v in data_list:
                dict_v = to_dict(v)
                sp = SysProjectPhaseConfiguration()
                sp.point_name = dict_v.get('point_name')
                sp.pid = dict_v.get('pid')
                sp.sort = dict_v.get('sort')
                sp.creat_user_id = self.user_id
                add_list.append(sp)

            with DbSession.create(EnumDb.main) as db:
                db.bulk_save_objects(add_list)
                db.commit()

            return ok()
        except:
            log(traceback.format_exc(), 'ChandaoManage.add_sys_project_phase_configuration', '新增阶段失败！')
            return failure(msg='新增阶段失败!')

    def update_sys_project_phase_configuration(self, update_dict):
        try:
            ids = [to_int(key) for key in update_dict]
            with DbSession.create(EnumDb.main) as db:
                for v in ids:
                    db.query(SysProjectPhaseConfiguration). \
                        filter(SysProjectPhaseConfiguration.id == to_int(v)). \
                        update({SysProjectPhaseConfiguration.point_name: update_dict[to_string(v)].get('point_name'),
                                SysProjectPhaseConfiguration.sort: update_dict[to_string(v)].get('sort'),
                                SysProjectPhaseConfiguration.is_delete: to_int(update_dict[to_string(v)].get('is_delete')),
                                SysProjectPhaseConfiguration.creat_time: datetime.datetime.now()
                                }, synchronize_session='fetch')

            return ok()
        except:
            log(traceback.format_exc(), 'ChandaoManage.update_sys_project_phase_configuration', '更新阶段失败！')
            return failure(msg='更新阶段失败!')

    def chandao_set_project_phase(self, project_id, data_list, come_from):
        '''
        新增系统配置的项目阶段配置
        data_list = [{
        "point_name": "",
        "configuration_id":"",
        "configuration_pid":"",
        "configuration_sort":"",
        "plan_start_time":"",
        "plan_end_time"::"",
        }]
        '''

        try:
            with DbSession.create(EnumDb.main) as db:
                query = db.query(ChandaoProject).filter(ChandaoProject.id == project_id).first()
                if not query:
                    return failure(msg='项目不存在，请查证后再试。')

                # pc端
                user = db.query(User.chandao_user, User.is_admin).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
                if not user:
                    return failure(msg='您没有查看该项目的权限。')

                if not user[1]:
                    team_user = db.query(ChandaoProjectTeamUser.position). \
                        filter(ChandaoProjectTeamUser.come_from == come_from). \
                        filter(ChandaoProjectTeamUser.account == user[0]). \
                        filter(ChandaoProjectTeamUser.chandao_project_id == project_id). \
                        filter(ChandaoProjectTeamUser.is_delete == 0). \
                        first()
                    if not team_user:
                        return failure(msg='您没有查看该项目的权限。')

                    if team_user[0] != 'PM':
                        return failure(msg='您不是项目经理，不能设置项目阶段')

                # 查詢項目已有階段
                project_cpp_id_list = []
                cpp_query = db.query(ChandaoProjectPhase.configuration_id).filter(ChandaoProjectPhase.project_id == project_id).filter(
                    ChandaoProjectPhase.is_delete == 0)
                for cpp in cpp_query:
                    project_cpp_id_list.append(cpp[0])

            add_list = []
            update_dict = {}
            data_list_p_ids = []
            delete_list = []
            for v in data_list:
                configuration_id = to_int(v['configuration_id'])
                data_list_p_ids.append(configuration_id)
                if configuration_id in project_cpp_id_list:
                    update_dict[to_int(v.get('phase_id'))] = {
                        "point_name": v.get('point_name'),
                        "configuration_id": v.get('configuration_id'),
                        "configuration_pid": v.get('configuration_pid'),
                        "configuration_sort": v.get('configuration_sort'),
                        "plan_start_time": v.get('plan_start_time'),
                        "plan_end_time": v.get('plan_end_time')
                    }

                if configuration_id not in project_cpp_id_list:
                    cpp = ChandaoProjectPhase()
                    cpp.project_id = project_id
                    cpp.point_name = v.get('point_name')
                    cpp.configuration_id = v.get('configuration_id')
                    cpp.configuration_pid = v.get('configuration_pid')
                    cpp.configuration_sort = v.get('configuration_sort')
                    cpp.plan_start_time = v.get('plan_start_time')
                    cpp.plan_end_time = v.get('plan_end_time')
                    cpp.creat_user_id = self.user_id
                    cpp.is_delete = 0
                    add_list.append(cpp)

            delete_list = list(set(project_cpp_id_list).difference(set(data_list_p_ids)))  # project_cpp_id_list中有而data_list_p_ids中没有的

            with DbSession.create(EnumDb.main) as db:
                if update_dict:
                    for v in update_dict:
                        db.query(ChandaoProjectPhase). \
                            filter(ChandaoProjectPhase.configuration_id == v). \
                            update({ChandaoProjectPhase.point_name: update_dict[v].get('point_name'),
                                    ChandaoProjectPhase.configuration_pid: update_dict[v].get('configuration_pid'),
                                    ChandaoProjectPhase.configuration_sort: update_dict[v].get('configuration_sort'),
                                    ChandaoProjectPhase.plan_start_time: update_dict[v].get('plan_start_time'),
                                    ChandaoProjectPhase.plan_end_time: update_dict[v].get('plan_end_time')}, synchronize_session='fetch')

                if delete_list:
                    db.query(ChandaoProjectPhase). \
                        filter(ChandaoProjectPhase.configuration_id.in_(delete_list)).update({ChandaoProjectPhase.is_delete: 1}, synchronize_session='fetch')

                if add_list:
                    db.bulk_save_objects(add_list)
                    db.commit()

            return ok()

        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_set_project_phase', '设置项目阶段失败！')
            return failure(msg='设置项目阶段失败!')

    def chandao_get_project_phase(self, project_id):
        try:
            project_phase = []

            with DbSession.create(EnumDb.main) as db:
                query = db.query(ChandaoProject).filter(ChandaoProject.id == project_id).first()
                if not query:
                    return failure(msg='项目不存在，请查证后再试。')

                project_msg = format_dict(query.get_dict())

                project_phase_query = db.query(ChandaoProjectPhase). \
                    filter(ChandaoProjectPhase.project_id == project_id). \
                    filter(ChandaoProjectPhase.is_delete == 0). \
                    order_by(ChandaoProjectPhase.configuration_sort)

                list1 = []
                # 生成根节点
                for f in project_phase_query:
                    if (f.configuration_pid == 0):
                        list1.append(f)
                for f in list1:
                    project_phase.append(self.__get_child(f, project_phase_query))

            project_msg['project_phase'] = project_phase

            return ok(data=project_msg)
        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_get_project_phase', '获取项目阶段配置（树）失败！')
            return failure(msg='获取项目阶段配置（树）失败!')

    # 查询子集
    def __get_child(self, obj=None, query=[]):
        if not obj:
            return None

        # 获取子节点
        ff = {
            "id": obj.id,
            "project_id": obj.project_id,
            "point_name": obj.point_name,
            "configuration_id": obj.configuration_id,
            "configuration_pid": obj.configuration_pid,
            "configuration_sort": obj.configuration_sort,
            "plan_start_time": to_string(obj.plan_start_time),
            "plan_end_time": to_string(obj.plan_end_time),
            "start_time": to_string(obj.start_time),
            "end_time": to_string(obj.end_time),
            "creat_time": to_string(obj.creat_time),
            "creat_user_id": obj.creat_user_id,
            "childs": []
        }
        childs = []
        parentID = obj.configuration_id
        for c in query:
            if parentID == c.configuration_pid:
                childs.append(c)
        for c in childs:
            ff.get("childs").append(self.__get_child(c, query))
        return ff

    # 判断是否是PM
    def __judge_PM(self, project_id):
        '''
        返回值  第一个值是是否存在，第二个值是否是项目经理
        :param project_id:
        :return:
        '''
        with DbSession.create(EnumDb.main) as db:
            query = db.query(User).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
            if not query:
                return False, False

            is_admin = query.is_admin
            if is_admin == 1:
                return True, True

            else:
                chandao_user = query.chandao_user
                if not chandao_user:
                    return False, False
                else:
                    team_query = db.query(ChandaoProjectTeamUser). \
                        filter(ChandaoProjectTeamUser.chandao_project_id == project_id). \
                        filter(ChandaoProjectTeamUser.account == chandao_user). \
                        filter(User.is_delete == 0). \
                        first()
                    if not team_query:
                        return False, False
                    else:
                        if team_query.position == 'PM':
                            return True, True
                        else:
                            return True, False

    def chandao_project_phase_update(self, project_id, phase_id, start_time, end_time):
        try:
            is_team, is_pm = self.__judge_PM(project_id)
            if not is_pm:
                return failure(msg='您无权修改项目阶段计划！')

            with DbSession.create(EnumDb.main) as db:
                phase_query = db.query(ChandaoProjectPhase). \
                    filter(ChandaoProjectPhase.project_id == project_id). \
                    filter(ChandaoProjectPhase.id == phase_id). \
                    first()
                if not phase_query:
                    return failure(msg='信息不存在，请查证后再试！')

                if start_time:
                    phase_query.start_time = start_time
                if end_time:
                    phase_query.end_time = end_time

            return ok()
        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_project_phase_update', '查询阶段失败！')
            return failure(msg='查询阶段失败！')

    def chandao_project_share_binding(self, project_id, share_user_id, wechat_user_id):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(ChandaoProjectShareBinding). \
                filter(ChandaoProjectShareBinding.project_id == project_id). \
                filter(ChandaoProjectShareBinding.wechat_user_id == wechat_user_id). \
                filter(ChandaoProjectShareBinding.is_delete == 0). \
                first()
            if not query:
                cpsb = ChandaoProjectShareBinding()
                cpsb.project_id = project_id
                cpsb.wechat_user_id = wechat_user_id
                cpsb.share_user_id = share_user_id
                cpsb.is_delete = 0
                db.add(cpsb)

        return ok()

    def chandao_project_share_binding_get_list(self, project_id):
        list = []
        with DbSession.create(EnumDb.main) as db:
            bingding_query = db.query(ChandaoProjectShareBinding). \
                filter(ChandaoProjectShareBinding.project_id == project_id). \
                filter(ChandaoProjectShareBinding.is_delete == 0)
            wechat_user_id_list = [v.wechat_user_id for v in bingding_query]
            if not wechat_user_id_list:
                return ok(data=[])

            wu_query = db.query(WechatUser).filter(WechatUser.id.in_(wechat_user_id_list))
            for v in wu_query:
                list.append(format_dict(v.get_dict()))

        num = 1
        for v in list:
            if not v['nike_name']:
                v['nike_name'] = '游客' + to_string(num)
                num += 1

        return ok(data=list)

    def chandao_project_team_get_list(self, chandao_project_id):
        list = []
        with DbSession.create(EnumDb.main) as db:
            for v in db.query(ChandaoProjectTeamUser).filter(ChandaoProjectTeamUser.chandao_old_project_id == chandao_project_id):
                list.append(format_dict(v.get_dict()))

        return ok(data=list)

    def __retun_dict(self, project_id, v):
        dicdata = format_dict({
            'project_id': project_id,  # 系统中的项目id
            'task_id': v[0],  # 主键ID
            'chandao_project_old_id': v[1],  # 禅道的项目ID
            'parent': v[2],  # 父级ID
            'name': v[3],  # 任务名称
            'type': v[4],  # 任务类型(design 设计 | devel 开发 | request 需求 | test 测试 | study 研究 | discuss 讨论 | ui 界面 | affair 事务 | misc 其他)
            'deadline': v[5],  # 截止日期
            'status': v[6],  # 状态(wait 未开始 | doing 进行中 | done 已完成 | closed 已关闭 | cancel 已取消)
            'estStarted': v[7],  # 预计开始日期
            'realStarted': v[8],  # 实际开始时间
            'finishedDate': v[9],  # 完成时间
            'pri': v[10],  # 优先级
            'assignedTo': v[11]  # 责任人
        })

        return dicdata

    def chandao_get_project_task_first(self, project_id):
        try:
            chandao_project_id = 0
            task_list = []
            with DbSession.create(EnumDb.main) as db:
                # 查询禅道项目id
                query = db.query(ChandaoProject.chandao_project_id).filter(ChandaoProject.id == project_id).first()
                if not query:
                    return failure(msg='项目不存在，请核对后再试！')

                chandao_project_id = query[0]

            if not chandao_project_id:
                return failure(msg='项目不存在，请核对后再试！')

            with DbSession.create(EnumDb.chandao) as db:
                task_query_count = db.query(func.count(ZtTask.id)). \
                    filter(ZtTask.project == chandao_project_id). \
                    filter(ZtTask.parent.in_([-1, 0])). \
                    filter(ZtTask.deleted == '0'). \
                    first()[0]
                if not task_query_count:
                    return ok(data={'count': 0, 'task_list': task_list})

                task_query = db.query(ZtTask.id,  # 主键ID
                                      ZtTask.project,  # 禅道的项目ID
                                      ZtTask.parent,  # 父级ID
                                      ZtTask.name,  # 任务名称
                                      ZtTask.type,  # 任务类型(design 设计 | devel 开发 | request 需求 | test 测试 | study 研究 | discuss 讨论 | ui 界面 | affair 事务 | misc 其他)
                                      ZtTask.deadline,  # 截止日期
                                      ZtTask.status,  # 状态(wait 未开始 | doing 进行中 | done 已完成 | closed 已关闭 | cancel 已取消)
                                      ZtTask.estStarted,  # 预计开始日期
                                      ZtTask.realStarted,  # 实际开始时间
                                      ZtTask.finishedDate,  # 完成时间
                                      ZtTask.pri,  # 优先级
                                      ZtTask.assignedTo  # 责任人
                                      ). \
                    filter(ZtTask.project == chandao_project_id). \
                    filter(ZtTask.deleted == '0'). \
                    filter(ZtTask.parent.in_([-1, 0])). \
                    order_by(desc(ZtTask.deadline))
                for v in task_query:
                    task_list.append(self.__retun_dict(project_id, v))

                return ok(data={'count': task_query_count, 'task_list': task_list})
        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_get_project_task_first', '查询任务失败！')
            return failure(msg='查询任务失败！')

    def chandao_get_project_task_subset(self, project_id, parent):
        try:
            chandao_project_id = 0
            task_list = []
            with DbSession.create(EnumDb.main) as db:
                # 查询禅道项目id
                query = db.query(ChandaoProject.chandao_project_id).filter(ChandaoProject.id == project_id).first()
                if not query:
                    return failure(msg='项目不存在，请核对后再试！')

                chandao_project_id = query[0]

            if not chandao_project_id:
                return failure(msg='项目不存在，请核对后再试！')

            with DbSession.create(EnumDb.chandao) as db:
                task_query_count = db.query(func.count(ZtTask.id)). \
                    filter(ZtTask.project == chandao_project_id). \
                    filter(ZtTask.parent == parent). \
                    filter(ZtTask.deleted == '0'). \
                    first()[0]
                if not task_query_count:
                    return ok(data={'count': 0, 'task_list': task_list})

                task_query = db.query(ZtTask.id,  # 主键ID
                                      ZtTask.project,  # 禅道的项目ID
                                      ZtTask.parent,  # 父级ID
                                      ZtTask.name,  # 任务名称
                                      ZtTask.type,  # 任务类型(design 设计 | devel 开发 | request 需求 | test 测试 | study 研究 | discuss 讨论 | ui 界面 | affair 事务 | misc 其他)
                                      ZtTask.deadline,  # 截止日期
                                      ZtTask.status,  # 状态(wait 未开始 | doing 进行中 | done 已完成 | closed 已关闭 | cancel 已取消)
                                      ZtTask.estStarted,  # 预计开始日期
                                      ZtTask.realStarted,  # 实际开始时间
                                      ZtTask.finishedDate,  # 完成时间
                                      ZtTask.pri,  # 优先级
                                      ZtTask.assignedTo  # 责任人
                                      ). \
                    filter(ZtTask.project == chandao_project_id). \
                    filter(ZtTask.deleted == '0'). \
                    filter(ZtTask.parent == parent). \
                    order_by(desc(ZtTask.deadline))
                for v in task_query:
                    task_list.append(self.__retun_dict(project_id, v))

                return ok(data={'count': task_query_count, 'task_list': task_list})
        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_get_project_task_subset', '获取子级禅道任务！')
            return failure(msg='获取子级禅道任务！')

    def chandao_add_project_customer_plan(self, come_from, project_id, task, information_list, plan_time, customer_facilitator):
        try:
            is_team, is_pm = self.__judge_PM(project_id)
            if not is_team:
                return failure(msg='您不是该项目团队成员，不允许创建客户协助任务！')

            item_id = 0
            with DbSession.create(EnumDb.main) as db:
                project_query = db.query(ChandaoProject.project_name).filter(ChandaoProject.id == project_id).first()
                if not project_query:
                    project_name = ''
                else:
                    project_name = project_query[0]

                cpcp = ChandaoProjectCustomerPlan()
                cpcp.project_id = project_id
                cpcp.task = task
                cpcp.information_list = information_list
                cpcp.customer_facilitator = customer_facilitator
                cpcp.plan_time = plan_time
                cpcp.creat_id = self.user_id
                cpcp.come_from = come_from
                db.add(cpcp)
                db.flush()
                item_id = cpcp.id

            # TODO 发送消息
            # self.send_exposures_message(wechat_user_ids=[customer_wechat_user_id],
            #                             project_id=project_id,
            #                             project_name=project_name,
            #                             item_id=item_id)

            return ok()
        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_add_project_customer_plan', '添加客户协助任务失败！')
            return failure(msg='添加客户协助任务失败！')

    def chandao_get_project_customer_plan(self, project_id, page, size):
        try:
            plan_list = []
            with DbSession.create(EnumDb.main) as db:
                # 查询禅道项目id
                proejct_query = db.query(ChandaoProject).filter(ChandaoProject.id == project_id).first()
                if not proejct_query:
                    return failure(msg='项目不存在，请核对后再试！')

                c_plan_count = db.query(func.count(ChandaoProjectCustomerPlan.id)). \
                    filter(ChandaoProjectCustomerPlan.project_id == project_id). \
                    filter(ChandaoProjectCustomerPlan.is_delete == 0). \
                    order_by(desc(ChandaoProjectCustomerPlan.plan_time)). \
                    first()[0]
                if not c_plan_count:
                    return ok(data={'count': 0, 'plan_list': plan_list})

                c_plan_query = db.query(ChandaoProjectCustomerPlan). \
                    filter(ChandaoProjectCustomerPlan.project_id == project_id). \
                    filter(ChandaoProjectCustomerPlan.is_delete == 0). \
                    order_by(desc(ChandaoProjectCustomerPlan.plan_time)). \
                    offset(page * size).limit(size)
                for v in c_plan_query:
                    plan_list.append(format_dict(v.get_dict()))

                return ok(data={'count': c_plan_count, 'plan_list': plan_list})
        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_get_project_customer_plan', '获取客户协助任务失败！')
            return failure(msg='获取客户协助任务失败！')

    def chandao_update_project_customer_plan(self, project_id, end_time, customer_plan_id):
        try:
            with DbSession.create(EnumDb.main) as db:
                query = db.query(ChandaoProjectCustomerPlan). \
                    filter(ChandaoProjectCustomerPlan.id == customer_plan_id). \
                    filter(ChandaoProjectCustomerPlan.project_id == project_id). \
                    filter(ChandaoProjectCustomerPlan.is_delete == 0). \
                    first()
                if not query:
                    return failure(msg='数据不存在，请核对后再试！')

                admin = db.query(User.is_admin).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
                if not admin:
                    is_admin = False
                else:
                    is_admin = admin[0]

                if query.creat_id != self.user_id:
                    if is_admin == False:
                        return failure(msg='您不是创建人，不允许修改！')

                query.end_time = end_time

            return ok()
        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_update_project_customer_plan', '更新客户协助任务失败！')
            return failure(msg='更新客户协助任务失败！')

    def chandao_add_project_customer_plan_track_record(self, come_from, project_id, customer_plan_id, track_record):
        try:
            with DbSession.create(EnumDb.main) as db:
                cpcptr = ChandaoProjectCustomerPlanTrackRecord()
                cpcptr.customer_plan_id = customer_plan_id
                cpcptr.track_record = track_record
                cpcptr.creat_id = self.user_id
                cpcptr.come_from = come_from
                db.add(cpcptr)

            return ok()
        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_add_project_customer_plan_track_record', '新增跟踪记录失败！')
            return failure(msg='新增跟踪记录失败！')

    def chandao_get_project_customer_plan_track_record(self, project_id, customer_plan_id):
        try:
            list = []
            with DbSession.create(EnumDb.main) as db:
                count = db.query(func.count(ChandaoProjectCustomerPlanTrackRecord.id)). \
                    filter(ChandaoProjectCustomerPlanTrackRecord.customer_plan_id == customer_plan_id). \
                    filter(ChandaoProjectCustomerPlanTrackRecord.is_delete == 0). \
                    first()[0]
                if not count:
                    return ok(data={'count': 0, 'list': []})

                query = db.query(ChandaoProjectCustomerPlanTrackRecord). \
                    filter(ChandaoProjectCustomerPlanTrackRecord.customer_plan_id == customer_plan_id). \
                    filter(ChandaoProjectCustomerPlanTrackRecord.is_delete == 0). \
                    order_by(desc(ChandaoProjectCustomerPlanTrackRecord.creat_time))
                for v in query:
                    list.append(format_dict(v.get_dict()))

                return ok(data={'count': count, 'list': list})
        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_get_project_customer_plan_track_record', '获取客户协助任务跟踪记录失败！')
            return failure(msg='获取客户协助任务跟踪记录失败！')

    def chandao_add_project_risk(self, project_id, come_from, risk_comment, plan_end_time, risk_level):
        try:
            with DbSession.create(EnumDb.main) as db:
                project_query = db.query(ChandaoProject.project_name).filter(ChandaoProject.id == project_id).first()
                if not project_query:
                    return failure(msg='项目不存在，请核对后再试！')
                else:
                    project_name = project_query[0]

                user_ids = []
                # 查询要发送的人管理员，项目团队成员，收藏的人
                user_query = db.query(User.user_id).filter(User.is_admin == 1).filter(User.is_delete == 0).all()
                for v in user_query:
                    user_ids.append(v[0])

                account_list = []
                chandao_team_query = db.query(ChandaoProjectTeamUser.account). \
                    filter(ChandaoProjectTeamUser.chandao_project_id == project_id). \
                    filter(ChandaoProjectTeamUser.is_delete == 0). \
                    all()
                for f in chandao_team_query:
                    account_list.append(f[0])

                user_query_2 = db.query(User.user_id).filter(User.chandao_user.in_(account_list)).filter(User.is_delete == 0).all()
                for v2 in user_query_2:
                    user_ids.append(v2[0])

                wechat_user_ids = []
                wechat_query = db.query(WechatUser.id).filter(WechatUser.user_id.in_(user_ids)).all()
                for w in wechat_query:
                    wechat_user_ids.append(w[0])

                # 追加收藏的人
                binding_query = db.query(ChandaoProjectShareBinding.wechat_user_id). \
                    filter(ChandaoProjectShareBinding.project_id == project_id). \
                    filter(ChandaoProjectShareBinding.is_delete == 0). \
                    all()
                for b in binding_query:
                    wechat_user_ids.append(b[0])

                cdpr = ChandaoProjectRisk()
                cdpr.project_id = project_id
                cdpr.risk_comment = risk_comment
                cdpr.plan_end_time = plan_end_time
                cdpr.risk_level = risk_level
                cdpr.creat_user_id = self.user_id
                cdpr.come_from = come_from
                db.add(cdpr)
                db.flush()
                item_id = cdpr.id

            self.send_exposures_message(wechat_user_ids=wechat_user_ids,
                                        project_id=project_id,
                                        project_name=project_name,
                                        item_id=item_id,
                                        risk_comment=risk_comment
                                        )

            return ok()
        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_add_project_risk', '新增风险失败！')
            return failure(msg='新增风险失败！')

    def chandao_get_project_risk(self, project_id, page, size):
        try:
            list = []
            with DbSession.create(EnumDb.main) as db:
                count = db.query(func.count(ChandaoProjectRisk.id)). \
                    filter(ChandaoProjectRisk.project_id == project_id). \
                    filter(ChandaoProjectRisk.is_delete == 0). \
                    first()[0]
                if not count:
                    return ok(data={'count': 0, 'list': []})

                query = db.query(ChandaoProjectRisk). \
                    filter(ChandaoProjectRisk.project_id == project_id). \
                    filter(ChandaoProjectRisk.is_delete == 0). \
                    order_by(desc(ChandaoProjectRisk.plan_end_time)). \
                    offset(page * size).limit(size)
                for v in query:
                    list.append(format_dict(v.get_dict()))

                return ok(data={'count': count, 'list': list})
        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_get_project_risk', '获取风险失败！')
            return failure(msg='获取风险失败！')

    def chandao_update_project_risk(self, project_id, risk_id, end_time):
        try:
            with DbSession.create(EnumDb.main) as db:
                query = db.query(ChandaoProjectRisk). \
                    filter(ChandaoProjectRisk.id == risk_id). \
                    filter(ChandaoProjectRisk.project_id == project_id). \
                    filter(ChandaoProjectRisk.is_delete == 0). \
                    first()
                if not query:
                    return failure(msg='数据不存在，请核对后再试！')

                admin = db.query(User.is_admin).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
                if not admin:
                    is_admin = False
                else:
                    is_admin = admin[0]

                if query.creat_user_id != self.user_id:
                    if is_admin == False:
                        return failure(msg='您不是创建人，不允许修改！')

                query.end_time = end_time

            return ok()

        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_update_project_risk', '更新风险信息失败！')
            return failure(msg='更新风险信息失败！')

    def chandao_add_project_risk_track_record(self, project_id, risk_id, track_content, come_from):
        try:
            with DbSession.create(EnumDb.main) as db:
                query = db.query(ChandaoProjectRisk). \
                    filter(ChandaoProjectRisk.id == risk_id). \
                    filter(ChandaoProjectRisk.project_id == project_id). \
                    filter(ChandaoProjectRisk.is_delete == 0). \
                    first()
                if not query:
                    return failure(msg='数据不存在，请核对后再试！')

                add_query = ChandaoProjectRiskTrackRecord()
                add_query.risk_id = risk_id
                add_query.track_content = track_content
                add_query.creat_user_id = self.user_id
                add_query.come_from = come_from
                db.add(add_query)

            return ok()
        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_add_project_risk_track_record', '新增风险跟进记录失败！')
            return failure(msg='新增风险跟进记录失败！')

    def chandao_get_project_risk_track_record(self, project_id, risk_id, page, size):
        try:
            list = []
            with DbSession.create(EnumDb.main) as db:
                query = db.query(ChandaoProjectRisk). \
                    filter(ChandaoProjectRisk.id == risk_id). \
                    filter(ChandaoProjectRisk.project_id == project_id). \
                    filter(ChandaoProjectRisk.is_delete == 0). \
                    first()
                if not query:
                    return failure(msg='数据不存在，请核对后再试！')

                track_count = db.query(func.count(ChandaoProjectRiskTrackRecord.id)). \
                    filter(ChandaoProjectRiskTrackRecord.risk_id == risk_id). \
                    filter(ChandaoProjectRiskTrackRecord.is_delete == 0). \
                    first()[0]

                if not track_count:
                    return ok(data={'count': 0, 'list': list})

                track_query = db.query(ChandaoProjectRiskTrackRecord). \
                    filter(ChandaoProjectRiskTrackRecord.risk_id == risk_id). \
                    filter(ChandaoProjectRiskTrackRecord.is_delete == 0). \
                    order_by(desc(ChandaoProjectRiskTrackRecord.track_time)). \
                    offset(page * size).limit(size)
                for v in track_query:
                    list.append(format_dict(v.get_dict()))

                return ok(data={'count': track_count, 'list': list})

        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_get_project_risk_track_record', '查询风险跟进记录失败！')
            return failure(msg='查询风险跟进记录失败！')

    def chandao_get_project_risk_first(self, project_id, itme_id, page, size):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(ChandaoProjectRisk). \
                filter(ChandaoProjectRisk.id == itme_id). \
                filter(ChandaoProjectRisk.project_id == project_id). \
                filter(ChandaoProjectRisk.is_delete == 0). \
                first()
            if not query:
                return failure(msg='风险信息不存在，请查证后重试！')

            data = format_dict(query.get_dict())

        record_res = self.chandao_get_project_risk_track_record(project_id, itme_id, page, size)
        record_data = record_res.data

        data['record_risk_data'] = record_data

        return ok(data=data)

    def send_exposures_message(self, wechat_user_ids, project_id, project_name, item_id, risk_comment):
        try:
            # 避免测试环境测试时不停的发消息
            # if SERVER_TYPE == 'beta':
            #     return ok()

            open_ids = []
            unionid_list = set([])
            with DbSession.create(EnumDb.main) as db:
                # 查询对应的公众号opend_id
                recommend_user = db.query(WechatUser).filter(WechatUser.id.in_(wechat_user_ids))
                for v in recommend_user:
                    if v.union_id:
                        unionid_list.add(v.union_id)

                # 通过 unionid 查询type=1 的那个人的openid
                wiki_open_query = db.query(WechatUser.id, WechatUser.open_id, WechatUser.is_customer_service, WechatUser.user_id). \
                    filter(WechatUser.type == 2).filter(WechatUser.union_id.in_(unionid_list))
                for v in wiki_open_query:
                    open_ids.append({'wechat_user_id': v[0], 'open_id': v[1], 'judge_is_customer_service': v[2], 'customer_server_user_id': v[3]})

            if not open_ids:
                return ok()

            url = '%s/api/wechat/authorization' % (DOMAIN_CURRENT)
            encode_url = quote(url, 'utf-8')

            # 跳转风险详情页面
            page_path = '/pages/plan/trackingRecords?&id=%s&project_id=%s' % (to_string(item_id), to_string(project_id))
            miniprogram = {"appid": WECHAT_PRO_APPID,
                           "pagepath": page_path
                           }

            add_list = []
            for v in open_ids:
                wechat_id = v['wechat_user_id']
                open_id = v['open_id']

                # is_customer_service, type, item_id, user_id, project_id, model
                state = to_string(v['judge_is_customer_service']) + '-_-' + '1' + '-_-' + to_string(item_id) + '-_-' + \
                        to_string(v['customer_server_user_id']) + '-_-' + to_string(project_id) + '-_-' + to_string(1)

                redirect_uri = '%s&response_type=code&scope=snsapi_userinfo&state=%s' % (encode_url, to_string(state))
                url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s#wechat_redirect' % \
                      (to_string(WECHAT_PRO_APPID), redirect_uri)
                data = {
                    "touser": open_id,
                    "template_id": WECHAT_EXPOSURES_TEMPLATE_ID,  # 模板id
                    "url": url,  # 需要跳转的链接 ，因为跳转了小程序，这个url被认为是备用的跳转链接
                    "miniprogram": miniprogram,  # 需要跳转的小程序
                    "data": {
                        "thing5": {  # 项目名称
                            "value": to_string(project_name)[:20],
                            "color": "#173177"  # 模板内容字体颜色，不填默认为黑色
                        },
                        "thing2": {  # 风险名称
                            "value": to_string(risk_comment)[:20],
                            "color": "#173177"
                        },
                        "time3": {  # 整改期限
                            "value": to_string(datetime.datetime.now())[:19],
                            "color": "#173177"
                        }
                    }
                }

                # 保存数据到 wechat_template_msg_logs
                wtml = SendMsgLog()
                wtml.json = to_string(data)
                wtml.open_id = open_id
                wtml.wechat_user_id = wechat_id
                wtml.item_id = item_id
                wtml.creat_time = current_timestamp()
                wtml.send_ok = 0
                wtml.model = 1
                wtml.project_id = project_id
                add_list.append(wtml)

            with DbSession.create(EnumDb.main) as db:
                db.bulk_save_objects(add_list)

            return ok()
        except:
            log(traceback.format_exc(), 'ChandaoManage.send_exposures_message')
            return failure(msg='操作失败')

    def chandao_project_task_get_list(self, project_id, task_name, start_time, end_time, is_my, time_type, page, size):
        # 0-本周，1-所有，-1-自定义时间，
        if time_type == 0:
            now = datetime.datetime.now()
            this_week_start = to_string(now - timedelta(days=now.weekday()))[:10] + ' 00:00:00'
            this_week_end = to_string(now + timedelta(days=6 - now.weekday()))[:10] + ' 23:59:59'

            time_query = and_(ProjectTask.plan_end_time >= this_week_start,
                              ProjectTask.plan_end_time <= this_week_end
                              )
        elif time_type == -1:
            time_query = and_(ProjectTask.plan_end_time >= to_string(start_time)[:10] + ' 00:00:00',
                              ProjectTask.plan_end_time <= to_string(end_time)[:10] + ' 23:59:59'
                              )
        else:
            time_query = True

        if is_my:
            my_user = ProjectTask.responsible_person_user_id == self.user_id
        else:
            my_user = ProjectTask.project_id == project_id

        if task_name:
            task_name_query = ProjectTask.task_name.like('%' + task_name + '%')
        else:
            task_name_query = True

        with DbSession.create(EnumDb.main) as db:
            list = []
            count = db.query(func.count(ProjectTask.id)). \
                filter(time_query). \
                filter(my_user). \
                filter(task_name_query). \
                filter(ProjectTask.is_delete == 0). \
                order_by(desc(ProjectTask.plan_end_time)). \
                first()
            if not count:
                return ok(data={'count': 0, 'list': list})

            query = db.query(ProjectTask). \
                filter(time_query). \
                filter(my_user). \
                filter(task_name_query). \
                filter(ProjectTask.is_delete == 0). \
                order_by(desc(ProjectTask.plan_end_time)). \
                offset(page * size). \
                limit(size). \
                all()

            pis = []
            for v in query:
                if v.pid != 0:
                    pis.append(v.pid)
            query_pid = db.query(ProjectTask). \
                filter(ProjectTask.id.in_(pis)). \
                filter(ProjectTask.is_delete == 0). \
                all()

            query = to_list(set(query_pid + query))

            sort_list = []
            for v in query:
                sort_list.append(v.get_dict())
            select_query = sorted(sort_list, key=lambda e: (e.__getitem__('plan_end_time'), e.__getitem__('id')), reverse=True)
            # select_query = sort_list.sort(key=lambda x: (x['plan_end_time'], x['id']), reverse=True)

            if not is_my:
                list1 = []
                # 生成根节点
                for f in select_query:
                    if (f['pid'] == 0):
                        list1.append(f)
                for f in list1:
                    data = self.__get_task_child_new(f, select_query)
                    list.append(data)
            else:
                for v in select_query:
                    list.append(format_dict(v))

            return ok(data={'count': count[0], 'list': list})

    def __get_task_child_new(self, obj=None, query=[]):
        if not obj:
            return None

        # 获取子节点
        ff = {
            "id": obj['id'],
            "pid": obj['pid'],
            "task_name": obj['task_name'],
            "task_desc": obj['task_desc'],
            "plan_start_time": to_string(obj['plan_start_time']),
            "plan_end_time": to_string(obj['plan_end_time']),
            "plan_staff_time": obj['plan_staff_time'],
            "start_time": to_string(obj['start_time']),
            "end_time": to_string(obj['end_time']),
            "staff_time": obj['staff_time'],
            "project_id": obj['project_id'],
            "project_phase_id": obj['project_phase_id'],
            "project_phase_name": obj['project_phase_name'],
            "responsible_person_user_id": obj['responsible_person_user_id'],
            "responsible_person_user_name": obj['responsible_person_user_name'],
            "is_confirmed": obj['is_confirmed'],
            "confirmed_wechat_user_id": obj['confirmed_wechat_user_id'],
            "confirmed_time": to_string(obj['confirmed_time']),
            "creat_time": to_string(obj['creat_time']),
            "creat_user_id": obj['creat_user_id'],
            "creat_user_name": obj['creat_user_name'],
            "edit_time": to_string(obj['edit_time']),
            "edit_user_id": obj['edit_user_id'],
            "edit_user_name": obj['edit_user_name'],
            "childs": []
        }
        childs = []
        parentID = obj['id']
        for c in query:
            if parentID == c['pid']:
                childs.append(c)
        for c in childs:
            ff.get("childs").append(self.__get_task_child_new(c, query))
        return ff

    def __get_task_child(self, obj=None, query=[]):
        if not obj:
            return None

        # 获取子节点
        ff = {
            "id": obj.id,
            "pid": obj.pid,
            "task_name": obj.task_name,
            "task_desc": obj.task_desc,
            "plan_start_time": to_string(obj.plan_start_time),
            "plan_end_time": to_string(obj.plan_end_time),
            "plan_staff_time": obj.plan_staff_time,
            "start_time": to_string(obj.start_time),
            "end_time": to_string(obj.end_time),
            "staff_time": obj.staff_time,
            "project_id": obj.project_id,
            "project_phase_id": obj.project_phase_id,
            "project_phase_name": obj.project_phase_name,
            "responsible_person_user_id": obj.responsible_person_user_id,
            "responsible_person_user_name": obj.responsible_person_user_name,
            "is_confirmed": obj.is_confirmed,
            "confirmed_wechat_user_id": obj.confirmed_wechat_user_id,
            "confirmed_time": to_string(obj.confirmed_time),
            "creat_time": to_string(obj.creat_time),
            "creat_user_id": obj.creat_user_id,
            "creat_user_name": obj.creat_user_name,
            "edit_time": to_string(obj.edit_time),
            "edit_user_id": obj.edit_user_id,
            "edit_user_name": obj.edit_user_name,
            "childs": []
        }
        childs = []
        parentID = obj.id
        for c in query:
            if parentID == c.pid:
                childs.append(c)
        for c in childs:
            ff.get("childs").append(self.__get_task_child(c, query))
        return ff

    def chandao_project_task_add(self, project_id, item_list):
        try:
            add_list = []
            all_plan_staff_time = 0
            pid = 0
            yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
            for v in item_list:
                obj = to_dict(v)
                plan_staff_time = to_decimal(obj.get('plan_staff_time')) * MAN_DAY_BASIS * 60
                pid = obj.get('pid')

                if pid and to_datetime(obj.get('plan_start_time')[:10]) < to_datetime(to_string(yesterday)[:10]):
                    return failure(msg='任务的开始时间不能小于昨天！')

                pt = ProjectTask()
                pt.pid = pid
                pt.task_name = obj.get('task_name')
                pt.task_level = obj.get('task_level')
                pt.task_desc = obj.get('task_desc')
                pt.plan_start_time = obj.get('plan_start_time')
                pt.plan_end_time = obj.get('plan_end_time')
                pt.plan_staff_time = to_int(plan_staff_time)  # 填写天，这合分钟，方便后面计算
                pt.project_id = project_id
                pt.project_phase_id = obj.get('project_phase_id')
                pt.project_phase_name = obj.get('project_phase_name')
                pt.responsible_person_user_id = obj.get('responsible_person_user_id')
                pt.responsible_person_user_name = obj.get('responsible_person_user_name')
                pt.responsible_person_name = obj.get('responsible_person_name')
                pt.creat_user_id = self.user_id
                pt.creat_user_name = self.user_name
                pt.is_delete = 0

                all_plan_staff_time += to_int(plan_staff_time)

                add_list.append(pt)

            # 判断分解任务的时间，是否超期。
            if pid != 0:
                with DbSession.create(EnumDb.main) as db:
                    pid_query = db.query(ProjectTask.plan_staff_time).filter(ProjectTask.id == pid).filter(ProjectTask.is_delete == 0).first()
                    if pid_query:
                        if all_plan_staff_time > pid_query[0]:
                            return failure(msg='您分解的任务工时，已超过计划工时，请修改后重新提交！')

            with DbSession.create(EnumDb.main) as db:
                project_query = db.query(ChandaoProject).filter(ChandaoProject.id == project_id).first()
                if not project_query:
                    return failure(msg='项目不存在，请核对后再试！')

                user_query = db.query(User.chandao_user, User.is_admin).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
                if not user_query:
                    return failure(msg='您不是项目团队成员，不能进行任务创建')

                if not user_query[1]:
                    user_team_query = db.query(ChandaoProjectTeamUser). \
                        filter(ChandaoProjectTeamUser.account == user_query[0]). \
                        filter(ChandaoProjectTeamUser.chandao_project_id == project_id). \
                        filter(ChandaoProjectTeamUser.is_delete == 0). \
                        first()
                    if not user_team_query:
                        return failure(msg='您不是项目团队成员，不能进行任务创建')

                db.bulk_save_objects(add_list)
                # db.commit()

            return ok()

        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_project_task_add')
            return failure(msg='新增项目任务失败！')

    def chandao_project_task_mandays_overrun(self, user_id, new_taks_plan_staff_time, task_start_time, end_start_time):
        new_taks_plan_staff_time = to_decimal(new_taks_plan_staff_time) * MAN_DAY_BASIS * 60
        project_id_list = []
        all_plan_staff_time = 0

        # 获取本周开始结束时间
        with DbSession.create(EnumDb.main) as db:
            query = db.query(ProjectTask.project_id, ProjectTask.plan_staff_time). \
                filter(ProjectTask.responsible_person_user_id == user_id). \
                filter(ProjectTask.is_delete == 0). \
                filter(ProjectTask.pid == 0). \
                filter(ProjectTask.plan_start_time >= task_start_time). \
                filter(ProjectTask.plan_end_time <= end_start_time)

            for v in query:
                project_id_list.append(v[0])
                all_plan_staff_time += v[1]

        return ok()

    def chandao_project_task_delete(self, project_id, task_id_list):
        with DbSession.create(EnumDb.main) as db:
            project_query = db.query(ChandaoProject).filter(ChandaoProject.id == project_id).first()
            if not project_query:
                return failure(msg='项目不存在，请核对后再试！')

            user_query = db.query(User.chandao_user, User.is_admin).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
            if not user_query:
                return failure(msg='您不是项目团队成员，不能删除任务！')

            if not user_query[1]:
                user_team_query = db.query(ChandaoProjectTeamUser). \
                    filter(ChandaoProjectTeamUser.account == user_query[0]). \
                    filter(ChandaoProjectTeamUser.chandao_project_id == project_id). \
                    filter(ChandaoProjectTeamUser.is_delete == 0). \
                    filter(ChandaoProjectTeamUser.position == 'PM'). \
                    first()
                if not user_team_query:
                    return failure(msg='您不是项目负责人，不能删除任务！')

            db.query(ProjectTask).filter(ProjectTask.id.in_(task_id_list)).update({ProjectTask.is_delete: 1,
                                                                                   ProjectTask.edit_user_id: self.user_id,
                                                                                   ProjectTask.edit_user_name: self.user_name,
                                                                                   ProjectTask.edit_time: datetime.datetime.now()
                                                                                   }, synchronize_session='fetch')

        return ok()

    def chandao_project_task_update_time(self, project_id, task_id, start_time, end_time, responsible_person_is_confirmed):
        try:
            with DbSession.create(EnumDb.main) as db:
                user_query = db.query(User.chandao_user, User.is_admin).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
                if not user_query:
                    return failure(msg='您不是项目团队成员，不能删除任务！')

                query = db.query(ProjectTask).filter(ProjectTask.id == task_id).filter(ProjectTask.project_id == project_id).filter(ProjectTask.is_delete == 0).first()
                if not query:
                    return failure(msg='任务不存在，请核对数据！')

                if not user_query[1] and query.responsible_person_user_id != to_int(self.user_id):
                    return failure(msg='您不是当前责任人，不能进行修改')

                if start_time and end_time:
                    if to_datetime(end_time) < to_datetime(start_time):
                        return failure(msg='结束时间不允许大于开始时间！')

                query.start_time = start_time
                if end_time:
                    query.end_time = end_time
                query.edit_time = datetime.datetime.now()
                query.edit_user_id = self.user_id
                query.edit_user_name = self.user_name
                query.responsible_person_is_confirmed = responsible_person_is_confirmed

                if query.pid:
                    pid_query = db.query(ProjectTask).filter(ProjectTask.id == query.pid).first()
                    pid_query.start_time = start_time

            with DbSession.create(EnumDb.main) as db:
                query = db.query(ProjectTask).filter(ProjectTask.id == task_id).filter(ProjectTask.project_id == project_id).filter(ProjectTask.is_delete == 0).first()
                if query.end_time and query.start_time:
                    staff_time_diff = datetime.datetime.fromtimestamp(to_timestamp(query.end_time)) - datetime.datetime.fromtimestamp(to_timestamp(query.start_time))
                    staff_time = staff_time_diff.total_seconds() / 60

                    query.staff_time = staff_time
                else:
                    query.staff_time = 0

            return ok()
        except:
            log(traceback.format_exc(), 'ChandaoManage.chandao_project_task_update_time')
            return failure(msg='更新时间失败！')

    def chandao_project_task_responsible_confirmed(self, project_id, task_id):
        with DbSession.create(EnumDb.main) as db:
            pid_query = db.query(ProjectTask). \
                filter(ProjectTask.id == task_id). \
                filter(ProjectTask.project_id == project_id). \
                filter(ProjectTask.is_delete == 0). \
                first()
            if not pid_query.pid:
                return failure(msg='该任务未创建子任务，不能直接完成父级任务')
            else:
                staff_time_query = db.query(func.sum(ProjectTask.staff_time)). \
                    filter(ProjectTask.pid == task_id). \
                    filter(ProjectTask.project_id == project_id). \
                    filter(ProjectTask.is_delete == 0). \
                    first()
                if not staff_time_query:
                    return failure(msg='该任务未创建子任务，不能直接完成父级任务')

                max_start_time = db.query(func.max(ProjectTask.end_time)). \
                    filter(ProjectTask.pid == task_id). \
                    filter(ProjectTask.project_id == project_id). \
                    filter(ProjectTask.is_delete == 0). \
                    first()

                pid_query.end_time = max_start_time[0]
                pid_query.responsible_person_is_confirmed = 1
                pid_query.responsible_person_is_confirmed_time = datetime.datetime.now()
                pid_query.staff_time = staff_time_query

        # TODO 发消息

        return ok()

    def chandao_project_task_pm_confirmed(self, project_id, task_id, come_from):
        with DbSession.create(EnumDb.main) as db:
            pid_query = db.query(ProjectTask). \
                filter(ProjectTask.id == task_id). \
                filter(ProjectTask.project_id == project_id). \
                filter(ProjectTask.is_delete == 0). \
                first()

            user = db.query(User.chandao_user, User.is_admin).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
            if not user:
                return failure(msg='您没有查看该项目的权限。')

            if not user[1]:
                team_user = db.query(ChandaoProjectTeamUser.position). \
                    filter(ChandaoProjectTeamUser.come_from == come_from). \
                    filter(ChandaoProjectTeamUser.account == user[0]). \
                    filter(ChandaoProjectTeamUser.chandao_project_id == project_id). \
                    filter(ChandaoProjectTeamUser.is_delete == 0). \
                    first()
                if not team_user:
                    return failure(msg='您不是项目经理，不能设置项目阶段')

                if team_user[0] != 'PM':
                    return failure(msg='您不是项目经理，不能设置项目阶段')

            pid_query.pm_is_confirmed = 1
            pid_query.pm_is_confirmed_user_id = self.user_id
            pid_query.pm_is_confirmed_time = datetime.datetime.now()

        return ok()

    def chandao_project_task_confirmed(self, project_id, task_id_list):
        with DbSession.create(EnumDb.main) as db:
            db.query(ProjectTask). \
                filter(ProjectTask.project_id == project_id). \
                filter(ProjectTask.id.in_(task_id_list)).update({ProjectTask.is_confirmed: 1,
                                                                 ProjectTask.confirmed_wechat_user_id: self.user_id
                                                                 }, synchronize_session='fetch')

            return ok()

    def chandao_project_task_progress_add(self, project_id, task_id, progress):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(ProjectTask).filter(ProjectTask.id == task_id).filter(ProjectTask.project_id == project_id).first()
            if not query:
                return failure(msg='任务不存在，请核对后再试！')

            ptp = ProjectTaskProgress()
            ptp.project_id = project_id
            ptp.task_id = task_id
            ptp.progress = progress
            ptp.user_id = self.user_id
            ptp.user_name = self.user_name
            ptp.is_delete = 0
            db.add(ptp)

        # TODO 发消息

        return ok()

    def chandao_project_task_progress_get(self, project_id, task_id, page, size):
        with DbSession.create(EnumDb.main) as db:
            list = []
            count_query = db.query(func.count(ProjectTaskProgress.id)). \
                filter(ProjectTaskProgress.is_delete == 0). \
                filter(ProjectTaskProgress.project_id == project_id). \
                filter(ProjectTaskProgress.task_id == task_id). \
                order_by(desc(ProjectTaskProgress.creat_time)). \
                first()
            if not count_query:
                return ok(data={"count": 0, 'list': []})

            query = db.query(ProjectTaskProgress). \
                filter(ProjectTaskProgress.is_delete == 0). \
                filter(ProjectTaskProgress.project_id == project_id). \
                filter(ProjectTaskProgress.task_id == task_id). \
                order_by(desc(ProjectTaskProgress.creat_time)). \
                offset(page * size).limit(size)
            for v in query:
                list.append(format_dict(v.get_dict()))

            return ok(data={"count": count_query[0], "list": list})

    def chandao_project_task_progress_delete(self, project_id, task_id, progress_id_list):
        with DbSession.create(EnumDb.main) as db:
            wechat_union_query = db.query(WechatUser.union_id).filter(WechatUser.id == self.user_id).first()
            if not wechat_union_query:
                return failure(msg='操作失败！')

            wechat_query = db.query(WechatUser.user_id).filter(WechatUser.union_id == wechat_union_query[0]).filter(WechatUser.user_id != 0).first()
            if not wechat_query:
                return failure(msg='您没有删除任务的权限！')

            user_query = db.query(User.chandao_user, User.is_admin).filter(User.user_id == wechat_query[0]).filter(User.is_delete == 0).first()
            if not user_query:
                return failure(msg='您不是项目团队成员，不能删除任务！')

            if not user_query[1]:
                user_team_query = db.query(ChandaoProjectTeamUser). \
                    filter(ChandaoProjectTeamUser.account == user_query[0]). \
                    filter(ChandaoProjectTeamUser.chandao_project_id == project_id). \
                    filter(ChandaoProjectTeamUser.is_delete == 0). \
                    filter(ChandaoProjectTeamUser.position == 'PM'). \
                    first()
                if not user_team_query:
                    return failure(msg='您不是项目负责人，不能删除任务！')

            db.query(ProjectTaskProgress). \
                filter(ProjectTaskProgress.project_id == project_id). \
                filter(ProjectTaskProgress.task_id == task_id). \
                filter(ProjectTaskProgress.id.in_(progress_id_list)). \
                update({ProjectTaskProgress.is_delete: 1, ProjectTaskProgress.delete_time: datetime.datetime.now()}, synchronize_session='fetch')

        return ok()

    def chandao_project_update_information_update(self, project_id, project_objectives, order_staff_data, budget_staff_data):
        with DbSession.create(EnumDb.main) as db:
            wechat_union_query = db.query(WechatUser.union_id).filter(WechatUser.id == self.user_id).first()
            if not wechat_union_query:
                return failure(msg='操作失败！')

            wechat_query = db.query(WechatUser.user_id).filter(WechatUser.union_id == wechat_union_query[0]).filter(WechatUser.user_id != 0).first()
            if not wechat_query:
                return failure(msg='您没有删除任务的权限！')

            user_query = db.query(User.chandao_user, User.is_admin, User.user_id, User.user_name). \
                filter(User.user_id == wechat_query[0]). \
                filter(User.is_delete == 0). \
                first()
            if not user_query:
                return failure(msg='您没有删除任务的权限！')

            if not user_query[1]:
                user_team_query = db.query(ChandaoProjectTeamUser). \
                    filter(ChandaoProjectTeamUser.account == user_query[0]). \
                    filter(ChandaoProjectTeamUser.chandao_project_id == project_id). \
                    filter(ChandaoProjectTeamUser.is_delete == 0). \
                    filter(ChandaoProjectTeamUser.position == 'PM'). \
                    first()
                if not user_team_query:
                    return failure(msg='您不是项目负责人，不能删除任务！')

            pi_query = db.query(ProjectInformation).filter(ProjectInformation.project_id == project_id).first()
            if not pi_query:
                pi = ProjectInformation()
                pi.project_id = project_id
                pi.project_objectives = project_objectives
                pi.order_staff_data = order_staff_data
                pi.budget_staff_data = budget_staff_data
                pi.creat_user_id = user_query[2]
                pi.creat_user_name = user_query[3]
                db.add(pi)
            else:
                pi_query.project_objectives = project_objectives
                pi_query.order_staff_data = order_staff_data
                pi_query.budget_staff_data = budget_staff_data
                pi_query.edit_time = datetime.datetime.now()
                pi_query.edit_user_id = user_query[2]
                pi_query.edit_user_name = user_query[3]

        return ok()

    def chandao_project_update_information_get(self, project_id):
        '''
        获取基本信息
        O  order    订单人天，指合同签约人天，作为参考。
        B  buget    预算人天，指完成蓝图评审后的实施和开发人天。
        A  actuality  实际人天，交付人员按自然日提报的实际工作时间分配的累计。
        V  value     有效人天，所有交付工程师包括实施顾问和开发人员的有效人
        
        :param project_id:
        :return:
        '''
        pass

    def chandao_project_calendar(self, year, month):
        add_list = []
        week_dict = {0: '星期一', 1: '星期二', 2: '星期三', 3: '星期四', 4: '星期五', 5: '星期六', 6: '星期日'}
        point_day = 1
        workdays = get_workdays(year, month)
        work_days = [v.day for v in workdays]
        month_all_days = calendar.monthrange(year, month)[1]

        while point_day <= month_all_days:
            if point_day in work_days:
                is_workdays = 1
            else:
                is_workdays = 0

            c = Calendar()
            c.year = year
            c.month = month
            c.day = point_day
            string_day = to_string(year) + to_string(month).rjust(2, '0') + to_string(point_day).rjust(2, '0')
            c.weekday = week_dict.get(datetime.datetime.strptime(string_day, "%Y%m%d").weekday())
            c.is_workdays = is_workdays

            point_day += 1

            add_list.append(c)

        work_days = []
        with DbSession.create(EnumDb.main) as db:
            query = db.query(Calendar).filter(Calendar.year == year).filter(Calendar.month == month).first()
            if not query:
                db.bulk_save_objects(add_list)

        with DbSession.create(EnumDb.main) as db:
            get_query = db.query(Calendar).filter(Calendar.year == year).filter(Calendar.month == month)
            for v in get_query:
                work_days.append(format_dict(v.get_dict()))

        return ok(data=work_days)

    def chandao_project_calendar_update(self, update_dict):
        with DbSession.create(EnumDb.main) as db:
            for key, value in update_dict.items():
                db.query(Calendar).filter(Calendar.id == to_int(key)).update({Calendar.is_workdays: to_int(value)}, synchronize_session='fetch')

        return ok()

    def chandao_project_task_working_daily_get_list(self, select_user_name, page, size):
        if select_user_name:
            select_query = WorkReporting.reporting_user_name.like('%' + select_user_name + '%')
        else:
            select_query = True

        with DbSession.create(EnumDb.main) as db:
            list = []
            user = db.query(User.is_admin).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
            if not user:
                return ok(data={'count': 0, 'list': []})

            is_admin = user[0]

            if is_admin:
                filter_user_id = True
            else:
                filter_user_id = WorkReporting.reporting_user_id == self.user_id

            count = db.query(func.count(WorkReporting.id)).filter(select_query).filter(filter_user_id).first()
            if not count:
                return ok(data={'count': 0, 'list': []})

            wr_query = db.query(WorkReporting). \
                filter(select_query). \
                filter(filter_user_id). \
                order_by(WorkReporting.reporting_date.desc()). \
                offset(page * size).limit(size)
            for v in wr_query:
                list.append(format_dict(v.get_dict()))

            return ok(data={'count': count[0], 'list': list})

    def chandao_project_task_working_daily_add(self, reporting_msg, reporting_date):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(WorkReporting).filter(WorkReporting.reporting_user_id == self.user_id).filter(WorkReporting.reporting_date == reporting_date).first()
            if query:
                return failure(msg='您已经提交过今天的工作日报！')

            wr = WorkReporting()
            wr.reporting_user_id = self.user_id
            wr.reporting_user_name = self.user_name
            wr.reporting_date = reporting_date
            wr.reporting_msg = reporting_msg
            wr.creat_time = datetime.datetime.now()
            db.add(wr)

        return ok()

    def chandao_project_actuality_add(self, actuality_list, year, month, is_edit=0):
        with DbSession.create(EnumDb.main) as db:
            if is_edit:
                query = db.query(ProjectActualityDetail). \
                    filter(ProjectActualityDetail.year == year). \
                    filter(ProjectActualityDetail.month == month). \
                    filter(ProjectActualityDetail.creat_user_name == self.user_id). \
                    first()
                pa_id = query.id

                db.query(ProjectActualityDetail). \
                    filter(ProjectActualityDetail.year == year). \
                    filter(ProjectActualityDetail.month == month). \
                    filter(ProjectActualityDetail.creat_user_name == self.user_id). \
                    delete(synchronize_session='fetch')

            else:
                pa = ProjectActuality()
                pa.year = year
                pa.month = month

                pa.creat_user_id = self.user_id
                pa.creat_user_name = self.user_name

                db.add(pa)
                db.flush()
                pa_id = pa.id

            for v in actuality_list:
                v = to_dict(v)
                pad = ProjectActualityDetail()
                pad.project_actuality_id = pa_id
                pad.year = year
                pad.month = month
                pad.project_id = v['project_id']
                pad.project_no = v.get('project_no')
                pad.project_name = v['project_name']
                pad.day = v['day']
                pad.time_taken = v['time_taken']
                pad.creat_user_id = self.user_id
                pad.creat_user_name = self.user_name

                db.add(pad)

        return ok()

    def chandao_project_actuality_get_list(self, year, month, user_id):
        list = []
        with DbSession.create(EnumDb.main) as db:
            user_query = db.query(User.is_admin).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()

            is_admin = user_query[0]

            if is_admin:
                select_user = True
            else:
                select_user = ProjectActuality.creat_user_id == user_id

            query_count = db.query(func.count(ProjectActuality.id)). \
                filter(ProjectActuality.year == year). \
                filter(ProjectActuality.month == month). \
                filter(select_user). \
                order_by(ProjectActuality.creat_user_id). \
                first()

            if not query_count[0]:
                return ok(data={'count': 0, 'list': list})

            query = db.query(ProjectActuality). \
                filter(ProjectActuality.year == year). \
                filter(ProjectActuality.month == month). \
                filter(select_user). \
                order_by(ProjectActuality.creat_user_id)

            for v in query:
                list.append(format_dict(v.get_dict()))

            return ok(data={'count': query_count[0], 'list': list})

    def chandao_project_actuality_get_first(self, year, month, user_id):

        list = []
        with DbSession.create(EnumDb.main) as db:
            query_count = db.query(ProjectActualityDetail). \
                filter(ProjectActualityDetail.year == year). \
                filter(ProjectActualityDetail.month == month). \
                filter(ProjectActualityDetail.creat_user_id == user_id). \
                order_by(ProjectActualityDetail.id)

            for v in query_count:
                list.append(format_dict(v.get_dict()))

        return ok(data=list)

    def chandao_project_actuality_get_month(self, year, start_month, end_month):
        list = []
        chandao_project_ids = []
        with DbSession.create(EnumDb.main) as db:
            query = db.query(func.sum(ProjectTaskWorkingDailyDetail.staff_time),
                             ProjectTaskWorkingDailyDetail.project_name,
                             ProjectTaskWorkingDailyDetail.reporting_person_user_name,
                             ProjectTaskWorkingDailyDetail.month,
                             ProjectTaskWorkingDailyDetail.chandao_project_id). \
                filter(ProjectTaskWorkingDailyDetail.year == year). \
                filter(ProjectTaskWorkingDailyDetail.month >= start_month). \
                filter(ProjectTaskWorkingDailyDetail.month <= end_month). \
                filter(ProjectTaskWorkingDailyDetail.chandao_project_id > 0). \
                group_by(ProjectTaskWorkingDailyDetail.month,
                         ProjectTaskWorkingDailyDetail.project_name,
                         ProjectTaskWorkingDailyDetail.reporting_person_user_name,
                         ProjectTaskWorkingDailyDetail.chandao_project_id)

            for v in query:
                data = {
                    'time_taken': v[0],
                    'project_no': '',
                    'project_name': v[1],
                    'creat_user_name': v[2],
                    'month': v[3],
                    'chandao_project_id': v[4]
                }
                list.append(format_dict(data))
                chandao_project_ids.append(v[4])

            project_msg_dict = {}
            project_msg_query = db.query(ChandaoProject.chandao_project_id, ChandaoProject.code, ChandaoProject.project_name). \
                filter(ChandaoProject.chandao_project_id.in_(chandao_project_ids))
            for v in project_msg_query:
                project_msg_dict[v[0]] = {'code': v[1], 'project_name': v[2]}

        for p in list:
            chandao_project_id = p['chandao_project_id']
            p['chandao_project_code'] = project_msg_dict[chandao_project_id]['code']
            p['project_name'] = project_msg_dict[chandao_project_id]['project_name']

        return ok(data=list)

    def chandao_project_actuality_get_month_export(self, year, start_month, end_month):
        res = self.chandao_project_actuality_get_month(year, start_month, end_month)
        if not res.is_ok():
            return failure(msg='导出失败！')

        list = res.data
        heads_0 = to_string(year) + '年' + to_string(start_month) + '月~' + to_string(end_month) + '月人天统计导出'
        file_nama = 'temporary-' + heads_0 + '-' + to_string(to_timestamp(datetime.datetime.now())) + '.xls'

        export_res = self.form_excel_write(list, file_nama, heads_0)
        if not export_res:
            return failure(msg='导出失败')

        path = os.path.join('https://wechatserver.qianxingit.com/excel/', file_nama)

        return ok(data=path)

    def form_excel_write(self, list, file_nama, heads_0):
        heads = ['月份',
                 '项目合同编号',
                 '项目名称',
                 '项目成员',
                 '天数'
                 ]

        data_list = []

        for v in list:
            v_lsit = []
            v_lsit.append(v['month'])
            v_lsit.append(v['chandao_project_code'])
            v_lsit.append(v['project_name'])
            v_lsit.append(v['creat_user_name'])
            v_lsit.append(v['time_taken'])

            data_list.append(v_lsit)

        data_list.insert(0, heads)

        res = self.write_to_excel(rows=data_list, file_name_1=file_nama, heads_0=heads_0, row_len=4)
        if not res:
            return failure(msg='导出失败')

        size = get_FileSize(EXCEL_CACHE, file_nama)
        with DbSession.create(EnumDb.main) as db:
            f = File()
            f.file_name = file_nama
            f.old_name = file_nama
            f.time = datetime.datetime.now()
            f.is_temporary = 1
            f.host = SERVER_HOST
            f.path = EXCEL_CACHE
            f.size = size
            db.add(f)

        return ok(data=file_nama)

    def write_to_excel(self, rows,
                       file_name_1,
                       head_style_str=r'pattern: pattern solid, fore_colour pale_blue;font:height 240; align: vert centre, horiz center;',
                       heads_0='',
                       row_len=0):
        '''
        写入excel
        :param user_id:
        :type user_id:
        :param directory:
        :type directory:
        :param rows:
        :type rows:
        :param head_style_str:
        :type head_style_str:
        :return:
        :rtype:
        '''
        header_style = easyxf('pattern: pattern solid, fore_colour pale_blue;font:height 240; align: vert centre, horiz center;')
        heads_0_style = self.style_center(height=400)
        col_style = self.style_center(warp=False)

        try:
            f = xlwt.Workbook(encoding='utf-8')  # 创建工作簿
            work_sheet = f.add_sheet('Sheet1', cell_overwrite_ok=True)  # 创建sheet

            if heads_0:
                row_index = 1
                work_sheet.write_merge(0, 0, 0, row_len, heads_0, heads_0_style)  # 合并第一行，第0列到第14列的单元格 居中
            else:
                row_index = 0

            error_no = ''
            for row in rows:
                col_end = get_len(row)
                if heads_0:
                    if row_index == 1:
                        for col in range(0, col_end):
                            if header_style:
                                work_sheet.write(row_index, col, row[col], header_style)
                            else:
                                work_sheet.write(row_index, col, row[col], header_style)

                            if col == 1:
                                work_sheet.col(col).width = get_len(str(row[col])) * 1500
                            elif col == 2:
                                work_sheet.col(col).width = get_len(str(row[col])) * 6000
                            elif col == 3:
                                work_sheet.col(col).width = get_len(str(row[col])) * 1500

                    else:
                        for col in range(0, col_end):
                            work_sheet.write(row_index, col, to_string(row[col]), col_style)

                    error_no = row[0]
                    row_index += 1

                else:
                    if row_index == 0:
                        for col in range(0, col_end):
                            if header_style:
                                work_sheet.write(row_index, col, row[col], header_style)
                            else:
                                work_sheet.write(row_index, col, row[col], header_style)

                            if col == 1:
                                work_sheet.col(col).width = get_len(str(row[col])) * 1500
                            elif col == 2:
                                work_sheet.col(col).width = get_len(str(row[col])) * 6000
                            elif col == 3:
                                work_sheet.col(col).width = get_len(str(row[col])) * 1500

                    else:
                        for col in range(0, col_end):
                            work_sheet.write(row_index, col, to_string(row[col]), col_style)

                    error_no = row[0]
                    row_index += 1

            file_name = os.path.join(EXCEL_CACHE, file_name_1)
            f.save(file_name)

            return file_name
        except Exception as e:
            print(error_no)
            log(traceback.format_exc(), ' write_to_excel', '创建excel失败')
            return ''

    def style_center(self, warp=True, bold=False, background_color=None, font_color=None, height=0x00C8, border_color=None, borders=True):
        '''
        中间对齐
        :param warp:是否换行
        :type warp: bool
        :param bold:是否加粗:
        :type bold: bool
        :param background_color:   背景色
        :type background_color: int
        :param font_color: 字体颜色
        :type font_color:  int
        :return:  样式
        :rtype: XFStyle
        '''
        style = xlwt.XFStyle()  # 初始化样式
        alignment = xlwt.Alignment()
        alignment.horz = xlwt.Alignment.HORZ_CENTER  # 中间对齐
        alignment.vert = xlwt.Alignment.VERT_CENTER  # 水平对齐
        self._common_style(alignment, background_color, bold, font_color, style, warp, height, border_color, borders)
        return style

    def _common_style(self, alignment, background_color, bold, font_color, style, warp, height, border_color, borders=True):
        if alignment is not None:
            if warp:
                alignment.wrap = xlwt.Alignment.WRAP_AT_RIGHT  # 自动换行
            style.alignment = alignment

        if background_color and isinstance(background_color, int):
            pattern = xlwt.Pattern()
            pattern.pattern_fore_colour = background_color
            pattern.pattern = xlwt.Pattern.SOLID_PATTERN
            style.pattern = pattern

        font = xlwt.Font()
        if font_color and isinstance(font_color, int):
            font.colour_index = font_color

        if bold:
            font.bold = True
        font.height = height
        font.name = u'微软雅黑'

        style.font = font

        if borders:
            borders = xlwt.Borders()
            borders.left = 1
            borders.right = 1
            borders.top = 1
            borders.bottom = 1
            # borders.bottom_colour = border_color
            style.borders = borders

    def update_new_chandao_project_id(self):
        # 查询进行中的项目
        project_result = self.chandao_get_porject_list()
        if not project_result.is_ok():
            return failure(msg=project_result.msg)

        if not project_result.data:
            return ok()

        project_list = project_result.data

        with DbSession.create(EnumDb.main) as db:
            for v in project_list:
                code = v.get('code')
                db.query(ChandaoProject).filter(ChandaoProject.code == code). \
                    update({ChandaoProject.new_chandao_project_id: v.get('id')}, synchronize_session='fetch')

        # 更新相关的表数据
        oldId_newId_dict = {}
        with DbSession.create(EnumDb.main) as db:
            query = db.query(ChandaoProject.new_chandao_project_id, ChandaoProject.old_chandao_project_id)
            for v in query:
                oldId_newId_dict[v[1]] = v[0]

            for cpp in db.query(ChandaoProjectPayback):
                if oldId_newId_dict.get(cpp.chandao_project_id):
                    cpp.chandao_project_id = oldId_newId_dict.get(cpp.chandao_project_id)

            for cpsp in db.query(ChandaoProjectSummaryPhases):
                if oldId_newId_dict.get(cpsp.chandao_project_id):
                    cpsp.chandao_project_id = oldId_newId_dict.get(cpsp.chandao_project_id)

            for cptu in db.query(ChandaoProjectTeamUser):
                if oldId_newId_dict.get(cptu.chandao_project_id):
                    cptu.chandao_project_id = oldId_newId_dict.get(cptu.chandao_project_id)

            for cpwp in db.query(ChandaoProjectWeekPlan):
                if oldId_newId_dict.get(cpwp.chandao_project_id):
                    cpwp.chandao_project_id = oldId_newId_dict.get(cpwp.chandao_project_id)

            for ptwdd in db.query(ProjectTaskWorkingDailyDetail):
                if oldId_newId_dict.get(ptwdd.chandao_project_id):
                    ptwdd.chandao_project_id = oldId_newId_dict.get(ptwdd.chandao_project_id)

            for ptwwd in db.query(ProjectTaskWorkingWeekDailyDetail):
                if oldId_newId_dict.get(ptwwd.chandao_project_id):
                    ptwwd.chandao_project_id = oldId_newId_dict.get(ptwwd.chandao_project_id)

            for ptwwnd in db.query(ProjectTaskWorkingWeekDailyNextDetail):
                if oldId_newId_dict.get(ptwwnd.chandao_project_id):
                    ptwwnd.chandao_project_id = oldId_newId_dict.get(ptwwnd.chandao_project_id)

            for cp in db.query(ChandaoProject):
                cp.chandao_project_id = cp.new_chandao_project_id

            for cpsb in db.query(ChandaoProjectShareBinding):
                cpsb.project_id = oldId_newId_dict.get(cpsb.project_id)

        return ok()


if __name__ == '__main__':
    cdm = ChandaoManage({'user_id': 1, 'user_name': 'super_admin'})
    res = cdm.save_project()
    # res = cdm.update_new_chandao_project_id()
    # res = cdm.chandao_project_actuality_get_month_export(year='2024', start_month=12, end_month=12)
    print(res)
