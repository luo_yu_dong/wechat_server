#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: server_base.py
@time: 2020/10/28 17:19
@desc:
'''


import traceback
from datetime import datetime

from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.log_controller import log
from framework.msg import ok
from framework.utilities import to_string, to_list
from models.exception_log.exception_logs import ExceptionLogs
from models.exception_log.sys_operation_log import SysOperationLog
from models.users.user import User


class ServerBase():
    def __init__(self, user):
        self.user = user
        self.operation_user_id = user['user_id']

    def log(self, object_ids, model, operation, db=None):
        try:
            add_lg = self.__save_log__(to_list(object_ids), model, operation)

            if not db:
                with DbSession.create(EnumDb.main) as db:
                    db.bulk_save_objects(add_lg)
            else:
                db.bulk_save_objects(add_lg)
        except Exception as e:
            log(traceback.format_exc(), 'operation=%s,object_id=%s,model=%s' % (to_string(operation), to_string(object_ids), to_string(model)))

    # 操作日志记录
    def __save_log__(self, object_ids, model, operation):
        add_list = []
        for object_id in object_ids:
            lg = SysOperationLog()
            lg.object_id = object_id
            lg.model = model
            lg.operation = operation
            lg.operation_time = datetime.now()
            lg.operation_user_id = self.operation_user_id
            add_list.append(lg)

        return add_list

    def exception_logs(self, body):
        '''
        body = {
            'body': to_string(e, '未获到异常结果'),
            'type': to_string(to_int(error_type)),
            'pos': except_position,
            'val': option_value_str,
            'time': to_string(datetime.now())
        }
        '''
        with DbSession.create(EnumDb.main) as db:
            lg = ExceptionLogs()
            lg.type = body.get('type')
            lg.body = body.get('body')
            lg.log_time = datetime.now()
            lg.position = body.get('pos')
            lg.option_value = body.get('val')
            db.add(lg)

        return ok()

    # def server_file(self, file_name, file_size, file_path):
    #     try:
    #         with DbSession.create(EnumDb.main) as db:
    #             f = File()
    #             f.file_name = file_name
    #             f.old_name = file_name
    #             f.time = datetime.now()
    #             f.is_temporary = 1
    #             f.host = SERVER_HOST
    #             f.path = file_path
    #             f.size = file_size
    #             db.add(f)
    #     except:
    #         log(traceback.format_exc(), 'server_file')

    def get_user_name_dict(self, db, user_ids):
        userId_Name = {}
        user_query = db.query(User.user_id, User.user_name).filter(User.user_id.in_(user_ids))
        for v in user_query:
            userId_Name[v[0]] = v[1]

        return userId_Name