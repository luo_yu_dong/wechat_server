#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: wechat_program_base.py
@time: 2020/10/19 9:14
@desc:
'''
import json
import os
import time
import traceback
import uuid
from datetime import datetime
from urllib.parse import urlencode
from urllib.request import Request, urlopen

import emoji
import requests
from tornado.gen import coroutine
from tornado.httpclient import HTTPRequest, AsyncHTTPClient, HTTPError
from wechatpy import WeChatClient

from enums.enum_cache_type import EnumCacheType
from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.log_controller import log
from framework.msg import ok, failure
from framework.utilities import to_string, to_dict, current_timestamp, to_decimal, to_timestamp
from models.file.file import File
from models.file.qrcode_log import QrcodeLog
from models.users.user import User
from models.users.wechat_user import WechatUser
from services.file_manage import FileManage
from setting import FILE_CACHE, SERVER_HOST, WECHAT_WIKI_APPID, WECHAT_WIKI_SECRET
from thirdpart import redis_helper as re


class WechatProgramBase():

    def wechat_login(self, js_code, appid, secret):
        req_params = {
            "appid": appid,  # 小程序的 ID
            "secret": secret,  # 小程序的 secret
            "js_code": js_code,
            "grant_type": 'authorization_code'
        }
        req_result = requests.get(url='https://api.weixin.qq.com/sns/jscode2session', params=req_params, timeout=3, verify=False)
        return req_result.json()

    # 保存用户信息
    def save_user_msg(self, user_info, is_customer_service, type, encryptedData=None, sessionKey=None, is_attention_wiki=0):
        '''

        :param user_info:
        :param business_id:
        :param user_id:
        :param is_customer_service:  1-客服，2-客户
        :param type: 1-小程序，2-公众号
        :return:
        '''
        try:
            if not user_info:
                return ok()

            unionid = to_string(user_info.get('unionid'), '')
            nike_name = user_info.get('nickName')

            logo = user_info.get('avatarUrl')
            new_logo = user_info.get('new_logo')
            openid = user_info.get('openid')

            if new_logo:
                f = FileManage()
                res = f.file_save(file_list=new_logo)
                if res.is_ok():
                    if res.data['file_list']:
                        data_file = res.data['file_list']
                    else:
                        data_file = ''
                else:
                    data_file = ''
                    return failure(msg='保存文件失败！')
            else:
                data_file = ''

            new_logo = data_file

            if nike_name:
                nike_name = emoji.demojize(nike_name)

            print('---------------------------------- 打印解析后 nikename ---------------------------------- ')
            print('---------------------------------- 打印解析后 nikename ---------------------------------- ')
            print('---------------------------------- 打印解析后 nikename ---------------------------------- ')
            print('---------------------------------- 打印解析后 nikename ---------------------------------- ')
            print(nike_name)

            if not sessionKey:
                with DbSession.create(EnumDb.main) as db:
                    # 查询用户信息是否存在
                    user_query = db.query(WechatUser).filter(WechatUser.open_id == openid).first()
                    sessionKey = user_query.session_key

            with DbSession.create(EnumDb.main) as db:
                # 查询用户信息是否存在
                user_query = db.query(WechatUser).filter(WechatUser.open_id == openid).filter(WechatUser.type == type).first()
                if not user_query:
                    wu = WechatUser()
                    wu.union_id = unionid
                    wu.open_id = openid
                    wu.logo = to_string(logo)
                    wu.nike_name = nike_name
                    wu.type = type
                    wu.is_customer_service = is_customer_service
                    wu.is_attention_wiki = is_attention_wiki
                    if is_attention_wiki:
                        wu.attention_wiki_time = datetime.now()
                    wu.session_key = sessionKey
                    wu.imgs = to_string(new_logo)
                    db.add(wu)
                    db.flush()
                    id = wu.id
                else:
                    # 如果原来是客服，现在不是客服了，就要把user_id 清空掉
                    if user_query.is_customer_service == 1 and is_customer_service == 0:
                        user_query.user_id = 0

                    if logo:
                        user_query.logo = to_string(logo)
                    if new_logo:
                        user_query.imgs = to_string(new_logo)
                    if nike_name:
                        user_query.nike_name = nike_name
                    if unionid:
                        user_query.union_id = unionid
                    user_query.is_customer_service = is_customer_service
                    user_query.session_key = sessionKey

                    user_query.is_attention_wiki = is_attention_wiki
                    if is_attention_wiki:
                        user_query.attention_wiki_time = datetime.now()

                    id = user_query.id

            with DbSession.create(EnumDb.main) as db:
                # 查询用户信息是否存在
                user_query = db.query(WechatUser).filter(WechatUser.open_id == openid).filter(WechatUser.type == 1).first()

                new_logo = user_query.imgs
                new_nike_name = user_query.nike_name

                if unionid:
                    attention_query = db.query(WechatUser).filter(WechatUser.union_id == unionid).filter(WechatUser.type == 2).first()
                    if not attention_query:
                        is_attention_wiki = 0
                        bind_user_id = 0

                    else:
                        is_attention_wiki = attention_query.is_attention_wiki
                        user_query.is_attention_wiki = 1
                        attention_query.logo = new_logo
                        attention_query.nike_name = new_nike_name

                        bind_user_id = attention_query.user_id
                else:
                    is_attention_wiki = 0
                    bind_user_id = 0

                if bind_user_id:
                    # 查询这个人的信息
                    user_query = db.query(User).filter(User.user_id == bind_user_id).first()
                    if not user_query:
                        judge_is_customer_service = 0
                        judge_is_customer_service_admin = 0
                        bind_user_name = ''
                        role = ''
                    else:
                        judge_is_customer_service = 1
                        judge_is_customer_service_admin = user_query.is_admin
                        bind_user_name = user_query.user_name
                        role = user_query.role
                else:
                    judge_is_customer_service = 0
                    judge_is_customer_service_admin = 0
                    bind_user_name = ''
                    role = ''

            if unionid:
                with DbSession.create(EnumDb.main) as db:
                    attention_query = db.query(WechatUser).filter(WechatUser.union_id == unionid).filter(WechatUser.type == 2).first()
                    if attention_query:
                        is_customer_service = attention_query.is_customer_service

                        query = db.query(WechatUser).filter(WechatUser.union_id == unionid).all()
                        for v in query:
                            v.is_customer_service = is_customer_service
                            v.is_attention_wiki = is_attention_wiki

                            if new_nike_name:
                                v.nike_name = new_nike_name

            return ok(data={'wechat_user_id': to_string(id),
                            'openid': to_string(openid),
                            'unionid': to_string(unionid),
                            'session_key': sessionKey,
                            'nick_name': new_nike_name,
                            'logo': new_logo,
                            'is_attention_wiki': is_attention_wiki,
                            'judge_is_customer_service_admin': judge_is_customer_service_admin,
                            'judge_is_customer_service': judge_is_customer_service,
                            'customer_server_user_id': bind_user_id,
                            'bind_user_name': bind_user_name,
                            'role': role
                            })
        except:
            log(traceback.format_exc(), 'WeChatProgramManage.save_user_msg(user_info=%s)' % (to_string(user_info)), '获取信息失败!')
            return failure(msg='请求失败')

    # 获取小程序的 access_token
    def get_access_token(self, appid, secret):
        try:
            req_result = requests.get('https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s' %
                                      (to_string(appid), to_string(secret)))

            req_dict = req_result.json()
            access_token = req_dict['access_token']
            expires_in = req_dict['expires_in']

            timestamp = current_timestamp()

            # 存到 sns 的redis
            re.set(key='program_access_token_time', value=to_string(current_timestamp() + 7200), time=expires_in - 60 * 5, cache_type=EnumCacheType.sns)
            re.set(key='program_access_token', value=to_string(access_token), time=expires_in - 60 * 5, cache_type=EnumCacheType.sns)

            return timestamp, access_token
        except:
            log(traceback.format_exc(), 'get_access_token')
            return 0, None

    # @coroutine
    def make_qrcode(self):
        try:
            now = current_timestamp()  # 当前时间戳
            # is_customer_service, type, item_id, user_id, project_id, model
            scene_str = '1-_-1-_-0-_-0-_-0-_-0'  # 生成公众号客服专用二维码

            with DbSession.create(EnumDb.main) as db:
                wq_ticket = db.query(QrcodeLog).filter(QrcodeLog.is_customer_service == 1).filter(QrcodeLog.project_id == 0).first()
                if wq_ticket:
                    # if to_int(wq_ticket.creat_timestamp) + 2592000 > now:
                    return ok(data={'url': to_string(wq_ticket.qrcode)})

            client = WeChatClient(WECHAT_WIKI_APPID, WECHAT_WIKI_SECRET)
            res = client.qrcode.create({
                # 'expire_seconds': 2592000,  # 有效时间（秒）
                'action_name': 'QR_LIMIT_STR_SCENE',
                'action_info': {  # 二维码详细信息
                    'scene': {'scene_str': scene_str},  # QR_STR_SCENE时是字符串
                }
            })

            qrcdoe = client.qrcode.get_url(res['ticket'])

            # 查询这个二维码是否是生成过的
            with DbSession.create(EnumDb.main) as db:
                wq_ticket = db.query(QrcodeLog).filter(QrcodeLog.is_customer_service == 1).filter(QrcodeLog.project_id == 0).first()
                if not wq_ticket:
                    ql = QrcodeLog()
                    ql.ticket = res['ticket']
                    ql.qrcode = to_string(qrcdoe)
                    ql.is_customer_service = 1
                    ql.creat_timestamp = now
                    ql.project_id = 0
                    db.add(ql)

                else:
                    wq_ticket.ticket = res['ticket']
                    wq_ticket.qrcode = to_string(qrcdoe)
                    wq_ticket.creat_timestamp = now

            return ok(data={'url': to_string(qrcdoe)})

        except Exception as e:
            log(traceback.format_exc(), 'WechatProgramBase.make_qrcode')
            return failure(msg='获取二维码失败!')

    @coroutine
    def post(self, post_body, url):
        # 模板消息请求URL
        headers = {}
        headers['Content-Type'] = "application/json"
        reques = HTTPRequest(url, method='POST', body=json.dumps(post_body), headers=headers)
        client = AsyncHTTPClient()
        try:
            res = yield client.fetch(reques)
            return res.body
        except HTTPError as e:
            log(traceback.format_exc(), except_position='report_export_manage.post')
        except Exception as e:
            log(traceback.format_exc(), except_position='report_export_manage.post')
        return None

    @coroutine
    def send_post(self, url, post_data, header=None):
        if isinstance(post_data, dict):
            post_data = json.dumps(post_data, ensure_ascii=False)

        reqs = HTTPRequest(url, method='POST', headers=header, body=post_data)
        clients = AsyncHTTPClient()

        try:
            res = yield clients.fetch(reqs)
            body = to_dict(res.body)
            if body.get('errcode'):
                log('WechatProgramBase.to_post()', 'url=%s,body=%s,post_data=%s' % (to_string(url), to_string(body), to_string(post_data)))
                return {}
            return body
        except HTTPError as e:
            log('WechatProgramBase.to_get()', 'url=%s,post_data=%s' % (to_string(url), to_string(post_data)))
            return {}

    @coroutine
    def to_short_link(self, access_token, url):
        wexin_url = 'https://api.weixin.qq.com/cgi-bin/shorturl?access_token=' + access_token
        post_data = {
            'action': 'long2short',
            'long_url': url
        }

        request_result = yield self.send_post(wexin_url, post_data)
        if not request_result:
            log(e='转换短网址失败', except_position='to_short_link.to_post()', option_value=to_string(request_result))
            return False, '转换短网址失败'
        else:
            if 'short_url' not in request_result:
                log(e='转换短网址失败', except_position='to_short_link.to_post()', option_value=to_string(request_result))
                return True, url
            else:
                return True, request_result['short_url']

    def add_imgs(self, req_data, ext, old_ext, body, old_file_name, access_token, file_url):
        try:
            # 存放文件名称
            filename = 'temporary-' + str(uuid.uuid1()).replace('-', '') + '-' + to_string(to_timestamp(datetime.now())) + '.' + ext

            size = to_decimal(len(body) / (1024 * 1024))

            full_path = os.path.join(FILE_CACHE, filename)  # 文件存放路径

            file = open(full_path, 'wb')
            file.write(body)
            file.close()

            # files = {
            #     "media": SERVER_IMGS_POST + filename,
            #     # "media": 'http://39.77.123.192:7080/images/temporary-f9c3164ac74211ea8aa200e269245ecb-1594890106.jpeg'
            # }
            #
            # check_url = "https://api.weixin.qq.com/wxa/img_sec_check?access_token=" + access_token
            # response = requests.post(check_url, files=files)
            # content = response.content
            # ret = to_dict(content)
            # if ret.get('errcode') != 0:
            #     return failure(msg='您上传的图片含有非法内容，请重新上传！')

            time = datetime.now()
            with DbSession.create(EnumDb.main) as db:
                f = File()
                f.file_name = filename
                f.old_name = old_file_name
                f.time = time
                f.is_temporary = 1
                f.host = SERVER_HOST
                f.path = FILE_CACHE
                f.size = size
                db.add(f)
                db.flush()
                file_id = f.id

            data = {'file_name': filename,
                    'old_name': old_file_name,
                    'type': 'new',
                    'size': to_string(size),
                    'file_id': file_id,
                    'imgs_path': FILE_CACHE}

            print(data)
            return ok(data=data)
        except:
            log(traceback.format_exc())
            return failure(msg='操作失败')


class XviHttp():
    def __init__(self, url, echo=False, cookie_key=None):
        self.echo = echo
        self.url = url
        self.status_code = 404
        self.reason = ''
        self.__response_json = {}
        self.cookie_key = cookie_key

    def http_post(self, dic, header_dic=None):
        postdata = json.dumps(dic).encode('utf8')
        req = Request(self.url, postdata)
        self.__add_headers(header_dic, req)
        req.get_method = lambda: "POST"
        return self.__http_method(req)

    def http_get(self, query_str, header_dic=None):
        if isinstance(query_str, dict):
            query_str = urlencode(query_str)

        if query_str:
            if '?' in self.url:
                self.url += r'&'
            else:
                self.url += r'?'

            self.url += query_str

        req = Request(self.url)
        self.__add_headers(header_dic, req)
        return self.__http_method(req)

    def http_put(self, dic, header_dic=None):
        req = Request(self.url, json.dumps(dic).encode('utf8'))
        self.__add_headers(header_dic, req)
        req.get_method = lambda: "PUT"
        return self.__http_method(req)

    def http_delete(self, header_dic=None):
        req = Request(self.url)
        self.__add_headers(header_dic, req)
        req.get_method = lambda: "DELETE"
        return self.__http_method(req)

    def __http_method(self, req):
        st = time.time()
        try:
            res = urlopen(req)
            self.__save_set_cookies(res)  # 保存cookie
            self.status_code = res.code  # 保存状态
            self.reason = res.reason  # 保存原因
            self.__response_json = XviHttp.to_dict(XviHttp.to_str(res.read()))
        except  HTTPError as e:
            self.status_code = e.code
            self.__response_json = XviHttp.get_error_body(e)
            if self.echo:
                print(e)
        except Exception as ex:
            if self.echo:
                print(ex)
        finally:
            et = time.time()
            self.__response_json['st'] = st
            self.__response_json['et'] = et
            return self.__response_json

    def __add_headers(self, header_dic, req):
        if header_dic and isinstance(header_dic, dict):
            for key in header_dic:
                req.add_header(key, header_dic[key])

    def json(self):
        return self.__response_json or {}

    def __save_set_cookies(self, req):
        if not self.cookie_key:
            return

        cookies = {}
        for v in req.headers._headers:
            if v[0] != 'Set-Cookie':
                continue
            cks = v[1].split(';')
            name = []
            for c in cks[0]:
                if c != '=':
                    name.append(c)
                    continue
                break

            cookies[''.join(name)] = cks[0][len(name) + 2:-1]
        CookieManage.set(self.cookie_key, cookies)

    @staticmethod
    def add_cookies(header, cookie_key=None):
        if not cookie_key:
            return header
        cookie = CookieManage.get(cookie_key)
        if not cookie:
            return header
        if not header:
            header = {}
        header['Cookie'] = XviHttp.to_str(CookieManage.get(cookie_key))
        return header

    @staticmethod
    def to_dict(s, replace_obj={}):
        try:
            return json.loads(s, encoding='utf-8')
        except:
            return replace_obj

    @staticmethod
    def to_str(obj, failure_replace_value=None):
        try:
            if obj is None:
                return failure_replace_value

            if isinstance(obj, str):
                new_str = obj
            elif isinstance(obj, bytes):
                new_str = obj.decode(encoding='utf-8')
            elif isinstance(obj, (list, dict)):
                new_str = json.dumps(obj, ensure_ascii=False)
            else:
                new_str = str(obj)
            return new_str
        except:
            return failure_replace_value

    def report(self, verify_data_is_null=False):
        '''
        报告执行状态
        :param verify_data_is_null:是否坚持数据为空
        :type verify_data_is_null:  bool
        '''
        if self.status_code != 200:
            print('失败:{0}\t(状态码:{1})'.format(self.url, str(self.status_code)))
            return

        res = self.json()
        if not res or res.get('status') != 1:
            print('失败:{0}\t(错误信息:{1})'.format(self.url, XviHttp.to_str(res.get('msg'), '')))
            return

        if verify_data_is_null:
            if not res.get('data'):
                print('失败:{0}\t(错误信息:数据为空)'.format(self.url, XviHttp.to_str(res.get('msg'), '')))
                return
        print('成功')

    @staticmethod
    def get_error_body(e):
        try:
            return XviHttp.to_dict(XviHttp.to_str(e.read()))
        except:
            return {}


class CookieManage():
    '''
    cookie管理
    '''
    _cookie_containers = {}

    @classmethod
    def set(cls, key, cookies):
        if not key or not cookies:
            return

        old_cookies = cls._cookie_containers.get(key) or {}
        old_cookies.update(cookies)
        cls._cookie_containers[key] = old_cookies

    @classmethod
    def get(cls, key):
        ls = []
        for v in cls._cookie_containers.get(key, {}).items():
            ls.append('{0}="{1}";'.format(v[0], v[1]))
        return ''.join(ls)

    @classmethod
    def delete(cls, key):
        try:
            if key in cls._cookie_containers:
                del cls._cookie_containers[key]
        except:
            pass


def send_tem_post(url, dic, header=None, echo=False, cookie_key=None):
    '''
    提交post请求
    :param url: 请求地址
    :type url: str
    :param dic:post data
    :type dic: dict
    :param header:http头
    :type header:dict
    :param echo: 是否打印结果
    :type echo: bool
    :param cookie_key: cookie key
    :type cookie_key: object
    :return: XviHttp实例
    :rtype:XviHttp
    '''
    inst = XviHttp(url, echo, cookie_key)
    inst.http_post(dic, header_dic=XviHttp.add_cookies(header, cookie_key))
    return inst
