#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: user_manage.py
@time: 2020/1/2 11:13
@desc:用户管理
'''

import traceback
from datetime import datetime

from sqlalchemy import func, and_, desc

from cache import user_cache
from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.log_controller import trycatch, log
from framework.msg import ok, failure
from framework.utilities import gen_encrypt_password, gen_access_token, gen_expire_in, gen_token, to_string, to_int, format_dict
from models.customer_form.customer_submmint_form import CustomerSubmmintForm
from models.exception_log.remind_logs import RemindLogs
from models.exception_log.sys_operation_log import SysOperationLog
from models.users.user import User
from models.users.user_info_model import UserInfoModel
from models.users.wechat_user import WechatUser
from services.server_base import ServerBase


class UserManage():
    def __init__(self, name=None):
        self.name = name

    # 用户创建
    # @trycatch(failure(msg='创建用户失败！'))
    def user_creat(self, remark, usr, mobile, crm_user, is_admin=0, role=None, logo=None):
        expire = gen_expire_in()
        try:
            with DbSession.create(EnumDb.main) as db:
                user = User()
                user.user_name = self.name
                user.mobile = mobile
                user.crm_user = crm_user
                user.password = gen_encrypt_password(self.name, 'qianxing123456')
                user.create_time = datetime.now()
                user.remark = remark
                user.is_delete = 0
                user.is_admin = is_admin
                user.is_super_admin = 0
                user.expire = expire
                user.token = gen_token(self.name, expire)
                user.logo = logo
                user.role = role
                db.add(user)

            sb = ServerBase(usr)
            sb.log(object_ids=[usr['user_id']], model=User.__tablename__, operation='创建用户')

            return ok()
        except:
            print(traceback.format_exc())
            return failure(msg='操作失败')

    # web用户登陆
    @trycatch(failure(msg='登陆失败！'))
    def user_login(self, usr, pwd, token):
        if pwd:
            if usr['password'] == gen_encrypt_password(self.name, pwd.strip(' ')):
                result = self.__gen_new_access_token()
                if not result.is_ok():
                    return failure(msg=result.msg)

                sb = ServerBase(user=usr)
                sb.log(object_ids=[usr['user_id']], model=User.__tablename__, operation='用户web登录')
                return result
            else:
                return failure(msg='密码错误，请核对后再试！')

        if token:
            if token == gen_access_token(expire=usr['expire'], token=usr['token'], name=self.name):
                result = self.__gen_new_access_token()
                if not result.is_ok():
                    return failure(msg=result.msg)

                sb = ServerBase(user=usr)
                sb.log(object_ids=[usr['user_id']], model=User.__tablename__, operation='用户web登录')
                return result
            else:
                result = failure(msg='请登录')
                result.set_status(401)
                return result

    def remind_un_read_list(self, usr, page, size):
        list = []
        with DbSession.create(EnumDb.main) as db:
            # 获取未读消息数量和列表
            remind_count_quer = db.query(func.count(RemindLogs.id)).filter(RemindLogs.user_id == usr['user_id']).filter(RemindLogs.is_read == 0).first()[0]
            if not remind_count_quer:
                return ok(data={'count': 0, 'list': []})

            item_id_list = []
            remind_quer = db.query(RemindLogs). \
                filter(RemindLogs.user_id == usr['user_id']). \
                filter(RemindLogs.is_read == 0). \
                order_by(desc(RemindLogs.creat_time)). \
                offset(page * size).limit(size)
            for v in remind_quer:
                item_id_list.append(v.item_id)
                list.append(format_dict(v.get_dict()))

            # 查询服务单数据
            form_dict = {}
            form_query = db.query(CustomerSubmmintForm.id, CustomerSubmmintForm.no, CustomerSubmmintForm.title). \
                filter(CustomerSubmmintForm.id.in_(item_id_list))
            for v in form_query:
                form_dict[v[0]] = {'no': v[1], 'title': v[2]}

        new_list = []
        for v in list:
            form_dic = form_dict.get(v['item_id'], {})
            data = dict(v, **form_dic)
            new_list.append(data)

        return ok(data={'count': remind_count_quer, 'list': new_list})

    def remind_list(self, usr, page, size):
        list = []
        with DbSession.create(EnumDb.main) as db:
            # 获取未读消息数量和列表
            remind_count_quer = db.query(func.count(RemindLogs.id)).filter(RemindLogs.user_id == usr['user_id']).first()[0]
            if not remind_count_quer:
                return ok(data={'count': 0, 'list': []})

            item_id_list = []
            user_ids = []
            remind_quer = db.query(RemindLogs). \
                filter(RemindLogs.user_id == usr['user_id']). \
                order_by(RemindLogs.is_read). \
                order_by(desc(RemindLogs.creat_time)). \
                offset(page * size).limit(size)
            for v in remind_quer:
                item_id_list.append(v.item_id)
                user_ids.append(v.opertaion_user_id)

                list.append(format_dict(v.get_dict()))

            # 查询名字
            user_dict = {}
            user_query = db.query(User.user_id, User.user_name).filter(User.user_id.in_(user_ids))
            for v in user_query:
                user_dict[v[0]] = v[1]

            # 查询服务单数据
            form_dict = {}
            form_query = db.query(CustomerSubmmintForm.id, CustomerSubmmintForm.no, CustomerSubmmintForm.title). \
                filter(CustomerSubmmintForm.id.in_(item_id_list))
            for v in form_query:
                form_dict[v[0]] = {'no': v[1], 'title': v[2]}

        new_list = []
        for v in list:
            v['opertaion_user_name'] = user_dict.get(v['opertaion_user_id'], '')
            form_dic = form_dict.get(v['item_id'], {})
            data = dict(v, **form_dic)
            new_list.append(data)

        return ok(data={'count': remind_count_quer, 'list': new_list})

    def remind_read(self, user, remind_id):
        if remind_id == -1:
            cons_remind = True
        else:
            cons_remind = RemindLogs.item_id == remind_id

        with DbSession.create(EnumDb.main) as db:
            db.query(RemindLogs). \
                filter(RemindLogs.type == 0). \
                filter(cons_remind). \
                filter(RemindLogs.user_id == user['user_id']). \
                update({RemindLogs.is_read: 1,
                        RemindLogs.read_time: datetime.now()}, synchronize_session='fetch')

        return ok()

    @trycatch(failure(msg='退出登陆失败'))
    def logout(self, usr):
        phone = to_int(usr['mobile'])
        uid = usr['user_id']
        expire = usr['expire']

        # if token != gen_access_token(expire=expire, token=usr['token'], phone=phone):
        #     result = failure(msg='登出失败')
        #     result.set_status(-2)
        #     return result

        expire = gen_expire_in()
        with DbSession.create(EnumDb.main) as db:
            user = db.query(User).filter_by(user_id=uid).one()
            user.expire = expire

        user_cache.delete(phone)
        user_cache.clear_user_role_cache(uid)

        sb = ServerBase(usr)
        sb.log(object_ids=[uid], model=User.__tablename__, operation='退出登陆')

        return ok()

    @trycatch(failure(msg='登陆失败'))
    def __gen_new_access_token(self, login_come_from=0):
        '''
        登陆成功,设置新的 access_toknen,并保持数据库和更新缓存
        :param phone:
        :type phone:
        :return:
        :rtype:
        '''
        expire = gen_expire_in()
        with DbSession.create(EnumDb.main) as db:
            user = db.query(User).filter_by(user_name=self.name).filter(User.is_delete == 0).first()
            if not user:
                return failure(msg='用户不存在！')
            user.expire = expire
            userinfo = user.get_dict()

            # # 查询用户角色
            # if user.is_super_admin:
            #     role = '系统管理员'
            # elif user.is_admin:
            #     role = '管理员'
            # else:
            #     role = '无角色'
            #     # user_role = db.query(UserRole).filter(UserRole.user_id == userinfo['user_id']).first()
            #     # if not user_role:
            #     #     role = '无角色'
            #     # else:
            #     #     role_query = db.query(Role).filter(Role.role_id == user_role.role_id).first()
            #     #     if not role_query:
            #     #         role = '无角色'
            #     #     else:
            #     #         role = role_query.name

        user_cache.set(self.name, userinfo)
        return_data = UserInfoModel.create_user_info(use_cache_model=userinfo)
        return_data['not_read_message_count'] = 0
        # return_data['role'] = role
        if login_come_from == 0:
            return_data['token'] = gen_access_token(expire=expire, token=userinfo['token'], name=self.name)
        else:
            return_data['app_token'] = gen_access_token(expire=expire, token=userinfo['app_token'], name=self.name)

        return ok(msg='登陆成功', data=return_data)

    @trycatch(failure(msg='请求失败'))
    def user_edit_basics(self, user_id, phone, name, remark, usr, crm_user, logo, role):
        with DbSession.create(EnumDb.main) as db:
            user_query = db.query(User).filter(User.user_id == user_id).filter(User.is_delete == 0).first()
            if not user_query:
                return failure(msg='数据不存在，请刷新后再试！')

            user_phone_query = db.query(User).filter(User.user_id != user_id).filter(User.mobile == phone).filter(User.is_delete == 0).first()
            if user_phone_query:
                return failure(msg='电话号码已存在，请确认后重试！')

            user_query.mobile = phone
            user_query.user_name = name
            user_query.remark = remark
            user_query.crm_user = crm_user
            user_query.logo = logo
            user_query.role = role

            sb = ServerBase(usr)
            sb.log(object_ids=[user_id], model=User.__tablename__, operation='编辑用户信息', db=db)

        return ok()

    @trycatch(failure(msg='请求失败'))
    def user_edit_pwd(self, user_id, pwd, usr):
        expire = gen_expire_in()
        with DbSession.create(EnumDb.main) as db:
            if usr['user_id'] != user_id:
                query = db.query(User).filter(User.user_id == usr['user_id']).filter(User.is_delete == 0).first()
                is_admin = query.is_admin
                if not is_admin:
                    return failure(msg='您不是管理员，不能修改他人密码！')

            user_query = db.query(User).filter(User.user_id == user_id).filter(User.is_delete == 0).first()
            if not user_query:
                return failure(msg='数据不存在，请刷新后再试！')
            user_name = user_query.user_name

            md5_pwd = gen_encrypt_password(user_name, pwd.strip(' '))
            db.query(User).filter(User.user_id == user_id). \
                filter(User.is_delete == 0). \
                update({User.password: md5_pwd,
                        User.create_time: datetime.now(),
                        User.expire: expire,
                        User.token: gen_token(User.user_name, expire)
                        }, synchronize_session='fetch')

            sb = ServerBase(usr)
            sb.log(object_ids=[user_id], model=User.__tablename__, operation='修改用户密码', db=db)

        return ok()

    @trycatch(failure(msg='请求失败'))
    def user_edit_admin(self, user_id, is_admin, usr):
        expire = gen_expire_in()
        with DbSession.create(EnumDb.main) as db:
            if usr['user_id'] != user_id:
                query = db.query(User).filter(User.user_id == usr['user_id']).filter(User.is_delete == 0).first()
                query_is_admin = query.is_admin
                if not query_is_admin:
                    return failure(msg='您不是管理员不能修改管理员选项！')

            user_query = db.query(User).filter(User.user_id == user_id).first()
            if not user_query:
                return failure(msg='数据不存在，请刷新后重试')

            if user_query.is_super_admin:
                return failure(msg='超级管理员不允许修改管理员权限')

            db.query(User).filter(User.user_id == user_id). \
                filter(User.is_delete == 0). \
                filter(User.is_super_admin == 0). \
                update({User.is_admin: is_admin,
                        User.create_time: datetime.now(),
                        User.expire: expire,
                        User.token: gen_token(User.user_name, expire)
                        }, synchronize_session='fetch')

            sb = ServerBase(usr)
            sb.log(object_ids=[user_id], model=User.__tablename__, operation='设置用户管理员权限', db=db)

        return ok()

    @trycatch(failure(msg='操作失败'))
    def user_edit_mobile(self, user_id, mobile, usr):
        with DbSession.create(EnumDb.main) as db:
            db.query(User).filter(User.user_id == user_id). \
                filter(User.is_delete == 0). \
                update({User.mobile: mobile}, synchronize_session='fetch')

            sb = ServerBase(usr)
            sb.log(object_ids=[user_id], model=User.__tablename__, operation='修改用户电话', db=db)

        return ok()

    @trycatch(failure(msg='操作失败'))
    def user_edit_crm_user(self, user_id, crm_user, usr):
        with DbSession.create(EnumDb.main) as db:
            if usr['user_id'] != user_id:
                query = db.query(User).filter(User.user_id == usr['user_id']).filter(User.is_delete == 0).first()
                query_is_admin = query.is_admin
                if not query_is_admin:
                    return failure(msg='您不是管理员不能修改其他人的信息！')

            db.query(User).filter(User.user_id == user_id). \
                filter(User.is_delete == 0). \
                update({User.crm_user: crm_user}, synchronize_session='fetch')

            sb = ServerBase(usr)
            sb.log(object_ids=[user_id], model=User.__tablename__, operation='修改CRM绑定账号', db=db)

        return ok()

    @trycatch(failure(msg='操作失败'))
    def user_edit_chandao_user(self, user_id, chandao_user, usr):
        with DbSession.create(EnumDb.main) as db:
            if usr['user_id'] != user_id:
                query = db.query(User).filter(User.user_id == usr['user_id']).filter(User.is_delete == 0).first()
                query_is_admin = query.is_admin
                if not query_is_admin:
                    return failure(msg='您不是管理员不能修改其他人的信息！')

            db.query(User).filter(User.user_id == user_id). \
                filter(User.is_delete == 0). \
                update({User.chandao_user: chandao_user}, synchronize_session='fetch')

            sb = ServerBase(usr)
            sb.log(object_ids=[user_id], model=User.__tablename__, operation='修改CRM绑定账号', db=db)

        return ok()

    @trycatch(failure(msg='请求失败'))
    def user_delete(self, user_id, usr):
        with DbSession.create(EnumDb.main) as db:
            user_query = db.query(User).filter(User.user_id == user_id).filter(User.is_delete == 0).first()
            if not user_query:
                return failure(msg='数据不存在，请刷新后再试！')

            is_admin = user_query.is_admin
            is_super_admin = user_query.is_super_admin
            if is_super_admin or is_admin:
                return failure(msg='系统管理员不能删除！')

            db.query(User).filter(User.user_id == user_id).filter(User.is_delete == 0).update({User.is_delete: 1}, synchronize_session='fetch')

            sb = ServerBase(usr)
            sb.log(object_ids=[user_id], model=User.__tablename__, operation='删除用户', db=db)

        return ok()

    # @trycatch(failure(msg='请求失败'))
    def user_get_list(self, select, page, size):
        user_ids = []
        list = []
        if select:
            condition_select = User.user_name.like('%' + to_string(select).replace(' ', '') + '%')
        else:
            condition_select = True

        try:
            with DbSession.create(EnumDb.main) as db:
                user_count = db.query(func.count(User.user_id)). \
                    filter(User.is_delete == 0). \
                    filter(condition_select). \
                    first()[0]

                query = db.query(User). \
                    filter(User.is_delete == 0). \
                    filter(condition_select). \
                    order_by(User.user_id). \
                    offset(page * size).limit(size)
                for v in query:
                    data = {'user_id': v.user_id,
                            'user_name': v.user_name,
                            'is_admin': v.is_admin,
                            'create_time': to_string(v.create_time),
                            'gender': v.gender,
                            'mobile': v.mobile,
                            'crm_user': v.crm_user,
                            'logo': v.logo,
                            'chandao_user': v.chandao_user
                            }
                    user_ids.append(v.user_id)
                    list.append(data)

                # 查询绑定的3微信账号昵称
                wechat_dict = {}
                wechat_query = db.query(WechatUser.user_id, WechatUser.nike_name).filter(WechatUser.user_id.in_(user_ids))
                for v in wechat_query:
                    wechat_dict[to_int(v[0])] = v[1]

            for v in list:
                v['wechat_nike_name'] = wechat_dict.get(v['user_id'], '')

            return ok(data={'count': user_count, 'list': list})
        except:
            log(traceback.format_exc())

    @trycatch(failure(msg='操作失败'))
    def user_get_list_group(self):
        result_dict = {}
        with DbSession.create(EnumDb.main) as db:
            query = db.query(User).filter(User.is_delete == 0)
            for v in query:
                is_admin = v.is_admin
                is_admin_name = '管理员' if is_admin else '普通用户'
                data = {'user_id': v.user_id, 'user_name': v.user_name, 'gender': v.gender, 'logo': v.logo}

                if is_admin_name not in result_dict:
                    result_dict[is_admin_name] = [data]
                else:
                    result_dict[is_admin_name].append(data)

        return ok(data=result_dict)

    @trycatch(failure(msg='请求失败'))
    def user_sys_operation_log_list(self, user_id, start_time, end_time, size, page):
        if user_id:
            cons_user_id = SysOperationLog.operation_user_id == user_id
        else:
            cons_user_id = True

        if start_time and end_time:
            cons_time = and_(
                SysOperationLog.operation_time >= to_string(start_time)[:10] + ' 00:00:00',
                SysOperationLog.operation_time <= to_string(end_time)[:10] + ' 23:59:59'
            )
        else:
            cons_time = True

        list = []
        with DbSession.create(EnumDb.main) as db:
            count = db.query(func.count(SysOperationLog.id)). \
                filter(cons_user_id). \
                filter(cons_time). \
                order_by(desc(SysOperationLog.operation_time)). \
                first()[0]

            if not count:
                return ok(data={'list': list, 'count': 0})

            query = db.query(SysOperationLog). \
                filter(cons_user_id). \
                filter(cons_time). \
                order_by(desc(SysOperationLog.operation_time)). \
                offset(page * size).limit(size)

            user_ids = []
            for v in query:
                user_ids.append(v.operation_user_id)
                data = {
                    'time': to_string(v.operation_time),
                    'user_id': v.operation_user_id,
                    'operation': v.operation,
                    'id': v.id
                }
                list.append(data)

            user_dict = {}
            user_query = db.query(User.user_id, User.user_name).filter(User.user_id.in_(user_ids))
            for v in user_query:
                user_dict[v[0]] = v[1]

        for v in list:
            v['user_name'] = user_dict.get(v['user_id'])

        return ok(data={'count': count, 'list': list})


if __name__ == '__main__':
    # from tornado.ioloop import IOLoop
    #
    #
    # @coroutine
    # def run():
    #     um = UserManage(phone='19786541111')
    #     res = yield um.user_creat(pwd='123456', name='赵六', remark='', usr={'user_id': 1}, is_admin=1)
    #     print(res)
    #
    #
    # IOLoop.current().run_sync(run)

    um = UserManage(name='孙丽萍')
    res = um.user_creat(remark='', usr={'user_id': 1}, mobile=15020040762, crm_user='', is_admin=1, logo='"/static/img/slices/braided_girl.png"')

    # um = UserManage()
    # res = um.user_edit_pwd(user_id=61, pwd='Luo12345', usr={'user_id': 1})

    print(res)
