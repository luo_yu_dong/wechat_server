#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: wechat_wiki_manage.py
@time: 2020/10/20 7:50
@desc:
'''
import datetime
import traceback
import uuid
from urllib.parse import quote

import requests
from wechatpy import WeChatOAuth, WeChatClient
from wechatpy.utils import check_signature

from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.log_controller import log
from framework.msg import failure, ok
from framework.utilities import to_string, to_int_date, current_timestamp
from models.chandao.chandao_project_share_binding import ChandaoProjectShareBinding
from models.exception_log.send_msg_log import SendMsgLog
from models.file.qrcode_log import QrcodeLog
from models.users.wechat_user import WechatUser
from services.wechat_program_base import WechatProgramBase
from setting import DOMAIN_CURRENT, WECHAT_PRO_APPID, WECHAT_WIKI_APPID, WECHAT_WIKI_SECRET, WECHAT_NEW_FORM_TO_WORK_TEMPLATE_ID, \
    WECHAT_SCORE_TO_WORK_TEMPLATE_ID, WECHAT_COMPLETE_FORM_TO_CUSTOMER_TEMPLATE_ID, WECHAT_ASSIGNING_JOB_TEMPLATE_ID, SERVER_TYPE


class WeChatWikiManage(WechatProgramBase):

    def __init__(self):
        super().__init__()

    def verify(self, data):
        try:
            echostr = to_string(data.get('echostr')[0])
            signature = to_string(data.get('signature')[0])
            nonce = to_string(data.get('nonce')[0])
            timestamp = to_string(data.get('timestamp')[0])

            check_signature('749drgwi', signature, timestamp, nonce)

            return echostr

        except Exception as e:
            log(traceback.format_exc(), 'WeChatWikiManage.verify(data=%s)' % (str(data)))
            return failure(msg='微信请求验证失败!')

    def user_info(self, access_token, FromUserName):
        req_result = requests.get('https://api.weixin.qq.com/cgi-bin/user/info?access_token=%s&openid=%s' %
                                  (to_string(access_token), to_string(FromUserName)))
        res = req_result.json()
        if 'errcode' in res:
            return None

        return res

    # 生成小程序回调
    def get_redirect_uri(self, appid, type):
        url = '%s/api/wechat/authorization' % (DOMAIN_CURRENT)
        encode_url = quote(url, 'utf-8')
        redirect_uri = '%s&response_type=code&scope=snsapi_userinfo&type=%s' % (encode_url, to_string(type))

        url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s#wechat_redirect' \
              % (to_string(appid), redirect_uri)

        return url

    # 关注公众号
    def attention_accounts(self, unionid):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(WechatUser).filter(WechatUser.union_id == unionid).first()
            if query:
                query.is_attention_wiki = 1
                query.attention_wiki_time = datetime.datetime.now()

        return ok()

    # 推送关注消息
    def push_attention_msg(self, url, FromUserName, ToUserName):
        CreateTime = to_int_date(datetime.datetime.now())
        reply_content = '感谢关注乾行公众号，我们一起创建数字化企业运营管理系统。PDS敏捷交付创造价值！' \
                        '<a data-miniprogram-appid=%s data-miniprogram-path="/pages/workHours/index" href="">请点击跳转</a>查看项目交付、提报投诉建议' \
                        % (to_string(WECHAT_PRO_APPID))

        textTpl = """<xml> <ToUserName><![CDATA[%s]]></ToUserName><FromUserName><![CDATA[%s]]></FromUserName> <CreateTime>%s</CreateTime> <MsgType><![CDATA[%s]]></MsgType> <Content><![CDATA[%s]]></Content></xml>"""
        out = textTpl % (FromUserName, ToUserName, CreateTime, 'text', reply_content)

        return out

    def un_attention_accounts(self, openid):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(WechatUser).filter(WechatUser.open_id == openid).first()
            if query:
                query.is_attention_wiki = 0
                query.attention_wiki_time = None
        return ok()

    def un_chandao_project_share_binding_get_list(self, unionid):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(WechatUser).filter(WechatUser.unionid == unionid)
            wechat_user_list = [v.id for v in query]

            db.query(ChandaoProjectShareBinding). \
                filter(ChandaoProjectShareBinding.wechat_user_id.in_(wechat_user_list)). \
                update({ChandaoProjectShareBinding.is_delete: 1}, synchronize_session='fetch')

        return ok()

    def authorization(self, code, state):
        '''
        state 参数说明：is_customer_service, type, item_id, user_id, project_id, model
        参数间使用-_-分割
        is_customer_service = 0-客户，1-客服
        type = 1-查看详情，0-进入小程序
        item_id ：当type=1的时候 item_id 参数必填
        user_id:分享人的user_id # 暂时没有
        project_id:项目id
        model: 0-原来的扫码登录的，1-项目
        :param code:
        :param state:
        :return:
        '''
        try:
            # is_customer_service, type, item_id, user_id, project_id, model
            is_customer_service, type, item_id, user_id, project_id, model = state.split('-_-')
            redirect_uri = '/pages/logs/wxml?is_authorization=1&state=%s' % (state)  # 主页

            cals = WeChatOAuth(app_id=WECHAT_WIKI_APPID, secret=WECHAT_WIKI_SECRET, redirect_uri=redirect_uri, scope='snsapi_userinfo')
            msg = cals.fetch_access_token(code)
            if not msg:
                raise Exception('code erro')

            wiki_access_token_time = current_timestamp()
            user_info = cals.get_user_info(openid=msg['openid'], access_token=msg['access_token'], lang='zh_CN')
            user_info['openid'] = msg['openid']

            # 保存用户信息
            res = self.save_user_msg(user_info=user_info, is_customer_service=is_customer_service, type=2)
            wechat_user_id = res.data['id']

            return ok(data={'unionid': msg['unionid'],
                            'redirect_uri': redirect_uri,
                            'access_token': msg['access_token'],
                            'wiki_access_token_time': wiki_access_token_time,
                            'wechat_user_id': wechat_user_id,
                            'session_key': to_string(uuid.uuid1())})
        except:
            log(traceback.format_exc(), 'WechatProviderManage.authorization')
            return failure(msg='操作失败')

    # 新消息发送
    # @coroutine
    def send_new_form_wechat_message(self, wechat_user_ids, item_id, form_no, form_title, form_time):
        try:
            # 避免测试环境测试时不停的发消息
            if SERVER_TYPE == 'beta':
                return ok()

            open_ids = []
            unionid_list = set([])
            with DbSession.create(EnumDb.main) as db:
                # 查询对应的公众号opend_id
                recommend_user = db.query(WechatUser).filter(WechatUser.id.in_(wechat_user_ids))
                for v in recommend_user:
                    unionid_list.add(v.union_id)

                # 通过 unionid 查询type=1 的那个人的OPENID
                wiki_open_query = db.query(WechatUser.id, WechatUser.open_id, WechatUser.is_customer_service, WechatUser.user_id). \
                    filter(WechatUser.type == 2).filter(WechatUser.union_id.in_(unionid_list))
                for v in wiki_open_query:
                    open_ids.append({'wechat_user_id': v[0], 'open_id': v[1], 'judge_is_customer_service': v[2], 'customer_server_user_id': v[3]})

            url = '%s/api/wechat/authorization' % (DOMAIN_CURRENT)
            encode_url = quote(url, 'utf-8')

            # 换取短链接
            # wb = WechatProgramBase()
            # is_ok, sort_url = yield wb.to_short_link(access_token, url)
            # if is_ok:
            #     date_url = sort_url
            # else:
            #     date_url = ''

            page_path = '/pages/my/details?&id=%s' % (to_string(item_id))
            miniprogram = {"appid": WECHAT_PRO_APPID,
                           "pagepath": page_path
                           }

            add_list = []
            for v in open_ids:
                wechat_id = v['wechat_user_id']
                open_id = v['open_id']

                # is_customer_service, type, item_id, user_id, project_id, model
                state = to_string(v['judge_is_customer_service']) + '-_-' + '1' + '-_-' + to_string(item_id) + '-_-' + \
                        to_string(v['customer_server_user_id']) + '-_-0-_-0'

                redirect_uri = '%s&response_type=code&scope=snsapi_userinfo&state=%s' % (encode_url, to_string(state))
                url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s#wechat_redirect' % \
                      (to_string(WECHAT_PRO_APPID), redirect_uri)

                data = {
                    "touser": open_id,
                    "template_id": WECHAT_NEW_FORM_TO_WORK_TEMPLATE_ID,  # 模板id
                    "url": url,  # 需要跳转的链接 ，因为跳转了小程序，这个url被认为是备用的跳转链接
                    "miniprogram": miniprogram,  # 需要跳转的小程序
                    "data": {
                        "first": {
                            "value": '您有一个新的服务单待受理，请及时处理。',
                            "color": "#173177"  # 模板内容字体颜色，不填默认为黑色
                        },
                        "keyword1": {
                            "value": '售后服务单',
                            "color": "#173177"
                        },
                        "keyword2": {
                            "value": form_no,
                            "color": "#173177"
                        },
                        "keyword3": {
                            "value": form_title,
                            "color": "#173177"
                        },
                        "keyword4": {
                            "value": to_string(form_time)[:19],
                            "color": "#173177"
                        },
                        "remark": {
                            "value": '',
                            "color": "#173177"
                        }
                    }
                }

                # 保存数据到 wechat_template_msg_logs
                wtml = SendMsgLog()
                wtml.json = to_string(data)
                wtml.open_id = open_id
                wtml.wechat_user_id = wechat_id
                wtml.item_id = item_id
                wtml.creat_time = current_timestamp()
                wtml.send_ok = 0
                add_list.append(wtml)

            with DbSession.create(EnumDb.main) as db:
                db.bulk_save_objects(add_list)

            return ok()
        except:
            log(traceback.format_exc(), 'WechatProviderManage.send_new_form_wechat_message')
            return failure(msg='操作失败')

    # 评分结果反馈
    # @coroutine
    def send_score_form_wechat_message(self, wechat_user_ids, item_id, form_no, form_score, form_desc):
        try:
            # 避免测试环境测试时不停的发消息
            if SERVER_TYPE == 'beta':
                return ok()

            open_ids = []
            unionid_list = set([])
            with DbSession.create(EnumDb.main) as db:
                # 查询对应的公众号opend_id
                recommend_user = db.query(WechatUser).filter(WechatUser.id.in_(wechat_user_ids))
                for v in recommend_user:
                    unionid_list.add(v.union_id)

                # 通过 unionid 查询type=1 的那个人的openid
                wiki_open_query = db.query(WechatUser.id, WechatUser.open_id, WechatUser.is_customer_service, WechatUser.user_id). \
                    filter(WechatUser.type == 2).filter(WechatUser.union_id.in_(unionid_list))
                for v in wiki_open_query:
                    open_ids.append({'wechat_user_id': v[0], 'open_id': v[1], 'judge_is_customer_service': v[2], 'customer_server_user_id': v[3]})

            if not open_ids:
                return ok()

            url = '%s/api/wechat/authorization' % (DOMAIN_CURRENT)
            encode_url = quote(url, 'utf-8')

            page_path = '/pages/my/details?&id=%s' % (to_string(item_id))
            miniprogram = {"appid": WECHAT_PRO_APPID,
                           "pagepath": page_path
                           }

            add_list = []
            for v in open_ids:
                wechat_id = v['wechat_user_id']
                open_id = v['open_id']

                # is_customer_service, type, item_id, user_id, project_id, model
                state = to_string(v['judge_is_customer_service']) + '-_-' + '1' + '-_-' + to_string(item_id) + '-_-' + \
                        to_string(v['customer_server_user_id']) + '-_-0-_-0'

                redirect_uri = '%s&response_type=code&scope=snsapi_userinfo&state=%s' % (encode_url, to_string(state))
                url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s#wechat_redirect' % \
                      (to_string(WECHAT_PRO_APPID), redirect_uri)

                data = {
                    "touser": open_id,
                    "template_id": WECHAT_SCORE_TO_WORK_TEMPLATE_ID,  # 模板id
                    "url": url,  # 需要跳转的链接 ，因为跳转了小程序，这个url被认为是备用的跳转链接
                    "miniprogram": miniprogram,  # 需要跳转的小程序
                    "data": {
                        "first": {
                            "value": '服务单号为【' + form_no + '】的评分结果通知。',
                            "color": "#173177"  # 模板内容字体颜色，不填默认为黑色
                        },
                        "CreateDate": {
                            "value": to_string(datetime.datetime.now())[:19],
                            "color": "#173177"
                        },
                        "ProcessingResults": {
                            "value": '得分：' + to_string(form_score),
                            "color": "#173177"
                        },
                        "remark": {
                            "value": '评价：' + form_desc,
                            "color": "#173177"
                        }
                    }
                }

                # 保存数据到 wechat_template_msg_logs
                wtml = SendMsgLog()
                wtml.json = to_string(data)
                wtml.open_id = open_id
                wtml.wechat_user_id = wechat_id
                wtml.item_id = item_id
                wtml.creat_time = current_timestamp()
                wtml.send_ok = 0
                add_list.append(wtml)

            with DbSession.create(EnumDb.main) as db:
                db.bulk_save_objects(add_list)

            return ok()
        except:
            log(traceback.format_exc(), 'WechatProviderManage.send_score_form_wechat_message')
            return failure(msg='操作失败')

    # @coroutine
    def send_complete_form_to_customer_wechat_message(self, wechat_user_ids, item_id, form_no, reply_result):
        try:
            # 避免测试环境测试时不停的发消息
            if SERVER_TYPE == 'beta':
                return ok()

            open_ids = []
            unionid_list = set([])
            with DbSession.create(EnumDb.main) as db:
                # 查询对应的公众号opend_id
                recommend_user = db.query(WechatUser).filter(WechatUser.id.in_(wechat_user_ids))
                for v in recommend_user:
                    if v.union_id:
                        unionid_list.add(v.union_id)

                # 通过 unionid 查询type=1 的那个人的openid
                wiki_open_query = db.query(WechatUser.id, WechatUser.open_id, WechatUser.is_customer_service, WechatUser.user_id). \
                    filter(WechatUser.type == 2).filter(WechatUser.union_id.in_(unionid_list))
                for v in wiki_open_query:
                    open_ids.append({'wechat_user_id': v[0], 'open_id': v[1], 'judge_is_customer_service': v[2], 'customer_server_user_id': v[3]})

            if not open_ids:
                return ok()

            url = '%s/api/wechat/authorization' % (DOMAIN_CURRENT)
            encode_url = quote(url, 'utf-8')

            page_path = '/pages/my/details?&id=%s' % (to_string(item_id))
            miniprogram = {"appid": WECHAT_PRO_APPID,
                           "pagepath": page_path
                           }

            add_list = []
            for v in open_ids:
                wechat_id = v['wechat_user_id']
                open_id = v['open_id']

                # is_customer_service, type, item_id, user_id, project_id, model
                state = to_string(v['judge_is_customer_service']) + '-_-' + '1' + '-_-' + to_string(item_id) + '-_-' + \
                        to_string(v['customer_server_user_id']) + '-_-0-_-0'

                redirect_uri = '%s&response_type=code&scope=snsapi_userinfo&state=%s' % (encode_url, to_string(state))
                url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s#wechat_redirect' % \
                      (to_string(WECHAT_PRO_APPID), redirect_uri)
                data = {
                    "touser": open_id,
                    "template_id": WECHAT_COMPLETE_FORM_TO_CUSTOMER_TEMPLATE_ID,  # 模板id
                    "url": url,  # 需要跳转的链接 ，因为跳转了小程序，这个url被认为是备用的跳转链接
                    "miniprogram": miniprogram,  # 需要跳转的小程序
                    "data": {
                        "first": {
                            "value": '尊敬的客户您好，您提交的服务单' + form_no + '已受理完成，请及时查阅。',
                            "color": "#173177"  # 模板内容字体颜色，不填默认为黑色
                        },
                        "keyword1": {
                            "value": to_string(datetime.datetime.now())[:19],
                            "color": "#173177"
                        },
                        "keyword2": {
                            "value": reply_result,
                            "color": "#173177"
                        },
                        "remark": {
                            "value": '',
                            "color": "#173177"
                        }
                    }
                }

                # 保存数据到 wechat_template_msg_logs
                wtml = SendMsgLog()
                wtml.json = to_string(data)
                wtml.open_id = open_id
                wtml.wechat_user_id = wechat_id
                wtml.item_id = item_id
                wtml.creat_time = current_timestamp()
                wtml.send_ok = 0
                add_list.append(wtml)

            with DbSession.create(EnumDb.main) as db:
                db.bulk_save_objects(add_list)

            return ok()
        except:
            log(traceback.format_exc(), 'WechatProviderManage.send_complete_form_to_customer_wechat_message')
            return failure(msg='操作失败')

    def send_assigning_job_wechat_message(self, wechat_user_id, item_list, assigning_user_name, user_id):
        '''
        分配信息时给负责人发送模板消息
        :param wechat_user_id:
        :param item_list: [{'item_id'：xx, 'form_no':xx, 'form_title':xxx}]
        :return:
        '''
        try:
            # 避免测试环境测试时不停的发消息
            if SERVER_TYPE == 'beta':
                return ok()

            with DbSession.create(EnumDb.main) as db:
                # 查询对应的公众号opend_id
                wechat_user = db.query(WechatUser).filter(WechatUser.id == wechat_user_id).first()
                if not wechat_user:
                    return ok()

                # 通过 unionid 查询type=1 的那个人的openid
                wiki_open_query = db.query(WechatUser.open_id). \
                    filter(WechatUser.type == 2).filter(WechatUser.union_id == wechat_user.union_id). \
                    first()
                if not wiki_open_query:
                    return ok()

                open_id = wiki_open_query[0]

            if not open_id:
                return ok()

            url = '%s/api/wechat/authorization' % (DOMAIN_CURRENT)
            encode_url = quote(url, 'utf-8')

            add_list = []
            for v in item_list:
                item_id = v['item_id']
                form_no = v['form_no']
                form_title = v['form_title']

                page_path = '/pages/my/details?&id=%s' % (to_string(item_id))
                miniprogram = {"appid": WECHAT_PRO_APPID,
                               "pagepath": page_path
                               }

                # is_customer_service, type, item_id, user_id, project_id, model
                state = '1-_-1-_-' + to_string(item_id) + '-_-' + to_string(user_id) + '-_-0-_-0'

                redirect_uri = '%s&response_type=code&scope=snsapi_userinfo&state=%s' % (encode_url, to_string(state))
                url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s#wechat_redirect' % \
                      (to_string(WECHAT_PRO_APPID), redirect_uri)
                data = {
                    "touser": open_id,
                    "template_id": WECHAT_ASSIGNING_JOB_TEMPLATE_ID,  # 模板id
                    "url": url,  # 需要跳转的链接 ，因为跳转了小程序，这个url被认为是备用的跳转链接
                    "miniprogram": miniprogram,  # 需要跳转的小程序
                    "data": {
                        "first": {
                            "value": assigning_user_name + '为您分配了新的服务单，请及时处理！',
                            "color": "#173177"  # 模板内容字体颜色，不填默认为黑色
                        },
                        "keyword1": {
                            "value": '【' + form_no + '】' + form_title,
                            "color": "#173177"
                        },
                        "keyword2": {
                            "value": to_string(datetime.datetime.now())[:19],
                            "color": "#173177"
                        },
                        "remark": {
                            "value": '',
                            "color": "#173177"
                        }
                    }
                }

                # 保存数据到 wechat_template_msg_logs
                wtml = SendMsgLog()
                wtml.json = to_string(data)
                wtml.open_id = open_id
                wtml.wechat_user_id = wechat_user_id
                wtml.item_id = item_id
                wtml.creat_time = current_timestamp()
                wtml.send_ok = 0
                add_list.append(wtml)

            with DbSession.create(EnumDb.main) as db:
                db.bulk_save_objects(add_list)

            return ok()
        except:
            log(traceback.format_exc(), 'WechatProviderManage.send_complete_form_to_customer_wechat_message')
            return failure(msg='操作失败')

    def get_access_token(self, appid, secret):
        req_result = requests.get('https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s' %
                                  (to_string(appid), to_string(secret)))

        access_token = req_result.json()['access_token']

        return access_token

    def wechat_make_WXACode(self, project_id):
        # is_customer_service, type, item_id, user_id, project_id, model
        now = current_timestamp()  # 当前时间戳
        scene_str = '0-_-1-_-0-_-0-_-' + to_string(project_id) + '-_-1'  # 生成公众号客服专用二维码

        with DbSession.create(EnumDb.main) as db:
            wq_ticket = db.query(QrcodeLog).filter(QrcodeLog.project_id == project_id).first()
            if wq_ticket:
                # if to_int(wq_ticket.creat_timestamp) + 2592000 > now:
                return ok(data={'url': to_string(wq_ticket.qrcode)})

        client = WeChatClient(WECHAT_WIKI_APPID, WECHAT_WIKI_SECRET)
        res = client.qrcode.create({
            # 'expire_seconds': 2592000,  # 有效时间（秒）
            'action_name': 'QR_LIMIT_STR_SCENE',
            'action_info': {  # 二维码详细信息
                'scene': {'scene_str': scene_str},  # QR_STR_SCENE时是字符串
            }
        })

        qrcdoe = client.qrcode.get_url(res['ticket'])

        # 查询这个二维码是否是生成过的
        with DbSession.create(EnumDb.main) as db:
            wq_ticket = db.query(QrcodeLog).filter(QrcodeLog.project_id == project_id).first()
            if not wq_ticket:
                ql = QrcodeLog()
                ql.ticket = res['ticket']
                ql.qrcode = to_string(qrcdoe)
                ql.is_customer_service = 0
                ql.creat_timestamp = now
                ql.project_id = project_id
                db.add(ql)

            else:
                wq_ticket.ticket = res['ticket']
                wq_ticket.qrcode = to_string(qrcdoe)
                wq_ticket.creat_timestamp = now

        return ok(data={'url': to_string(qrcdoe)})

    def save_project_dinding(self, unionid, project_id):
        with DbSession.create(EnumDb.main) as db:
            wechat_user = db.query(WechatUser.id).filter(WechatUser.union_id == unionid).first()
            if not wechat_user:
                return ok()
            wechat_user_id = wechat_user[0]

            query = db.query(ChandaoProjectShareBinding). \
                filter(ChandaoProjectShareBinding.project_id == project_id). \
                filter(ChandaoProjectShareBinding.wechat_user_id == wechat_user_id). \
                filter(ChandaoProjectShareBinding.is_delete == 0). \
                first()
            if not query:
                cpsb = ChandaoProjectShareBinding()
                cpsb.project_id = project_id
                cpsb.wechat_user_id = wechat_user_id
                cpsb.share_user_id = 0
                cpsb.is_delete = 0
                db.add(cpsb)
            else:
                query.project_id = project_id

        return ok()


if __name__ == '__main__':
    wcwm = WeChatWikiManage()
    # res = wcwm.send_new_form_wechat_message(wechat_user_ids=[7],
    #                                         item_id=16,
    #                                         form_no='SHFWD20201209003',
    #                                         form_title='1',
    #                                         form_time='2020-12-09 15:17:04')  # 发消息给客服
    res = wcwm.wechat_make_WXACode(558)

    print(res)
