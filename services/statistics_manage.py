#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: statistics_manage.py
@time: 2020/11/9 8:13
@desc:
'''
import datetime
import math
import traceback

from sqlalchemy import func

from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.log_controller import log
from framework.msg import ok, failure
from framework.utilities import to_string, to_int, to_decimal, to_int_date, format_dict, to_datetime
from models.chandao.chandao_project import ChandaoProject
from models.chandao.chandao_project_payback import ChandaoProjectPayback
from models.chandao.chandao_project_risk import ChandaoProjectRisk
from models.chandao.chandao_project_share_binding import ChandaoProjectShareBinding
from models.chandao.chandao_project_summary_phases import ChandaoProjectSummaryPhases
from models.chandao.chandao_project_summary_phases_detail import ChandaoProjectSummaryPhasesDetail
from models.chandao.chandao_project_team_user import ChandaoProjectTeamUser
from models.chandao.chandao_project_week_plan import ChandaoProjectWeekPlan
from models.chandao.chandao_project_week_plan_detail import ChandaoProjectWeekPlanDetail
from models.customer_form.customer_submmint_form import CustomerSubmmintForm
from models.task_working.project_task_working_daily_detail import ProjectTaskWorkingDailyDetail
from models.users.user import User
from models.users.wechat_user import WechatUser
from services.server_base import ServerBase


class StatisticsManage(ServerBase):
    def __init__(self, user):
        super().__init__(user=user)
        self.user_id = user['user_id']
        self.user = user

    def statistics_home(self):
        now = datetime.datetime.now()
        yesterday = now - datetime.timedelta(days=1)

        # 7天书卷
        day_7_time = now - datetime.timedelta(days=6)

        try:
            with DbSession.create(EnumDb.main) as db:
                # user_query
                user_dict = {}
                user_log = {}
                user_ids = []
                user_query = db.query(User.user_id, User.user_name, User.logo, User.is_delete)
                for v in user_query:
                    user_dict[v[0]] = v[1]
                    user_log[v[0]] = v[2]
                    if v[3] == 0:
                        user_ids.append(v[0])

                # 7天的服务单折现
                day_7_dict = {}
                day_7_query = db.query(CustomerSubmmintForm.creat_int_date, func.count(CustomerSubmmintForm.id)). \
                    filter(CustomerSubmmintForm.creat_int_date <= to_int_date(now)). \
                    filter(CustomerSubmmintForm.creat_int_date >= to_int_date(day_7_time)). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    group_by(CustomerSubmmintForm.creat_int_date). \
                    all()
                for v in day_7_query:
                    day_7_dict[to_int(v[0])] = v[1]

                day_7_repl_dict = {}
                day_7_repl_query = db.query(CustomerSubmmintForm.int_date, func.count(CustomerSubmmintForm.id)). \
                    filter(CustomerSubmmintForm.int_date <= to_int_date(now)). \
                    filter(CustomerSubmmintForm.int_date >= to_int_date(day_7_time)). \
                    filter(CustomerSubmmintForm.is_reply == 1). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    group_by(CustomerSubmmintForm.int_date). \
                    all()
                for v in day_7_repl_query:
                    day_7_repl_dict[to_int(v[0])] = v[1]

                # 服务单总数
                all_count = db.query(func.count(CustomerSubmmintForm.id)).filter(CustomerSubmmintForm.is_delete == 0).first()[0]

                # 未处理总数
                all_unreply_count = db.query(func.count(CustomerSubmmintForm.id)). \
                    filter(CustomerSubmmintForm.is_reply == 0). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    first()[0]

                # 已处理
                all_reply_count = db.query(func.count(CustomerSubmmintForm.id)). \
                    filter(CustomerSubmmintForm.is_reply == 1). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    first()[0]

                # 今日全部服务单数量
                day_count = db.query(func.count(CustomerSubmmintForm.id)). \
                    filter(CustomerSubmmintForm.creat_year == now.year). \
                    filter(CustomerSubmmintForm.creat_month == now.month). \
                    filter(CustomerSubmmintForm.creat_day == now.day). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    first()[0]

                # 今日未处理服务单数量
                day_un_count = db.query(func.count(CustomerSubmmintForm.id)). \
                    filter(CustomerSubmmintForm.creat_year == now.year). \
                    filter(CustomerSubmmintForm.creat_month == now.month). \
                    filter(CustomerSubmmintForm.creat_day == now.day). \
                    filter(CustomerSubmmintForm.is_reply == 0). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    first()[0]

                # 今日已处理服务单数量
                day_reply_count = db.query(func.count(CustomerSubmmintForm.id)). \
                    filter(CustomerSubmmintForm.int_date == to_int_date(now)). \
                    filter(CustomerSubmmintForm.is_reply == 1). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    first()[0]

                # 昨日服务单数量
                yesday_count = db.query(func.count(CustomerSubmmintForm.id)). \
                    filter(CustomerSubmmintForm.creat_year == yesterday.year). \
                    filter(CustomerSubmmintForm.creat_month == yesterday.month). \
                    filter(CustomerSubmmintForm.creat_day == yesterday.day). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    first()[0]

                # 昨日已处理服务单数量
                yesday_reply_count = db.query(func.count(CustomerSubmmintForm.id)). \
                    filter(CustomerSubmmintForm.int_date == to_int_date(yesterday)). \
                    filter(CustomerSubmmintForm.is_reply == 1). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    first()[0]

                # 整体平均回复时长
                all_reply_second = db.query(func.sum(CustomerSubmmintForm.reply_time_second)). \
                    filter(CustomerSubmmintForm.is_reply == 1). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    first()[0]

                # 平均回复时长 （分钟）
                if all_reply_count == 0 or all_reply_second == 0:
                    agv_reply_second = 0
                else:
                    agv_reply_second = math.ceil((all_reply_second / all_reply_count) / 60)

                # 全体已评分的数量
                all_reply_score_count = db.query(func.count(CustomerSubmmintForm.id)). \
                    filter(CustomerSubmmintForm.is_reply == 1). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    filter(CustomerSubmmintForm.score != None). \
                    filter(CustomerSubmmintForm.reply_user_id.in_(user_ids)). \
                    first()[0]

                all_reply_score_sum = db.query(func.sum(CustomerSubmmintForm.score)). \
                    filter(CustomerSubmmintForm.is_reply == 1). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    filter(CustomerSubmmintForm.score != None). \
                    filter(CustomerSubmmintForm.reply_user_id.in_(user_ids)). \
                    first()[0]

                if all_reply_score_count == 0 or all_reply_score_sum == 0:
                    agv_reply_score = 0
                else:
                    agv_reply_score = all_reply_score_sum / all_reply_score_count

                # 每个人已处理的服务单数量
                user_reply_count_list = []
                user_reply_count_dict = {}
                user_reply_count = db.query(CustomerSubmmintForm.reply_user_id, func.count(CustomerSubmmintForm.id)). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    filter(CustomerSubmmintForm.is_reply == 1). \
                    filter(CustomerSubmmintForm.reply_user_id.in_(user_ids)). \
                    group_by(CustomerSubmmintForm.reply_user_id). \
                    all()
                for v in user_reply_count:
                    user_reply_count_dict[to_int(v[0])] = v[1]
                    user_reply_count_list.append({
                        'user_id': v[0],
                        'count': v[1],
                        'user_name': user_dict.get(to_int(v[0])),
                        'user_logo': user_log.get(to_int(v[0]))
                    })

                # 每个人已处理的服务单时长
                user_reply_second_dict = {}
                user_reply_second = db.query(CustomerSubmmintForm.reply_user_id, func.sum(CustomerSubmmintForm.reply_time_second)). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    filter(CustomerSubmmintForm.is_reply == 1). \
                    filter(CustomerSubmmintForm.reply_user_id.in_(user_ids)). \
                    group_by(CustomerSubmmintForm.reply_user_id). \
                    all()
                for v in user_reply_second:
                    user_reply_second_dict[to_int(v[0])] = v[1]

                # 每个人未处理的服务单数量
                user_unreply_count_dict = {}
                user_unreply_count = db.query(CustomerSubmmintForm.reply_user_id, func.count(CustomerSubmmintForm.id)). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    filter(CustomerSubmmintForm.is_reply == 0). \
                    filter(CustomerSubmmintForm.reply_user_id.in_(user_ids)). \
                    group_by(CustomerSubmmintForm.reply_user_id). \
                    all()
                for v in user_unreply_count:
                    user_unreply_count_dict[to_int(v[0])] = v[1]

                # 计算评分用的数量
                user_reply_score_count_dict = {}
                user_reply_score_count = db.query(CustomerSubmmintForm.reply_user_id, func.count(CustomerSubmmintForm.id)). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    filter(CustomerSubmmintForm.is_reply == 1). \
                    filter(CustomerSubmmintForm.score != None). \
                    filter(CustomerSubmmintForm.reply_user_id.in_(user_ids)). \
                    group_by(CustomerSubmmintForm.reply_user_id). \
                    all()
                for v in user_reply_score_count:
                    user_reply_score_count_dict[to_int(v[0])] = v[1]

                # 个人评分
                user_reply_score_dict = {}
                user_reply_score = db.query(CustomerSubmmintForm.reply_user_id, func.sum(CustomerSubmmintForm.score / 2)). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    filter(CustomerSubmmintForm.is_reply == 1). \
                    filter(CustomerSubmmintForm.score != None). \
                    filter(CustomerSubmmintForm.reply_user_id.in_(user_ids)). \
                    group_by(CustomerSubmmintForm.reply_user_id). \
                    all()
                for v in user_reply_score:
                    user_reply_score_dict[to_int(v[0])] = v[1]

            user_unreply_count_list = []
            user_reply_second_list = []  # 个人时长统计
            user_reply_second_0_list = []
            user_reply_score_list = []  # 个人评分统计
            for user_id, user_name in user_dict.items():
                user_reply_second = user_reply_second_dict.get(user_id, 0)
                user_reply_count = user_reply_count_dict.get(user_id, 0)

                if user_reply_second == 0 or user_reply_count == 0:
                    user_agv_reply_second = 0
                else:
                    user_agv_reply_second = math.ceil((user_reply_second / user_reply_count))

                if user_agv_reply_second == 0:
                    user_reply_second_0_list.append({
                        'user_id': user_id,
                        'user_name': user_name,
                        'second': '0',
                        'user_logo': user_log.get(to_int(user_id))
                    })
                else:
                    user_reply_second_list.append({
                        'user_id': user_id,
                        'user_name': user_name,
                        'second': to_decimal(obj=user_agv_reply_second, precision='0.0'),
                        'user_logo': user_log.get(to_int(user_id))
                    })

                user_reply_score_count = user_reply_score_count_dict.get(user_id, 0)
                user_reply_score_sum = user_reply_score_dict.get(user_id, 0)

                if user_reply_score_sum == 0 or user_reply_score_count == 0:
                    second = 0
                else:
                    second = math.ceil(user_reply_score_sum / user_reply_score_count)

                if user_id in user_ids:
                    if second:
                        user_reply_score_list.append({
                            'user_id': user_id,
                            'user_name': user_name,
                            'second': second,
                            'user_logo': user_log.get(to_int(user_id))
                        })

                    user_unreply_count_list.append({
                        'user_id': user_id,
                        'user_name': user_name,
                        'count': user_unreply_count_dict.get(user_id, 0),
                        'user_logo': user_log.get(to_int(user_id))
                    })

            day_7_list = []
            new_int_start_data = day_7_time
            while new_int_start_data <= now:
                day_7_list.append({
                    'show_time': to_string(new_int_start_data)[:10],
                    'int_time': to_int_date(new_int_start_data),
                    'all_count': day_7_dict.get(to_int_date(new_int_start_data), 0),
                    'reply_count': day_7_repl_dict.get(to_int_date(new_int_start_data), 0),
                })

                new_int_start_data += datetime.timedelta(days=1)

            if user_reply_score_list:
                user_reply_score_list.sort(key=lambda a: (a['second'], a['user_id']), reverse=True)

            for v in user_reply_score_list:
                v['second'] = to_string(v['second'])

            if user_reply_count_list:
                user_reply_count_list.sort(key=lambda a: (a['count'], a['user_id']), reverse=True)

            if user_reply_second_list:
                user_reply_second_list.sort(key=lambda a: (a['second'], a['user_id']), reverse=False)

            if user_reply_second_0_list:
                user_reply_second_0_list.sort(key=lambda a: a['user_id'], reverse=True)

            for v in user_reply_second_list:
                v['second'] = to_string(v['second'])

            result = {
                'all_count': all_count,  # 服务单总数量
                'all_unreply_count': all_unreply_count,  # 未处理的服务单总数量
                'all_reply_count': all_reply_count,  # 已处理的服务单总数据量
                'day_count': day_count,  # 今日单据
                'agv_reply_score': to_string(to_decimal(obj=agv_reply_score, precision='0.0')),  # 整体评分
                'day_un_count': day_un_count,  # 今日新增未处理
                'day_reply_count': day_reply_count,  # 今日处理单据
                'yesday_count': yesday_count,  # 昨日服务单数量
                'yesday_reply_count': yesday_reply_count,  # 昨日已处理服务单据
                'agv_reply_second': to_string(math.ceil(agv_reply_second)),  # 平均回复时长
                'user_reply_second_list': user_reply_second_list,  # 个人时长统计
                'user_reply_score_list': user_reply_score_list,  # 个人评分统计,
                'user_reply_count_list': user_reply_count_list,  # 每个人处理的服务单数量
                'day_7_list': day_7_list
            }

            return ok(data=result)
        except:
            log(traceback.format_exc(), 'statistics_home')
            return failure(msg='操作失败')

    def mainsecond(self):
        with DbSession.create(EnumDb.main) as db:
            for v in db.query(CustomerSubmmintForm).filter(CustomerSubmmintForm.is_reply == 1):
                second = (v.replyl_time - v.creat_time).seconds
                v.reply_time_second = second

        return ok()

    def project_progress_board_statistics(self, chandao_project_id):
        '''
        任务总量

        近7天任务趋势

        每个人的任务数量

        合同人天、预算人天（蓝图）、实际人天（工时人天）、有效人天（人天统计）

        项目预算金额，回款总金额。

        回款金额列表

        :param project_id:
        :return:
        '''
        now_time = datetime.datetime.now()

        day_7_time = now_time - datetime.timedelta(days=7)
        yesdate = now_time - datetime.timedelta(days=1)

        date_dict = {}
        begin_date = datetime.datetime.strptime(to_string(day_7_time)[:10], "%Y-%m-%d")
        end_date = datetime.datetime.strptime(to_string(now_time)[:10], "%Y-%m-%d")
        while begin_date <= end_date:
            date_str = begin_date.strftime("%Y-%m-%d")
            date_dict[to_string(date_str)] = 0
            begin_date += datetime.timedelta(days=1)

        with DbSession.create(EnumDb.main) as db:
            query = db.query(ChandaoProject).filter(ChandaoProject.chandao_project_id == chandao_project_id).first()
            if not query:
                return failure(msg='项目信息不存在，请核对后重试！')

            project_dict = format_dict(query.get_dict())

            # 阶段
            phases_query = db.query(ChandaoProjectSummaryPhases).filter(ChandaoProjectSummaryPhases.chandao_project_id == chandao_project_id).first()
            if not phases_query:
                phases_dict = {}

            else:
                summary_phases_id = phases_query.id
                phases_dict = format_dict(phases_query.get_dict())
                phases_detail_query = db.query(ChandaoProjectSummaryPhasesDetail). \
                    filter(ChandaoProjectSummaryPhasesDetail.summary_phases_id == summary_phases_id). \
                    all()
                phases_detail_list = []
                for v in phases_detail_query:
                    phases_detail_list.append(format_dict(v.get_dict()))
                phases_dict['phases_detail_list'] = phases_detail_list

            # 任务总量
            working_count_query = db.query(func.count(ProjectTaskWorkingDailyDetail.id)). \
                filter(ProjectTaskWorkingDailyDetail.chandao_project_id == chandao_project_id). \
                first()[0]
            if not working_count_query:
                working_count = 0
            else:
                working_count = working_count_query

            # 近7天的任务量趋势
            working_count_7_query = db.query(func.count(ProjectTaskWorkingDailyDetail.id),
                                             ProjectTaskWorkingDailyDetail.report_time). \
                filter(ProjectTaskWorkingDailyDetail.chandao_project_id == chandao_project_id). \
                filter(ProjectTaskWorkingDailyDetail.report_time <= to_string(yesdate)[:10]). \
                filter(ProjectTaskWorkingDailyDetail.report_time >= to_string(day_7_time)[:10]). \
                group_by(ProjectTaskWorkingDailyDetail.report_time). \
                all()
            for v in working_count_7_query:
                date_dict[to_string(v[1])] = v[0]

            # 每个人的任务量
            working_user_count = {}
            working_user_count_query = db.query(func.count(ProjectTaskWorkingDailyDetail.id),
                                                ProjectTaskWorkingDailyDetail.reporting_person_user_name). \
                filter(ProjectTaskWorkingDailyDetail.chandao_project_id == chandao_project_id). \
                group_by(ProjectTaskWorkingDailyDetail.reporting_person_user_name). \
                all()
            for v in working_user_count_query:
                working_user_count[v[1]] = v[0]

            # 回款总金额
            payback_count_query = db.query(func.sum(ChandaoProjectPayback.payback_amount)). \
                filter(ChandaoProjectPayback.chandao_project_id == chandao_project_id). \
                filter(ChandaoProjectPayback.is_confirmed == 1). \
                first()[0]
            if not payback_count_query:
                payback_count = 0
            else:
                payback_count = to_string(payback_count_query)

            # 回款列表
            payback_list = []
            payback_query = db.query(ChandaoProjectPayback).filter(ChandaoProjectPayback.chandao_project_id == chandao_project_id)
            for v in payback_query:
                payback_list.append(format_dict(v.get_dict()))

        return ok(data={
            'project_dict': project_dict,
            'phases_dict': phases_dict,
            'working_count': working_count,
            'working_count_7': date_dict,
            'working_user_count': working_user_count,
            'payback_count': payback_count,
            'payback_list': payback_list
        })

    def project_data_statistics(self, project_name):
        '''
        统计 周计划、项目概要、甲方人员、项目回款信息
        :param project_name:
        :return:
        '''

        today = datetime.datetime.now()
        monday, sunday = today, today
        one_day = datetime.timedelta(days=1)

        while monday.weekday() != 0:
            monday -= one_day
        while sunday.weekday() != 6:
            sunday += one_day

        first_day = str(datetime.datetime.strftime(monday, '%Y-%m-%d'))
        last_day = str(datetime.datetime.strftime(sunday, '%Y-%m-%d'))

        if project_name:
            select_project_name = ChandaoProject.project_name.like('%' + project_name + '%')
        else:
            select_project_name = True

        project_list = []
        chandao_project_ids = []
        project_week_plan_list = []
        chandao_project_summary_phases_list = []
        chandao_project_payback_list = []
        with DbSession.create(EnumDb.main) as db:
            for v in db.query(ChandaoProject.chandao_project_id, ChandaoProject.project_name, ChandaoProject.id, ChandaoProject.code). \
                    filter(select_project_name).filter(ChandaoProject.status.in_(['wait', 'doing'])):
                chandao_project_ids.append(v[0])
                project_list.append({
                    'chandao_project_id': v[0],
                    'project_name': v[1],
                    'project_id': v[2],
                    'code': v[3]
                })

            # 周计划
            for d in db.query(ChandaoProjectWeekPlan.chandao_project_id). \
                    filter(ChandaoProjectWeekPlan.chandao_project_id.in_(chandao_project_ids)). \
                    filter(ChandaoProjectWeekPlan.plan_time >= first_day). \
                    filter(ChandaoProjectWeekPlan.plan_time <= last_day):
                project_week_plan_list.append(d[0])

            for p in db.query(ChandaoProjectSummaryPhases.chandao_project_id). \
                    filter(ChandaoProjectSummaryPhases.chandao_project_id.in_(chandao_project_ids)):
                chandao_project_summary_phases_list.append(p[0])

            # 甲方人员
            bingding_query = db.query(ChandaoProjectShareBinding.wechat_user_id, ChandaoProjectShareBinding.project_id). \
                filter(ChandaoProjectShareBinding.project_id.in_(chandao_project_ids)). \
                filter(ChandaoProjectShareBinding.is_delete == 0)
            wechat_user_id_list = []
            pro_bind_dict = {}
            for w in bingding_query:
                wechat_user_id_list.append(w[0])
                if w[1] not in pro_bind_dict:
                    pro_bind_dict[w[1]] = [w[0]]
                else:
                    pro_bind_dict[w[1]].append(w[0])

            wechat_user_dict = {}
            wu_query = db.query(WechatUser.id, WechatUser.nike_name).filter(WechatUser.id.in_(wechat_user_id_list))
            for u in wu_query:
                wechat_user_dict[u[0]] = u[1]

            # 项目回款
            for pp in db.query(ChandaoProjectPayback.chandao_project_id).filter(ChandaoProjectPayback.chandao_project_id.in_(chandao_project_ids)):
                chandao_project_payback_list.append(pp[0])

            # team
            team_user_dict = {}
            for tu in db.query(ChandaoProjectTeamUser.chandao_old_project_id, ChandaoProjectTeamUser.realname). \
                    filter(ChandaoProjectTeamUser.chandao_old_project_id.in_(chandao_project_ids)). \
                    filter(ChandaoProjectTeamUser.is_delete == 0):
                if tu[0] not in team_user_dict:
                    team_user_dict[tu[0]] = [tu[1]]
                else:
                    team_user_dict[tu[0]].append(tu[1])

        for v in project_list:
            chandao_project_id = v['chandao_project_id']

            if chandao_project_id in project_week_plan_list:
                v['is_project_week_plan'] = 1
            else:
                v['is_project_week_plan'] = 0

            if chandao_project_id in chandao_project_summary_phases_list:
                v['is_chandao_project_summary_phases'] = 1
            else:
                v['is_chandao_project_summary_phases'] = 0

            customer_list = []
            if chandao_project_id in pro_bind_dict:
                pro_bind_list = pro_bind_dict[chandao_project_id]
                for e in pro_bind_list:
                    num = 1
                    if e in wechat_user_dict:
                        wechat_user_name = wechat_user_dict[e]
                    else:
                        wechat_user_name = '游客' + to_string(num)
                        num += 1

                    customer_list.append(wechat_user_name)

            v['customer_list'] = customer_list

            if chandao_project_id in chandao_project_payback_list:
                v['is_chandao_project_payback'] = 1
            else:
                v['is_chandao_project_payback'] = 0

            if chandao_project_id in team_user_dict:
                v['team_list'] = team_user_dict[chandao_project_id]
            else:
                v['team_list'] = []

        return ok(data=project_list)

    def project_list_statistics(self, project_name):
        # 查询项目的当前阶段
        # 查询项目阶段变更历史
        # 当前阶段超过20天未变更，则提醒
        # 项目风险列表
        # 项目回款节点及金额
        # 本周计划工作
        # 项目预算人天
        # 项目实际人天
        # 项目合同金额
        #
        today = datetime.datetime.now()
        monday, sunday = today, today
        one_day = datetime.timedelta(days=1)

        while monday.weekday() != 0:
            monday -= one_day
        while sunday.weekday() != 6:
            sunday += one_day

        first_day = str(datetime.datetime.strftime(monday, '%Y-%m-%d'))
        last_day = str(datetime.datetime.strftime(sunday, '%Y-%m-%d'))

        project_list = []
        chandao_project_ids = []
        project_week_plan_list = []
        project_ids = []
        with DbSession.create(EnumDb.main) as db:
            for v in db.query(ChandaoProject.chandao_project_id, ChandaoProject.project_name, ChandaoProject.id, ChandaoProject.code). \
                    filter(ChandaoProject.status.in_(['doing'])):
                chandao_project_ids.append(v[0])
                project_ids.append(v[2])
                project_list.append({
                    'chandao_project_id': v[0],
                    'project_name': v[1],
                    'project_id': v[2],
                    'code': v[3]
                })

            for d in db.query(ChandaoProjectWeekPlan.chandao_project_id). \
                    filter(ChandaoProjectWeekPlan.chandao_project_id.in_(chandao_project_ids)). \
                    filter(ChandaoProjectWeekPlan.plan_time >= first_day). \
                    filter(ChandaoProjectWeekPlan.plan_time <= last_day):
                project_week_plan_list.append(d[0])

            project_now_phases_dict = {}
            project_phases_dict = {}
            phases_query = db.query(ChandaoProjectSummaryPhases.chandao_project_id,
                                    ChandaoProjectSummaryPhases.phase_name,
                                    ChandaoProjectSummaryPhases.creat_time,
                                    ChandaoProjectSummaryPhases.planned_completion_time,
                                    ChandaoProjectSummaryPhases.contractor_days,
                                    ChandaoProjectSummaryPhases.budget_days,
                                    ChandaoProjectSummaryPhases.payment_this_period). \
                filter(ChandaoProjectSummaryPhases.chandao_project_id.in_(chandao_project_ids)). \
                order_by(ChandaoProjectSummaryPhases.id.desc())
            for p in phases_query:
                phases_data = format_dict({'phases_name': p[1], 'creat_time': p[2], 'planned_completion_time': p[3]})
                if p[0] not in project_now_phases_dict:
                    if p[3] and p[3] < datetime.date.today():
                        phases_status = '已延期'
                    elif not p[3]:
                        phases_status = '未设置阶段回款时间'
                    else:
                        phases_status = '未到期'
                    project_now_phases_dict[p[0]] = format_dict({'phases_name': p[1],
                                                                 'creat_time': p[2],
                                                                 'planned_completion_time': p[3],
                                                                 'phases_status': phases_status,
                                                                 'contractor_days': p[4],
                                                                 'budget_days': p[5],
                                                                 'payment_this_period': p[6]
                                                                 })

                if p[0] not in project_phases_dict:
                    project_phases_dict[p[0]] = [phases_data]
                else:
                    project_phases_dict[p[0]].append(phases_data)

            # 项目回款
            payback_dict = {}
            for pp in db.query(ChandaoProjectPayback).filter(ChandaoProjectPayback.chandao_project_id.in_(chandao_project_ids)):
                if pp.chandao_project_id not in payback_dict:
                    payback_dict[pp.chandao_project_id] = [format_dict(pp.get_dict())]
                else:
                    payback_dict[pp.chandao_project_id].append(format_dict(pp.get_dict()))

            project_week_plan_id_list = []
            plamid_projectid_dict = {}
            for d in db.query(ChandaoProjectWeekPlan.id, ChandaoProjectWeekPlan.chandao_project_id). \
                    filter(ChandaoProjectWeekPlan.chandao_project_id.in_(chandao_project_ids)). \
                    filter(ChandaoProjectWeekPlan.plan_time >= first_day). \
                    filter(ChandaoProjectWeekPlan.plan_time <= last_day):
                project_week_plan_id_list.append(d[0])
                plamid_projectid_dict[d[0]] = d[1]

            week_plan_dict = {}
            for wpd in db.query(ChandaoProjectWeekPlanDetail).filter(ChandaoProjectWeekPlanDetail.plan_id.in_(project_week_plan_id_list)):
                chandao_project_id = plamid_projectid_dict[wpd.plan_id]
                if chandao_project_id not in week_plan_dict:
                    week_plan_dict[chandao_project_id] = [format_dict(wpd.get_dict())]
                else:
                    week_plan_dict[chandao_project_id].append(format_dict(wpd.get_dict()))

            risk_dict = {}
            risk_query = db.query(ChandaoProjectRisk). \
                filter(ChandaoProjectRisk.project_id.in_(project_ids)). \
                filter(ChandaoProjectRisk.is_delete == 0). \
                order_by(ChandaoProjectRisk.plan_end_time.desc())
            for cpr in risk_query:
                if cpr.project_id not in risk_dict:
                    risk_dict[cpr.project_id] = [format_dict(cpr.get_dict())]
                else:
                    risk_dict[cpr.project_id].append(format_dict(cpr.get_dict()))

        for v in project_list:
            chandao_project_id = v['chandao_project_id']
            project_id = v['project_id']
            v['project_now_phases'] = project_now_phases_dict.get(chandao_project_id)
            v['project_phases_list'] = project_phases_dict.get(chandao_project_id)
            v['payback_list'] = payback_dict.get(chandao_project_id)
            v['week_plan_list'] = week_plan_dict.get(chandao_project_id)
            v['risk_list'] = risk_dict.get(project_id)

        return ok(data=project_list)


if __name__ == '__main__':
    a = StatisticsManage({'user_id': 1})
    res = a.mainsecond()
    print(res)
