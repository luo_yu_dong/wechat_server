#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: excel_export_base.py
@time: 2021/11/23 10:15
@desc:
'''
import os
import traceback

import xlwt
from xlwt import easyxf

from framework.log_controller import log, trycatch
from framework.utilities import get_len, to_string
from setting import EXCEL_CACHE


@trycatch()
def write_to_excel(rows,
                   file_name_1,
                   head_style_str=r'pattern: pattern solid, fore_colour pale_blue;font:height 240; align: vert centre, horiz center;',
                   heads_0='',
                   row_len=0):
    '''
    写入excel
    :param user_id:
    :type user_id:
    :param directory:
    :type directory:
    :param rows:
    :type rows:
    :param head_style_str:
    :type head_style_str:
    :return:
    :rtype:
    '''
    header_style = easyxf('pattern: pattern solid, fore_colour pale_blue;font:height 240; align: vert centre, horiz center;')
    heads_0_style = style_center(height=400)
    col_style = style_center(warp=False)

    try:
        f = xlwt.Workbook(encoding='utf-8')  # 创建工作簿
        work_sheet = f.add_sheet('Sheet1', cell_overwrite_ok=True)  # 创建sheet

        if heads_0:
            row_index = 1
            work_sheet.write_merge(0, 0, 0, row_len, heads_0, heads_0_style)  # 合并第一行，第0列到第14列的单元格 居中
        else:
            row_index = 0

        error_no = ''
        for row in rows:
            col_end = get_len(row)
            if heads_0:
                if row_index == 1:
                    for col in range(0, col_end):
                        if header_style:
                            work_sheet.write(row_index, col, row[col], header_style)
                        else:
                            work_sheet.write(row_index, col, row[col], header_style)

                        if col == 3:
                            work_sheet.col(col).width = get_len(str(row[col])) * 3000
                        elif col in [6, 7, 8]:
                            work_sheet.col(col).width = get_len(str(row[col])) * 6000
                        elif col in [9, 11]:
                            work_sheet.col(col).width = get_len(str(row[col])) * 1500
                        else:
                            work_sheet.col(col).width = get_len(str(row[col])) * 1000

                else:
                    for col in range(0, col_end):
                        work_sheet.write(row_index, col, to_string(row[col]), col_style)

                error_no = row[0]
                row_index += 1

            else:
                if row_index == 0:
                    for col in range(0, col_end):
                        if header_style:
                            work_sheet.write(row_index, col, row[col], header_style)
                        else:
                            work_sheet.write(row_index, col, row[col], header_style)

                        if col == 3:
                            work_sheet.col(col).width = get_len(str(row[col])) * 3000
                        elif col in [6, 7, 8]:
                            work_sheet.col(col).width = get_len(str(row[col])) * 6000
                        elif col in [9, 11]:
                            work_sheet.col(col).width = get_len(str(row[col])) * 1500
                        else:
                            work_sheet.col(col).width = get_len(str(row[col])) * 1000

                else:
                    for col in range(0, col_end):
                        work_sheet.write(row_index, col, to_string(row[col]), col_style)

                error_no = row[0]
                row_index += 1

        file_name = os.path.join(EXCEL_CACHE, file_name_1)
        f.save(file_name)

        return file_name
    except Exception as e:
        print(error_no)
        log(traceback.format_exc(), ' write_to_excel', '创建excel失败')
        return ''


def style_center(warp=True, bold=False, background_color=None, font_color=None, height=0x00C8, border_color=None, borders=True):
    '''
    中间对齐
    :param warp:是否换行
    :type warp: bool
    :param bold:是否加粗:
    :type bold: bool
    :param background_color:   背景色
    :type background_color: int
    :param font_color: 字体颜色
    :type font_color:  int
    :return:  样式
    :rtype: XFStyle
    '''
    style = xlwt.XFStyle()  # 初始化样式
    alignment = xlwt.Alignment()
    alignment.horz = xlwt.Alignment.HORZ_CENTER  # 中间对齐
    alignment.vert = xlwt.Alignment.VERT_CENTER  # 水平对齐
    _common_style(alignment, background_color, bold, font_color, style, warp, height, border_color, borders)
    return style


def _common_style(alignment, background_color, bold, font_color, style, warp, height, border_color, borders=True):
    if alignment is not None:
        if warp:
            alignment.wrap = xlwt.Alignment.WRAP_AT_RIGHT  # 自动换行
        style.alignment = alignment

    if background_color and isinstance(background_color, int):
        pattern = xlwt.Pattern()
        pattern.pattern_fore_colour = background_color
        pattern.pattern = xlwt.Pattern.SOLID_PATTERN
        style.pattern = pattern

    font = xlwt.Font()
    if font_color and isinstance(font_color, int):
        font.colour_index = font_color

    if bold:
        font.bold = True
    font.height = height
    font.name = u'宋体'

    style.font = font

    if borders:
        borders = xlwt.Borders()
        borders.left = 1
        borders.right = 1
        borders.top = 1
        borders.bottom = 1
        # borders.bottom_colour = border_color
        style.borders = borders
