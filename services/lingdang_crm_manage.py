#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: lingdang_crm_manage.py
@time: 2020/11/16 8:15
@desc:
'''
import json
import math
import traceback
from datetime import datetime

import requests

from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.log_controller import log
from framework.msg import failure, ok
from framework.utilities import to_dict, to_string
from models.customer_form.customer_submmint_form import CustomerSubmmintForm
from models.exception_log.save_crm_log import SaveCrmLog

lingdang_appsecret = {
    'ip': 'http://cloud.qianxingit.com:85',
    'apikey': 'qianxing0313'
}


class LingdangCRMManage():
    def __init__(self):
        pass

    def get_token(self):
        url = '%s/crm/crmapi/apiKey.php' % (to_string(lingdang_appsecret['ip']))
        post_body = {
            'authen_code': lingdang_appsecret['apikey']
        }

        is_ok, data, error_msg = self.lingdap_to_post(url=url, post_body=post_body)
        if not is_ok:
            self.save_crm_to_log(crm_error_msg=error_msg, is_ok=0, post_data=to_string(post_body), url=url)
            return failure(msg='上传数据失败')

        return ok(data=data)

    def add_crm_hepldesk(self, business_name,
                         form_contacts,
                         user_name,
                         crm_username,
                         ticket_no,
                         ticket_title,
                         solution,
                         realtime,
                         createdtime,
                         description,
                         form_id,
                         ticketcategories,
                         form_contacts_phone):
        '''
        :param business_name: 企业名称
        :param form_contacts: 联系人
        :param user_name: 创建人、服务人
        :param ticket_no: 服务单编号
        :param ticket_title:
        :param solution:
        :param realtime:
        :param createdtime:
        :param description:
        :return:
        '''
        res_data = self.get_token()

        apikey = res_data.data.get('key')
        token = res_data.data.get('token')

        accountid, contactid = self.get_crm_account_id(business_name=business_name, form_contacts=form_contacts, apikey=apikey, token=token)

        crm_user_id = self.get_user_id(user_name=user_name, apikey=apikey, token=token)

        with DbSession.create(EnumDb.main) as db:
            from_res = db.query(CustomerSubmmintForm.reply_time_second).filter(CustomerSubmmintForm.id == form_id).filter(CustomerSubmmintForm.is_delete == 0).first()
            if not from_res:
                consumetime = 0
            else:
                if from_res[0]:
                    reply_time_second = math.ceil(from_res[0] / 3600)
                else:
                    reply_time_second = 0

                consumetime = to_string(reply_time_second) + '小时'

        # 2023-06-28 CRM升级，接口变更
        # url = '%s/crm/crmapi/crmoperation.php' % (to_string(lingdang_appsecret['ip']))
        url = '%s/crm/crmapi/api_crmoperation.php' % (to_string(lingdang_appsecret['ip']))

        post_body = {
            "apikey": apikey,
            "token": token,
            "module": "HelpDesk",
            "func": "newApiSave",
            "username": crm_username,
            "dataList": [
                {
                    "ticket_no": ticket_no,
                    "ticketstatus": '成功关闭',
                    "ticket_title": ticket_title,
                    "solution": solution,
                    "servicetype": '小程序服务单',
                    "realtime": to_string(realtime),
                    "account_id": business_name,  # 这是个id，需要先查询 ld_account 表有没有相关数据   2023-06-29，新接口說這個傳名字。
                    "contact_id": contactid,  # 这个也是id ,需要先查询 ld_contactdetails 表有没有相关数据
                    "ticketcategories": ticketcategories,
                    "smcreatorid": crm_username,  # 这个需要填写服务人的id。 2023-06-29，新接口說這個傳名字。
                    "createdtime": createdtime,
                    "description": description,
                    "assigned_user_id": user_name,  # 2023-06-29，新接口說這個傳名字。
                    "cf_2706": business_name,  # 企业名称（小程序
                    "cf_2707": form_contacts,  # 联系人（小程序）
                    "cf_2708": to_string(form_contacts_phone),  # 联系电话（小程序）
                    "cf_2702": to_string(form_contacts_phone),
                    "productcategory": "金蝶",
                    "hopetime": createdtime,
                    "consumetime": consumetime,
                }
            ],
        }
        post_body = json.dumps(post_body)
        is_ok, data, error_msg = self.lingdap_to_post(url=url, post_body=post_body, func=form_id)
        if not is_ok:
            self.save_crm_to_log(crm_error_msg=error_msg, is_ok=0, post_data=to_string(post_body), url=url)
            return failure(msg='上传失败')

        with DbSession.create(EnumDb.main) as db:
            db.query(CustomerSubmmintForm). \
                filter(CustomerSubmmintForm.id == form_id). \
                update({CustomerSubmmintForm.is_to_crm: 1}, synchronize_session='fetch')

        return ok()

    def get_user_id(self, user_name, apikey, token):
        try:
            url = lingdang_appsecret['ip'] + '/crm/crmapi/getidApi.php'

            post_data = {
                "apikey": apikey,
                "token": token,
                "func": 'getuserid',
                "last_name": user_name,
            }
            res = requests.post(url=url, data=post_data)
            result = to_dict(res.content)

            return result[0]
        except:
            return 0

    # 获取客户id
    def get_crm_account_id(self, business_name, form_contacts, apikey, token):

        url = '%s/crm/crmapi/crmoperation.php' % (to_string(lingdang_appsecret['ip']))

        post_data = {
            "module": "Accounts",
            "func": "getList",
            "apikey": apikey,
            "token": token,
            "username": "admin",
            "searchtext": json.dumps([
                {
                    "groupid": "1",
                    "module": "Accounts",
                    "fieldname": "accountname",
                    "comparator": "等于",
                    "value": business_name,
                    "columncondition": "",
                    "groupcondition": ""
                }
            ])
        }

        is_ok, data, error_msg = self.lingdap_to_post(url=url, post_body=post_data)
        if not is_ok:
            self.save_crm_to_log(crm_error_msg=error_msg, is_ok=0, post_data=to_string(post_data), url=url)
            return 0, 0

        if not data.get('result'):
            lingdang_crm_account_id = 0
            lingdang_crm_contacts_id = 0
        else:
            lingdang_crm_data = data.get('result')[0]
            if not lingdang_crm_data:
                lingdang_crm_account_id = 0
                lingdang_crm_contacts_id = 0
            else:
                lingdang_crm_account_id = 0
                lingdang_crm_contacts_id = 0
                # 联系人信息
                contacts_list = lingdang_crm_data.get('Detail_Contacts')
                for v in contacts_list:
                    if form_contacts == v.get('crealname'):
                        lingdang_crm_contacts_id = v.get('cid')
                        continue
                    else:
                        if not lingdang_crm_contacts_id:
                            lingdang_crm_contacts_id = v.get('cid')

                lingdang_crm_account_id = lingdang_crm_data.get('accountid')

        return lingdang_crm_account_id, lingdang_crm_contacts_id

    def lingdap_to_get(self, url):
        try:
            rq = requests.session()
            res = rq.get(url=url)
            result = to_dict(res.content)

            code = result.get('code')
            msg = result.get('msg')
            if code == 'error':
                return False, {}, msg

            return True, result, ''
        except:
            return False, {}, traceback.format_exc()

    def lingdap_to_post(self, url, post_body, func=None):
        try:
            rq = requests.session()
            res = rq.post(url=url, data=post_body)
            result = to_dict(res.content)

            if not result:
                result = to_string(res.content)
                if 'error' in result:
                    print(to_string(func) + '-----------' + to_string(res.content))
                    return False, {}, result

            code = result.get('code')
            msg = result.get('msg')

            if code == 'error':
                if func:
                    print(to_string(func) + '-----------' + to_string(res.content))
                return False, {}, msg

            return True, result, ''
        except:
            return False, {}, traceback.format_exc()

    # 保存请求日志
    def save_crm_to_log(self, crm_error_msg, is_ok, post_data, url):
        try:
            with DbSession.create(EnumDb.main) as db:
                scl = SaveCrmLog()
                scl.crm_erroe_msg = crm_error_msg
                scl.is_ok = is_ok
                scl.post_data = to_string(post_data)
                scl.url = url
                scl.post_time = datetime.now()
                db.add(scl)

            return ok()
        except:
            log(traceback.format_exc(), 'LingdangCRMManage.save_crm_to_log')
            return failure(msg='操作失败')


if __name__ == '__main__':
    a = LingdangCRMManage()

    res = a.add_crm_hepldesk(business_name='青岛洲际之星船务有限公司',
                             form_contacts='郝',
                             user_name='栾一豪',
                             crm_username='luanyihao',  # crm的账号
                             ticket_no='SHFWD20230628005',
                             ticket_title='其他应收单中的凭证模板调整',
                             solution='凭证模板调整',  # 解决方法
                             realtime='2023-06-29 13:52:23',
                             createdtime='2023-06-28 15:43:35',
                             description='其他应收单中的凭证模板调整',
                             form_id=1934,
                             ticketcategories='其他问题',
                             form_contacts_phone='13864892676')

    # gettoken = a.get_token()
    # token = gettoken.data.get('token')
    # res = a.get_user_id(user_name='于世凤', apikey=lingdang_appsecret['apikey'], token=token)

    # res = a.get_crm_account_id(business_name='测试测试123', form_contacts='发发发')

    print(res)
