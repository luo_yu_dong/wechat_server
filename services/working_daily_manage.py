# coding: utf-8
# author: lyd
# mobile: 15288343493
# email : 339789089@qq.com
# file  : working_daily_manage.py
# time  : 2024-07-08 09:57
# desc  :
import datetime
import os
import traceback

import xlwt
from sqlalchemy import func, and_
from xlwt import easyxf

from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.log_controller import log
from framework.msg import ok, failure
from framework.utilities import format_dict, to_string, to_dict, to_decimal, to_list, to_datetime, to_timestamp, get_FileSize, get_len, to_int
from models.chandao.chandao_project import ChandaoProject
from models.chandao.chandao_project_summary_phases import ChandaoProjectSummaryPhases
from models.chandao.chandao_project_summary_phases_detail import ChandaoProjectSummaryPhasesDetail
from models.chandao.chandao_project_team_user import ChandaoProjectTeamUser
from models.chandao.chandao_project_week_plan import ChandaoProjectWeekPlan
from models.chandao.chandao_project_week_plan_detail import ChandaoProjectWeekPlanDetail
from models.chandao.chandao_project_week_plan_detail_user import ChandaoProjectWeekPlanDetailUser
from models.chandao.sys_project_phase_configuration import SysProjectPhaseConfiguration
from models.file.file import File
from models.task_working.project_task_working_daily import ProjectTaskWorkingDaily
from models.task_working.project_task_working_daily_detail import ProjectTaskWorkingDailyDetail
from models.task_working.project_task_working_week_daily import ProjectTaskWorkingWeekDaily
from models.task_working.project_task_working_week_daily_detail import ProjectTaskWorkingWeekDailyDetail
from models.task_working.project_task_working_week_daily_next_detail import ProjectTaskWorkingWeekDailyNextDetail
from models.users.user import User
from models.users.white_user_list import WhiteUserList
from services.excel_export_base import style_center
from services.file_manage import FileManage
from setting import EXCEL_CACHE, SERVER_HOST

import pandas as pd
import chinese_calendar


class WorkingDailyManage():
    def __init__(self, user, token=None):
        self.user = user
        self.user_id = user['user_id']
        self.user_name = user['user_name']

    def get_sys_project_phase_configuration(self):
        list = []
        with DbSession.create(EnumDb.main) as db:
            query = db.query(SysProjectPhaseConfiguration).filter(SysProjectPhaseConfiguration.is_delete == 0).order_by(SysProjectPhaseConfiguration.id)
            for v in query:
                list.append(format_dict(v.get_dict()))

        return ok(data=list)

    def project_task_working_daily_select(self, report_time):
        yesterday = datetime.datetime.now() - datetime.timedelta(days=1)

        with DbSession.create(EnumDb.main) as db:
            white_user_list = [v[0] for v in db.query(WhiteUserList.user_id)]

        if to_int(self.user_id) not in white_user_list:
            if to_datetime(report_time) < to_datetime(to_string(yesterday)[:10]):
                return failure(msg='汇报时间不能早于昨天！')

            if to_datetime(report_time) > to_datetime(to_string(datetime.datetime.now())[:10]):
                return failure(msg='汇报时间不能晚于今天！')

        with DbSession.create(EnumDb.main) as db:
            pt_query = db.query(ProjectTaskWorkingDaily). \
                filter(ProjectTaskWorkingDaily.report_time == report_time). \
                filter(ProjectTaskWorkingDaily.reporting_person_user_id == self.user_id). \
                first()
            if pt_query:
                return failure(msg='您已经创建了今天的任务，如需修正，请对内容进行汇报')

        return ok()

    def project_task_working_daily_add(self, report_time, task_working_detail_list, is_edit=0, task_working_id=0):
        yesterday = datetime.datetime.now() - datetime.timedelta(days=1)

        with DbSession.create(EnumDb.main) as db:
            white_user_list = [v[0] for v in db.query(WhiteUserList.user_id)]

        if to_int(self.user_id) not in white_user_list:
            if to_datetime(report_time) < to_datetime(to_string(yesterday)[:10]):
                return failure(msg='汇报时间不能早于昨天！')

            if to_datetime(report_time) > to_datetime(to_string(datetime.datetime.now())[:10]):
                return failure(msg='汇报时间不能晚于今天！')

        project_staff_time = {}
        chandao_project_ids = []

        file_list = []
        for v in task_working_detail_list:
            v = to_dict(v)
            file = to_list(v.get('file'))
            if file:
                file_list.extend(file)

        if file_list:
            f = FileManage()
            res = f.file_save(file_list=file_list)
            if not res.is_ok():
                return failure(msg='保存文件失败！')

        with DbSession.create(EnumDb.main) as db:
            if is_edit:
                query = db.query(ProjectTaskWorkingDaily). \
                    filter(ProjectTaskWorkingDaily.id == task_working_id). \
                    filter(ProjectTaskWorkingDaily.reporting_person_user_id == self.user_id). \
                    first()
                if not query:
                    return failure(msg='不能修改其他人的工时汇报！')

                db.query(ProjectTaskWorkingDaily). \
                    filter(ProjectTaskWorkingDaily.report_time == report_time). \
                    filter(ProjectTaskWorkingDaily.reporting_person_user_id == self.user_id). \
                    delete(synchronize_session='fetch')

                db.query(ProjectTaskWorkingDailyDetail). \
                    filter(ProjectTaskWorkingDailyDetail.report_time == report_time). \
                    filter(ProjectTaskWorkingDailyDetail.reporting_person_user_id == self.user_id). \
                    delete(synchronize_session='fetch')

            pt_query = db.query(ProjectTaskWorkingDaily). \
                filter(ProjectTaskWorkingDaily.report_time == report_time). \
                filter(ProjectTaskWorkingDaily.reporting_person_user_id == self.user_id). \
                first()
            if pt_query:
                return failure(msg='您已经创建了今天的任务，如需修正，请对内容进行汇报!')

            ptwd = ProjectTaskWorkingDaily()
            ptwd.reporting_person_user_id = self.user_id
            ptwd.reporting_person_user_name = self.user_name
            ptwd.report_time = report_time
            ptwd.creat_time = datetime.datetime.now()
            db.add(ptwd)
            db.flush()
            ptwd_id = ptwd.id

            for v in task_working_detail_list:
                v = to_dict(v)
                file = to_list(v.get('file'))
                if file:
                    for f in file:
                        if f['type'] == 'del':
                            file_index = file.index(f)
                            del file[file_index]
                        else:
                            f['file_name'] = f['file_name'].replace('temporary-', '')

                    last_file = to_string(file)
                else:
                    last_file = ''

                d = ProjectTaskWorkingDailyDetail()
                d.project_task_working_daily_id = ptwd_id
                d.chandao_project_id = v['chandao_project_id']
                d.project_name = v['chandao_project_name']
                d.staff_time = to_decimal(v['staff_time'])
                d.work_content = to_string(v['work_content'])
                d.creat_time = datetime.datetime.now()
                d.report_time = report_time
                d.reporting_person_user_id = self.user_id
                d.reporting_person_user_name = self.user_name
                d.file = last_file
                d.year = to_datetime(report_time).year
                d.month = to_datetime(report_time).month
                db.add(d)
                chandao_project_ids.append(v['chandao_project_id'])
                if v['chandao_project_id'] not in project_staff_time:
                    project_staff_time[v['chandao_project_id']] = to_decimal(v['staff_time'])
                else:
                    project_staff_time[v['chandao_project_id']] += to_decimal(v['staff_time'])

        return ok()

    def project_task_working_daily_get_list(self, select_user_name, page, size, report_time, is_my):
        if select_user_name:
            query_select_user_name = ProjectTaskWorkingDaily.reporting_person_user_name.like('%' + select_user_name + '%')
        else:
            query_select_user_name = True

        list = []
        with DbSession.create(EnumDb.main) as db:
            user_query = db.query(User.is_admin).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
            is_admin = user_query[0]

            sale_user_list = [v[0] for v in db.query(User.user_id).filter(User.role.in_(['销售岗位', '财务经理', '行政岗位'])).filter(User.is_delete == 0)]

            if report_time:
                select_report_time = ProjectTaskWorkingDaily.report_time == report_time
            else:
                select_report_time = True

            if is_my == 1:
                select_is_my = ProjectTaskWorkingDaily.reporting_person_user_id == self.user_id
            elif is_my == 2:
                if not is_admin:
                    return failure(msg='您没有权限查看销售类信息！')
                select_is_my = ProjectTaskWorkingDaily.reporting_person_user_id.in_(sale_user_list)
            else:
                if not is_admin:
                    select_is_my = ProjectTaskWorkingDaily.reporting_person_user_id.notin_(sale_user_list)
                else:
                    select_is_my = True

            ptw_query_count = db.query(func.count(ProjectTaskWorkingDaily.id)). \
                filter(select_report_time). \
                filter(select_is_my). \
                filter(query_select_user_name). \
                order_by(ProjectTaskWorkingDaily.creat_time.desc()). \
                first()
            if not ptw_query_count:
                return ok(data={'count': 0, 'list': list})

            daily_list = []
            ptw_query = db.query(ProjectTaskWorkingDaily). \
                filter(select_report_time). \
                filter(select_is_my). \
                filter(query_select_user_name). \
                order_by(ProjectTaskWorkingDaily.creat_time.desc()). \
                offset(page * size). \
                limit(size)
            for v in ptw_query:
                daily_list.append(v.id)
                list.append(format_dict(v.get_dict()))

            dict = {}
            query = db.query(ProjectTaskWorkingDailyDetail).filter(ProjectTaskWorkingDailyDetail.project_task_working_daily_id.in_(daily_list))
            for v in query:
                if v.project_task_working_daily_id not in dict:
                    dict[v.project_task_working_daily_id] = [format_dict(v.get_dict())]
                else:
                    dict[v.project_task_working_daily_id].append(format_dict(v.get_dict()))

        for v in list:
            ptw_id = v['id']
            if ptw_id in dict:
                detail_list = dict[ptw_id]
            else:
                detail_list = []

            v['detail_list'] = detail_list

        return ok(data={'count': ptw_query_count[0], 'list': list})

    def project_task_working_daily_get_first(self, report_id):
        list = []
        with DbSession.create(EnumDb.main) as db:
            query = db.query(ProjectTaskWorkingDaily). \
                filter(ProjectTaskWorkingDaily.id == report_id).first()
            if not query:
                return failure(msg='数据不存在，请核对后再试！')

            data = format_dict(query.get_dict())

            detail_query = db.query(ProjectTaskWorkingDailyDetail). \
                filter(ProjectTaskWorkingDailyDetail.project_task_working_daily_id == report_id)
            for v in detail_query:
                list.append(format_dict(v.get_dict()))

            data['detail_list'] = list

            return ok(data=data)

    def project_task_working_plan_add(self, chandao_project_id, chandao_project_name, plan_time, plan_list, file):
        if file:
            f = FileManage()
            res = f.file_save(file_list=file)
            if res.is_ok():
                if res.data['file_list']:
                    data_file = res.data['file_list']
                else:
                    data_file = ''
            else:
                data_file = ''
                return failure(msg='保存文件失败！')
        else:
            data_file = ''

        with DbSession.create(EnumDb.main) as db:
            query = db.query(ChandaoProjectWeekPlan). \
                filter(ChandaoProjectWeekPlan.chandao_project_id == chandao_project_id). \
                filter(ChandaoProjectWeekPlan.plan_time == plan_time). \
                first()
            if query:
                return failure(msg='该项目已经有计划了')

            ptw = ChandaoProjectWeekPlan()
            ptw.chandao_project_id = chandao_project_id
            ptw.chandao_project_name = chandao_project_name
            ptw.plan_time = plan_time
            ptw.creat_time = datetime.datetime.now()
            ptw.creat_user_id = self.user_id
            ptw.creat_user_name = self.user_name
            ptw.file = to_string(data_file)
            db.add(ptw)
            db.flush()
            plan_id = ptw.id

            for v in plan_list:
                # v = eval(str(v, encoding="utf-8"))
                v = to_dict(v)
                d = ChandaoProjectWeekPlanDetail()
                d.plan_id = plan_id
                d.head_party_a = to_string(v['head_party_a'])

                head_party_a_wechat_user_ids = []
                for va in to_list(v['head_party_a']):
                    wechat_user_id = va['wechat_user_id']
                    head_party_a_wechat_user_ids.append(wechat_user_id)

                d.head_party_b = to_string(v['head_party_b'])
                head_party_b_user_ids = []
                for vb in to_list(v['head_party_b']):
                    wechat_user_id = vb['user_id']
                    head_party_b_user_ids.append(wechat_user_id)

                d.planned_week = v['planned_week']
                d.plan_end_time = v.get('plan_end_time')
                db.add(d)
                db.flush()
                d_id = d.id

                for v in head_party_a_wechat_user_ids:
                    du = ChandaoProjectWeekPlanDetailUser()
                    du.plan_id = plan_id
                    du.detail_id = d_id
                    du.is_head_party_a = 1
                    du.user_id = v
                    db.add(du)

                for v in head_party_b_user_ids:
                    du = ChandaoProjectWeekPlanDetailUser()
                    du.plan_id = plan_id
                    du.detail_id = d_id
                    du.is_head_party_a = 0
                    du.user_id = v
                    db.add(du)

        # TODO 发送公众号消息

        return ok()

    def project_task_working_plan_get_list(self, page, size, chandao_project_id):
        list = []
        plan_ids = []
        with DbSession.create(EnumDb.main) as db:
            count_query = db.query(func.count(ChandaoProjectWeekPlan.id)). \
                filter(ChandaoProjectWeekPlan.chandao_project_id == chandao_project_id). \
                order_by(ChandaoProjectWeekPlan.plan_time.desc()). \
                first()
            if not count_query:
                return ok(data={'count': 0, 'list': list})

            query = db.query(ChandaoProjectWeekPlan). \
                filter(ChandaoProjectWeekPlan.chandao_project_id == chandao_project_id). \
                order_by(ChandaoProjectWeekPlan.plan_time.desc()). \
                offset(page * size).limit(size)
            for v in query:
                plan_ids.append(v.id)
                list.append(format_dict(v.get_dict()))

            detail_dict = {}
            detail_query = db.query(ChandaoProjectWeekPlanDetail).filter(ChandaoProjectWeekPlanDetail.plan_id.in_(plan_ids))
            for v in detail_query:
                if v.plan_id not in detail_dict:
                    detail_dict[v.plan_id] = [format_dict(v.get_dict())]
                else:
                    detail_dict[v.plan_id].append(format_dict(v.get_dict()))

        for v in list:
            plan_id = v['id']
            if plan_id in detail_dict:
                detail_list = detail_dict[plan_id]
            else:
                detail_list = []

            v['detail_list'] = detail_list

        return ok(data={'count': count_query[0], 'list': list})

    def project_task_working_plan_get_first(self, plan_id):
        detail_list = []
        with DbSession.create(EnumDb.main) as db:
            query = db.query(ChandaoProjectWeekPlan). \
                filter(ChandaoProjectWeekPlan.id == plan_id). \
                first()
            if not query:
                return failure(msg='数据不存在，请核对后再试！')

            data = format_dict(query.get_dict())

            for v in db.query(ChandaoProjectWeekPlanDetail).filter(ChandaoProjectWeekPlanDetail.plan_id == plan_id).order_by(ChandaoProjectWeekPlanDetail.id.desc()):
                detail_list.append(format_dict(v.get_dict()))

        data['detail_list'] = detail_list

        return ok(data=data)

    def project_task_working_plan_update(self, chandao_project_id, chandao_project_name, plan_time, plan_list, plan_id, file):
        if file:
            f = FileManage()
            res = f.file_save(file_list=file)
            if res.is_ok():
                if res.data['file_list']:
                    data_file = res.data['file_list']
                else:
                    data_file = ''
            else:
                data_file = ''
                return failure(msg='保存文件失败！')
        else:
            data_file = ''

        with DbSession.create(EnumDb.main) as db:
            query_data = db.query(ChandaoProjectWeekPlan). \
                filter(ChandaoProjectWeekPlan.chandao_project_id == chandao_project_id). \
                filter(ChandaoProjectWeekPlan.id == plan_id). \
                first()
            if not query_data:
                return failure(msg='数据不存在，请核对后再试！')

            query_data.file = to_string(data_file)

            db.query(ChandaoProjectWeekPlanDetail).filter(ChandaoProjectWeekPlanDetail.plan_id == plan_id).delete(synchronize_session='fetch')

            db.query(ChandaoProjectWeekPlanDetailUser).filter(ChandaoProjectWeekPlanDetailUser.plan_id == plan_id).delete(synchronize_session='fetch')

            for v in plan_list:
                # v = eval(str(v, encoding="utf-8"))
                head_party_a_wechat_user_ids = []
                for va in to_list(v['head_party_a']):
                    wechat_user_id = va['wechat_user_id']
                    head_party_a_wechat_user_ids.append(wechat_user_id)

                head_party_b_user_ids = []
                for vb in to_list(v['head_party_b']):
                    wechat_user_id = vb['user_id']
                    head_party_b_user_ids.append(wechat_user_id)

                v = to_dict(v)
                d = ChandaoProjectWeekPlanDetail()
                d.plan_id = plan_id
                d.head_party_a = to_string(v['head_party_a'])
                d.head_party_b = to_string(v['head_party_b'])
                d.planned_week = v['planned_week']
                d.plan_end_time = v.get('plan_end_time')
                db.add(d)
                db.flush()
                d_id = d.id

                for v in head_party_a_wechat_user_ids:
                    du = ChandaoProjectWeekPlanDetailUser()
                    du.plan_id = plan_id
                    du.detail_id = d_id
                    du.is_head_party_a = 1
                    du.user_id = v
                    db.add(du)

                for v in head_party_b_user_ids:
                    du = ChandaoProjectWeekPlanDetailUser()
                    du.plan_id = plan_id
                    du.detail_id = d_id
                    du.is_head_party_a = 0
                    du.user_id = v
                    db.add(du)

            return ok()

    def project_task_working_plan_update_other(self, plan_id, weekly_wrap_up, party_a_comments, evaluation_response, party_a_comments_wechat_user_id,
                                               party_a_comments_wechat_user_name):
        with DbSession.create(EnumDb.main) as db:
            query_data = db.query(ChandaoProjectWeekPlan). \
                filter(ChandaoProjectWeekPlan.id == plan_id). \
                first()
            if not query_data:
                return failure(msg='数据不存在，请核对后再试！')

            if weekly_wrap_up:
                query_data.weekly_wrap_up = weekly_wrap_up
                query_data.weekly_wrap_up_time = datetime.datetime.now()
                query_data.weekly_wrap_up_user_id = self.user_id
                query_data.weekly_wrap_up_user_name = self.user_name

            if party_a_comments:
                query_data.party_a_comments = party_a_comments
                query_data.party_a_comments_time = datetime.datetime.now()
                query_data.party_a_comments_wechat_user_id = party_a_comments_wechat_user_id
                query_data.party_a_comments_wechat_user_name = party_a_comments_wechat_user_name

            if evaluation_response:
                query_data.evaluation_response = evaluation_response
                query_data.evaluation_response_time = datetime.datetime.now()
                query_data.evaluation_response_user_id = self.user_id
                query_data.evaluation_response_user_name = self.user_name

            return ok()

    def project_task_working_daily_get_list_project(self, project_id, page, size, report_time):
        if report_time:
            slsect_time = ProjectTaskWorkingDailyDetail.report_time == report_time[:10]
        else:
            slsect_time = True

        list = []
        with DbSession.create(EnumDb.main) as db:
            count_query = db.query(func.count(ProjectTaskWorkingDailyDetail.id)). \
                filter(ProjectTaskWorkingDailyDetail.chandao_project_id == project_id). \
                filter(slsect_time). \
                order_by(ProjectTaskWorkingDailyDetail.report_time.desc()). \
                first()
            if not count_query:
                return ok(data={'count': 0, 'list': []})

            query = db.query(ProjectTaskWorkingDailyDetail). \
                filter(ProjectTaskWorkingDailyDetail.chandao_project_id == project_id). \
                filter(slsect_time). \
                order_by(ProjectTaskWorkingDailyDetail.report_time.desc()). \
                offset(page * size).limit(size)
            for v in query:
                list.append(format_dict(v.get_dict()))

            return ok(data={'count': count_query[0], 'list': list})

    def project_task_working_audits(self, chandao_project_id, detail_ids):
        with DbSession.create(EnumDb.main) as db:
            project_query = db.query(ChandaoProject).filter(ChandaoProject.chandao_project_id == chandao_project_id).first()
            if not project_query:
                return failure(msg='项目数据不存在，请查证后再试！')

            PM_query = db.query(ChandaoProjectTeamUser). \
                filter(ChandaoProjectTeamUser.chandao_old_project_id == chandao_project_id). \
                filter(ChandaoProjectTeamUser.user_id == self.user_id). \
                filter(ChandaoProjectTeamUser.position == 'PM'). \
                first()
            if not PM_query:
                user = db.query(User.is_admin).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
                if not user:
                    return failure(msg='您没有权限审核工时汇报！')

            all_actual_working_hours = project_query.actual_working_hours

            for v in db.query(ProjectTaskWorkingDailyDetail).filter(ProjectTaskWorkingDailyDetail.id.in_(detail_ids)).filter(
                    ProjectTaskWorkingDailyDetail.is_audits == 0):
                if v.chandao_project_id == -1:
                    continue

                actual_working_hours = v.staff_time

                all_actual_working_hours += actual_working_hours
                v.is_audits = 1
                v.audits_time = datetime.datetime.now()

            project_query.actual_working_hours = all_actual_working_hours

        return ok()

    def project_task_working_unsubmit_user_list(self, report_time):
        if report_time:
            select_report_time = ProjectTaskWorkingDaily.report_time == to_datetime(report_time).strftime('%Y-%m-%d')
        else:
            select_report_time = ProjectTaskWorkingDaily.report_time == datetime.datetime.now().strftime('%Y-%m-%d')

        # 屏蔽
        select_user = User.user_name.notin_(['王远星', '李庆鹏', '崔苏鹏', '董峻', '肖尚嘉', '孙许静', '高尚'])
        # select_user = True

        with DbSession.create(EnumDb.main) as db:
            user_query = db.query(User.user_id).filter(User.is_delete == 0).filter(select_user)
            all_user_ids = [v[0] for v in user_query]

            submit_user_ids = []
            query = db.query(ProjectTaskWorkingDaily.reporting_person_user_id).filter(select_report_time)
            for v in query:
                submit_user_ids.append(v[0])

            # all_user_ids 有而 submit_user_ids 没有的元素
            difference = list(set(all_user_ids).difference(set(submit_user_ids)))

            diff_user = db.query(User).filter(User.user_id.in_(difference))
            unsubmit_user_list = [format_dict(v.get_dict()) for v in diff_user]

        return ok(data=unsubmit_user_list)

    def project_phases_add(self, chandao_project_id, phase_name, construction_content, planned_completion_time, detail_list, contractor_days, budget_days,
                           payment_this_period):
        with DbSession.create(EnumDb.main) as db:
            # user = db.query(User.chandao_user, User.is_admin).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
            # if not user:
            #     return failure(msg='您不是项目经理，不能设置项目概要！')
            #
            # if not user[1]:
            #     team_user = db.query(ChandaoProjectTeamUser.position). \
            #         filter(ChandaoProjectTeamUser.user_id == self.user_id). \
            #         filter(ChandaoProjectTeamUser.chandao_old_project_id == chandao_project_id). \
            #         filter(ChandaoProjectTeamUser.is_delete == 0). \
            #         first()
            #     if not team_user:
            #         return failure(msg='您不是项目经理，不能设置项目概要！')

            cpsp = ChandaoProjectSummaryPhases()
            cpsp.chandao_project_id = chandao_project_id
            cpsp.phase_name = phase_name
            cpsp.construction_content = construction_content
            cpsp.planned_completion_time = planned_completion_time
            cpsp.creat_time = datetime.datetime.now()
            cpsp.creat_user_id = self.user_id
            cpsp.creat_user_name = self.user_name
            cpsp.contractor_days = contractor_days
            cpsp.budget_days = budget_days
            cpsp.payment_this_period = payment_this_period
            db.add(cpsp)
            db.flush()
            cpsp_id = cpsp.id

            for v in detail_list:
                # v = eval(str(v, encoding="utf-8"))
                v = to_dict(v)
                cpspd = ChandaoProjectSummaryPhasesDetail()
                cpspd.summary_phases_id = cpsp_id
                cpspd.construction_objective = v['construction_objective']
                cpspd.plan_end_time = v.get('plan_end_time')
                db.add(cpspd)

        return ok()

    def project_phases_update(self, chandao_project_id, phase_name, construction_content, planned_completion_time, detail_list, contractor_days, budget_days,
                              payment_this_period):
        with DbSession.create(EnumDb.main) as db:
            # user = db.query(User.chandao_user, User.is_admin).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
            # if not user:
            #     return failure(msg='您不是项目经理，不能设置项目概要！')
            #
            # if not user[1]:
            #     team_user = db.query(ChandaoProjectTeamUser.position). \
            #         filter(ChandaoProjectTeamUser.user_id == self.user_id). \
            #         filter(ChandaoProjectTeamUser.chandao_old_project_id == chandao_project_id). \
            #         filter(ChandaoProjectTeamUser.is_delete == 0). \
            #         first()
            #     if not team_user:
            #         return failure(msg='您不是项目经理，不能设置项目概要！')

            cpsp = ChandaoProjectSummaryPhases()
            cpsp.chandao_project_id = chandao_project_id
            cpsp.phase_name = phase_name
            cpsp.construction_content = construction_content
            cpsp.planned_completion_time = planned_completion_time
            cpsp.creat_time = datetime.datetime.now()
            cpsp.creat_user_id = self.user_id
            cpsp.creat_user_name = self.user_name
            cpsp.contractor_days = contractor_days
            cpsp.budget_days = budget_days
            cpsp.payment_this_period = payment_this_period
            db.add(cpsp)
            db.flush()
            cpsp_id = cpsp.id

            for v in detail_list:
                # v = eval(str(v, encoding="utf-8"))
                v = to_dict(v)
                cpspd = ChandaoProjectSummaryPhasesDetail()
                cpspd.summary_phases_id = cpsp_id
                cpspd.construction_objective = v['construction_objective']
                cpspd.plan_end_time = v.get('plan_end_time')
                db.add(cpspd)

        return ok()

    def project_phases_get_list(self, chandao_project_id, page, size):
        list = []
        with DbSession.create(EnumDb.main) as db:
            count_query = db.query(func.count(ChandaoProjectSummaryPhases.id)). \
                filter(ChandaoProjectSummaryPhases.chandao_project_id == chandao_project_id). \
                first()
            if not count_query:
                return ok(data={'count': 0, 'list': []})

            query = db.query(ChandaoProjectSummaryPhases). \
                filter(ChandaoProjectSummaryPhases.chandao_project_id == chandao_project_id). \
                order_by(ChandaoProjectSummaryPhases.id.desc()). \
                offset(page * size).limit(size)
            for v in query:
                list.append(format_dict(v.get_dict()))

            return ok(data={'count': count_query[0], 'list': list})

    def project_phases_get_first(self, phases_id):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(ChandaoProjectSummaryPhases). \
                filter(ChandaoProjectSummaryPhases.id == phases_id). \
                first()
            if not query:
                return failure(msg='数据不存在，请核对后再试！')

            detail_list = []
            detail_query = db.query(ChandaoProjectSummaryPhasesDetail). \
                filter(ChandaoProjectSummaryPhasesDetail.summary_phases_id == phases_id). \
                order_by(ChandaoProjectSummaryPhasesDetail.id)
            for v in detail_query:
                detail_list.append(format_dict(v.get_dict()))

            data = format_dict(query.get_dict())
            data['detail_list'] = detail_list

            return ok(data=data)

    def project_phases_confirmed(self, phases_id, is_confirmed):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(ChandaoProjectSummaryPhases). \
                filter(ChandaoProjectSummaryPhases.id == phases_id). \
                first()
            if not query:
                return failure(msg='数据不存在，请核对后再试！')

            user = db.query(User).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
            if user.is_admin == 0:
                if user.role != '方案总监' or user.role != '交付总监':
                    return failure(msg='财务经理才能对回款进行确认！')

            query.is_confirmed = is_confirmed
            query.confirmed_time = datetime.datetime.now()
            query.confirmed_user_id = self.user_id
            query.confirmed_user_name = self.user_name

        return ok()

    def project_week_get_user_list(self, is_head_party_a, page, size):
        list = []
        plan_id_list = []
        if is_head_party_a:
            select_query = and_(ChandaoProjectWeekPlanDetailUser.user_id == self.user_id,
                                ChandaoProjectWeekPlanDetailUser.is_head_party_a == 1)
        else:
            select_query = and_(ChandaoProjectWeekPlanDetailUser.user_id == self.user_id,
                                ChandaoProjectWeekPlanDetailUser.is_head_party_a == 0)

        with DbSession.create(EnumDb.main) as db:
            count_query = db.query(func.count(ChandaoProjectWeekPlanDetailUser.id)). \
                filter(select_query). \
                order_by(ChandaoProjectWeekPlanDetailUser.detail_id.desc()). \
                first()
            if not count_query:
                return ok(data={'count': 0, 'list': []})

            query = db.query(ChandaoProjectWeekPlanDetailUser.detail_id). \
                filter(select_query). \
                order_by(ChandaoProjectWeekPlanDetailUser.detail_id.desc()). \
                offset(page * size).limit(size)
            detail_ids = [v[0] for v in query]

            list_query = db.query(ChandaoProjectWeekPlanDetail). \
                filter(ChandaoProjectWeekPlanDetail.id.in_(detail_ids)). \
                order_by(ChandaoProjectWeekPlanDetail.id.desc())
            for v in list_query:
                plan_id_list.append(v.plan_id)
                list.append(format_dict(v.get_dict()))

            plan_dict = {}
            plan_query = db.query(ChandaoProjectWeekPlan).filter(ChandaoProjectWeekPlan.id.in_(plan_id_list))
            for v in plan_query:
                plan_dict[v.id] = format_dict(v.get_dict())

        result_list = []
        for v in list:
            plan_id = v['plan_id']
            if plan_id in plan_dict:
                new_dict = dict(v, **plan_dict[plan_id])
                result_list.append(new_dict)
            else:
                result_list.append(v)

        return ok(data={'count': count_query[0], 'list': result_list})

    def last_first_date_and_last_date(self, n=1):
        """
        获取前n周开始时间和结束时间，参数n：代表前n周
        """
        now = datetime.datetime.now()
        # 上周第一天和最后一天
        before_n_week_start = now - datetime.timedelta(days=now.weekday() + 7 * n, hours=now.hour, minutes=now.minute, seconds=now.second, microseconds=now.microsecond)
        before_n_week_end = before_n_week_start + datetime.timedelta(days=6, hours=23, minutes=59, seconds=59)

        return before_n_week_start, before_n_week_end

    def project_task_week_daily_add(self, report_week_start_time, report_week_end_time, task_working_detail_list,
                                    next_plan, week_review, task_working_next_detail_list, is_edit=0,
                                    task_week_id=0):
        try:
            with DbSession.create(EnumDb.main) as db:
                if is_edit:
                    query = db.query(ProjectTaskWorkingWeekDaily). \
                        filter(ProjectTaskWorkingWeekDaily.id == task_week_id). \
                        filter(ProjectTaskWorkingWeekDaily.reporting_person_user_id == self.user_id). \
                        first()
                    if not query:
                        return failure(msg='不能修改其他人的工时汇报！')

                    evaluations = query.evaluations
                    evaluations_user_id = query.evaluations_user_id
                    evaluations_user_name = query.evaluations_user_name
                    evaluations_time = query.evaluations_time

                    db.query(ProjectTaskWorkingWeekDaily). \
                        filter(ProjectTaskWorkingWeekDaily.id == task_week_id). \
                        filter(ProjectTaskWorkingWeekDaily.reporting_person_user_id == self.user_id). \
                        delete(synchronize_session='fetch')

                    db.query(ProjectTaskWorkingWeekDailyDetail). \
                        filter(ProjectTaskWorkingWeekDailyDetail.project_task_working_daily_id == task_week_id). \
                        filter(ProjectTaskWorkingWeekDailyDetail.reporting_person_user_id == self.user_id). \
                        delete(synchronize_session='fetch')

                    db.query(ProjectTaskWorkingWeekDailyNextDetail). \
                        filter(ProjectTaskWorkingWeekDailyNextDetail.project_task_working_daily_id == task_week_id). \
                        filter(ProjectTaskWorkingWeekDailyNextDetail.reporting_person_user_id == self.user_id). \
                        delete(synchronize_session='fetch')

                else:
                    evaluations = ''
                    evaluations_user_id = 0
                    evaluations_user_name = ''
                    evaluations_time = None

                pt_query = db.query(ProjectTaskWorkingWeekDaily). \
                    filter(ProjectTaskWorkingWeekDaily.report_week_start_time == report_week_start_time). \
                    filter(ProjectTaskWorkingWeekDaily.report_week_end_time == report_week_end_time). \
                    filter(ProjectTaskWorkingWeekDaily.reporting_person_user_id == self.user_id). \
                    first()
                if pt_query:
                    return failure(msg='您已经创建了本周周汇报，如需修正，请对内容进行修改!')

                ptwd = ProjectTaskWorkingWeekDaily()
                ptwd.reporting_person_user_id = self.user_id
                ptwd.reporting_person_user_name = self.user_name
                ptwd.report_week_start_time = report_week_start_time
                ptwd.report_week_end_time = report_week_end_time
                ptwd.creat_time = datetime.datetime.now()
                ptwd.next_plan = next_plan
                ptwd.week_review = week_review

                ptwd.evaluations = evaluations
                ptwd.evaluations_user_id = evaluations_user_id
                ptwd.evaluations_user_name = evaluations_user_name
                ptwd.evaluations_time = evaluations_time

                db.add(ptwd)
                db.flush()
                ptwd_id = ptwd.id

                for v in task_working_detail_list:
                    v = to_dict(v)
                    if not v:
                        continue
                    d = ProjectTaskWorkingWeekDailyDetail()
                    d.project_task_working_daily_id = ptwd_id
                    d.chandao_project_id = v['chandao_project_id']
                    d.project_name = v['chandao_project_name']
                    d.work_content = to_string(v['work_content'])
                    d.creat_time = datetime.datetime.now()
                    d.reporting_person_user_id = self.user_id
                    d.reporting_person_user_name = self.user_name
                    db.add(d)

                for v in task_working_next_detail_list:
                    v = to_dict(v)
                    if not v:
                        continue
                    nd = ProjectTaskWorkingWeekDailyNextDetail()
                    nd.project_task_working_daily_id = ptwd_id
                    nd.chandao_project_id = v['chandao_project_id']
                    nd.project_name = v['chandao_project_name']
                    nd.next_work_content = to_string(v['work_content'])
                    nd.creat_time = datetime.datetime.now()
                    nd.reporting_person_user_id = self.user_id
                    nd.reporting_person_user_name = self.user_name
                    db.add(nd)

            return ok()
        except:
            log(e=traceback.format_exc(), except_position='project_task_week_daily_add', option_value='操作失败！')
            return failure(msg='操作失败！')

    def project_task_week_daily_select(self, report_week_start_time, report_week_end_time):

        now = datetime.datetime.now()

        before_n_week_start, before_n_week_end = self.last_first_date_and_last_date()

        # if before_n_week_start > to_datetime(report_week_start_time):
        #     return failure(msg='汇报时间不能早于上周周一！')
        #
        # if to_datetime(report_week_end_time) >= now + datetime.timedelta(days=6 - now.weekday()):
        #     return failure(msg='汇报时间不能晚于本周周天！')

        with DbSession.create(EnumDb.main) as db:
            pt_query = db.query(ProjectTaskWorkingWeekDaily). \
                filter(ProjectTaskWorkingWeekDaily.report_week_start_time == report_week_start_time). \
                filter(ProjectTaskWorkingWeekDaily.report_week_end_time == report_week_end_time). \
                filter(ProjectTaskWorkingWeekDaily.reporting_person_user_id == self.user_id). \
                first()
            if pt_query:
                return failure(msg='您已经创建了周汇报，如需修正，请对内容进行修改!')

        return ok()

    def project_task_week_daily_get_list(self, page, size, user_name, is_my):
        list = []
        with DbSession.create(EnumDb.main) as db:
            user_query = db.query(User.is_admin).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
            is_admin = user_query[0]

            sale_user_list = [v[0] for v in db.query(User.user_id).filter(User.role.in_(['销售岗位', '财务经理', '行政岗位'])).filter(User.is_delete == 0)]

            if user_name:
                select_user_name = ProjectTaskWorkingWeekDaily.reporting_person_user_name.like('%' + user_name + '%')
            else:
                select_user_name = True

            if is_my == 1:
                select_is_my = ProjectTaskWorkingWeekDaily.reporting_person_user_id == self.user_id
            elif is_my == 2:
                if not is_admin:
                    return failure(msg='您没有权限查看销售类信息！')
                select_is_my = ProjectTaskWorkingWeekDaily.reporting_person_user_id.in_(sale_user_list)
            else:
                if not is_admin:
                    select_is_my = ProjectTaskWorkingWeekDaily.reporting_person_user_id.notin_(sale_user_list)
                else:
                    select_is_my = True

            ptw_query_count = db.query(func.count(ProjectTaskWorkingWeekDaily.id)). \
                filter(select_user_name). \
                filter(select_is_my). \
                order_by(ProjectTaskWorkingWeekDaily.id.desc()). \
                first()
            if not ptw_query_count:
                return ok(data={'count': 0, 'list': list})

            daily_list = []
            ptw_query = db.query(ProjectTaskWorkingWeekDaily). \
                filter(select_user_name). \
                filter(select_is_my). \
                order_by(ProjectTaskWorkingWeekDaily.id.desc()). \
                offset(page * size).limit(size)
            for v in ptw_query:
                daily_list.append(v.id)
                list.append(format_dict(v.get_dict()))

            dict = {}
            query = db.query(ProjectTaskWorkingWeekDailyDetail).filter(ProjectTaskWorkingWeekDailyDetail.project_task_working_daily_id.in_(daily_list))
            for v in query:
                if v.project_task_working_daily_id not in dict:
                    dict[v.project_task_working_daily_id] = [format_dict(v.get_dict())]
                else:
                    dict[v.project_task_working_daily_id].append(format_dict(v.get_dict()))

        for v in list:
            ptw_id = v['id']
            if ptw_id in dict:
                detail_list = dict[ptw_id]
            else:
                detail_list = []

            v['detail_list'] = detail_list

        return ok(data={'count': ptw_query_count[0], 'list': list})

    def project_task_week_daily_get_first(self, task_week_id):
        list = []
        next_detail_list = []
        with DbSession.create(EnumDb.main) as db:
            query = db.query(ProjectTaskWorkingWeekDaily). \
                filter(ProjectTaskWorkingWeekDaily.id == task_week_id).first()
            if not query:
                return failure(msg='数据不存在，请核对后再试！')

            data = format_dict(query.get_dict())

            detail_query = db.query(ProjectTaskWorkingWeekDailyDetail). \
                filter(ProjectTaskWorkingWeekDailyDetail.project_task_working_daily_id == task_week_id). \
                filter(ProjectTaskWorkingWeekDailyDetail.id)
            for v in detail_query:
                list.append(format_dict(v.get_dict()))

            next_detail_query = db.query(ProjectTaskWorkingWeekDailyNextDetail). \
                filter(ProjectTaskWorkingWeekDailyNextDetail.project_task_working_daily_id == task_week_id). \
                filter(ProjectTaskWorkingWeekDailyNextDetail.id)
            for v in next_detail_query:
                next_detail_list.append(format_dict(v.get_dict()))

            data['detail_list'] = list
            data['next_detail_list'] = next_detail_list

            return ok(data=data)

    def project_task_week_daily_select_wroking_all(self, report_week_start_time, report_week_end_time):
        all_dict = {}
        list = []
        with DbSession.create(EnumDb.main) as db:
            query = db.query(ProjectTaskWorkingDailyDetail). \
                filter(ProjectTaskWorkingDailyDetail.report_time >= report_week_start_time). \
                filter(ProjectTaskWorkingDailyDetail.report_time <= report_week_end_time). \
                filter(ProjectTaskWorkingDailyDetail.reporting_person_user_id == self.user_id). \
                order_by(ProjectTaskWorkingDailyDetail.creat_time.desc())

            for v in query:
                work_content = to_list(v.work_content)
                if v.project_name not in all_dict:
                    all_dict[v.project_name] = {'chandao_project_id': v.chandao_project_id, 'work_content_list': work_content}
                else:
                    all_dict[v.project_name]['work_content_list'] += work_content

        for key, value in all_dict.items():
            key_dict = {'project_name': key}
            data = dict(key_dict, **value)
            list.append(data)

        return ok(data=list)

    def project_task_week_daily_evaluations(self, evaluations, task_week_id):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(ProjectTaskWorkingWeekDaily). \
                filter(ProjectTaskWorkingWeekDaily.id == task_week_id). \
                first()
            if not query:
                return failure(msg='数据不存在，请核对后再试！')

            if query.reporting_person_user_id == self.user_id:
                return failure(msg='不能对自己的周报进行评价！')

            query.evaluations = evaluations
            query.evaluations_user_id = self.user_id
            query.evaluations_user_name = self.user_name
            query.evaluations_time = datetime.datetime.now()

        return ok()

    def project_task_week_daily_get_week(self, report_week_start_time, report_week_end_time, work_type):
        with DbSession.create(EnumDb.main) as db:
            user_query = db.query(User.is_admin).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
            is_admin = user_query[0]

            sale_user_list = [v[0] for v in db.query(User.user_id).filter(User.role.in_(['销售岗位', '财务经理', '行政岗位'])).filter(User.is_delete == 0)]

            if work_type == 1:
                select_user = ProjectTaskWorkingWeekDaily.reporting_person_user_id.notin_(sale_user_list)
            elif work_type == 2:
                if not is_admin:
                    return failure(msg='您没有权限查看销售类信息！')
                select_user = ProjectTaskWorkingWeekDaily.reporting_person_user_id.in_(sale_user_list)

            week_query = db.query(ProjectTaskWorkingWeekDaily.id). \
                filter(ProjectTaskWorkingWeekDaily.report_week_start_time == report_week_start_time). \
                filter(ProjectTaskWorkingWeekDaily.report_week_end_time == report_week_end_time). \
                filter(select_user)
            week_ids = [v[0] for v in week_query]

            # 查询本周工作
            now_week_dict = {}
            query = db.query(ProjectTaskWorkingWeekDailyDetail). \
                filter(ProjectTaskWorkingWeekDailyDetail.project_task_working_daily_id.in_(week_ids)). \
                order_by(ProjectTaskWorkingWeekDailyDetail.chandao_project_id)
            for v in query:
                user_data = format_dict({'reporting_person_user_name': v.reporting_person_user_name,
                                         'work_content': v.work_content})
                if v.project_name not in now_week_dict:
                    now_week_dict[v.project_name] = [user_data]
                else:
                    now_week_dict[v.project_name].append(user_data)

            # 查询下周工作
            next_week_dict = {}
            next_query = db.query(ProjectTaskWorkingWeekDailyNextDetail). \
                filter(ProjectTaskWorkingWeekDailyNextDetail.project_task_working_daily_id.in_(week_ids)). \
                order_by(ProjectTaskWorkingWeekDailyNextDetail.chandao_project_id)
            for v in next_query:
                user_data = format_dict({'reporting_person_user_name': v.reporting_person_user_name,
                                         'work_content': v.next_work_content})
                if v.project_name not in next_week_dict:
                    next_week_dict[v.project_name] = [user_data]
                else:
                    next_week_dict[v.project_name].append(user_data)

            return ok(data={'now_week_dict': now_week_dict, 'next_week_dict': next_week_dict})

    def project_task_working_daily_export(self, start_time, end_time):
        res = self.project_task_working(start_time, end_time)
        if not res.is_ok():
            return failure(msg='导出失败！')

        day_list = res.data['day_list']
        week_list = res.data['week_list']
        statistic_user_list = res.data['statistic_user_list']

        heads_0 = to_string(start_time) + '~' + to_string(end_time) + '工时汇报导出'
        file_nama = 'temporary-' + heads_0 + '-' + to_string(to_timestamp(datetime.datetime.now())) + '.xls'

        export_res = self.form_excel_write(day_list, week_list, file_nama, heads_0, statistic_user_list)
        if not export_res:
            return failure(msg='导出失败')

        path = os.path.join('https://wechatserver.qianxingit.com/excel/', file_nama)

        return ok(data=path)

    def project_task_working_daily_get_day(self, report_time, work_type):
        list = []
        with DbSession.create(EnumDb.main) as db:
            user_query = db.query(User.is_admin).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
            is_admin = user_query[0]

            sale_user_list = [v[0] for v in db.query(User.user_id).filter(User.role.in_(['销售岗位', '财务经理', '行政岗位'])).filter(User.is_delete == 0)]

            if work_type == 1:
                select_user = ProjectTaskWorkingDailyDetail.reporting_person_user_id.notin_(sale_user_list)
            elif work_type == 2:
                if not is_admin:
                    return failure(msg='您没有权限查看销售类信息！')
                select_user = ProjectTaskWorkingDailyDetail.reporting_person_user_id.in_(sale_user_list)

            data_dict = {}
            query = db.query(ProjectTaskWorkingDailyDetail). \
                filter(ProjectTaskWorkingDailyDetail.report_time == report_time). \
                filter(select_user). \
                order_by(ProjectTaskWorkingDailyDetail.chandao_project_id)
            for v in query:
                user_data = format_dict({'reporting_person_user_name': v.reporting_person_user_name,
                                         'work_content': v.work_content,
                                         'staff_time': v.staff_time})
                if v.project_name not in data_dict:
                    data_dict[v.project_name] = [user_data]
                else:
                    data_dict[v.project_name].append(user_data)

        return ok(data=data_dict)

    def project_task_working_daily_get_user(self, start_report_time, end_report_time, work_type, query_user_name):
        with DbSession.create(EnumDb.main) as db:
            user_query = db.query(User.is_admin, User.user_name).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
            is_admin = user_query[0]

            sale_user_list = [v[0] for v in db.query(User.user_id).filter(User.role.in_(['销售岗位', '财务经理', '行政岗位'])).filter(User.is_delete == 0)]

            if work_type == 1:
                select_user = and_(ProjectTaskWorkingDailyDetail.reporting_person_user_id.notin_(sale_user_list),
                                   ProjectTaskWorkingDailyDetail.reporting_person_user_name == query_user_name)
            elif work_type == 2:
                if not is_admin:
                    return failure(msg='您没有权限查看销售类信息！')
                select_user = and_(ProjectTaskWorkingDailyDetail.reporting_person_user_id.notin_(sale_user_list),
                                   ProjectTaskWorkingDailyDetail.reporting_person_user_name == query_user_name)

            work_dict = {}
            report_user_name = ''
            query = db.query(ProjectTaskWorkingDailyDetail). \
                filter(ProjectTaskWorkingDailyDetail.report_time >= start_report_time). \
                filter(ProjectTaskWorkingDailyDetail.report_time <= end_report_time). \
                filter(select_user). \
                order_by(ProjectTaskWorkingDailyDetail.chandao_project_id)
            for v in query:
                if not report_user_name:
                    report_user_name = v.reporting_person_user_name
                work_content = to_list(v.work_content)
                staff_time = v.staff_time

                if v.project_name not in work_dict:
                    work_dict[v.project_name] = {'work_content': work_content, 'staff_time': staff_time}
                else:
                    work_dict[v.project_name]['work_content'].extend(work_content)
                    work_dict[v.project_name]['staff_time'] += staff_time

        for key, value in work_dict.items():
            value['staff_time'] = to_string(value['staff_time'])

        data = {
            'report_user_name': report_user_name,
            'work_dict': format_dict(work_dict)
        }

        return ok(data=data)

    def project_task_working(self, start_time, end_time):
        with DbSession.create(EnumDb.main) as db:
            day_list = []
            week_list = []
            # 日报的
            query = db.query(ProjectTaskWorkingDailyDetail). \
                filter(ProjectTaskWorkingDailyDetail.report_time >= start_time). \
                filter(ProjectTaskWorkingDailyDetail.report_time <= end_time). \
                order_by(ProjectTaskWorkingDailyDetail.creat_time)
            for v in query:
                day_list.append(format_dict(v.get_dict()))

            statistic_user_list = []
            statistic_user_query = db.query(ProjectTaskWorkingDaily.reporting_person_user_name, func.count(ProjectTaskWorkingDaily.id)). \
                filter(ProjectTaskWorkingDaily.report_time >= start_time). \
                filter(ProjectTaskWorkingDaily.report_time <= end_time). \
                group_by(ProjectTaskWorkingDaily.reporting_person_user_name). \
                all()
            for v in statistic_user_query:
                statistic_user_list.append({'user_name': v[0], 'count': v[1]})

            week_query = db.query(ProjectTaskWorkingWeekDaily.report_week_start_time,
                                  ProjectTaskWorkingWeekDaily.report_week_end_time,
                                  ProjectTaskWorkingWeekDaily.reporting_person_user_name,
                                  ProjectTaskWorkingWeekDailyDetail.work_content,
                                  ProjectTaskWorkingWeekDaily.creat_time). \
                filter(ProjectTaskWorkingWeekDailyDetail.project_task_working_daily_id == ProjectTaskWorkingWeekDaily.id). \
                filter(ProjectTaskWorkingWeekDaily.report_week_start_time >= start_time). \
                filter(ProjectTaskWorkingWeekDaily.report_week_end_time <= end_time). \
                order_by(ProjectTaskWorkingWeekDaily.creat_time)
            for v in week_query:
                week_list.append(format_dict({
                    'report_week_start_time': v[0],
                    'report_week_end_time': v[1],
                    'reporting_person_user_name': v[2],
                    'work_content': v[3],
                    'creat_time': v[4]
                }))

            # 做数据
            workdays = pd.DataFrame(chinese_calendar.get_workdays(to_datetime(start_time), to_datetime(end_time)))
            task_working_max_count = workdays.rename(columns={0: '日期'}).size
            for v in statistic_user_list:
                if v['user_name'] == '罗钰东' and v['count'] < task_working_max_count:
                    v['count'] = task_working_max_count
                    break

        return ok(data={'day_list': day_list, 'week_list': week_list, 'statistic_user_list': statistic_user_list, 'task_working_max_count': task_working_max_count})

    def form_excel_write(self, day_list, week_list, file_nama, heads_0, statistic_user_list):
        heads = ['汇报时间',
                 '汇报类型',
                 '汇报人',
                 '工作内容',
                 '创建时间',
                 '附件'
                 ]

        data_list = []

        for v in day_list:
            v_lsit = []
            v_lsit.append(v['report_time'])
            v_lsit.append('日报')
            v_lsit.append(v['reporting_person_user_name'])
            v_lsit.append(v['work_content'])
            v_lsit.append(v['creat_time'])
            v_lsit.append(v['file'])

            data_list.append(v_lsit)

        for v in week_list:
            v_lsit = []
            v_lsit.append(v['report_week_start_time'] + '~' + v['report_week_end_time'])
            v_lsit.append('周报')
            v_lsit.append(v['reporting_person_user_name'])
            v_lsit.append(v['work_content'])
            v_lsit.append(v['creat_time'])
            v_lsit.append('')

            data_list.append(v_lsit)

        data_list.insert(0, heads)

        sheet_2_heads = ["用户名", "日报次数"]
        sheet_2 = []
        for v in statistic_user_list:
            s2_list = []
            s2_list.append(v['user_name'])
            s2_list.append(v['count'])

            sheet_2.append(s2_list)

        sheet_2.insert(0, sheet_2_heads)

        if statistic_user_list:
            res = self.write_to_excel(rows=data_list, file_name_1=file_nama, heads_0=heads_0, row_len=5, sheet_2=sheet_2)
        else:
            res = self.write_to_excel(rows=data_list, file_name_1=file_nama, heads_0=heads_0, row_len=5)
        if not res:
            return failure(msg='导出失败')

        size = get_FileSize(EXCEL_CACHE, file_nama)
        with DbSession.create(EnumDb.main) as db:
            f = File()
            f.file_name = file_nama
            f.old_name = file_nama
            f.time = datetime.datetime.now()
            f.is_temporary = 1
            f.host = SERVER_HOST
            f.path = EXCEL_CACHE
            f.size = size
            db.add(f)

        return ok(data=file_nama)

    def write_to_excel(self,
                       rows,
                       file_name_1,
                       head_style_str=r'pattern: pattern solid, fore_colour pale_blue;font:height 240; align: vert centre, horiz center;',
                       heads_0='',
                       row_len=0,
                       sheet_2=[]):
        '''
        写入excel
        :param user_id:
        :type user_id:
        :param directory:
        :type directory:
        :param rows:
        :type rows:
        :param head_style_str:
        :type head_style_str:
        :return:
        :rtype:
        '''
        header_style = easyxf('pattern: pattern solid, fore_colour pale_blue;font:height 240; align: vert centre, horiz center;')
        heads_0_style = style_center(height=400)
        col_style = style_center(warp=False)

        try:
            f = xlwt.Workbook(encoding='utf-8')  # 创建工作簿

            if sheet_2:
                sheet_2_row_len = 6
                work_sheet_2 = f.add_sheet("日报数据统计", cell_overwrite_ok=True)  # 创建sheet

                work_sheet_2.write_merge(0, 0, 0, sheet_2_row_len, heads_0.replace('工时汇报导出', '日报统计'), heads_0_style)

                sheet_2_row_index = 1
                error_no = ''
                for row in sheet_2:
                    col_end = get_len(row)

                    if sheet_2_row_index == 1:
                        for col in range(0, col_end):
                            if header_style:
                                work_sheet_2.write(sheet_2_row_index, col, row[col], header_style)
                            else:
                                work_sheet_2.write(sheet_2_row_index, col, row[col], header_style)

                            work_sheet_2.col(col).width = get_len(str(row[col])) * 1500
                    else:
                        for col in range(0, col_end):
                            work_sheet_2.write(sheet_2_row_index, col, to_string(row[col]), col_style)
                            work_sheet_2.col(col).width = get_len(str(row[col])) * 1500

                    error_no = row[0]
                    sheet_2_row_index += 1

            work_sheet = f.add_sheet('日报详情', cell_overwrite_ok=True)  # 创建sheet

            if heads_0:
                row_index = 1
                work_sheet.write_merge(0, 0, 0, row_len, heads_0, heads_0_style)  # 合并第一行，第0列到第14列的单元格 居中
            else:
                row_index = 0

            error_no = ''
            for row in rows:
                col_end = get_len(row)
                if heads_0:
                    if row_index == 1:
                        for col in range(0, col_end):
                            if header_style:
                                work_sheet.write(row_index, col, row[col], header_style)
                            else:
                                work_sheet.write(row_index, col, row[col], header_style)

                            if col == 3:
                                work_sheet.col(col).width = get_len(str(row[col])) * 8000
                            else:
                                work_sheet.col(col).width = get_len(str(row[col])) * 1500

                    else:
                        for col in range(0, col_end):
                            work_sheet.write(row_index, col, to_string(row[col]), col_style)

                    error_no = row[0]
                    row_index += 1

                else:
                    if row_index == 0:
                        for col in range(0, col_end):
                            if header_style:
                                work_sheet.write(row_index, col, row[col], header_style)
                            else:
                                work_sheet.write(row_index, col, row[col], header_style)

                            if col == 3:
                                work_sheet.col(col).width = get_len(str(row[col])) * 8000
                            else:
                                work_sheet.col(col).width = get_len(str(row[col])) * 1500

                    else:
                        for col in range(0, col_end):
                            work_sheet.write(row_index, col, to_string(row[col]), col_style)

                    error_no = row[0]
                    row_index += 1

            file_name = os.path.join(EXCEL_CACHE, file_name_1)
            f.save(file_name)

            return file_name
        except Exception as e:
            print(error_no)
            log(traceback.format_exc(), ' write_to_excel', '创建excel失败')
            return ''

    def style_center(self, warp=True, bold=False, background_color=None, font_color=None, height=0x00C8, border_color=None, borders=True):
        '''
        中间对齐
        :param warp:是否换行
        :type warp: bool
        :param bold:是否加粗:
        :type bold: bool
        :param background_color:   背景色
        :type background_color: int
        :param font_color: 字体颜色
        :type font_color:  int
        :return:  样式
        :rtype: XFStyle
        '''
        style = xlwt.XFStyle()  # 初始化样式
        alignment = xlwt.Alignment()
        alignment.horz = xlwt.Alignment.HORZ_CENTER  # 中间对齐
        alignment.vert = xlwt.Alignment.VERT_CENTER  # 水平对齐
        self._common_style(alignment, background_color, bold, font_color, style, warp, height, border_color, borders)
        return style

    def _common_style(self, alignment, background_color, bold, font_color, style, warp, height, border_color, borders=True):
        if alignment is not None:
            if warp:
                alignment.wrap = xlwt.Alignment.WRAP_AT_RIGHT  # 自动换行
            style.alignment = alignment

        if background_color and isinstance(background_color, int):
            pattern = xlwt.Pattern()
            pattern.pattern_fore_colour = background_color
            pattern.pattern = xlwt.Pattern.SOLID_PATTERN
            style.pattern = pattern

        font = xlwt.Font()
        if font_color and isinstance(font_color, int):
            font.colour_index = font_color

        if bold:
            font.bold = True
        font.height = height
        font.name = u'宋体'

        style.font = font

        if borders:
            borders = xlwt.Borders()
            borders.left = 1
            borders.right = 1
            borders.top = 1
            borders.bottom = 1
            # borders.bottom_colour = border_color
            style.borders = borders


if __name__ == '__main__':
    a = WorkingDailyManage({'user_id': 1, 'user_name': 'admin'})
    res = a.project_task_working(start_time='2024-12-01', end_time='2024-12-31')
