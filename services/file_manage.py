#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: file_manage.py
@time: 2020/10/20 11:56
@desc:
'''
import base64
import os
import traceback
import uuid
from datetime import datetime

from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.log_controller import log
from framework.msg import failure, ok
from framework.utilities import to_decimal, to_string, to_timestamp, to_list
from models.file.file import File
from setting import FILE_CACHE, SERVER_HOST


class FileManage():
    def __init__(self):
        pass

    # @trycatch(optional_exception_return_value='保存图片失败')
    def upload_one_file_to_dir(self, ext, body, file_name):
        '''
        将单个文件上传到指定目录下面
        :param file_request:request
        :type file_request:
        :param dir:存放文件的目录
        :type dir:str
        :return:
        :rtype:
        '''

        try:
            size = to_decimal(len(body) / (1024 * 1024))

            if size > 100:  # Mb为单位
                return failure(msg='单个文件不允许超过100M')

            lens = len(to_string(body))
            lenx = lens - (lens % 4 if lens % 4 else 4)
            body = base64.decodestring(body[:lenx])

            # 存放文件名称
            filename = 'temporary-' + str(uuid.uuid1()).replace('-', '') + '-' + to_string(to_timestamp(datetime.now())) + ext

            full_path = os.path.join(FILE_CACHE, filename)  # 文件存放路径
            #
            file = open(full_path, 'wb')
            file.write(body)
            file.close()

            with DbSession.create(EnumDb.main) as db:
                f = File()
                f.file_name = filename
                f.old_name = file_name
                f.time = datetime.now()
                f.is_temporary = 1
                f.host = SERVER_HOST
                f.path = FILE_CACHE
                f.size = size
                db.add(f)
                db.flush()
                file_id = f.id

                data = {'file_name': filename,
                        'old_name': file_name,
                        'type': 'new',
                        'size': to_string(size),
                        'file_id': file_id,
                        'imgs_path': FILE_CACHE}

            print(data)
            return ok(data=data)
        except:
            log(traceback.format_exc())

    # 保存图片
    # @trycatch(optional_exception_return_value='保存图片失败')
    # @coroutine
    def file_save(self, file_list):
        '''
        :param file_list:
        规则
        file_list = [{file_name:xx, old_name:xx, type:new, 'size':size}, {...}, ...]
        type参数规则：
        type='new' 新增的图片
        type='del' 需要删除的图片
        type=''原封不动的图片
        :return:
        '''
        try:
            if not file_list:
                return ok(data={'file_list': []})

            if isinstance(file_list, str):
                file_list = to_list(file_list)

            last_dic = []
            del_file_name_list = []
            add_file_name_list = []
            for v in file_list:
                type = v['type']
                if type == 'new':
                    add_file_name_list.append(v['file_id'])
                elif type == 'del':
                    del_file_name_list.append(v['file_id'])
                else:
                    last_dic.append(v)

            with DbSession.create(EnumDb.main) as db:
                if add_file_name_list:
                    query = db.query(File).filter(File.id.in_(add_file_name_list))
                    for v in query:
                        file_name = v.file_name

                        src = os.path.join(FILE_CACHE, file_name)
                        dst = os.path.join(FILE_CACHE, file_name.replace('temporary-', ''))
                        os.rename(src, dst)

                        db.query(File).filter(File.id == v.id). \
                            update({File.is_temporary: 0, File.file_name: file_name.replace('temporary-', '')}, synchronize_session='fetch')

                        dic = {
                            'file_name': v.file_name,
                            'old_name': v.old_name,
                            'type': '',
                            'size': to_string(v.size),
                            'file_id': v.id,
                            'imgs_path': v.path
                        }
                        last_dic.append(dic)

            return ok(data={'file_list': last_dic})
        except:
            log(traceback.format_exc(), except_position='file_save')
            return failure(msg='保存图片失败')

    def delete_file(self, file_list, db=None):
        if not file_list:
            return ok(data=[])

        if isinstance(file_list, str):
            file_list = to_list(file_list)

        del_file_name_list = []
        for v in file_list:
            del_file_name_list.append(v['file_name'])

        if not del_file_name_list:
            return ok()

        if del_file_name_list:
            self.__delete(del_file_name_list, db)

        return ok()

    def __delete(self, del_file_name_list, db=None):
        del_file_ids = []

        if db:
            del_query = db.query(File).filter(File.id.in_(del_file_name_list))
            for v in del_query:
                path = os.path.join(v.path, v.file_name)
                os.remove(path)
                del_file_ids.append(v.id)

            db.query(File).filter(File.id.in_(del_file_ids)).delete(synchronize_session='fetch')

            return True
        else:
            with DbSession.create(EnumDb.main) as db:
                del_query = db.query(File).filter(File.id.in_(del_file_name_list))
                for v in del_query:
                    path = os.path.join(v.path, v.file_name)
                    os.remove(path)
                    del_file_ids.append(v.id)

                db.query(File).filter(File.id.in_(del_file_ids)).delete(synchronize_session='fetch')

                return True


if __name__ == '__main__':
    pass
