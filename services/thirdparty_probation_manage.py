#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: thirdparty_probation_manage.py.py
@time: 2020/10/22 17:37
@desc:
'''
import hashlib
import traceback

from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.log_controller import log
from framework.thirdparty_msg import thirdparty_ok, thirdparty_failure
from framework.utilities import to_string, current_timestamp, gen_token
from models.thirdparty_out import ThirdpartyOut


class ThirdpartyProbationManage():
    def __init__(self, appkey=None, sing=None, timestamp=None, token=None):
        self.appkey = appkey
        self.sing = sing
        self.timestamp = timestamp
        self.token = token

    # 验证鉴权
    def thirdparty_verify(self):
        # 查询是否有权限
        try:
            status, code, thirdparty_business_id = self.__thirdparty_authentication()
            if not status:
                log(e=to_string(code), except_position='thirdparty_verify')
                return thirdparty_failure(code=code)

            # 查询企业是否信息
            with DbSession.create(EnumDb.main) as db:
                business_msg = db.query(ThirdpartyOut). \
                    filter(ThirdpartyOut.id == thirdparty_business_id). \
                    filter(ThirdpartyOut.is_delete == 0). \
                    first()
                if not business_msg:
                    log(e='11000', except_position='thirdparty_verify')
                    return thirdparty_failure(code=11000)

                business_name = business_msg.business_name

                expire = current_timestamp() + 3600 * 2
                token = gen_token(business_name, expire)

                business_msg.expire = expire
                business_msg.token = token

            return thirdparty_ok(data={'token': token, 'expire_time': 7200})
        except:
            log(traceback.format_exc(), except_position='thirdparty_verify')
            return thirdparty_failure(code=11111)

    # 接口鉴权
    def __thirdparty_authentication(self):
        # 查询 appkey 是否可用
        with DbSession.create(EnumDb.main) as db:
            query = db.query(ThirdpartyOut).filter(ThirdpartyOut.is_delete == 0).filter(ThirdpartyOut.appkey == self.appkey).first()
            if not query:
                log(e='10002', except_position='__thirdparty_authentication')
                return False, 10002, 0
            thirdparty_business_id = query.id

            appsecret = query.appsecret

        # MD5 加密
        res = self.__md5_sign(appsecret)
        if not res:
            log(e='10003', except_position='__thirdparty_authentication')
            return False, 10003, 0
        else:
            return True, 10000, thirdparty_business_id

    # 验证加密
    def __md5_sign(self, appsecret):
        # 加密规则 appsecret + appkey + timestamp
        string = appsecret + self.appkey + to_string(self.timestamp)

        m = hashlib.md5(string.encode("utf8"))
        res = m.hexdigest()

        if res == self.sing:
            return True
        else:
            return False
