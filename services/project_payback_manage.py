# coding: utf-8
# author: lyd
# mobile: 15288343493
# email : 339789089@qq.com
# file  : project_payback_manage.py
# time  : 2024-07-29 10:56
# desc  :
import datetime

from sqlalchemy import func

from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.msg import ok, failure
from framework.utilities import to_decimal, format_dict
from models.chandao.chandao_project_payback import ChandaoProjectPayback
from models.users.user import User


class ProjectPaybackManage():

    def __init__(self, user, token=None):
        self.token = token
        self.user = user
        self.user_id = user['user_id']
        self.user_name = user['user_name']

    def project_payback_add(self, project_id, chandao_project_id, payback_amount, payback_point, payback_time, desc):
        with DbSession.create(EnumDb.main) as db:
            cpa = ChandaoProjectPayback()
            cpa.project_id = project_id
            cpa.chandao_project_id = chandao_project_id
            cpa.payback_amount = to_decimal(payback_amount)
            cpa.payback_point = payback_point
            cpa.payback_time = payback_time
            cpa.is_confirmed = 0
            cpa.creat_user_id = self.user_id
            cpa.creat_user_name = self.user_name
            cpa.desc = desc
            db.add(cpa)

        return ok()

    def project_payback_get_list(self, chandao_project_id):
        list = []

        with DbSession.create(EnumDb.main) as db:
            count_query = db.query(func.count(ChandaoProjectPayback.id)).filter(ChandaoProjectPayback.chandao_project_id == chandao_project_id).first()[0]
            if not count_query:
                return ok(data={'count': 0, 'list': list})

            query = db.query(ChandaoProjectPayback). \
                filter(ChandaoProjectPayback.chandao_project_id == chandao_project_id). \
                order_by(ChandaoProjectPayback.creat_time.desc())
            for v in query:
                list.append(format_dict(v.get_dict()))

            return ok(data={'count': count_query, 'list': list})

    def project_payback_get_first(self, payback_id):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(ChandaoProjectPayback).filter(ChandaoProjectPayback.id == payback_id).first()
            if not query:
                return ok(msg='数据不存在')

            data = format_dict(query.get_dict())

        return ok(data=data)

    def project_payback_update(self, payback_id, payback_amount, payback_point, payback_time, desc):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(ChandaoProjectPayback).filter(ChandaoProjectPayback.id == payback_id).first()
            if not query:
                return ok(msg='数据不存在')

            if query.is_confirmed == 1:
                return failure(msg='已确认不可修改')

            query.payback_amount = to_decimal(payback_amount)
            query.payback_point = payback_point
            query.payback_time = payback_time
            query.desc = desc
            query.edit_user_id = self.user_id
            query.edit_user_name = self.user_name
            query.edit_time = datetime.datetime.now()
            db.commit()

        return ok()

    def project_payback_confirmed(self, payback_id, is_confirmed):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(ChandaoProjectPayback).filter(ChandaoProjectPayback.id == payback_id).first()
            if not query:
                return ok(msg='数据不存在')

            user = db.query(User).filter(User.user_id == self.user_id).filter(User.is_delete == 0).first()
            if user.role != '财务经理':
                return failure(msg='财务经理才能对回款进行确认！')

            query.is_confirmed = is_confirmed
            query.confirmed_time = datetime.datetime.now()
            query.confirmed_user_id = self.user_id
            db.commit()

        return ok()
