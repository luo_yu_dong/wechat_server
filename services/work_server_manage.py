#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: work_server_manage.py
@time: 2020/10/22 17:31
@desc:
'''
import traceback
from datetime import datetime

from sqlalchemy import func, desc, and_, or_

from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.log_controller import log
from framework.msg import ok, failure
from framework.utilities import format_dict, to_string, to_int_date, get_quanpin, to_int, to_decimal, to_timestamp, get_FileSize
from models.customer_form.customer_submmint_form import CustomerSubmmintForm
from models.customer_form.customer_submmint_form_desc import CustomerSubmmintFormDesc
from models.exception_log.remind_logs import RemindLogs
from models.file.file import File
from models.users.user import User
from models.users.wechat_user import WechatUser
from services.excel_export_base import write_to_excel
from services.file_manage import FileManage
from services.lingdang_crm_manage import LingdangCRMManage
from services.server_base import ServerBase
from services.wechat_wiki_manage import WeChatWikiManage
from setting import SERVER_HOST, EXCEL_CACHE


class WorkServerManage():
    def __init__(self, user):
        self.user = user
        self.user_id = user['user_id']

    def work_server_user_list(self, select_name, page, size):
        if select_name:
            cons_select_name = WechatUser.nike_name.like('%' + select_name + '%')
        else:
            cons_select_name = True

        list = []
        with DbSession.create(EnumDb.main) as db:
            count = db.query(func.count(WechatUser.id)). \
                filter(WechatUser.is_customer_service == 1). \
                filter(WechatUser.type == 2). \
                filter(cons_select_name). \
                first()[0]

            if not count:
                return ok(data={'count': 0, 'list': []})

            query = db.query(WechatUser). \
                filter(WechatUser.is_customer_service == 1). \
                filter(WechatUser.type == 2). \
                filter(cons_select_name). \
                order_by(desc(WechatUser.creat_time)). \
                offset(page * size).limit(size)
            for v in query:
                list.append({
                    'nike_name': v.nike_name,
                    'user_id': v.user_id,
                    'is_attention_wiki': v.is_attention_wiki,
                    'logo': v.logo,
                    'id': v.id
                })

        return ok(data={'count': count, 'list': list})

    def work_server_user_bind(self, user_id, wechat_user_id):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(WechatUser). \
                filter(WechatUser.id == wechat_user_id). \
                filter(WechatUser.is_customer_service == 1). \
                filter(WechatUser.type == 2). \
                first()
            if not query:
                return failure(msg='数据不存在')

            # 把其他这个user_id绑定的值都置为0
            db.query(WechatUser).filter(WechatUser.user_id == user_id).update({WechatUser.user_id: 0}, synchronize_session='fetch')

            query.user_id = user_id
            unionid = query.union_id

            db.query(WechatUser).filter(WechatUser.union_id == unionid).update({WechatUser.user_id: user_id}, synchronize_session='fetch')

        return ok()

    def form_get_list(self, user_id, is_admin, page, size, select_type, select_business_name, select_is_reply, select_score, select_user_id, select_comment,
                      time_type, start_time, end_time, is_server_time):
        # -1,全部，1-创建时间，2-处理时间
        if time_type == -1:
            cons_time_type = True
        elif time_type == 1:
            cons_time_type = and_(CustomerSubmmintForm.creat_int_date >= to_int_date(start_time),
                                  CustomerSubmmintForm.creat_int_date <= to_int_date(end_time)
                                  )
        else:
            cons_time_type = and_(CustomerSubmmintForm.replyl_time >= to_string(start_time)[:10] + ' 00:00:00',
                                  CustomerSubmmintForm.replyl_time <= to_string(end_time)[:10] + ' 23:59:59'
                                  )

        if is_server_time != -1:
            cons_is_server_time = CustomerSubmmintForm.is_server_time == is_server_time
        else:
            cons_is_server_time = True

        if is_admin != 1:
            cons_is_admin = CustomerSubmmintForm.reply_user_id == user_id
        else:
            cons_is_admin = True

        if select_comment:
            cons_select_comment = or_(CustomerSubmmintForm.title.like('%' + select_comment + '%'),
                                      CustomerSubmmintForm.comment.like('%' + select_comment + '%'),
                                      CustomerSubmmintForm.reply_result.like('%' + select_comment + '%'),
                                      CustomerSubmmintForm.reply_user_name.like('%' + select_comment + '%'),
                                      )
        else:
            cons_select_comment = True

        if select_user_id != -1:
            cons_select_user_id = CustomerSubmmintForm.reply_user_id == select_user_id
        else:
            cons_select_user_id = True

        if select_type != -1:
            cons_select_type = CustomerSubmmintForm.form_type == select_type
        else:
            cons_select_type = True

        if select_business_name:
            cons_select_business_name = CustomerSubmmintForm.business_name.like('%' + select_business_name + '%')
        else:
            cons_select_business_name = True

        if select_is_reply != -1:
            cons_select_is_reply = CustomerSubmmintForm.is_reply == select_is_reply
        else:
            cons_select_is_reply = True

        if select_score != -1:
            cons_select_score = CustomerSubmmintForm.score == to_decimal(select_score) * 2
        else:
            cons_select_score = True

        cons = and_(cons_is_admin, cons_select_type, cons_select_business_name, cons_select_is_reply, cons_select_score, cons_select_user_id, cons_select_comment,
                    cons_time_type, cons_is_server_time)

        list = []
        with DbSession.create(EnumDb.main) as db:
            count = db.query(func.count(CustomerSubmmintForm.id)). \
                filter(cons). \
                filter(CustomerSubmmintForm.is_delete == 0). \
                order_by(desc(CustomerSubmmintForm.creat_time)). \
                first()[0]
            if not count:
                return ok(data={'count': 0, 'list': []})

            query = db.query(CustomerSubmmintForm). \
                filter(cons). \
                filter(CustomerSubmmintForm.is_delete == 0). \
                order_by(desc(CustomerSubmmintForm.creat_time)). \
                offset(page * size).limit(size)
            for v in query:
                list.append(format_dict(v.get_dict()))

        return ok(data={'count': count, 'list': list})

    def form_export(self, user_id, is_admin, page, size, select_type, select_business_name, select_is_reply, select_score, select_user_id, select_comment,
                    time_type, start_time, end_time, is_server_time):

        res = self.form_get_list(user_id, is_admin, page, size, select_type, select_business_name, select_is_reply, select_score, select_user_id, select_comment,
                                 time_type, start_time, end_time, is_server_time)
        if not res.is_ok():
            return failure(msg='导出失败！')

        list = res.data['list']

        file_nama = 'temporary-服务单数据导出-' + to_string(to_timestamp(datetime.now())) + '.xls'
        if time_type == -1:
            heads_0 = '服务单数据导出'
        elif time_type == 1:
            heads_0 = '创建时间' + to_string(start_time)[:10] + '~' + to_string(end_time)[:10] + '服务单数据导出'
        else:
            heads_0 = '处理时间' + to_string(start_time)[:10] + '~' + to_string(end_time)[:10] + '服务单数据导出'

        export_res = self.form_excel_write(list, file_nama, heads_0)
        if not export_res:
            return failure(msg='导出失败')

        return ok(data=export_res.data)

    def form_excel_write(self, list, file_nama, heads_0):
        heads = ['服务单编号',
                 '表单类型',
                 '是否在服务期',
                 '企业名称',
                 '联系人',
                 '联系电话',
                 '标题',
                 '描述',
                 '处理结果',
                 '创建时间',
                 '是否已处理',
                 '处理时间',
                 '处理人',
                 '处理时长（分）',
                 '评分'
                 ]

        data_list = []

        for v in list:
            v_lsit = []
            v_lsit.append(v['no'])

            if v['form_type'] == 1:
                form_type_name = '需求'
            elif v['form_type'] == 2:
                form_type_name = '投诉'
            else:
                form_type_name = '问题'
            v_lsit.append(form_type_name)

            v_lsit.append('是' if v['is_server_time'] == 1 else '否')
            v_lsit.append(v['business_name'])
            v_lsit.append(v['contacts'])
            v_lsit.append(v['contacts_phone'])
            v_lsit.append(v['title'])
            v_lsit.append(v['comment'])
            v_lsit.append(v['reply_result'])
            v_lsit.append(v['creat_time'])
            v_lsit.append('已处理' if v['is_reply'] == 1 else '未处理')
            v_lsit.append(v['replyl_time'])
            v_lsit.append(v['reply_user_name'])
            v_lsit.append(to_string(v['reply_time_second']))
            v_lsit.append(to_string(to_int(v['score'] / 2)) if v['score'] else '未评分')

            data_list.append(v_lsit)

        data_list.insert(0, heads)

        res = write_to_excel(rows=data_list, file_name_1=file_nama, heads_0=heads_0, row_len=14)
        if not res:
            return failure(msg='导出失败')

        size = get_FileSize(EXCEL_CACHE, file_nama)
        with DbSession.create(EnumDb.main) as db:
            f = File()
            f.file_name = file_nama
            f.old_name = file_nama
            f.time = datetime.now()
            f.is_temporary = 1
            f.host = SERVER_HOST
            f.path = EXCEL_CACHE
            f.size = size
            db.add(f)

        return ok(data=file_nama)

    def form_allocation_bach(self, item_id_list, reply_user_id, reply_user_name):
        try:
            with DbSession.create(EnumDb.main) as db:
                wechat_user_query = db.query(WechatUser.id). \
                    filter(WechatUser.user_id == reply_user_id). \
                    first()
                if not wechat_user_query:
                    wechat_user_id = 0
                else:
                    wechat_user_id = wechat_user_query[0]

                form_list = []
                form_query = db.query(CustomerSubmmintForm.id, CustomerSubmmintForm.title, CustomerSubmmintForm.no). \
                    filter(CustomerSubmmintForm.id.in_(item_id_list))
                for v in form_query:
                    form_list.append({'item_id': v[0], 'form_no': v[2], 'form_title': v[1]})

                db.query(CustomerSubmmintForm). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    filter(CustomerSubmmintForm.id.in_(item_id_list)). \
                    update({CustomerSubmmintForm.reply_user_id: reply_user_id,
                            CustomerSubmmintForm.reply_user_name: reply_user_name}, synchronize_session='fetch')

            # 记录提醒日志
            self.add_remind_log(form_list=form_list, to_user_id=reply_user_id)

            # 给负责人发送消息
            if wechat_user_id:
                wcwm = WeChatWikiManage()
                wcwm.send_assigning_job_wechat_message(wechat_user_id=wechat_user_id,
                                                       item_list=form_list,
                                                       assigning_user_name=self.user['user_name'],
                                                       user_id=self.user_id)

            return ok()
        except Exception as e:
            log(e=traceback.format_exc(), except_position='form_allocation_bach', option_value='分配任务失败!')
            return failure(msg='分配任务失败!')

    def form_get_first(self, item_id):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(CustomerSubmmintForm). \
                filter(CustomerSubmmintForm.id == item_id). \
                filter(CustomerSubmmintForm.is_delete == 0). \
                first()
            if not query:
                return failure(msg='数据不存在，请核对后重试！')

            result = format_dict(query.get_dict())

            desc_list = []
            desc_query = db.query(CustomerSubmmintFormDesc). \
                filter(CustomerSubmmintFormDesc.form_id == item_id). \
                order_by(desc(CustomerSubmmintFormDesc.creat_time))
            for v in desc_query:
                desc_list.append(format_dict(v.get_dict()))

            result['desc_list'] = desc_list

            # 置为已读
            db.query(RemindLogs). \
                filter(RemindLogs.type == 0). \
                filter(RemindLogs.item_id == item_id). \
                filter(RemindLogs.user_id == self.user_id). \
                update({RemindLogs.is_read: 1,
                        RemindLogs.read_time: datetime.now()}, synchronize_session='fetch')

        return ok(data=result)

    # @coroutine
    def form_reply(self, item_id, reply_result, reply_imgs):
        if reply_imgs:
            f = FileManage()
            res = f.file_save(reply_imgs)
            if res.is_ok():
                if res.data['file_list']:
                    data_file = res.data['file_list']
                else:
                    data_file = ''
            else:
                data_file = ''
                return failure(msg='操作失败')
        else:
            data_file = ''

        now = datetime.now()

        with DbSession.create(EnumDb.main) as db:
            query = db.query(CustomerSubmmintForm). \
                filter(CustomerSubmmintForm.id == item_id). \
                filter(CustomerSubmmintForm.is_delete == 0). \
                first()
            if not query:
                return failure(msg='数据不存在，请刷新后重试')

            query_dict = query.get_dict()

            if query_dict.get('form_type') == 1:
                form_type_name = '需求'
            elif query_dict.get('form_type') == 2:
                form_type_name = '投诉'
            else:
                form_type_name = '问题'

            reply_user_id = query.reply_user_id
            if not reply_user_id:
                query.reply_user_id = self.user_id
                query.reply_user_name = self.user['user_name']
                reply_user_id = self.user_id

            crm_user_query = db.query(User.crm_user).filter(User.user_id == reply_user_id).first()
            if not crm_user_query:
                crm_user_name = ''
            else:
                crm_user_name = crm_user_query[0]

            query.is_reply = 1
            query.reply_result = reply_result
            query.reply_imgs = to_string(data_file)
            query.replyl_time = now
            query.year = now.year
            query.month = now.month
            query.day = now.day
            query.int_date = to_int_date(now)

            reply_time_second = (now - query.creat_time).total_seconds() / 60
            query.reply_time_second = reply_time_second

            customer_wechat_user_id = query.wechat_user_id
            form_no = query.no

        sb = ServerBase(user=self.user)
        sb.log(object_ids=[item_id], model=CustomerSubmmintForm.__tablename__, operation='回复表单问题')

        # 给客户发消息
        wcwm = WeChatWikiManage()
        wcwm.send_complete_form_to_customer_wechat_message(wechat_user_ids=[customer_wechat_user_id],
                                                           item_id=item_id,
                                                           form_no=form_no,
                                                           reply_result=reply_result)

        # 回复完了算是完成了，讲数据传到crm
        if crm_user_name:
            ldCRM = LingdangCRMManage()
            ldCRM.add_crm_hepldesk(
                business_name=query_dict.get('business_name'),
                form_contacts=query_dict.get('contacts'),
                user_name=crm_user_name,
                crm_username=get_quanpin(crm_user_name),
                ticket_no=query_dict.get('no'),
                ticket_title=query_dict.get('title'),
                solution=reply_result,
                realtime=to_string(now)[:19],
                createdtime=to_string(query_dict.get('creat_time'))[:19],
                description=query_dict.get('comment'),
                form_id=item_id,
                ticketcategories=form_type_name,
                form_contacts_phone=query_dict.get('contacts_phone')
            )

        return ok()

    def add_remind_log(self, form_list, to_user_id):
        now = datetime.now()

        add_list = []
        for v in form_list:
            rl = RemindLogs()
            rl.type = 0
            rl.is_read = 0
            rl.item_id = v['item_id']
            rl.user_id = to_user_id
            rl.opertaion_user_id = self.user_id
            rl.creat_time = now
            add_list.append(rl)

        with DbSession.create(EnumDb.main) as db:
            db.bulk_save_objects(add_list)

        return ok()

    def clc_reply_time(self):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(CustomerSubmmintForm).filter(CustomerSubmmintForm.is_reply == 1).filter(CustomerSubmmintForm.is_delete == 0)
            for v in query:
                time = (v.replyl_time - v.creat_time).total_seconds() / 60
                v.reply_time_second = time

        return ok()

    def form_reply_desc(self, item_id, reply_desc, come_from):
        with DbSession.create(EnumDb.main) as db:
            csfd = CustomerSubmmintFormDesc()
            csfd.form_id = item_id
            csfd.desc = reply_desc
            csfd.creat_time = datetime.now()
            csfd.creat_user_name = self.user['user_name']
            csfd.creat_user_id = self.user_id
            db.add(csfd)

        sb = ServerBase(user=self.user)
        sb.log(object_ids=[item_id], model=CustomerSubmmintFormDesc.__tablename__, operation='添加备忘录')

        return ok()

    def form_delete_bach(self, item_id_list):
        with DbSession.create(EnumDb.main) as db:
            db.query(CustomerSubmmintForm). \
                filter(CustomerSubmmintForm.is_delete == 0). \
                filter(CustomerSubmmintForm.id.in_(item_id_list)). \
                update({CustomerSubmmintForm.is_delete: 1, CustomerSubmmintForm.delete_user_id: self.user_id}, synchronize_session='fetch')

        return ok()


if __name__ == '__main__':
    a = WorkServerManage({'user_id': 1})
    res = a.clc_reply_time()
    print(res)
