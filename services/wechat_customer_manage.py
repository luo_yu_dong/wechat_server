#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: wechat_customer_manage.py
@time: 2020/10/20 10:33
@desc:
'''
import traceback
from datetime import datetime

from sqlalchemy import func, desc, and_, or_

from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.log_controller import log
from framework.msg import failure, ok
from framework.utilities import to_string, format_dict, to_decimal, to_int_date
from models.customer_form.customer_submmint_form import CustomerSubmmintForm
from models.customer_form.customer_submmint_form_desc import CustomerSubmmintFormDesc
from models.customer_form.customer_submmint_form_max_no import CustomerSubmmintFormMaxNo
from models.exception_log.remind_logs import RemindLogs
from models.users.user import User
from models.users.wechat_user import WechatUser
from services.file_manage import FileManage
from services.wechat_program_base import WechatProgramBase
from services.wechat_wiki_manage import WeChatWikiManage


class WechatCustomerManage(WechatProgramBase):
    def __init__(self, wechat_user_id):
        super().__init__()
        self.wechat_user_id = wechat_user_id

    # @trycatch(failure(msg='操作失败'))
    # @coroutine
    def submmint_form(self, form_type, business_name, contacts, contacts_phone, comment, imgs, title, access_token, is_server_time):
        # 判断内容是否非法
        # if comment:
        #     url = 'https://api.weixin.qq.com/wxa/msg_sec_check?access_token=%s' % (access_token)
        #     post_body = {
        #         'content': comment
        #     }
        #     res = requests.post(url=url, data=json.dumps(post_body))
        #     res = to_dict(res.content)
        #     if res.get('errmsg') != 'ok':
        #         return failure(msg='您提交的信息存在非法信息，请确认后重试')
        #
        # url = 'https://api.weixin.qq.com/wxa/msg_sec_check?access_token=%s' % (access_token)
        # post_body = {
        #     'content': title
        # }
        # res = requests.post(url=url, data=json.dumps(post_body))
        # res = to_dict(res.content)
        # if res.get('errmsg') != 'ok':
        #     return failure(msg='您提交的信息存在非法信息，请确认后重试')

        # url = 'https://api.weixin.qq.com/wxa/msg_sec_check?access_token=%s' % (access_token)
        # post_body = {
        #     'content': contacts
        # }
        # res = requests.post(url=url, data=json.dumps(post_body))
        # res = to_dict(res.content)
        # if res.get('errmsg') != 'ok':
        #     return failure(msg='您提交的信息存在非法信息，请确认后重试')

        if imgs:
            f = FileManage()
            res = f.file_save(file_list=imgs)
            if res.is_ok():
                if res.data['file_list']:
                    data_file = res.data['file_list']
                else:
                    data_file = ''
            else:
                data_file = ''
                return failure(msg='保存文件失败！')
        else:
            data_file = ''

        now = datetime.now()
        int_date = to_int_date(now)
        wechat_user_ids = []

        try:
            with DbSession.create(EnumDb.main) as db:
                max_no_query = db.query(CustomerSubmmintFormMaxNo).filter(CustomerSubmmintFormMaxNo.int_date == int_date).first()
                if not max_no_query:
                    max_no = 1
                    csfmn = CustomerSubmmintFormMaxNo()
                    csfmn.int_date = int_date
                    csfmn.max_no = 1
                    db.add(csfmn)
                else:
                    max_no = max_no_query.max_no + 1
                    max_no_query.max_no = max_no

                no = 'SHFWD' + to_string(int_date) + to_string(max_no).zfill(3)

                csf = CustomerSubmmintForm()
                csf.wechat_user_id = self.wechat_user_id
                csf.no = no
                csf.form_type = form_type
                csf.is_server_time = is_server_time
                csf.title = title
                csf.business_name = business_name
                csf.contacts = contacts
                csf.contacts_phone = contacts_phone
                csf.comment = comment
                csf.imgs = to_string(data_file)
                csf.creat_time = now
                csf.creat_int_date = to_int_date(now)
                csf.creat_year = now.year
                csf.creat_month = now.month
                csf.creat_day = now.day
                csf.is_reply = 0
                db.add(csf)
                db.flush()
                item_id = csf.id

                # 获取管理员的信息
                user_ids_list = [v[0] for v in db.query(User.user_id).filter(User.is_admin == 1)]

                # 查询客服人员
                work_query = db.query(WechatUser.id). \
                    filter(WechatUser.is_attention_wiki == 1). \
                    filter(WechatUser.is_customer_service == 1). \
                    filter(WechatUser.user_id.in_(user_ids_list))
                for v in work_query:
                    wechat_user_ids.append(v[0])

                # # 查询客户是否关注了公众号
                # customer_wechat = db.query(WechatUser).filter(WechatUser.id == self.wechat_user_id).filter(WechatUser.is_attention_wiki == 1).first()
                # if customer_wechat:
                #     wechat_user_ids.append(self.wechat_user_id)

            # 发消息
            if wechat_user_ids:
                wcwm = WeChatWikiManage()
                wcwm.send_new_form_wechat_message(wechat_user_ids=wechat_user_ids,
                                                  item_id=item_id,
                                                  form_no=no,
                                                  form_title=title,
                                                  form_time=now)  # 发消息给客服

            return ok()
        except:
            log(traceback.format_exc(), 'WxProgramManage.submmint_form', '保存失败!')
            return failure(msg='保存失败')

    def customer_server_submmint_form_list(self, select_name, form_type, is_reply, page, size, judge_is_customer_service_admin, customer_server_user_id, select_comment):
        if select_name:
            cons_select_name = CustomerSubmmintForm.title.like('%' + select_name + '%')
        else:
            cons_select_name = True

        if select_comment:
            cons_select_comment = or_(CustomerSubmmintForm.title.like('%' + select_comment + '%'),
                                      CustomerSubmmintForm.comment.like('%' + select_comment + '%'),
                                      CustomerSubmmintForm.reply_result.like('%' + select_comment + '%'),
                                      )
        else:
            cons_select_comment = True

        if form_type:
            cons_form_type = CustomerSubmmintForm.form_type == form_type
        else:
            cons_form_type = True

        if is_reply:
            cons_is_reply = CustomerSubmmintForm.is_reply == is_reply
        else:
            cons_is_reply = True

        cons = and_(cons_select_name, cons_form_type, cons_is_reply, cons_select_comment)

        with DbSession.create(EnumDb.main) as db:
            if judge_is_customer_service_admin:
                count = db.query(func.count(CustomerSubmmintForm.id)). \
                    filter(cons). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    first()[0]

                query = db.query(CustomerSubmmintForm). \
                    filter(cons). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    order_by(desc(CustomerSubmmintForm.creat_time)). \
                    offset(page * size).limit(size)
            else:
                count = db.query(func.count(CustomerSubmmintForm.id)). \
                    filter(or_(CustomerSubmmintForm.wechat_user_id == self.wechat_user_id,
                               CustomerSubmmintForm.reply_user_id == customer_server_user_id)). \
                    filter(cons). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    first()[0]

                query = db.query(CustomerSubmmintForm). \
                    filter(or_(CustomerSubmmintForm.wechat_user_id == self.wechat_user_id,
                               CustomerSubmmintForm.reply_user_id == customer_server_user_id)). \
                    filter(cons). \
                    filter(CustomerSubmmintForm.is_delete == 0). \
                    order_by(desc(CustomerSubmmintForm.creat_time)). \
                    offset(page * size).limit(size)

            if not count:
                return ok(data={'count': 0, 'list': []})

            list = []
            for v in query:
                list.append(format_dict(v.get_dict()))

        return ok(data={'count': count, 'list': list})

    def submmint_form_list(self, select_name, form_type, is_reply, page, size, select_comment):
        if select_name:
            cons_select_name = CustomerSubmmintForm.title.like('%' + select_name + '%')
        else:
            cons_select_name = True

        if select_comment:
            cons_select_comment = or_(CustomerSubmmintForm.title.like('%' + select_comment + '%'),
                                      CustomerSubmmintForm.comment.like('%' + select_comment + '%'),
                                      CustomerSubmmintForm.reply_result.like('%' + select_comment + '%'),
                                      )
        else:
            cons_select_comment = True

        if form_type:
            cons_form_type = CustomerSubmmintForm.form_type == form_type
        else:
            cons_form_type = True

        if is_reply:
            cons_is_reply = CustomerSubmmintForm.is_reply == is_reply
        else:
            cons_is_reply = True

        cons = and_(cons_select_name, cons_form_type, cons_is_reply, cons_select_comment)

        with DbSession.create(EnumDb.main) as db:
            wechat_user = db.query(WechatUser).filter(WechatUser.id == self.wechat_user_id).first()
            if not wechat_user:
                return ok(data={'count': 0, 'list': []})

            count = db.query(func.count(CustomerSubmmintForm.id)). \
                filter(CustomerSubmmintForm.wechat_user_id == self.wechat_user_id). \
                filter(CustomerSubmmintForm.is_delete == 0). \
                filter(cons). \
                first()[0]

            query = db.query(CustomerSubmmintForm). \
                filter(CustomerSubmmintForm.wechat_user_id == self.wechat_user_id). \
                filter(CustomerSubmmintForm.is_delete == 0). \
                filter(cons). \
                order_by(desc(CustomerSubmmintForm.creat_time)). \
                offset(page * size).limit(size)

            if not count:
                return ok(data={'count': 0, 'list': []})

            list = []
            for v in query:
                list.append(format_dict(v.get_dict()))

        return ok(data={'count': count, 'list': list})

    def submmint_form_first(self, id, user_id):
        with DbSession.create(EnumDb.main) as db:
            query = db.query(CustomerSubmmintForm).filter(CustomerSubmmintForm.id == id).first()
            if not query:
                return failure(msg='数据不存在')

            result_dict = format_dict(query.get_dict())

            db.query(RemindLogs). \
                filter(RemindLogs.type == 0). \
                filter(RemindLogs.item_id == id). \
                filter(RemindLogs.user_id == user_id). \
                update({RemindLogs.is_read: 1,
                        RemindLogs.read_time: datetime.now()}, synchronize_session='fetch')

            desc_list = []
            desc_query = db.query(CustomerSubmmintFormDesc). \
                filter(CustomerSubmmintFormDesc.form_id == id). \
                order_by(desc(CustomerSubmmintFormDesc.creat_time))
            for v in desc_query:
                desc_list.append(format_dict(v.get_dict()))

            result_dict['desc_list'] = desc_list

        return ok(data=result_dict)

    # @coroutine
    def submmint_form_score(self, id, score, desc):
        wechat_user_ids = []

        with DbSession.create(EnumDb.main) as db:
            # 查询用户是客户还是客服
            wechat_user = db.query(WechatUser).filter(WechatUser.id == self.wechat_user_id).first()
            if not wechat_user:
                return failure(msg='用户不存在！')

            query = db.query(CustomerSubmmintForm). \
                filter(CustomerSubmmintForm.id == id). \
                filter(CustomerSubmmintForm.is_delete == 0). \
                filter(CustomerSubmmintForm.wechat_user_id == self.wechat_user_id). \
                first()
            if not query:
                return failure(msg='数据不存在')

            is_reply = query.is_reply
            if not is_reply:
                return failure(msg='该问题未处理，不能进行评分！')

            query.score = to_decimal(score) * 2
            query.desc = desc
            form_no = query.no

            reply_user_id = query.reply_user_id

            wechat_user_query = db.query(WechatUser.id).filter(WechatUser.user_id == reply_user_id).first()
            if not wechat_user_query:
                return ok()

            send_wechat_user_id = [wechat_user_query[0]]

        # 发消息
        wcwm = WeChatWikiManage()
        wcwm.send_score_form_wechat_message(wechat_user_ids=send_wechat_user_id,
                                            item_id=id,
                                            form_no=form_no,
                                            form_score=score,
                                            form_desc=desc)

        return ok()

    def user_get_list(self):
        list = []
        with DbSession.create(EnumDb.main) as db:
            for v in db.query(User.user_id, User.user_name).filter(User.is_delete == 0):
                list.append({
                    'user_id': v[0],
                    'user_name': v[1]
                })

        return ok(data=list)
