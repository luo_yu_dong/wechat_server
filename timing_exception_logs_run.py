#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: timing_exception_logs_run.py
@time: 2020/3/11 12:12
@desc:
'''

import sched
import time
import traceback

from enums.enum_cache_type import EnumCacheType
from enums.enum_db import EnumDb
from framework.db_session import DbSession
from framework.utilities import to_dict, to_datetime
from models.exception_log.exception_logs import ExceptionLogs
from thirdpart.redis_helper import RedisList

schedule = sched.scheduler(time.time, time.sleep)


class ExceptonLog():
    __last_send_mail_time = 0

    @classmethod
    def log(cls):
        exception_list = cls.get_exception_list()
        if not exception_list:
            return
        try:
            print('log_save')
            cls.log_save(exception_list)
        finally:
            print(traceback.format_exc())
            if exception_list:
                exception_list.clear()

    @classmethod
    def log_save(cls, ls):
        '''
        将异常保存到数据库
        :param ls:
        :type ls:
        :return:
        :rtype:
        '''
        if not ls:
            return
        save_list = []
        try:
            for v in ls:
                if not v:
                    continue
                item = ExceptionLogs()
                item.body = v.get('body')
                item.type = v.get('type')
                item.position = v.get('pos')
                item.option_value = v.get('val')
                item.log_time = to_datetime(v.get('time'))
                save_list.append(item)

            if not save_list:
                return

            with DbSession.create(EnumDb.main) as db:
                db.bulk_save_objects(save_list)
        except Exception as e:
            print(traceback.format_exc())
        finally:
            save_list.clear()

    @classmethod
    def get_exception_list(cls):
        '''
        获取异常列表
        时间间隔5分钟或者一次条数>=5条就进行发送
        :return:
        :rtype:
        '''
        cur_index = 0

        now = int(time.time())
        ls = []
        try:
            lenth = RedisList.length(list_name='__sys_exceptions', cache_type=EnumCacheType.log)
            if not lenth:
                return ls

            if lenth > 5 or now - cls.__last_send_mail_time >= 300:  # 5分钟
                cls.__last_send_mail_time = now
                max_get_count = lenth + 1
                while cur_index < max_get_count:
                    excs = RedisList.pop_from_bottom(list_name='__sys_exceptions', cache_type=EnumCacheType.log)
                    if excs:
                        ls.append(to_dict(excs))
                        cur_index += 1
                    else:
                        cur_index = max_get_count + 1
            return ls
        except:
            print(traceback.format_exc())
            cls.__last_send_mail_time = 0
            return ls


def perform_command(inc):
    # 安排inc秒后再次运行自己，即周期运行
    schedule.enter(inc, 0, perform_command, argument=(inc,))

    # ------------执行脚本---------------------------------------
    try:
        print('-------- 开始 ---------')
        tsm = ExceptonLog()
        tsm.log()
    except:
        print(traceback.format_exc())
        pass


if __name__ == '__main__':
    inc = 60  # 1分钟
    schedule.enter(inc, 0, perform_command, (inc,))
    schedule.run()
