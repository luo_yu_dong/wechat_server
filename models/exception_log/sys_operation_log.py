#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2020-01-06 09:10:28
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class SysOperationLog(BaseOrmClass):
    '''
    模型(表)备注:
系统操作记录
    '''
    __tablename__ = 'sys_operation_log'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(Integer(), primary_key=True)

    '''      '''
    object_id = Column(BigInteger(), default=0)

    '''      '''
    model = Column(String(50), nullable=False)

    '''      '''
    operation = Column(String(50), nullable=False)

    '''      '''
    operation_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''      '''
    operation_user_id = Column(BigInteger(), nullable=False)

    def __repr__(self):
        return "sys_operation_log(object_id='{self.object_id}',model='{self.model}',operation='{self.operation}',operation_time='{self.operation_time}',operation_user_id='{self.operation_user_id}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'object_id': self.object_id,
            'model': self.model,
            'operation': self.operation,
            'operation_time': self.operation_time,
            'operation_user_id': self.operation_user_id
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.object_id = obj.get('object_id')
        self.model = obj.get('model')
        self.operation = obj.get('operation')
        self.operation_time = obj.get('operation_time')
        self.operation_user_id = obj.get('operation_user_id')

    @staticmethod
    def db_map():
        return {SysOperationLog: EnumDb.main}
