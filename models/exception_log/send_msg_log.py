#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2020-10-20 14:36:24
@desc:
'''

from sqlalchemy import Column, Integer, String, BigInteger, TEXT
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class SendMsgLog(BaseOrmClass):
    '''
    模型(表)备注:
发送消息记录
    '''
    __tablename__ = 'send_msg_log'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''      '''
    wechat_user_id = Column(BigInteger(), nullable=False)

    '''   customer_submmint_form 表主键id   '''
    item_id = Column(BigInteger(), nullable=False)

    '''   0-发送不成功，1-发送成功   '''
    send_ok = Column(Integer(), nullable=False)

    '''   信息保存   '''
    error_msg = Column(String(500))

    '''   时间   '''
    creat_time = Column(Integer(), nullable=False)

    '''      '''
    json = Column(TEXT(), nullable=False)

    '''      '''
    open_id = Column(String(200), nullable=False)

    '''   0-售后消息，1-风险消息，2-风险跟踪，3-协助任务消息，4-协助任务跟踪   '''
    model = Column(Integer(), default=0, nullable=False)

    '''   项目ID   '''
    project_id = Column(BigInteger(), default=0, nullable=False)

    def __repr__(self):
        return "send_msg_log(wechat_user_id='{self.wechat_user_id}',send_from='{self.send_from}',item_id='{self.item_id}',send_ok='{self.send_ok}',opertaion='{self.opertaion}',operation_type='{self.operation_type}',creat_time='{self.creat_time}',json='{self.json}',open_id='{self.open_id}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'wechat_user_id': self.wechat_user_id,
            'item_id': self.item_id,
            'send_ok': self.send_ok,
            'error_msg': self.error_msg,
            'creat_time': self.creat_time,
            'json': self.json,
            'open_id': self.open_id,
            'model': self.model,
            'project_id': self.project_id
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.wechat_user_id = obj.get('wechat_user_id')
        self.item_id = obj.get('item_id')
        self.send_ok = obj.get('send_ok')
        self.error_msg = obj.get('error_msg')
        self.creat_time = obj.get('creat_time')
        self.json = obj.get('json')
        self.open_id = obj.get('open_id')
        self.model = obj.get('model')
        self.project_id = obj.get('project_id')

    @staticmethod
    def db_map():
        return {SendMsgLog: EnumDb.main}
