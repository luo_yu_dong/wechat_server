#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2020-11-12 13:58:42
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class RemindLogs(BaseOrmClass):
    '''
    模型(表)备注:
提醒消息
    '''
    __tablename__ = 'remind_logs'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   0-服务单分配   '''
    type = Column(Integer(), nullable=False)

    '''   0-未读，1-已读   '''
    is_read = Column(Integer(), nullable=False)

    '''   读取时间   '''
    read_time = Column(DateTime())

    '''   服务单id   '''
    item_id = Column(BigInteger(), nullable=False)

    '''   用户id   '''
    user_id = Column(BigInteger(), nullable=False)

    '''   操作人id   '''
    opertaion_user_id = Column(BigInteger(), nullable=False)

    '''   操作时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    def __repr__(self):
        return "remind_logs(type='{self.type}',is_read='{self.is_read}',item_id='{self.item_id}',user_id='{self.user_id}',opertaion_user_id='{self.opertaion_user_id}',creat_time='{self.creat_time}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'type': self.type,
            'is_read': self.is_read,
            'read_time': self.read_time,
            'item_id': self.item_id,
            'user_id': self.user_id,
            'opertaion_user_id': self.opertaion_user_id,
            'creat_time': self.creat_time
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.type = obj.get('type')
        self.is_read = obj.get('is_read')
        self.read_time = obj.get('read_time')
        self.item_id = obj.get('item_id')
        self.user_id = obj.get('user_id')
        self.opertaion_user_id = obj.get('opertaion_user_id')
        self.creat_time = obj.get('creat_time')

    @staticmethod
    def db_map():
        return {RemindLogs: EnumDb.main}
