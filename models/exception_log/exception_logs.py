#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2020-10-19 08:59:33
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, DateTime, BigInteger, TEXT
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ExceptionLogs(BaseOrmClass):
    '''
    模型(表)备注:
错误日志
    '''
    __tablename__ = 'exception_logs'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''      '''
    type = Column(Integer(), nullable=False)

    '''      '''
    body = Column(TEXT(), nullable=False)

    '''      '''
    log_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''      '''
    position = Column(TEXT(), nullable=False)

    '''      '''
    option_value = Column(TEXT(), nullable=False)

    def __repr__(self):
        return "exception_logs(type='{self.type}',body='{self.body}',log_time='{self.log_time}',position='{self.position}',option_value='{self.option_value}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'type': self.type,
            'body': self.body,
            'log_time': self.log_time,
            'position': self.position,
            'option_value': self.option_value
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.type = obj.get('type')
        self.body = obj.get('body')
        self.log_time = obj.get('log_time')
        self.position = obj.get('position')
        self.option_value = obj.get('option_value')

    @staticmethod
    def db_map():
        return {ExceptionLogs: EnumDb.main}
