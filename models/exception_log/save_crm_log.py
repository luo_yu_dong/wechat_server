#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2020-11-16 09:34:54
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class SaveCrmLog(BaseOrmClass):
    '''
    模型(表)备注:
保存至crm 日志
    '''
    __tablename__ = 'save_crm_log'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   错误日志   '''
    crm_erroe_msg = Column(String(200), nullable=False)

    '''   0-失败， 1-成功   '''
    is_ok = Column(Integer(), nullable=False)

    '''   请求参数   '''
    post_data = Column(String(2000))

    '''      '''
    url = Column(String(200), nullable=False)

    '''      '''
    post_time = Column(DateTime(), default=datetime.now, nullable=False)

    def __repr__(self):
        return "save_crm_log(ip='{self.ip}',crm_erroe_msg='{self.crm_erroe_msg}',is_ok='{self.is_ok}',post_data='{self.post_data}',url='{self.url}',post_time='{self.post_time}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'crm_erroe_msg': self.crm_erroe_msg,
            'is_ok': self.is_ok,
            'post_data': self.post_data,
            'url': self.url,
            'post_time': self.post_time
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.crm_erroe_msg = obj.get('crm_erroe_msg')
        self.is_ok = obj.get('is_ok')
        self.post_data = obj.get('post_data')
        self.url = obj.get('url')
        self.post_time = obj.get('post_time')

    @staticmethod
    def db_map():
        return {SaveCrmLog: EnumDb.main}
