#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-07-08 10:43:30
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, String, DateTime, BigInteger, Date
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ProjectTaskWorkingDaily(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'project_task_working_daily'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   汇报人user_id   '''
    reporting_person_user_id = Column(BigInteger(), nullable=False)

    '''   汇报人user_name   '''
    reporting_person_user_name = Column(String(255), nullable=False)

    '''   汇报时间   '''
    report_time = Column(Date(), nullable=False)

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    def __repr__(self):
        return "project_task_working_daily(reporting_person_user_id='{self.reporting_person_user_id}',reporting_person_user_name='{self.reporting_person_user_name}',creat_time='{self.creat_time}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'reporting_person_user_id': self.reporting_person_user_id,
            'reporting_person_user_name': self.reporting_person_user_name,
            'report_time': self.report_time,
            'creat_time': self.creat_time
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.reporting_person_user_id = obj.get('reporting_person_user_id')
        self.reporting_person_user_name = obj.get('reporting_person_user_name')
        self.report_time = obj.get('report_time')
        self.creat_time = obj.get('creat_time')

    @staticmethod
    def db_map():
        return {ProjectTaskWorkingDaily: EnumDb.main}
