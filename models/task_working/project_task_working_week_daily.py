#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-08-01 10:56:11
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, String, DateTime, BigInteger, TEXT, Date
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ProjectTaskWorkingWeekDaily(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'project_task_working_week_daily'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   汇报人user_id   '''
    reporting_person_user_id = Column(BigInteger(), nullable=False)

    '''   汇报人user_name   '''
    reporting_person_user_name = Column(String(255), nullable=False)

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   周汇报开始时间   '''
    report_week_start_time = Column(Date(), nullable=False)

    '''   周汇报结束时间   '''
    report_week_end_time = Column(Date(), nullable=False)

    '''   下周计划   '''
    next_plan = Column(TEXT())

    '''   本周总结   '''
    week_review = Column(TEXT())

    '''   评价   '''
    evaluations = Column(TEXT())

    '''   评价人   '''
    evaluations_user_id = Column(BigInteger())

    '''   评价人   '''
    evaluations_user_name = Column(String(255))

    '''   评价时间   '''
    evaluations_time = Column(DateTime())

    def __repr__(self):
        return "project_task_working_week_daily(reporting_person_user_id='{self.reporting_person_user_id}',reporting_person_user_name='{self.reporting_person_user_name}',creat_time='{self.creat_time}',report_week_start_time='{self.report_week_start_time}',report_week_end_time='{self.report_week_end_time}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'reporting_person_user_id': self.reporting_person_user_id,
            'reporting_person_user_name': self.reporting_person_user_name,
            'creat_time': self.creat_time,
            'report_week_start_time': self.report_week_start_time,
            'report_week_end_time': self.report_week_end_time,
            'next_plan': self.next_plan,
            'week_review': self.week_review,
            'evaluations': self.evaluations,
            'evaluations_user_id': self.evaluations_user_id,
            'evaluations_user_name': self.evaluations_user_name,
            'evaluations_time': self.evaluations_time
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.reporting_person_user_id = obj.get('reporting_person_user_id')
        self.reporting_person_user_name = obj.get('reporting_person_user_name')
        self.creat_time = obj.get('creat_time')
        self.report_week_start_time = obj.get('report_week_start_time')
        self.report_week_end_time = obj.get('report_week_end_time')
        self.next_plan = obj.get('next_plan')
        self.week_review = obj.get('week_review')
        self.evaluations = obj.get('evaluations')
        self.evaluations_user_id = obj.get('evaluations_user_id')
        self.evaluations_user_name = obj.get('evaluations_user_name')
        self.evaluations_time = obj.get('evaluations_time')

    @staticmethod
    def db_map():
        return {ProjectTaskWorkingWeekDaily: EnumDb.main}
