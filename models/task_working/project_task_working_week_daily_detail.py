#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-07-23 10:14:34
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ProjectTaskWorkingWeekDailyDetail(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'project_task_working_week_daily_detail'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   project_task_working_daily 表主键id   '''
    project_task_working_daily_id = Column(BigInteger(), nullable=False)

    '''   chandao_project 的 chandao_project_id   '''
    chandao_project_id = Column(BigInteger(), nullable=False)

    '''   chandao_project 的 project_name   '''
    project_name = Column(String(255), nullable=False)

    '''   工作内容   '''
    work_content = Column(String(5000), nullable=False)

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''      '''
    reporting_person_user_id = Column(BigInteger(), nullable=False)

    '''      '''
    reporting_person_user_name = Column(String(255))

    '''   0-未审核，1-已审核   '''
    is_audits = Column(Integer(), default=0)

    '''   审核时间   '''
    audits_time = Column(DateTime(), default=datetime.now)

    def __repr__(self):
        return "project_task_working_week_daily_detail(project_task_working_daily_id='{self.project_task_working_daily_id}',chandao_project_id='{self.chandao_project_id}',project_name='{self.project_name}',work_content='{self.work_content}',creat_time='{self.creat_time}',reporting_person_user_id='{self.reporting_person_user_id}',is_audits='{self.is_audits}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'project_task_working_daily_id': self.project_task_working_daily_id,
            'chandao_project_id': self.chandao_project_id,
            'project_name': self.project_name,
            'work_content': self.work_content,
            'creat_time': self.creat_time,
            'reporting_person_user_id': self.reporting_person_user_id,
            'reporting_person_user_name': self.reporting_person_user_name,
            'is_audits': self.is_audits,
            'audits_time': self.audits_time
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.project_task_working_daily_id = obj.get('project_task_working_daily_id')
        self.chandao_project_id = obj.get('chandao_project_id')
        self.project_name = obj.get('project_name')
        self.work_content = obj.get('work_content')
        self.creat_time = obj.get('creat_time')
        self.reporting_person_user_id = obj.get('reporting_person_user_id')
        self.reporting_person_user_name = obj.get('reporting_person_user_name')
        self.is_audits = obj.get('is_audits')
        self.audits_time = obj.get('audits_time')

    @staticmethod
    def db_map():
        return {ProjectTaskWorkingWeekDailyDetail: EnumDb.main}
