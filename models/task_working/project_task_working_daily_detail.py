#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-07-12 20:30:09
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Numeric, BigInteger, TEXT, Date
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ProjectTaskWorkingDailyDetail(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'project_task_working_daily_detail'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   project_task_working_daily 表主键id   '''
    project_task_working_daily_id = Column(BigInteger(), nullable=False)

    '''   chandao_project 的 chandao_project_id，正常有项目id的是正常项目，-1:其他，-2：销售，-3：财务，-4：行政   '''
    chandao_project_id = Column(BigInteger(), nullable=False)

    '''   chandao_project 的 project_name   '''
    project_name = Column(String(255), nullable=False)

    '''   工时，0.25、0.5、0.75、1   '''
    staff_time = Column(Numeric(4, 2), nullable=False)

    '''   工作内容   '''
    work_content = Column(String(5000), nullable=False)

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''      '''
    report_time = Column(Date(), nullable=False)

    '''      '''
    reporting_person_user_id = Column(BigInteger(), nullable=False)

    '''      '''
    reporting_person_user_name = Column(String(255))

    '''   0-未审核，1-已审核   '''
    is_audits = Column(Integer(), default=0)

    '''   审核时间   '''
    audits_time = Column(DateTime())

    '''      '''
    file = Column(TEXT())

    '''   年   '''
    year = Column(Integer(), nullable=False)

    '''   月   '''
    month = Column(Integer(), nullable=False)

    def __repr__(self):
        return "project_task_working_daily_detail(project_task_working_daily_id='{self.project_task_working_daily_id}',chandao_project_id='{self.chandao_project_id}',project_name='{self.project_name}',staff_time='{self.staff_time}',work_content='{self.work_content}',creat_time='{self.creat_time}',report_time='{self.report_time}',reporting_person_user_id='{self.reporting_person_user_id}',is_audits='{self.is_audits}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'project_task_working_daily_id': self.project_task_working_daily_id,
            'chandao_project_id': self.chandao_project_id,
            'project_name': self.project_name,
            'staff_time': self.staff_time,
            'work_content': self.work_content,
            'creat_time': self.creat_time,
            'report_time': self.report_time,
            'reporting_person_user_id': self.reporting_person_user_id,
            'reporting_person_user_name': self.reporting_person_user_name,
            'is_audits': self.is_audits,
            'audits_time': self.audits_time,
            'file': self.file,
            'year': self.year,
            'month': self.month
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.project_task_working_daily_id = obj.get('project_task_working_daily_id')
        self.chandao_project_id = obj.get('chandao_project_id')
        self.project_name = obj.get('project_name')
        self.staff_time = obj.get('staff_time')
        self.work_content = obj.get('work_content')
        self.creat_time = obj.get('creat_time')
        self.report_time = obj.get('report_time')
        self.reporting_person_user_id = obj.get('reporting_person_user_id')
        self.reporting_person_user_name = obj.get('reporting_person_user_name')
        self.is_audits = obj.get('is_audits')
        self.audits_time = obj.get('audits_time')
        self.file = obj.get('file')
        self.year = obj.get('year')
        self.month = obj.get('month')

    @staticmethod
    def db_map():
        return {ProjectTaskWorkingDailyDetail: EnumDb.main}
