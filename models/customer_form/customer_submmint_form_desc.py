#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2021-01-15 14:00:59
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class CustomerSubmmintFormDesc(BaseOrmClass):
    '''
    模型(表)备注:
服务单备忘记录
    '''
    __tablename__ = 'customer_submmint_form_desc'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   form表id   '''
    form_id = Column(BigInteger(), nullable=False)

    '''      '''
    desc = Column(String(50), nullable=False)

    '''      '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''      '''
    creat_user_name = Column(String(50), nullable=False)

    '''      '''
    creat_user_id = Column(BigInteger(), nullable=False)

    def __repr__(self):
        return "customer_submmint_form_desc(form_id='{self.form_id}',desc='{self.desc}',creat_time='{self.creat_time}',creat_user_name='{self.creat_user_name}',creat_user_id='{self.creat_user_id}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'form_id': self.form_id,
            'desc': self.desc,
            'creat_time': self.creat_time,
            'creat_user_name': self.creat_user_name,
            'creat_user_id': self.creat_user_id
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.form_id = obj.get('form_id')
        self.desc = obj.get('desc')
        self.creat_time = obj.get('creat_time')
        self.creat_user_name = obj.get('creat_user_name')
        self.creat_user_id = obj.get('creat_user_id')

    @staticmethod
    def db_map():
        return {CustomerSubmmintFormDesc: EnumDb.main}
