#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2020-10-23 14:04:51
@desc:
'''

from sqlalchemy import Column, Integer
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class CustomerSubmmintFormMaxNo(BaseOrmClass):
    '''
    模型(表)备注:
服务单最大编号记录表
    '''
    __tablename__ = 'customer_submmint_form_max_no'

    __table_args__ = {'schema': 'main'}

    '''      '''
    int_date = Column(Integer(), primary_key=True)

    '''      '''
    max_no = Column(Integer(), nullable=False)

    def __repr__(self):
        return "customer_submmint_form_max_no(max_no='{self.max_no}')".format(self=self)

    def get_dict(self):
        return {
            'int_date': self.int_date,
            'max_no': self.max_no
        }

    def set_object(self, obj={}):
        self.int_date = obj.get('int_date')
        self.max_no = obj.get('max_no')

    @staticmethod
    def db_map():
        return {CustomerSubmmintFormMaxNo: EnumDb.main}
