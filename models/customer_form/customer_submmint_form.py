#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2020-10-20 12:09:29
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger, TEXT
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class CustomerSubmmintForm(BaseOrmClass):
    '''
    模型(表)备注:
客户提交的表单数据
    '''
    __tablename__ = 'customer_submmint_form'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   wechat_user表主键id   '''
    wechat_user_id = Column(BigInteger(), nullable=False)

    '''  售后服务单单据编号   '''
    no = Column(String(50), nullable=False)

    '''  是否删除 1-删除，0-未删除  '''
    is_delete = Column(Integer(), default=0)

    '''  删除人id  '''
    delete_user_id = Column(BigInteger(), default=0)

    '''   表单类型 1-需求，2-问题，3-其他   '''
    form_type = Column(BigInteger(), nullable=False)

    '''  是否在服务期内，0-不在，1-在服务期  '''
    is_server_time = Column(Integer(), default=0)

    '''   企业名称   '''
    business_name = Column(String(200), nullable=False)

    '''   联系人   '''
    contacts = Column(String(200), nullable=False)

    '''   联系电话   '''
    contacts_phone = Column(BigInteger(), nullable=False)

    '''   标题   '''
    title = Column(String(200), nullable=False)

    '''   描述   '''
    comment = Column(String(500), nullable=False)

    '''   图片   '''
    imgs = Column(TEXT())

    '''      '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''    '''
    creat_int_date = Column(Integer(), nullable=False)

    '''    '''
    creat_year = Column(Integer(), nullable=False)

    '''    '''
    creat_month = Column(Integer(), nullable=False)

    '''    '''
    creat_day = Column(Integer(), nullable=False)

    '''   是否已处理   '''
    is_reply = Column(Integer(), default=0)

    '''   处理结果   '''
    reply_result = Column(String(200))

    '''      '''
    reply_imgs = Column(TEXT())

    '''   处理时间   '''
    replyl_time = Column(DateTime())

    '''      '''
    int_date = Column(Integer(), default=0)

    '''   处理人   '''
    reply_user_name = Column(String(50))

    '''   处理人用户id   '''
    reply_user_id = Column(String(50))

    '''   满分10分，对应实际分值*2进行保存，eg:score9分，那么客户的打分就是4.5分   '''
    score = Column(Integer())

    '''   得分说明   '''
    desc = Column(String(50))

    '''    '''
    year = Column(Integer())

    '''    '''
    month = Column(Integer())

    '''    '''
    day = Column(Integer())

    '''    '''
    reply_time_second = Column(Integer(), default=0)

    '''      '''
    is_to_crm = Column(Integer(), default=0)

    def __repr__(self):
        return "customer_submmint_form(wechat_user_id='{self.wechat_user_id}',business_name='{self.business_name}',contacts='{self.contacts}',contacts_phone='{self.contacts_phone}',comment='{self.comment}',imgs='{self.imgs}',creat_time='{self.creat_time}',is_reply='{self.is_reply}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'wechat_user_id': self.wechat_user_id,
            'no': self.no,
            'is_delete': self.is_delete,
            'delete_user_id': self.delete_user_id,
            'form_type': self.form_type,
            'is_server_time': self.is_server_time,
            'business_name': self.business_name,
            'contacts': self.contacts,
            'contacts_phone': self.contacts_phone,
            'title': self.title,
            'comment': self.comment,
            'imgs': self.imgs,
            'creat_time': self.creat_time,
            'creat_int_date': self.creat_int_date,
            'creat_year': self.creat_year,
            'creat_month': self.creat_month,
            'creat_day': self.creat_day,
            'is_reply': self.is_reply,
            'reply_result': self.reply_result,
            'reply_imgs': self.reply_imgs,
            'replyl_time': self.replyl_time,
            'reply_user_name': self.reply_user_name,
            'reply_user_id': self.reply_user_id,
            'score': self.score,
            'desc': self.desc,
            'year': self.year,
            'int_date': self.int_date,
            'month': self.month,
            'day': self.day,
            'reply_time_second': self.reply_time_second
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.wechat_user_id = obj.get('wechat_user_id')
        self.no = obj.get('no')
        self.is_delete = obj.get('is_delete')
        self.delete_user_id = obj.get('delete_user_id')
        self.form_type = obj.get('form_type')
        self.is_server_time = obj.get('is_server_time')
        self.business_name = obj.get('business_name')
        self.contacts = obj.get('contacts')
        self.contacts_phone = obj.get('contacts_phone')
        self.comment = obj.get('comment')
        self.title = obj.get('title')
        self.imgs = obj.get('imgs')
        self.creat_time = obj.get('creat_time')
        self.creat_int_date = obj.get('creat_int_date')
        self.creat_year = obj.get('creat_year')
        self.creat_month = obj.get('creat_month')
        self.creat_day = obj.get('creat_day')
        self.is_reply = obj.get('is_reply')
        self.reply_result = obj.get('reply_result')
        self.reply_imgs = obj.get('reply_imgs')
        self.replyl_time = obj.get('replyl_time')
        self.reply_user_name = obj.get('reply_user_name')
        self.reply_user_id = obj.get('reply_user_id')
        self.score = obj.get('score')
        self.desc = obj.get('desc')
        self.year = obj.get('year')
        self.month = obj.get('month')
        self.day = obj.get('day')
        self.int_date = obj.get('int_date')
        self.reply_time_second = obj.get('reply_time_second')
        self.is_to_crm = obj.get('is_to_crm')

    @staticmethod
    def db_map():
        return {CustomerSubmmintForm: EnumDb.main}
