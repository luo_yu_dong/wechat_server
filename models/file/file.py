#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2020-10-20 11:59:10
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Numeric, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class File(BaseOrmClass):
    '''
    模型(表)备注:
文件表
    '''
    __tablename__ = 'file'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''      '''
    file_name = Column(String(200), nullable=False)

    '''      '''
    old_name = Column(String(200), nullable=False)

    '''      '''
    time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   是否是临时文件   '''
    is_temporary = Column(Integer(), nullable=False)

    '''      '''
    host = Column(String(50), nullable=False)

    '''      '''
    path = Column(String(50), nullable=False)

    '''      '''
    size = Column(Numeric(18, 4), nullable=False)

    def __repr__(self):
        return "file(file_name='{self.file_name}',old_name='{self.old_name}',time='{self.time}',is_temporary='{self.is_temporary}',host='{self.host}',path='{self.path}',size='{self.size}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'file_name': self.file_name,
            'old_name': self.old_name,
            'time': self.time,
            'is_temporary': self.is_temporary,
            'host': self.host,
            'path': self.path,
            'size': self.size
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.file_name = obj.get('file_name')
        self.old_name = obj.get('old_name')
        self.time = obj.get('time')
        self.is_temporary = obj.get('is_temporary')
        self.host = obj.get('host')
        self.path = obj.get('path')
        self.size = obj.get('size')

    @staticmethod
    def db_map():
        return {File: EnumDb.main}
