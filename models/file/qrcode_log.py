#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2020-10-26 16:12:10
@desc:
'''

from sqlalchemy import Column, Integer, String, BigInteger, TEXT
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class QrcodeLog(BaseOrmClass):
    '''
    模型(表)备注:
二维码记录
    '''
    __tablename__ = 'qrcode_log'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''      '''
    ticket = Column(String(500), nullable=False)

    '''      '''
    qrcode = Column(TEXT())

    '''      '''
    is_customer_service = Column(Integer(), nullable=False)

    '''      '''
    creat_timestamp = Column(BigInteger(), nullable=False)

    '''      '''
    project_id = Column(BigInteger(), default=0)

    def __repr__(self):
        return "qrcode_log(file_id='{self.file_id}',ticket='{self.ticket}',is_customer_service='{self.is_customer_service}')".format(self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'ticket': self.ticket,
            'qrcode': self.qrcode,
            'is_customer_service': self.is_customer_service,
            'creat_timestamp': self.creat_timestamp,
            'project_id': self.project_id
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.ticket = obj.get('ticket')
        self.qrcode = obj.get('qrcode')
        self.is_customer_service = obj.get('is_customer_service')
        self.creat_timestamp = obj.get('creat_timestamp')
        self.project_id = obj.get('project_id')

    @staticmethod
    def db_map():
        return {QrcodeLog: EnumDb.main}
