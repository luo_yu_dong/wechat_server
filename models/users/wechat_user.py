#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2020-10-29 20:38:00
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger, TEXT
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class WechatUser(BaseOrmClass):
    '''
    模型(表)备注:
小程序用户
    '''
    __tablename__ = 'wechat_user'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   昵称   '''
    nike_name = Column(String(200))

    '''   头像   '''
    logo = Column(String(200))

    '''   小程序-公众号体系内唯一识别id   '''
    union_id = Column(String(200))

    '''   唯一识别id   '''
    open_id = Column(String(200))

    '''   1-小程序，2-公众号   '''
    type = Column(Integer(), nullable=False)

    '''   是否关注公众号，0-未关注，1-关注，只有关注了公众号的用户才能发送消息   '''
    is_attention_wiki = Column(Integer(), default=0)

    '''      '''
    attention_wiki_time = Column(DateTime(), default=datetime.now)

    '''      '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   是否是客服用户   '''
    is_customer_service = Column(Integer(), nullable=False)

    '''   user表主键id   '''
    user_id = Column(BigInteger(), default=0)

    '''    '''
    session_key = Column(String(200))

    '''   2024-6-3微信改了方式，因此增加字段，用以存储头像   '''
    imgs = Column(TEXT())

    def __repr__(self):
        return "wechat_user(type='{self.type}',is_attention_wiki='{self.is_attention_wiki}',creat_time='{self.creat_time}',is_customer_service='{self.is_customer_service}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'nike_name': self.nike_name,
            'logo': self.logo,
            'union_id': self.union_id,
            'open_id': self.open_id,
            'type': self.type,
            'is_attention_wiki': self.is_attention_wiki,
            'attention_wiki_time': self.attention_wiki_time,
            'creat_time': self.creat_time,
            'is_customer_service': self.is_customer_service,
            'user_id': self.user_id,
            'session_key': self.session_key,
            'imgs': self.imgs
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.nike_name = obj.get('nike_name')
        self.logo = obj.get('logo')
        self.union_id = obj.get('union_id')
        self.open_id = obj.get('open_id')
        self.type = obj.get('type')
        self.is_attention_wiki = obj.get('is_attention_wiki')
        self.attention_wiki_time = obj.get('attention_wiki_time')
        self.creat_time = obj.get('creat_time')
        self.is_customer_service = obj.get('is_customer_service')
        self.user_id = obj.get('user_id')
        self.session_key = obj.get('session_key')
        self.imgs = obj.get('imgs')

    @staticmethod
    def db_map():
        return {WechatUser: EnumDb.main}
