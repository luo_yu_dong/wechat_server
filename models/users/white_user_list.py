#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-09-02 14:02:52
@desc:
'''

from sqlalchemy import Column, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class WhiteUserList(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'white_user_list'

    __table_args__ = {'schema': 'main'}

    '''   user表主键id   '''
    user_id = Column(BigInteger(), primary_key=True)

    def get_dict(self):
        return {
            'user_id': self.user_id
        }

    def set_object(self, obj={}):
        self.user_id = obj.get('user_id')

    @staticmethod
    def db_map():
        return {WhiteUserList: EnumDb.main}
