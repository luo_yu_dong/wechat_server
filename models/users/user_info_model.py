#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: user_manage.py
@time: 2020/1/4 08:13
@desc:该模型为返回给调用端的数据
'''

from models.users.user import User


class UserInfoModel():
    @classmethod
    def create_user_info(cls, use_cache_model):
        try:
            if isinstance(use_cache_model, dict):
                return {
                    'uid': use_cache_model.get('user_id'),
                    'mobile': use_cache_model.get('mobile'),
                    'user_name': use_cache_model.get('user_name'),
                    'remark': use_cache_model.get('remark'),
                    'is_admin': use_cache_model.get('is_admin'),
                    'expire': use_cache_model.get('expire'),
                    'logo': use_cache_model.get('logo'),
                    'gender': use_cache_model.get('gender'),
                    'is_super_admin': use_cache_model.get('is_super_admin'),
                    'role': use_cache_model.get('role')
                }
            if isinstance(use_cache_model, User):
                return {
                    'uid': use_cache_model.user_id,
                    'user_name': use_cache_model.user_name,
                    'mobile': use_cache_model.mobile,
                    'remark': use_cache_model.remark,
                    'is_admin': use_cache_model.is_admin,
                    'expire': use_cache_model.expire,
                    'logo': use_cache_model.logo,
                    'gender': use_cache_model.gender,
                    'is_super_admin': use_cache_model.is_super_admin,
                    'role': use_cache_model.role
                }
            return {
                'uid': None,
                'user_name': None,
                'mobile': 0,
                'remark': None,
                'is_admin': 0,
                'expire': 0,
                'logo': None,
                'gender': None,
                'is_super_admin': 0,
                'role': None
            }
        except:
            return {}
