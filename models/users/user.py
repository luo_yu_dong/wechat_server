#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2020-07-29 09:33:11
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb
from framework.utilities import to_string

BaseOrmClass = declarative_base()


class User(BaseOrmClass):
    '''
    模型(表)备注:
用户信息表
    '''
    __tablename__ = 'user'

    __table_args__ = {'schema': 'main'}

    '''   主键   '''
    user_id = Column(BigInteger(), primary_key=True)

    '''   姓名   '''
    user_name = Column(String(50), nullable=False)

    '''   CRM账号   '''
    crm_user = Column(String(50))

    '''   手机   '''
    mobile = Column(BigInteger(), nullable=False)

    '''   头像   '''
    logo = Column(String(200))

    '''   性别   '''
    gender = Column(String(200))

    '''   密码   '''
    password = Column(String(50), nullable=False)

    '''   创建时间   '''
    create_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   用于加密access_token用于验证用户登陆过期   '''
    token = Column(String(50), nullable=False)

    '''   过期时间   '''
    expire = Column(Integer(), nullable=False)

    '''      '''
    remark = Column(String(50))

    '''   是否删除   '''
    is_delete = Column(Integer(), nullable=False)

    '''   是否是不可见系统管理员   '''
    is_super_admin = Column(Integer(), nullable=False)

    '''   是否是可见系统管理员   '''
    is_admin = Column(Integer(), nullable=False)

    '''   禅道用户名   '''
    chandao_user = Column(String(50), nullable=False)

    '''   角色   '''
    role = Column(String(50))

    def __repr__(self):
        return "user(user_name='{self.user_name}',password='{self.password}',create_time='{self.create_time}',token='{self.token}',expire='{self.expire}',is_delete='{self.is_delete}',is_super_admin='{self.is_super_admin}',is_admin='{self.is_admin}')".format(
            self=self)

    def get_dict(self):
        return {
            'user_id': self.user_id,
            'user_name': self.user_name,
            'crm_user': self.crm_user,
            'mobile': self.mobile,
            'logo': self.logo,
            'gender': self.gender,
            'password': self.password,
            'create_time': to_string(self.create_time),
            'token': self.token,
            'expire': self.expire,
            'remark': self.remark,
            'is_delete': self.is_delete,
            'is_super_admin': self.is_super_admin,
            'is_admin': self.is_admin,
            'chandao_user': self.chandao_user,
            'role': self.role
        }

    def set_object(self, obj={}):
        self.user_id = obj.get('user_id')
        self.user_name = obj.get('user_name')
        self.crm_user = obj.get('crm_user')
        self.mobile = obj.get('mobile')
        self.logo = obj.get('logo')
        self.gender = obj.get('gender')
        self.password = obj.get('password')
        self.create_time = obj.get('create_time')
        self.token = obj.get('token')
        self.expire = obj.get('expire')
        self.remark = obj.get('remark')
        self.is_delete = obj.get('is_delete')
        self.is_super_admin = obj.get('is_super_admin')
        self.is_admin = obj.get('is_admin')
        self.chandao_user = obj.get('chandao_user')
        self.role = obj.get('role')

    @staticmethod
    def db_map():
        return {User: EnumDb.main}
