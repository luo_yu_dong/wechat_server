#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2020-10-22 17:41:58
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ThirdpartyOut(BaseOrmClass):
    '''
    模型(表)备注:
对外授权
    '''
    __tablename__ = 'thirdparty_out'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''      '''
    business_name = Column(String(100), nullable=False)

    '''      '''
    appkey = Column(String(200))

    '''      '''
    appsecret = Column(String(200))

    '''      '''
    create_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   是否有效 1-无效 0-有效   '''
    is_delete = Column(Integer(), nullable=False)

    '''      '''
    token = Column(String(500))

    '''      '''
    expire = Column(Integer())

    '''      '''
    issue_ip = Column(String(50))

    def __repr__(self):
        return "thirdparty_out(business_name='{self.business_name}',create_time='{self.create_time}',is_delete='{self.is_delete}')".format(self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'business_name': self.business_name,
            'appkey': self.appkey,
            'appsecret': self.appsecret,
            'create_time': self.create_time,
            'is_delete': self.is_delete,
            'token': self.token,
            'expire': self.expire,
            'issue_ip': self.issue_ip
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.business_name = obj.get('business_name')
        self.appkey = obj.get('appkey')
        self.appsecret = obj.get('appsecret')
        self.create_time = obj.get('create_time')
        self.is_delete = obj.get('is_delete')
        self.token = obj.get('token')
        self.expire = obj.get('expire')
        self.issue_ip = obj.get('issue_ip')

    @staticmethod
    def db_map():
        return {ThirdpartyOut: EnumDb.main}
