#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-05-17 13:49:51
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ProjectTask(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'project_task'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   父级ID   '''
    pid = Column(BigInteger(), nullable=False)

    '''   任务名称   '''
    task_name = Column(String(255), nullable=False)

    '''   任务等级，0-低，1-中，2-高   '''
    task_level = Column(Integer(), nullable=False)

    '''   任务描述   '''
    task_desc = Column(String(255))

    '''   计划开始时间   '''
    plan_start_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   计划结束时间   '''
    plan_end_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   预期工时（分钟），前端以小时展示，用分钟进行存库   '''
    plan_staff_time = Column(Integer(), nullable=False)

    '''   实际开始时间   '''
    start_time = Column(DateTime())

    '''   实际结束时间   '''
    end_time = Column(DateTime())

    '''   工时分钟   '''
    staff_time = Column(Integer())

    '''   项目id   '''
    project_id = Column(BigInteger(), nullable=False)

    '''   所属阶段   '''
    project_phase_id = Column(BigInteger(), nullable=False)

    '''   所属阶段   '''
    project_phase_name = Column(String(255), nullable=False)

    '''   责任人user_id表   '''
    responsible_person_user_id = Column(BigInteger(), nullable=False)

    '''   责任人user_id表   '''
    responsible_person_user_name = Column(String(255), nullable=False)

    '''   责任人是否完成任务   '''
    responsible_person_is_confirmed = Column(Integer(), default=0)

    '''   责任人完成时间   '''
    responsible_person_is_confirmed_time = Column(DateTime())

    '''   项目经理是否确认   '''
    pm_is_confirmed = Column(Integer())

    '''      '''
    pm_is_confirmed_user_id = Column(Integer(), default=0)

    '''      '''
    pm_is_confirmed_user_name = Column(String(255))

    '''   项目经理确认时间   '''
    pm_is_confirmed_time = Column(DateTime())

    '''   客户是否确认。0-未确认，1-已确认   '''
    is_confirmed = Column(Integer())

    '''   确认人wechat_user_id   '''
    confirmed_wechat_user_id = Column(BigInteger())

    '''   确认人wechat_user_id   '''
    confirmed_wechat_user_name = Column(String(255))

    '''   确认时间   '''
    confirmed_time = Column(DateTime())

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   创建人id   '''
    creat_user_id = Column(BigInteger(), nullable=False)

    '''   创建人姓名   '''
    creat_user_name = Column(String(255), nullable=False)

    '''   修改时间   '''
    edit_time = Column(DateTime())

    '''   修改人id   '''
    edit_user_id = Column(BigInteger())

    '''   修改人姓名   '''
    edit_user_name = Column(String(255))

    '''   0-未删除，1-删除   '''
    is_delete = Column(Integer(), nullable=False)

    def __repr__(self):
        return "project_task(pid='{self.pid}',task_name='{self.task_name}',task_level='{self.task_level}',plan_start_time='{self.plan_start_time}',plan_end_time='{self.plan_end_time}',plan_staff_time='{self.plan_staff_time}',project_id='{self.project_id}',project_phase_id='{self.project_phase_id}',project_phase_name='{self.project_phase_name}',responsible_person_user_id='{self.responsible_person_user_id}',creat_time='{self.creat_time}',creat_user_id='{self.creat_user_id}',creat_user_name='{self.creat_user_name}',is_delete='{self.is_delete}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'pid': self.pid,
            'task_name': self.task_name,
            'task_level': self.task_level,
            'task_desc': self.task_desc,
            'plan_start_time': self.plan_start_time,
            'plan_end_time': self.plan_end_time,
            'plan_staff_time': self.plan_staff_time,
            'start_time': self.start_time,
            'end_time': self.end_time,
            'staff_time': self.staff_time,
            'project_id': self.project_id,
            'project_phase_id': self.project_phase_id,
            'project_phase_name': self.project_phase_name,
            'responsible_person_user_id': self.responsible_person_user_id,
            'responsible_person_user_name': self.responsible_person_user_name,
            'responsible_person_is_confirmed': self.responsible_person_is_confirmed,
            'responsible_person_is_confirmed_time': self.responsible_person_is_confirmed_time,
            'pm_is_confirmed': self.pm_is_confirmed,
            'pm_is_confirmed_user_id': self.pm_is_confirmed_user_id,
            'pm_is_confirmed_user_name': self.pm_is_confirmed_user_name,
            'pm_is_confirmed_time': self.pm_is_confirmed_time,
            'is_confirmed': self.is_confirmed,
            'confirmed_wechat_user_id': self.confirmed_wechat_user_id,
            'confirmed_wechat_user_name': self.confirmed_wechat_user_name,
            'confirmed_time': self.confirmed_time,
            'creat_time': self.creat_time,
            'creat_user_id': self.creat_user_id,
            'creat_user_name': self.creat_user_name,
            'edit_time': self.edit_time,
            'edit_user_id': self.edit_user_id,
            'edit_user_name': self.edit_user_name,
            'is_delete': self.is_delete
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.pid = obj.get('pid')
        self.task_name = obj.get('task_name')
        self.task_level = obj.get('task_level')
        self.task_desc = obj.get('task_desc')
        self.plan_start_time = obj.get('plan_start_time')
        self.plan_end_time = obj.get('plan_end_time')
        self.plan_staff_time = obj.get('plan_staff_time')
        self.start_time = obj.get('start_time')
        self.end_time = obj.get('end_time')
        self.staff_time = obj.get('staff_time')
        self.project_id = obj.get('project_id')
        self.project_phase_id = obj.get('project_phase_id')
        self.project_phase_name = obj.get('project_phase_name')
        self.responsible_person_user_id = obj.get('responsible_person_user_id')
        self.responsible_person_user_name = obj.get('responsible_person_user_name')
        self.responsible_person_is_confirmed = obj.get('responsible_person_is_confirmed')
        self.responsible_person_is_confirmed_time = obj.get('responsible_person_is_confirmed_time')
        self.pm_is_confirmed = obj.get('pm_is_confirmed')
        self.pm_is_confirmed_user_id = obj.get('pm_is_confirmed_user_id')
        self.pm_is_confirmed_user_name = obj.get('pm_is_confirmed_user_name')
        self.pm_is_confirmed_time = obj.get('pm_is_confirmed_time')
        self.is_confirmed = obj.get('is_confirmed')
        self.confirmed_wechat_user_id = obj.get('confirmed_wechat_user_id')
        self.confirmed_wechat_user_name = obj.get('confirmed_wechat_user_name')
        self.confirmed_time = obj.get('confirmed_time')
        self.creat_time = obj.get('creat_time')
        self.creat_user_id = obj.get('creat_user_id')
        self.creat_user_name = obj.get('creat_user_name')
        self.edit_time = obj.get('edit_time')
        self.edit_user_id = obj.get('edit_user_id')
        self.edit_user_name = obj.get('edit_user_name')
        self.is_delete = obj.get('is_delete')

    @staticmethod
    def db_map():
        return {ProjectTask: EnumDb.main}
