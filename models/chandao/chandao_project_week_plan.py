#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-07-09 09:45:50
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, String, DateTime, BigInteger, TEXT, Date
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ChandaoProjectWeekPlan(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'chandao_project_week_plan'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   chandao_project表 chandao_project_id   '''
    chandao_project_id = Column(BigInteger(), nullable=False)

    '''   chandao_project表 chandao_project_id   '''
    chandao_project_name = Column(String(255), nullable=False)

    '''   计划时间   '''
    plan_time = Column(Date(), nullable=False)

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   创建人   '''
    creat_user_id = Column(BigInteger(), nullable=False)

    '''   创建人   '''
    creat_user_name = Column(String(255), nullable=False)

    '''   修改时间   '''
    edit_time = Column(DateTime())

    '''   修改人   '''
    edit_user_id = Column(BigInteger())

    '''   修改人   '''
    edit_user_name = Column(String(255))

    '''   本周计划总结   '''
    weekly_wrap_up = Column(String(500))

    '''   本周计划总结时间   '''
    weekly_wrap_up_time = Column(DateTime())

    '''   本周计划总结人   '''
    weekly_wrap_up_user_id = Column(BigInteger())

    '''   本周计划总结人   '''
    weekly_wrap_up_user_name = Column(String(255))

    '''   甲方评价   '''
    party_a_comments = Column(String(500))

    '''   甲方评价时间   '''
    party_a_comments_time = Column(DateTime())

    '''   甲方评价人   '''
    party_a_comments_wechat_user_id = Column(BigInteger())

    '''   甲方评价人名字   '''
    party_a_comments_wechat_user_name = Column(String(255))

    '''   评价回复   '''
    evaluation_response = Column(String(500))

    '''   评价回复时间   '''
    evaluation_response_time = Column(DateTime())

    '''   评价回复人   '''
    evaluation_response_user_id = Column(BigInteger())

    '''   评价回复人   '''
    evaluation_response_user_name = Column(String(255))

    '''   附件   '''
    file = Column(TEXT())

    def __repr__(self):
        return "chandao_project_week_plan(chandao_project_id='{self.chandao_project_id}',plan_time='{self.plan_time}',creat_time='{self.creat_time}',creat_user_id='{self.creat_user_id}',creat_user_name='{self.creat_user_name}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'chandao_project_id': self.chandao_project_id,
            'chandao_project_name': self.chandao_project_name,
            'plan_time': self.plan_time,
            'creat_time': self.creat_time,
            'creat_user_id': self.creat_user_id,
            'creat_user_name': self.creat_user_name,
            'edit_time': self.edit_time,
            'edit_user_id': self.edit_user_id,
            'edit_user_name': self.edit_user_name,
            'weekly_wrap_up': self.weekly_wrap_up,
            'weekly_wrap_up_time': self.weekly_wrap_up_time,
            'weekly_wrap_up_user_id': self.weekly_wrap_up_user_id,
            'weekly_wrap_up_user_name': self.weekly_wrap_up_user_name,
            'party_a_comments': self.party_a_comments,
            'party_a_comments_time': self.party_a_comments_time,
            'party_a_comments_wechat_user_id': self.party_a_comments_wechat_user_id,
            'party_a_comments_wechat_user_name': self.party_a_comments_wechat_user_name,
            'evaluation_response': self.evaluation_response,
            'evaluation_response_time': self.evaluation_response_time,
            'evaluation_response_user_id': self.evaluation_response_user_id,
            'evaluation_response_user_name': self.evaluation_response_user_name,
            'file': self.file
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.chandao_project_id = obj.get('chandao_project_id')
        self.chandao_project_name = obj.get('chandao_project_name')
        self.plan_time = obj.get('plan_time')
        self.creat_time = obj.get('creat_time')
        self.creat_user_id = obj.get('creat_user_id')
        self.creat_user_name = obj.get('creat_user_name')
        self.edit_time = obj.get('edit_time')
        self.edit_user_id = obj.get('edit_user_id')
        self.edit_user_name = obj.get('edit_user_name')
        self.weekly_wrap_up = obj.get('weekly_wrap_up')
        self.weekly_wrap_up_time = obj.get('weekly_wrap_up_time')
        self.weekly_wrap_up_user_id = obj.get('weekly_wrap_up_user_id')
        self.weekly_wrap_up_user_name = obj.get('weekly_wrap_up_user_name')
        self.party_a_comments = obj.get('party_a_comments')
        self.party_a_comments_time = obj.get('party_a_comments_time')
        self.party_a_comments_wechat_user_id = obj.get('party_a_comments_wechat_user_id')
        self.party_a_comments_wechat_user_name = obj.get('party_a_comments_wechat_user_name')
        self.evaluation_response = obj.get('evaluation_response')
        self.evaluation_response_time = obj.get('evaluation_response_time')
        self.evaluation_response_user_id = obj.get('evaluation_response_user_id')
        self.evaluation_response_user_name = obj.get('evaluation_response_user_name')
        self.file = obj.get('file')

    @staticmethod
    def db_map():
        return {ChandaoProjectWeekPlan: EnumDb.main}
