#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-06-14 14:21:13
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, String, DateTime, BigInteger, Date
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class WorkReporting(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'work_reporting'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   汇报人   '''
    reporting_user_id = Column(BigInteger(), nullable=False)

    '''   汇报人   '''
    reporting_user_name = Column(String(255), nullable=False)

    '''   汇报日期   '''
    reporting_date = Column(Date(), nullable=False)

    '''   汇报内容reporting_msg = [

{"staff_time"：工时,"work_content":工作内容},

{"staff_time"：工时,"work_content":工作内容},

]   '''
    reporting_msg = Column(String(5000), nullable=False)

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    def __repr__(self):
        return "work_reporting(reporting_user_id='{self.reporting_user_id}',reporting_user_name='{self.reporting_user_name}',reporting_date='{self.reporting_date}',reporting_msg='{self.reporting_msg}',creat_time='{self.creat_time}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'reporting_user_id': self.reporting_user_id,
            'reporting_user_name': self.reporting_user_name,
            'reporting_date': self.reporting_date,
            'reporting_msg': self.reporting_msg,
            'creat_time': self.creat_time
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.reporting_user_id = obj.get('reporting_user_id')
        self.reporting_user_name = obj.get('reporting_user_name')
        self.reporting_date = obj.get('reporting_date')
        self.reporting_msg = obj.get('reporting_msg')
        self.creat_time = obj.get('creat_time')

    @staticmethod
    def db_map():
        return {WorkReporting: EnumDb.main}
