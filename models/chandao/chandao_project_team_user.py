#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-03-19 14:16:55
@desc:
'''

from sqlalchemy import Column, Integer, String, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ChandaoProjectTeamUser(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'chandao_project_team_user'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   禅道用户ID   '''
    chandao_user_id = Column(BigInteger(), default=0)

    '''   项目ID   '''
    chandao_project_id = Column(BigInteger(), nullable=False)

    '''   职位   '''
    position = Column(String(50))

    '''   团队成员名称   '''
    realname = Column(String(50))

    '''   禅道里面的项目ID   '''
    chandao_old_project_id = Column(BigInteger(), nullable=False)

    '''      '''
    account = Column(String(50), nullable=False)

    '''   是否删除   '''
    is_delete = Column(Integer(), nullable=False)

    '''   0-禅道，1-微信   '''
    come_from = Column(Integer(), nullable=False)

    '''   项目ID   '''
    user_id = Column(BigInteger(), default=0)

    def __repr__(self):
        return "chandao_project_team_user(chandao_user_id='{self.chandao_user_id}',chandao_project_id='{self.chandao_project_id}',realname='{self.realname}',chandao_old_project_id='{self.chandao_old_project_id}',account='{self.account}',is_delete='{self.is_delete}',come_from='{self.come_from}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'chandao_user_id': self.chandao_user_id,
            'chandao_project_id': self.chandao_project_id,
            'position': self.position,
            'realname': self.realname,
            'chandao_old_project_id': self.chandao_old_project_id,
            'account': self.account,
            'is_delete': self.is_delete,
            'come_from': self.come_from,
            'user_id': self.user_id
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.chandao_user_id = obj.get('chandao_user_id')
        self.chandao_project_id = obj.get('chandao_project_id')
        self.position = obj.get('position')
        self.realname = obj.get('realname')
        self.chandao_old_project_id = obj.get('chandao_old_project_id')
        self.account = obj.get('account')
        self.is_delete = obj.get('is_delete')
        self.come_from = obj.get('come_from')
        self.user_id = obj.get('user_id')

    @staticmethod
    def db_map():
        return {ChandaoProjectTeamUser: EnumDb.main}
