#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-07-10 18:23:58
@desc:
'''

from sqlalchemy import Column, Integer, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ChandaoProjectWeekPlanDetailUser(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'chandao_project_week_plan_detail_user'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''      '''
    plan_id = Column(BigInteger(), nullable=False)

    '''      '''
    detail_id = Column(BigInteger())

    '''   0-甲方，1-乙方   '''
    is_head_party_a = Column(Integer(), nullable=False)

    '''   甲方存的wechat_user_id，乙方存的 user_id   '''
    user_id = Column(BigInteger(), nullable=False)

    def __repr__(self):
        return "chandao_project_week_plan_detail_user(plan_id='{self.plan_id}',is_head_party_a='{self.is_head_party_a}',user_id='{self.user_id}')".format(self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'plan_id': self.plan_id,
            'detail_id': self.detail_id,
            'is_head_party_a': self.is_head_party_a,
            'user_id': self.user_id
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.plan_id = obj.get('plan_id')
        self.detail_id = obj.get('detail_id')
        self.is_head_party_a = obj.get('is_head_party_a')
        self.user_id = obj.get('user_id')

    @staticmethod
    def db_map():
        return {ChandaoProjectWeekPlanDetailUser: EnumDb.main}
