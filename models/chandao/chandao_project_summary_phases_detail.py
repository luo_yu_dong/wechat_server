#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-07-09 10:23:30
@desc:
'''

from sqlalchemy import Column, DateTime, BigInteger, TEXT
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ChandaoProjectSummaryPhasesDetail(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'chandao_project_summary_phases_detail'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   chandao_project_summary_phases 主键id   '''
    summary_phases_id = Column(BigInteger(), nullable=False)

    '''   建设目标   '''
    construction_objective = Column(TEXT(), nullable=False)

    plan_end_time = Column(DateTime())

    def __repr__(self):
        return "chandao_project_summary_phases_detail(summary_phases_id='{self.summary_phases_id}',construction_objective='{self.construction_objective}')".format(self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'summary_phases_id': self.summary_phases_id,
            'construction_objective': self.construction_objective,
            'plan_end_time': self.plan_end_time
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.summary_phases_id = obj.get('summary_phases_id')
        self.construction_objective = obj.get('construction_objective')
        self.plan_end_time = obj.get('plan_end_time')

    @staticmethod
    def db_map():
        return {ChandaoProjectSummaryPhasesDetail: EnumDb.main}
