#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-07-29 14:43:55
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger, TEXT, Date
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ChandaoProjectSummaryPhases(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'chandao_project_summary_phases'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   chandao_project表 chandao_project_id   '''
    chandao_project_id = Column(BigInteger(), nullable=False)

    '''   阶段名称   '''
    phase_name = Column(String(255), nullable=False)

    '''   建设内容   '''
    construction_content = Column(TEXT(), nullable=False)

    '''   预计完成时间   '''
    planned_completion_time = Column(Date(), nullable=False)

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   创建人   '''
    creat_user_id = Column(BigInteger(), nullable=False)

    '''   创建人   '''
    creat_user_name = Column(String(255), nullable=False)

    '''   修改时间   '''
    edit_time = Column(DateTime())

    '''   修改人   '''
    edit_user_id = Column(BigInteger())

    '''   修改人   '''
    edit_user_name = Column(String(255))

    '''   合同人天   '''
    contractor_days = Column(String(255))

    '''   预算人天   '''
    budget_days = Column(String(255))

    '''   本期回款   '''
    payment_this_period = Column(String(255))

    '''   是否确认   '''
    is_confirmed = Column(Integer(), default=0)

    '''   确认人   '''
    confirmed_user_id = Column(BigInteger())

    '''   确认人   '''
    confirmed_user_name = Column(String(255))

    '''   确认时间   '''
    confirmed_time = Column(Date())

    def __repr__(self):
        return "chandao_project_summary_phases(chandao_project_id='{self.chandao_project_id}',phase_name='{self.phase_name}',construction_content='{self.construction_content}',planned_completion_time='{self.planned_completion_time}',creat_time='{self.creat_time}',creat_user_id='{self.creat_user_id}',creat_user_name='{self.creat_user_name}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'chandao_project_id': self.chandao_project_id,
            'phase_name': self.phase_name,
            'construction_content': self.construction_content,
            'planned_completion_time': self.planned_completion_time,
            'creat_time': self.creat_time,
            'creat_user_id': self.creat_user_id,
            'creat_user_name': self.creat_user_name,
            'edit_time': self.edit_time,
            'edit_user_id': self.edit_user_id,
            'edit_user_name': self.edit_user_name,
            'contractor_days': self.contractor_days,
            'budget_days': self.budget_days,
            'payment_this_period': self.payment_this_period,
            'is_confirmed': self.is_confirmed,
            'confirmed_user_id': self.confirmed_user_id,
            'confirmed_user_name': self.confirmed_user_name,
            'confirmed_time': self.confirmed_time
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.chandao_project_id = obj.get('chandao_project_id')
        self.phase_name = obj.get('phase_name')
        self.construction_content = obj.get('construction_content')
        self.planned_completion_time = obj.get('planned_completion_time')
        self.creat_time = obj.get('creat_time')
        self.creat_user_id = obj.get('creat_user_id')
        self.creat_user_name = obj.get('creat_user_name')
        self.edit_time = obj.get('edit_time')
        self.edit_user_id = obj.get('edit_user_id')
        self.edit_user_name = obj.get('edit_user_name')
        self.contractor_days = obj.get('contractor_days')
        self.budget_days = obj.get('budget_days')
        self.payment_this_period = obj.get('payment_this_period')
        self.is_confirmed = obj.get('is_confirmed')
        self.confirmed_user_id = obj.get('confirmed_user_id')
        self.confirmed_user_name = obj.get('confirmed_user_name')
        self.confirmed_time = obj.get('confirmed_time')

    @staticmethod
    def db_map():
        return {ChandaoProjectSummaryPhases: EnumDb.main}
