#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-05-16 10:34:22
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class Calendar(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'calendar'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   年   '''
    year = Column(Integer(), nullable=False)

    '''   月   '''
    month = Column(Integer(), nullable=False)

    '''   天   '''
    day = Column(Integer(), nullable=False)

    '''   星期   '''
    weekday = Column(String(255), nullable=False)

    '''   1-工作日，0-非工作日   '''
    is_workdays = Column(Integer(), nullable=False)

    '''      '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    def __repr__(self):
        return "calendar(year='{self.year}',month='{self.month}',day='{self.day}',is_workdays='{self.is_workdays}',creat_time='{self.creat_time}')".format(self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'year': self.year,
            'month': self.month,
            'day': self.day,
            'weekday': self.weekday,
            'is_workdays': self.is_workdays,
            'creat_time': self.creat_time
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.year = obj.get('year')
        self.month = obj.get('month')
        self.day = obj.get('day')
        self.weekday = obj.get('weekday')
        self.is_workdays = obj.get('is_workdays')
        self.creat_time = obj.get('creat_time')

    @staticmethod
    def db_map():
        return {Calendar: EnumDb.main}
