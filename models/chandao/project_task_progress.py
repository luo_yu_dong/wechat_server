#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-05-17 10:44:46
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ProjectTaskProgress(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'project_task_progress'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   项目id   '''
    project_id = Column(BigInteger(), nullable=False)

    '''   project_task表主键id   '''
    task_id = Column(BigInteger(), nullable=False)

    '''   任务跟踪   '''
    progress = Column(String(255), nullable=False)

    '''   跟踪人的   '''
    wechat_user_id = Column(BigInteger(), nullable=False)

    '''      '''
    wechat_user_name = Column(String(255), nullable=False)

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   是否删除1-删除，0-不删除   '''
    is_delete = Column(Integer(), nullable=False)

    '''   删除时间   '''
    delete_time = Column(DateTime())

    def __repr__(self):
        return "project_task_progress(project_id='{self.project_id}',task_id='{self.task_id}',progress='{self.progress}',wechat_user_id='{self.wechat_user_id}',wechat_user_name='{self.wechat_user_name}',creat_time='{self.creat_time}',is_delete='{self.is_delete}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'project_id': self.project_id,
            'task_id': self.task_id,
            'progress': self.progress,
            'wechat_user_id': self.wechat_user_id,
            'wechat_user_name': self.wechat_user_name,
            'creat_time': self.creat_time,
            'is_delete': self.is_delete,
            'delete_time': self.delete_time
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.project_id = obj.get('project_id')
        self.task_id = obj.get('task_id')
        self.progress = obj.get('progress')
        self.wechat_user_id = obj.get('wechat_user_id')
        self.wechat_user_name = obj.get('wechat_user_name')
        self.creat_time = obj.get('creat_time')
        self.is_delete = obj.get('is_delete')
        self.delete_time = obj.get('delete_time')

    @staticmethod
    def db_map():
        return {ProjectTaskProgress: EnumDb.main}
