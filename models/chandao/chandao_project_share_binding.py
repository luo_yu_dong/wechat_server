#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-05-11 10:45:57
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ChandaoProjectShareBinding(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'chandao_project_share_binding'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   项目id   '''
    project_id = Column(BigInteger(), nullable=False)

    '''   wechat_user表主键id，存的是公众号的wechat_user_id,切记   '''
    wechat_user_id = Column(BigInteger(), nullable=False)

    '''   分享人的user_id   '''
    share_user_id = Column(BigInteger(), nullable=False)

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   0：保存。1：删除   '''
    is_delete = Column(Integer(), default=0, nullable=False)

    def __repr__(self):
        return "chandao_project_share_binding(project_id='{self.project_id}',wechat_user_id='{self.wechat_user_id}',share_user_id='{self.share_user_id}',creat_time='{self.creat_time}',is_delete='{self.is_delete}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'project_id': self.project_id,
            'wechat_user_id': self.wechat_user_id,
            'share_user_id': self.share_user_id,
            'creat_time': self.creat_time,
            'is_delete': self.is_delete
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.project_id = obj.get('project_id')
        self.wechat_user_id = obj.get('wechat_user_id')
        self.share_user_id = obj.get('share_user_id')
        self.creat_time = obj.get('creat_time')
        self.is_delete = obj.get('is_delete')

    @staticmethod
    def db_map():
        return {ChandaoProjectShareBinding: EnumDb.main}
