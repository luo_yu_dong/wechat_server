#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-07-10 18:13:54
@desc:
'''

from sqlalchemy import Column, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ChandaoProjectWeekPlanDetail(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'chandao_project_week_plan_detail'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   chandao_project_week_plan 表主键id   '''
    plan_id = Column(BigInteger(), nullable=False)

    '''   [{'wechat_user_id':xxxm,'wechat_user_name':xxxx}]   '''
    head_party_a = Column(String(2000))

    '''   [{'user_id':xxxm,'user_name':xxxx}]   '''
    head_party_b = Column(String(2000))

    '''   本周计划内容   '''
    planned_week = Column(String(5000), nullable=False)

    plan_end_time = Column(DateTime())

    def __repr__(self):
        return "chandao_project_week_plan_detail(plan_id='{self.plan_id}',planned_week='{self.planned_week}')".format(self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'plan_id': self.plan_id,
            'head_party_a': self.head_party_a,
            'head_party_b': self.head_party_b,
            'planned_week': self.planned_week,
            'plan_end_time': self.plan_end_time
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.plan_id = obj.get('plan_id')
        self.head_party_a = obj.get('head_party_a')
        self.head_party_b = obj.get('head_party_b')
        self.planned_week = obj.get('planned_week')
        self.plan_end_time = obj.get('plan_end_time')

    @staticmethod
    def db_map():
        return {ChandaoProjectWeekPlanDetail: EnumDb.main}
