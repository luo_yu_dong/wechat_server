#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-07-11 19:17:53
@desc:
'''

from sqlalchemy import Column, Integer, String, Numeric, Date
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ZtTeam(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'zt_team'

    __table_args__ = {'schema': 'zentao'}

    '''      '''
    id = Column(Integer(), primary_key=True)

    '''      '''
    root = Column(Integer(), nullable=False)

    '''      '''
    type = Column(String(30), nullable=False)

    '''      '''
    account = Column(String(30), nullable=False)

    '''      '''
    role = Column(String(30), nullable=False)

    '''      '''
    position = Column(String(30), nullable=False)

    '''      '''
    limited = Column(String(8), nullable=False)

    '''      '''
    join = Column(Date())

    '''      '''
    days = Column(Integer(), nullable=False)

    '''      '''
    hours = Column(Numeric(3, 1), nullable=False)

    '''      '''
    estimate = Column(Numeric(12, 2), nullable=False)

    '''      '''
    consumed = Column(Numeric(12, 2), nullable=False)

    '''      '''
    left = Column(Numeric(12, 2), nullable=False)

    '''      '''
    order = Column(Integer(), nullable=False)

    def __repr__(self):
        return "zt_team(root='{self.root}',type='{self.type}',account='{self.account}',role='{self.role}',position='{self.position}',limited='{self.limited}',days='{self.days}',hours='{self.hours}',estimate='{self.estimate}',consumed='{self.consumed}',left='{self.left}',order='{self.order}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'root': self.root,
            'type': self.type,
            'account': self.account,
            'role': self.role,
            'position': self.position,
            'limited': self.limited,
            'join': self.join,
            'days': self.days,
            'hours': self.hours,
            'estimate': self.estimate,
            'consumed': self.consumed,
            'left': self.left,
            'order': self.order
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.root = obj.get('root')
        self.type = obj.get('type')
        self.account = obj.get('account')
        self.role = obj.get('role')
        self.position = obj.get('position')
        self.limited = obj.get('limited')
        self.join = obj.get('join')
        self.days = obj.get('days')
        self.hours = obj.get('hours')
        self.estimate = obj.get('estimate')
        self.consumed = obj.get('consumed')
        self.left = obj.get('left')
        self.order = obj.get('order')

    @staticmethod
    def db_map():
        return {ZtTeam: EnumDb.chandao}
