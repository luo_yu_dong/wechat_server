#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-03-20 14:34:43
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Numeric, BigInteger, TEXT, Date
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ZtTask(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'zt_task'

    __table_args__ = {'schema': 'zentao'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''      '''
    project = Column(BigInteger(), nullable=False)

    '''      '''
    parent = Column(BigInteger(), nullable=False)

    '''      '''
    execution = Column(BigInteger(), nullable=False)

    '''      '''
    module = Column(BigInteger(), nullable=False)

    '''      '''
    design = Column(BigInteger(), nullable=False)

    '''      '''
    story = Column(BigInteger(), nullable=False)

    '''      '''
    storyVersion = Column(BigInteger(), nullable=False)

    '''      '''
    designVersion = Column(BigInteger(), nullable=False)

    '''      '''
    fromBug = Column(BigInteger(), nullable=False)

    '''      '''
    fromIssue = Column(BigInteger(), nullable=False)

    '''      '''
    feedback = Column(BigInteger(), nullable=False)

    '''      '''
    name = Column(String(255), nullable=False)

    '''      '''
    type = Column(String(20), nullable=False)

    '''      '''
    mode = Column(String(10), nullable=False)

    '''      '''
    pri = Column(BigInteger(), nullable=False)

    '''      '''
    estimate = Column(Numeric(18, 4), nullable=False)

    '''      '''
    consumed = Column(Numeric(18, 4), nullable=False)

    '''      '''
    left = Column(Numeric(18, 4), nullable=False)

    '''      '''
    deadline = Column(Date())

    '''      '''
    status = Column(String(10), nullable=False)  # enum('wait','doing','done','pause','cancel','closed')

    '''      '''
    subStatus = Column(String(30), nullable=False)

    '''      '''
    color = Column(String(7), nullable=False)

    '''      '''
    mailto = Column(TEXT())

    '''      '''
    desc = Column(TEXT())

    '''      '''
    version = Column(BigInteger(), nullable=False)

    '''      '''
    openedBy = Column(String(30), nullable=False)

    '''      '''
    openedDate = Column(DateTime(), default=datetime.now)

    '''      '''
    assignedTo = Column(String(30), nullable=False)

    '''      '''
    assignedDate = Column(DateTime(), default=datetime.now)

    '''      '''
    estStarted = Column(Date())

    '''      '''
    realStarted = Column(DateTime(), default=datetime.now)

    '''      '''
    finishedBy = Column(String(30), nullable=False)

    '''      '''
    finishedDate = Column(DateTime(), default=datetime.now)

    '''      '''
    finishedList = Column(TEXT())

    '''      '''
    canceledBy = Column(String(30), nullable=False)

    '''      '''
    canceledDate = Column(DateTime(), default=datetime.now)

    '''      '''
    closedBy = Column(String(30), nullable=False)

    '''      '''
    closedDate = Column(DateTime(), default=datetime.now)

    '''      '''
    realDuration = Column(Integer(), nullable=False)

    '''      '''
    planDuration = Column(Integer(), nullable=False)

    '''      '''
    closedReason = Column(String(30), nullable=False)

    '''      '''
    lastEditedBy = Column(String(30), nullable=False)

    '''      '''
    lastEditedDate = Column(DateTime(), default=datetime.now)

    '''      '''
    activatedDate = Column(DateTime(), default=datetime.now)

    '''      '''
    order = Column(BigInteger(), nullable=False)

    '''      '''
    repo = Column(BigInteger(), nullable=False)

    '''      '''
    mr = Column(BigInteger(), nullable=False)

    '''      '''
    entry = Column(String(255), nullable=False)

    '''      '''
    lines = Column(String(10), nullable=False)

    '''      '''
    v1 = Column(String(40), nullable=False)

    '''      '''
    v2 = Column(String(40), nullable=False)

    '''      '''
    deleted = Column(String(10), nullable=False)  # enum('0','1')

    '''      '''
    vision = Column(String(10), nullable=False)

    def __repr__(self):
        return "zt_task(project='{self.project}',parent='{self.parent}',execution='{self.execution}',module='{self.module}',design='{self.design}',story='{self.story}',storyVersion='{self.storyVersion}',designVersion='{self.designVersion}',fromBug='{self.fromBug}',fromIssue='{self.fromIssue}',feedback='{self.feedback}',name='{self.name}',type='{self.type}',mode='{self.mode}',pri='{self.pri}',estimate='{self.estimate}',consumed='{self.consumed}',left='{self.left}',status='{self.status}',subStatus='{self.subStatus}',color='{self.color}',version='{self.version}',openedBy='{self.openedBy}',assignedTo='{self.assignedTo}',finishedBy='{self.finishedBy}',canceledBy='{self.canceledBy}',closedBy='{self.closedBy}',realDuration='{self.realDuration}',planDuration='{self.planDuration}',closedReason='{self.closedReason}',lastEditedBy='{self.lastEditedBy}',order='{self.order}',repo='{self.repo}',mr='{self.mr}',entry='{self.entry}',lines='{self.lines}',v1='{self.v1}',v2='{self.v2}',deleted='{self.deleted}',vision='{self.vision}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'project': self.project,
            'parent': self.parent,
            'execution': self.execution,
            'module': self.module,
            'design': self.design,
            'story': self.story,
            'storyVersion': self.storyVersion,
            'designVersion': self.designVersion,
            'fromBug': self.fromBug,
            'fromIssue': self.fromIssue,
            'feedback': self.feedback,
            'name': self.name,
            'type': self.type,
            'mode': self.mode,
            'pri': self.pri,
            'estimate': self.estimate,
            'consumed': self.consumed,
            'left': self.left,
            'deadline': self.deadline,
            'status': self.status,
            'subStatus': self.subStatus,
            'color': self.color,
            'mailto': self.mailto,
            'desc': self.desc,
            'version': self.version,
            'openedBy': self.openedBy,
            'openedDate': self.openedDate,
            'assignedTo': self.assignedTo,
            'assignedDate': self.assignedDate,
            'estStarted': self.estStarted,
            'realStarted': self.realStarted,
            'finishedBy': self.finishedBy,
            'finishedDate': self.finishedDate,
            'finishedList': self.finishedList,
            'canceledBy': self.canceledBy,
            'canceledDate': self.canceledDate,
            'closedBy': self.closedBy,
            'closedDate': self.closedDate,
            'realDuration': self.realDuration,
            'planDuration': self.planDuration,
            'closedReason': self.closedReason,
            'lastEditedBy': self.lastEditedBy,
            'lastEditedDate': self.lastEditedDate,
            'activatedDate': self.activatedDate,
            'order': self.order,
            'repo': self.repo,
            'mr': self.mr,
            'entry': self.entry,
            'lines': self.lines,
            'v1': self.v1,
            'v2': self.v2,
            'deleted': self.deleted,
            'vision': self.vision
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.project = obj.get('project')
        self.parent = obj.get('parent')
        self.execution = obj.get('execution')
        self.module = obj.get('module')
        self.design = obj.get('design')
        self.story = obj.get('story')
        self.storyVersion = obj.get('storyVersion')
        self.designVersion = obj.get('designVersion')
        self.fromBug = obj.get('fromBug')
        self.fromIssue = obj.get('fromIssue')
        self.feedback = obj.get('feedback')
        self.name = obj.get('name')
        self.type = obj.get('type')
        self.mode = obj.get('mode')
        self.pri = obj.get('pri')
        self.estimate = obj.get('estimate')
        self.consumed = obj.get('consumed')
        self.left = obj.get('left')
        self.deadline = obj.get('deadline')
        self.status = obj.get('status')
        self.subStatus = obj.get('subStatus')
        self.color = obj.get('color')
        self.mailto = obj.get('mailto')
        self.desc = obj.get('desc')
        self.version = obj.get('version')
        self.openedBy = obj.get('openedBy')
        self.openedDate = obj.get('openedDate')
        self.assignedTo = obj.get('assignedTo')
        self.assignedDate = obj.get('assignedDate')
        self.estStarted = obj.get('estStarted')
        self.realStarted = obj.get('realStarted')
        self.finishedBy = obj.get('finishedBy')
        self.finishedDate = obj.get('finishedDate')
        self.finishedList = obj.get('finishedList')
        self.canceledBy = obj.get('canceledBy')
        self.canceledDate = obj.get('canceledDate')
        self.closedBy = obj.get('closedBy')
        self.closedDate = obj.get('closedDate')
        self.realDuration = obj.get('realDuration')
        self.planDuration = obj.get('planDuration')
        self.closedReason = obj.get('closedReason')
        self.lastEditedBy = obj.get('lastEditedBy')
        self.lastEditedDate = obj.get('lastEditedDate')
        self.activatedDate = obj.get('activatedDate')
        self.order = obj.get('order')
        self.repo = obj.get('repo')
        self.mr = obj.get('mr')
        self.entry = obj.get('entry')
        self.lines = obj.get('lines')
        self.v1 = obj.get('v1')
        self.v2 = obj.get('v2')
        self.deleted = obj.get('deleted')
        self.vision = obj.get('vision')

    @staticmethod
    def db_map():
        return {ZtTask: EnumDb.chandao}
