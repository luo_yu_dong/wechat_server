#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-03-16 15:13:15
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, String, DateTime, Numeric, BigInteger, Date
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ChandaoProject(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'chandao_project'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   禅道项目ID   '''
    chandao_project_id = Column(BigInteger(), nullable=False)

    '''   项目名称   '''
    project_name = Column(String(255), nullable=False)

    '''   项目开始时间   '''
    begin = Column(Date())

    '''   项目结束时间   '''
    end = Column(Date())

    '''   项目状态   '''
    status = Column(String(20))

    '''   项目预算   '''
    budget = Column(Numeric(18, 4))

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   实际工时   '''
    actual_working_hours = Column(Numeric(8, 2), default=0.00)

    '''   项目编号   '''
    code = Column(String(255))

    '''      '''
    old_chandao_project_id = Column(BigInteger())

    '''      '''
    new_chandao_project_id = Column(BigInteger())

    def __repr__(self):
        return "chandao_project(chandao_project_id='{self.chandao_project_id}',project_name='{self.project_name}')".format(self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'chandao_project_id': self.chandao_project_id,
            'project_name': self.project_name,
            'begin': self.begin,
            'end': self.end,
            'status': self.status,
            'budget': self.budget,
            'creat_time': self.creat_time,
            'actual_working_hours': self.actual_working_hours,
            'code': self.code,
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.chandao_project_id = obj.get('chandao_project_id')
        self.project_name = obj.get('project_name')
        self.begin = obj.get('begin')
        self.end = obj.get('end')
        self.status = obj.get('status')
        self.budget = obj.get('budget')
        self.creat_time = obj.get('creat_time')
        self.actual_working_hours = obj.get('actual_working_hours')
        self.code = obj.get('code')

    @staticmethod
    def db_map():
        return {ChandaoProject: EnumDb.main}
