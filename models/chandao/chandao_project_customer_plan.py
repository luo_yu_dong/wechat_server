#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-03-20 19:55:34
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ChandaoProjectCustomerPlan(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'chandao_project_customer_plan'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''      '''
    project_id = Column(BigInteger(), nullable=False)

    '''   任务名称   '''
    task = Column(String(255), nullable=False)

    '''   资料清单   '''
    information_list = Column(String(255))

    '''   客户方协助人   '''
    customer_facilitator = Column(String(255))

    '''   计划完成时间   '''
    plan_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   实际完成时间   '''
    end_time = Column(DateTime())

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   创建人   '''
    creat_id = Column(BigInteger(), nullable=False)

    '''   请求来源 0-PC端，1-小程序端   '''
    come_from = Column(Integer(), nullable=False)

    '''  是否删除 1-删除，0-未删除  '''
    is_delete = Column(Integer(), default=0)

    def __repr__(self):
        return "chandao_project_customer_plan(project_id='{self.project_id}',task='{self.task}',plan_time='{self.plan_time}',end_time='{self.end_time}',creat_time='{self.creat_time}',creat_id='{self.creat_id}',come_from='{self.come_from}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'project_id': self.project_id,
            'task': self.task,
            'information_list': self.information_list,
            'customer_facilitator': self.customer_facilitator,
            'plan_time': self.plan_time,
            'end_time': self.end_time,
            'creat_time': self.creat_time,
            'creat_id': self.creat_id,
            'come_from': self.come_from,
            'is_delete': self.is_delete
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.project_id = obj.get('project_id')
        self.task = obj.get('task')
        self.information_list = obj.get('information_list')
        self.customer_facilitator = obj.get('customer_facilitator')
        self.plan_time = obj.get('plan_time')
        self.end_time = obj.get('end_time')
        self.creat_time = obj.get('creat_time')
        self.creat_id = obj.get('creat_id')
        self.come_from = obj.get('come_from')
        self.is_delete = obj.get('is_delete')

    @staticmethod
    def db_map():
        return {ChandaoProjectCustomerPlan: EnumDb.main}
