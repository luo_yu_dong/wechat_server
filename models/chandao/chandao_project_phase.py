#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-03-18 17:45:36
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ChandaoProjectPhase(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'chandao_project_phase'

    __table_args__ = {'schema': 'main'}

    '''   主键id   '''
    id = Column(BigInteger(), primary_key=True)

    '''      '''
    project_id = Column(BigInteger(), nullable=False)

    '''   阶段名称   '''
    point_name = Column(String(50), nullable=False)

    '''   阶段id   '''
    configuration_id = Column(BigInteger(), nullable=False)

    '''   父级id   '''
    configuration_pid = Column(BigInteger(), nullable=False)

    '''   排序   '''
    configuration_sort = Column(Integer(), nullable=False)

    '''      '''
    plan_start_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''      '''
    plan_end_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   实际开始时间   '''
    start_time = Column(DateTime())

    '''   实际结束时间   '''
    end_time = Column(DateTime())

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   创建人   '''
    creat_user_id = Column(BigInteger(), nullable=False)

    '''   排序   '''
    is_delete = Column(Integer(), nullable=False, default=0)

    def __repr__(self):
        return "chandao_project_phase(project_id='{self.project_id}',point_name='{self.point_name}',configuration_id='{self.configuration_id}',configuration_pid='{self.configuration_pid}',configuration_sort='{self.configuration_sort}',plan_start_time='{self.plan_start_time}',plan_end_time='{self.plan_end_time}',creat_time='{self.creat_time}',creat_user_id='{self.creat_user_id}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'project_id': self.project_id,
            'point_name': self.point_name,
            'configuration_id': self.configuration_id,
            'configuration_pid': self.configuration_pid,
            'configuration_sort': self.configuration_sort,
            'plan_start_time': self.plan_start_time,
            'plan_end_time': self.plan_end_time,
            'start_time': self.start_time,
            'end_time': self.end_time,
            'creat_time': self.creat_time,
            'creat_user_id': self.creat_user_id,
            'is_delete': self.is_delete
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.project_id = obj.get('project_id')
        self.point_name = obj.get('point_name')
        self.configuration_id = obj.get('configuration_id')
        self.configuration_pid = obj.get('configuration_pid')
        self.configuration_sort = obj.get('configuration_sort')
        self.plan_start_time = obj.get('plan_start_time')
        self.plan_end_time = obj.get('plan_end_time')
        self.start_time = obj.get('start_time')
        self.end_time = obj.get('end_time')
        self.creat_time = obj.get('creat_time')
        self.creat_user_id = obj.get('creat_user_id')
        self.is_delete = obj.get('is_delete')

    @staticmethod
    def db_map():
        return {ChandaoProjectPhase: EnumDb.main}
