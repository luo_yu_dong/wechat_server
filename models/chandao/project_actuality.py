#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-07-31 16:26:10
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ProjectActuality(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'project_actuality'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   年   '''
    year = Column(Integer(), nullable=False)

    '''   月   '''
    month = Column(Integer(), nullable=False)

    '''   创建人user_id   '''
    creat_user_id = Column(BigInteger(), nullable=False)

    '''   创建人名称   '''
    creat_user_name = Column(String(255), nullable=False)

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   是否确认   '''
    is_confirmed = Column(Integer(), default=0)

    def __repr__(self):
        return "project_actuality(year='{self.year}',month='{self.month}',creat_user_id='{self.creat_user_id}',creat_user_name='{self.creat_user_name}',creat_time='{self.creat_time}',is_confirmed='{self.is_confirmed}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'year': self.year,
            'month': self.month,
            'creat_user_id': self.creat_user_id,
            'creat_user_name': self.creat_user_name,
            'creat_time': self.creat_time,
            'is_confirmed': self.is_confirmed
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.year = obj.get('year')
        self.month = obj.get('month')
        self.creat_user_id = obj.get('creat_user_id')
        self.creat_user_name = obj.get('creat_user_name')
        self.creat_time = obj.get('creat_time')
        self.is_confirmed = obj.get('is_confirmed')

    @staticmethod
    def db_map():
        return {ProjectActuality: EnumDb.main}
