#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-03-21 10:11:35
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ChandaoProjectCustomerPlanTrackRecord(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'chandao_project_customer_plan_track_record'

    __table_args__ = {'schema': 'main'}

    '''   主键id   '''
    id = Column(BigInteger(), primary_key=True)

    '''   chandao_project_customer_plan 表主键id   '''
    customer_plan_id = Column(BigInteger(), nullable=False)

    '''   记录内容   '''
    track_record = Column(String(255), nullable=False)

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   创建人id   '''
    creat_id = Column(BigInteger(), nullable=False)

    '''   请求来源 0-PC端，1-小程序端   '''
    come_from = Column(Integer(), nullable=False)

    '''   请求来源 0-PC端，1-小程序端   '''
    is_delete = Column(Integer(), default=0, nullable=False)

    def __repr__(self):
        return "chandao_project_customer_plan_track_record(customer_plan_id='{self.customer_plan_id}',track_record='{self.track_record}',creat_time='{self.creat_time}',creat_id='{self.creat_id}',come_from='{self.come_from}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'customer_plan_id': self.customer_plan_id,
            'track_record': self.track_record,
            'creat_time': self.creat_time,
            'creat_id': self.creat_id,
            'come_from': self.come_from
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.customer_plan_id = obj.get('customer_plan_id')
        self.track_record = obj.get('track_record')
        self.creat_time = obj.get('creat_time')
        self.creat_id = obj.get('creat_id')
        self.come_from = obj.get('come_from')
        self.is_delete = obj.get('is_delete')

    @staticmethod
    def db_map():
        return {ChandaoProjectCustomerPlanTrackRecord: EnumDb.main}
