#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-03-22 10:05:32
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ChandaoProjectRiskTrackRecord(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'chandao_project_risk_track_record'

    __table_args__ = {'schema': 'main'}

    '''   主键id   '''
    id = Column(BigInteger(), primary_key=True)

    '''   risk表主键id   '''
    risk_id = Column(BigInteger(), nullable=False)

    '''   跟踪进展   '''
    track_content = Column(String(255), nullable=False)

    '''   跟踪时间   '''
    track_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   创建人id   '''
    creat_user_id = Column(BigInteger(), nullable=False)

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   请求来源 0-PC端，1-小程序端   '''
    come_from = Column(Integer(), nullable=False)

    '''   1-删除  0-不删除   '''
    is_delete = Column(Integer(), default=0, nullable=False)

    def __repr__(self):
        return "chandao_project_risk_track_record(risk_id='{self.risk_id}',track_content='{self.track_content}',track_time='{self.track_time}',creat_user_id='{self.creat_user_id}',creat_time='{self.creat_time}',come_from='{self.come_from}',is_delete='{self.is_delete}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'risk_id': self.risk_id,
            'track_content': self.track_content,
            'track_time': self.track_time,
            'creat_user_id': self.creat_user_id,
            'creat_time': self.creat_time,
            'come_from': self.come_from,
            'is_delete': self.is_delete
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.risk_id = obj.get('risk_id')
        self.track_content = obj.get('track_content')
        self.track_time = obj.get('track_time')
        self.creat_user_id = obj.get('creat_user_id')
        self.creat_time = obj.get('creat_time')
        self.come_from = obj.get('come_from')
        self.is_delete = obj.get('is_delete')

    @staticmethod
    def db_map():
        return {ChandaoProjectRiskTrackRecord: EnumDb.main}
