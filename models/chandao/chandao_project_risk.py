#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-03-21 15:45:35
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ChandaoProjectRisk(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'chandao_project_risk'

    __table_args__ = {'schema': 'main'}

    '''   主键id   '''
    id = Column(BigInteger(), primary_key=True)

    '''   chandao_project表主键   '''
    project_id = Column(BigInteger(), nullable=False)

    '''   风险内容   '''
    risk_comment = Column(String(255), nullable=False)

    '''   计划解决时间   '''
    plan_end_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   0-低，1-中，2-高   '''
    risk_level = Column(Integer(), nullable=False)

    '''      '''
    end_time = Column(DateTime())

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   创建人user_id   '''
    creat_user_id = Column(BigInteger(), nullable=False)

    '''   数据来源   '''
    come_from = Column(Integer(), nullable=False)

    '''   请求来源 0-PC端，1-小程序端   '''
    is_delete = Column(Integer(), default=0, nullable=False)

    def __repr__(self):
        return "chandao_project_risk(project_id='{self.project_id}',risk_comment='{self.risk_comment}',plan_end_time='{self.plan_end_time}',risk_level='{self.risk_level}',creat_time='{self.creat_time}',creat_user_id='{self.creat_user_id}',come_from='{self.come_from}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'project_id': self.project_id,
            'risk_comment': self.risk_comment,
            'plan_end_time': self.plan_end_time,
            'risk_level': self.risk_level,
            'end_time': self.end_time,
            'creat_time': self.creat_time,
            'creat_user_id': self.creat_user_id,
            'come_from': self.come_from,
            'is_delete': self.is_delete
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.project_id = obj.get('project_id')
        self.risk_comment = obj.get('risk_comment')
        self.plan_end_time = obj.get('plan_end_time')
        self.risk_level = obj.get('risk_level')
        self.end_time = obj.get('end_time')
        self.creat_time = obj.get('creat_time')
        self.creat_user_id = obj.get('creat_user_id')
        self.come_from = obj.get('come_from')
        self.is_delete = obj.get('is_delete')

    @staticmethod
    def db_map():
        return {ChandaoProjectRisk: EnumDb.main}
