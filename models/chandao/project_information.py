#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-05-16 09:48:50
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ProjectInformation(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'project_information'

    __table_args__ = {'schema': 'main'}

    '''   项目id   '''
    project_id = Column(BigInteger(), primary_key=True)

    '''   项目目标   '''
    project_objectives = Column(String(255), nullable=False)

    '''   订单人天   '''
    order_staff_data = Column(Integer())

    '''   预算人天   '''
    budget_staff_data = Column(Integer())

    '''   创建时间   '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''   创建人user_id   '''
    creat_user_id = Column(BigInteger(), nullable=False)

    '''   创建人姓名   '''
    creat_user_name = Column(String(255), nullable=False)

    '''   修改时间   '''
    edit_time = Column(DateTime())

    '''   修改人user_id   '''
    edit_user_id = Column(BigInteger())

    '''   修改人姓名   '''
    edit_user_name = Column(String(255))

    def __repr__(self):
        return "project_information(project_objectives='{self.project_objectives}',creat_time='{self.creat_time}',creat_user_id='{self.creat_user_id}',creat_user_name='{self.creat_user_name}')".format(
            self=self)

    def get_dict(self):
        return {
            'project_id': self.project_id,
            'project_objectives': self.project_objectives,
            'order_staff_data': self.order_staff_data,
            'budget_staff_data': self.budget_staff_data,
            'creat_time': self.creat_time,
            'creat_user_id': self.creat_user_id,
            'creat_user_name': self.creat_user_name,
            'edit_time': self.edit_time,
            'edit_user_id': self.edit_user_id,
            'edit_user_name': self.edit_user_name
        }

    def set_object(self, obj={}):
        self.project_id = obj.get('project_id')
        self.project_objectives = obj.get('project_objectives')
        self.order_staff_data = obj.get('order_staff_data')
        self.budget_staff_data = obj.get('budget_staff_data')
        self.creat_time = obj.get('creat_time')
        self.creat_user_id = obj.get('creat_user_id')
        self.creat_user_name = obj.get('creat_user_name')
        self.edit_time = obj.get('edit_time')
        self.edit_user_id = obj.get('edit_user_id')
        self.edit_user_name = obj.get('edit_user_name')

    @staticmethod
    def db_map():
        return {ProjectInformation: EnumDb.main}
