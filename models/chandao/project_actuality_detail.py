#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-07-31 16:31:06
@desc:
'''

from sqlalchemy import Column, Integer, String, Numeric, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ProjectActualityDetail(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'project_actuality_detail'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   project_actuality 表id   '''
    project_actuality_id = Column(BigInteger(), nullable=False)

    '''   项目ID   '''
    project_id = Column(BigInteger(), nullable=False)

    '''   合同编号   '''
    project_no = Column(String(255))

    '''   项目名称   '''
    project_name = Column(String(255), nullable=False)

    '''   日期   '''
    day = Column(Integer(), nullable=False)

    '''   占用人天   '''
    time_taken = Column(Numeric(2, 1), nullable=False)

    '''   创建人名称   '''
    creat_user_name = Column(String(255), nullable=False)

    '''   创建人user_id   '''
    creat_user_id = Column(BigInteger(), nullable=False)

    '''   年   '''
    year = Column(Integer(), nullable=False)

    '''   月   '''
    month = Column(Integer(), nullable=False)

    def __repr__(self):
        return "project_actuality_detail(project_actuality_id='{self.project_actuality_id}',project_id='{self.project_id}',project_name='{self.project_name}',day='{self.day}',time_taken='{self.time_taken}',creat_user_name='{self.creat_user_name}',creat_user_id='{self.creat_user_id}',year='{self.year}',month='{self.month}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'project_actuality_id': self.project_actuality_id,
            'project_id': self.project_id,
            'project_no': self.project_no,
            'project_name': self.project_name,
            'day': self.day,
            'time_taken': self.time_taken,
            'creat_user_name': self.creat_user_name,
            'creat_user_id': self.creat_user_id,
            'year': self.year,
            'month': self.month
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.project_actuality_id = obj.get('project_actuality_id')
        self.project_id = obj.get('project_id')
        self.project_no = obj.get('project_no')
        self.project_name = obj.get('project_name')
        self.day = obj.get('day')
        self.time_taken = obj.get('time_taken')
        self.creat_user_name = obj.get('creat_user_name')
        self.creat_user_id = obj.get('creat_user_id')
        self.year = obj.get('year')
        self.month = obj.get('month')

    @staticmethod
    def db_map():
        return {ProjectActualityDetail: EnumDb.main}
