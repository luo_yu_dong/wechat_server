#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-03-18 13:48:41
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, BigInteger
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class SysProjectPhaseConfiguration(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'sys_project_phase_configuration'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   阶段名称   '''
    point_name = Column(String(255), nullable=False)

    '''   父级id   '''
    pid = Column(Integer(), nullable=False)

    '''      '''
    sort = Column(Integer(), nullable=False)

    '''   1:删除，0:未删除   '''
    is_delete = Column(Integer(), nullable=False, default=0)

    '''      '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''      '''
    creat_user_id = Column(BigInteger(), nullable=False)

    def __repr__(self):
        return "sys_project_phase_configuration(point_name='{self.point_name}',pid='{self.pid}',sort='{self.sort}',is_delete='{self.is_delete}',creat_time='{self.creat_time}',creat_user_id='{self.creat_user_id}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'point_name': self.point_name,
            'pid': self.pid,
            'sort': self.sort,
            'is_delete': self.is_delete,
            'creat_time': self.creat_time,
            'creat_user_id': self.creat_user_id
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.point_name = obj.get('point_name')
        self.pid = obj.get('pid')
        self.sort = obj.get('sort')
        self.is_delete = obj.get('is_delete')
        self.creat_time = obj.get('creat_time')
        self.creat_user_id = obj.get('creat_user_id')

    @staticmethod
    def db_map():
        return {SysProjectPhaseConfiguration: EnumDb.main}
