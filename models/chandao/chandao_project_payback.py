#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author:lyd
@software: 实体表
@time:2024-07-29 09:22:45
@desc:
'''

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Numeric, BigInteger, Date
from sqlalchemy.ext.declarative import declarative_base

from enums.enum_db import EnumDb

BaseOrmClass = declarative_base()


class ChandaoProjectPayback(BaseOrmClass):
    '''
    模型(表)备注:

    '''
    __tablename__ = 'chandao_project_payback'

    __table_args__ = {'schema': 'main'}

    '''      '''
    id = Column(BigInteger(), primary_key=True)

    '''   project表 chandao_project_id   '''
    chandao_project_id = Column(BigInteger(), nullable=False)

    '''   project表 id   '''
    project_id = Column(BigInteger(), nullable=False)

    '''   回款金额   '''
    payback_amount = Column(Numeric(18, 4), nullable=False)

    '''   回款阶段   '''
    payback_point = Column(String(255), nullable=False)

    '''   回款时间   '''
    payback_time = Column(Date(), nullable=False)

    '''   是否确认，0-未确认，1-已确认   '''
    is_confirmed = Column(Integer(), default=0)

    '''   确认时间   '''
    confirmed_time = Column(Date())

    '''   确认人   '''
    confirmed_user_id = Column(BigInteger())

    '''      '''
    creat_time = Column(DateTime(), default=datetime.now, nullable=False)

    '''      '''
    creat_user_id = Column(BigInteger(), nullable=False)

    '''      '''
    creat_user_name = Column(String(255), nullable=False)

    '''      '''
    edit_time = Column(DateTime())

    '''      '''
    edit_user_id = Column(BigInteger())

    '''      '''
    edit_user_name = Column(String(255))

    '''      '''
    desc = Column(String(255))

    def __repr__(self):
        return "chandao_project_payback(chandao_project_id='{self.chandao_project_id}',project_id='{self.project_id}',payback_amount='{self.payback_amount}',payback_point='{self.payback_point}',payback_time='{self.payback_time}',is_confirmed='{self.is_confirmed}',creat_time='{self.creat_time}',creat_user_id='{self.creat_user_id}',creat_user_name='{self.creat_user_name}')".format(
            self=self)

    def get_dict(self):
        return {
            'id': self.id,
            'chandao_project_id': self.chandao_project_id,
            'project_id': self.project_id,
            'payback_amount': self.payback_amount,
            'payback_point': self.payback_point,
            'payback_time': self.payback_time,
            'is_confirmed': self.is_confirmed,
            'confirmed_time': self.confirmed_time,
            'confirmed_user_id': self.confirmed_user_id,
            'creat_time': self.creat_time,
            'creat_user_id': self.creat_user_id,
            'creat_user_name': self.creat_user_name,
            'edit_time': self.edit_time,
            'edit_user_id': self.edit_user_id,
            'edit_user_name': self.edit_user_name,
            'desc': self.desc
        }

    def set_object(self, obj={}):
        self.id = obj.get('id')
        self.chandao_project_id = obj.get('chandao_project_id')
        self.project_id = obj.get('project_id')
        self.payback_amount = obj.get('payback_amount')
        self.payback_point = obj.get('payback_point')
        self.payback_time = obj.get('payback_time')
        self.is_confirmed = obj.get('is_confirmed')
        self.confirmed_time = obj.get('confirmed_time')
        self.confirmed_user_id = obj.get('confirmed_user_id')
        self.creat_time = obj.get('creat_time')
        self.creat_user_id = obj.get('creat_user_id')
        self.creat_user_name = obj.get('creat_user_name')
        self.edit_time = obj.get('edit_time')
        self.edit_user_id = obj.get('edit_user_id')
        self.edit_user_name = obj.get('edit_user_name')
        self.desc = obj.get('desc')

    @staticmethod
    def db_map():
        return {ChandaoProjectPayback: EnumDb.main}
