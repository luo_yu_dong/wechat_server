#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: redis数据类型
@file: redis_helper.py
@time: 2019/12/30 12:04
@desc:
'''
from enum import Enum


class EnumCacheType(Enum):
    normal = 0  # 基础缓存
    log = 1
    sns = 2
