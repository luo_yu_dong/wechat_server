# coding=utf-8
'''
author:董新强
createdate:2017-5-9
description:
'''
from enum import Enum


class EnumDb(Enum):
    main = r'main'  # 主数据库
    chandao = r'chandao'


def get_session_enumdb(key):
    if key == EnumDb.main.value:
        return 'main'

    if key == EnumDb.chandao.value:
        return 'chandao'


class EnumBind(Enum):
    master = 'master'  # 主库
    slave = 'slave'  # 从库（只读）
