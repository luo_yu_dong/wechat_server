# coding=utf-8
'''
author:LYD
createdate:2018-11-08
description:第三方错误信息



code:
    10000 正常
    10001 请求参数错误
    10002 无效的appkey
    10003 签名错误



'''
from enum import Enum


class EnumThirdpartyOutType(Enum):
    访问正常 = 10000
    请求参数错误 = 10001
    无效的appkey = 10002
    签名错误 = 10003
    token已过期 = 10004
    无效的token = 10005
    暂无数据 = 10006
    系统错误 = 11111
    未授权 = 11000
    请求失败 = 99999


def thirdparty_out_get_stage_name_by_value(stage):
    if stage == EnumThirdpartyOutType.访问正常.value:
        return '访问正常'
    if stage == EnumThirdpartyOutType.请求参数错误.value:
        return '请求参数错误'
    if stage == EnumThirdpartyOutType.无效的appkey.value:
        return '无效的appkey'
    if stage == EnumThirdpartyOutType.签名错误.value:
        return '签名错误'
    if stage == EnumThirdpartyOutType.暂无数据.value:
        return '暂无数据'
    if stage == EnumThirdpartyOutType.系统错误.value:
        return '系统错误'
    if stage == EnumThirdpartyOutType.企业不存在.value:
        return '企业不存在'
    if stage == EnumThirdpartyOutType.token已过期.value:
        return 'token已过期'
    if stage == EnumThirdpartyOutType.无效的token.value:
        return '无效的token'
    if stage == EnumThirdpartyOutType.请求失败.value:
        return '请求失败'
