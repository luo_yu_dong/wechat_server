#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 智慧桥梁错误码枚举类型
@file: enum_error_msg_type.py
@time: 2019/12/30 12:02
@desc:
'''

from enum import Enum


class EnumErrorType(Enum):
    '''
    群组类型
    '''
    common = r'0x000'  # 一般错误
    common_error = r'0x999'  # 数据不存在的错误
    interal_error = r'0x001'  # 服务器内部错误
    param_error = r'0x002'  # 参数错误
    access_denied = r'0x003'  # 没有权限
