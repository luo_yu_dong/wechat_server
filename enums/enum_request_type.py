# coding=utf-8
'''
author:董新强
createdate:2016年6月21日r
description:来自web请求还是来app的请求
'''
from enum import Enum


class EnumRequestType(Enum):
    none = 0  # 没有分类
    web = 1  # 来自web
    service = 2  # 来自服务app
    public = 3  # 来自业主端app
    wechat = 4  # 来自微信端(属于业主)
