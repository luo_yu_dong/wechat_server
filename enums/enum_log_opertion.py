#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
@author: Lyd
@contact: 339789089@qq.com
@software: 项目
@file: enum_log_opertion.py
@time: 2020/1/6 9:28
@desc:
'''

from enum import Enum


class EnumLogOpertion(Enum):
    add = '新增'
    edit = '编辑'
    delete = '删除'
    set_func = '设置权限'
    set_auth_business = '设置管理企业'
    set_pwd = '设置用户密码'
    complete = '审批完成/拒绝'
